﻿Imports System.IO
Imports System.Text

Imports iTextSharp.text.pdf
Imports iTextSharp.text.pdf.parser

''' <summary>Clase gestiona tota la logica del programa</summary>
''' <remarks>variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>DB <c>mysql</c></term><description>Objecte que gestiona les operacions a la base de dades</description>
'''   </item>
'''   <item>
'''      <term>rutaAlbarans <c>String = "Y:\"</c></term><description>Es la ruta on estan els albarans escanejats i pendents de gestionar</description>
'''   </item>
'''   <item>
'''      <term>rutaClear <c>String = "C:\pendents\clean.jpg"</c></term><description>Es la ruta on esta la imatge en blanc per a limpiar els webBrowsers</description>
'''   </item>
'''   <item>
'''      <term>rutaPendents <c>String = "C:\pendents\"</c></term><description>Es la ruta on es guarden els pdf pendents</description>
'''   </item>
'''   <item>
'''      <term>rutaGestionats <c>String = "C:\gestionats\"</c></term><description>Es la ruta on es guarden els pdf gestionats</description>
'''   </item>
'''   <item>
'''      <term>Log <c>LogClass</c></term><description>Objecte que gestiona els fitxers de log</description>
'''   </item>
'''   <item>
'''      <term>llistatCarpetes <c>List(Of String)</c></term><description>Llistat en les rutes de les carpetes pendents</description>
'''   </item>
'''    <item>
'''      <term>llistatDocs <c>List(Of FileInfo)</c></term><description>Llistat en los fitxers pendents</description>
'''   </item>
'''</list>
''' </remarks>

Public Class ControllerClass
    Dim DB As New mysql

    Public rutaAlbarans As String = "Y:\"
    Public rutaClear As String = "C:\pendents\clean.jpg"
    Dim rutaPendents As String = "C:\pendents\"
    Dim rutaGestionats As String = "C:\gestionats\"

    Public Log As New LogClass

    Public llistatCarpetes As New List(Of String)
    Public llistatDocs As New List(Of FileInfo)

    Private OPERACIONS_VALIDES = New String() {"00", "03"} ', "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62"}
    Private RUTES_VALIDES = New String() {"001", "011", "012", "016", "020", "021", "022", "023", "025", "032", "036", "056", "037", "042"}


    'DB
    'exercici
    'operacio
    'venedor
    'albaran
    'pagina
    'dataAlbaran
    'dataAlbaranFiltro
    'textOriginal
    'ruta
    'pdf
    'trobat
    'gestionat
    'textPdf


    ''' <summary>Funcio que fa els test inicials</summary>
    ''' <remarks>
    ''' </remarks>
    Public Function TestInici()
        ' inicio el log
        Log.Init()

        'miro si existeix la ruta per accedir als fitxers
        If Not Directory.Exists(rutaAlbarans) Then
            Log.Add("No puc accedir a la ruta " & rutaAlbarans, "ERROR")
            MessageBox.Show("No puc accedir a la ruta " & rutaAlbarans, "Error", MessageBoxButtons.OK)
            Return False
        End If

        'miro si existeix la ruta de pendents
        If Not Directory.Exists(rutaPendents) Then
            Directory.CreateDirectory(rutaPendents)
            'MessageBox.Show("No puc accedir a la ruta " & rutaPendents, "Error", MessageBoxButtons.OK)
            'Return False
        End If

        'miro si existeix la ruta de gestionats
        If Not Directory.Exists(rutaGestionats) Then
            Directory.CreateDirectory(rutaGestionats)
            'MessageBox.Show("No puc accedir a la ruta " & rutaGestionats, "Error", MessageBoxButtons.OK)
            'Return False
        End If

        'conecto a la base de dades
        If Not DB.connect() Then
            Log.Add("No puc inicial DB MySQL", "ERROR")
            Return False
        End If

        Return True
    End Function

    ''' <summary>Funcio que limpia l'objecte</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub Dispose()
        Log.Add("Aplicacio finalitzada")

        Log.Close()
    End Sub


    ''' <summary>Funcio que inicia el proces dels albarans by timer</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Progres"><c>ProgressBar</c>: Barra de progres que mostra el proces</param>
    Public Sub processAlbaransTimer(ByVal Progres As ProgressBar)
        If DB.test = False Then
            Log.Add("Processo albarans automaticament", "INFO")
            processAlbarans(Progres)
        End If
    End Sub

    ''' <summary>Funcio que inicia el proces dels albarans. carrega total</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Progres"><c>ProgressBar</c>: Barra de progres que mostra el proces</param>
    Public Sub processAlbarans(ByVal Progres As ProgressBar)
        Log.Add("Processo tots albarans a peticio de l'usuari", "INFO")

        'busco totes les carpetes a gestionar
        buscarCarpetes()

        Progres.Maximum = llistatCarpetes.Count
        Progres.Value = 0

        'recorro les carpetes per a buscar els fitxers
        For Each V As String In llistatCarpetes
            buscarDocs(rutaAlbarans & V)

            'recorro els fitxers per a gestionar-los
            For Each F As FileInfo In llistatDocs
                'intento gestionar el pdf
                gestionarAlbaran(F)
            Next

            If DB.test = False Then
                Try
                    Directory.Delete(rutaAlbarans & V, True)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If

            Progres.Value = Progres.Value + 1
        Next

    End Sub

    ''' <summary>Funcio que inicia el proces dels albarans. carrega manual</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="ruta"><c>String</c>: Ruta de la carpeta a carregar</param>
    ''' <param name="Progres"><c>ProgressBar</c>: Barra de progres que mostra el proces</param>
    Public Sub processAlbarans(ByVal ruta As String, ByVal Progres As ProgressBar)
        Log.Add("Processo ruta " & ruta, "INFO")

        'busco els fitxers
        buscarDocs(ruta)

        Progres.Maximum = llistatDocs.Count
        Progres.Value = 0

        'recorro els fitxers per a gestionar-los
        For Each F As FileInfo In llistatDocs
            'intento gestionar el pdf
            gestionarAlbaran(F)

            Progres.Value = Progres.Value + 1
        Next

        If DB.test = False Then
            Try
                Directory.Delete(ruta, True)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

    End Sub


    ''' <summary>Funcio que busca les carpetes a gestionar</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub buscarCarpetes()
        Dim carpeta As New DirectoryInfo(rutaAlbarans)

        llistatCarpetes.Clear()
        For Each D As DirectoryInfo In carpeta.GetDirectories()
            If D.Name <> "@Recycle" Then
                llistatCarpetes.Add(D.Name)
            End If
        Next
        llistatCarpetes.Sort()
    End Sub

    ''' <summary>Funcio que busca els documents d'una carpeta</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="path"><c>String</c>: Ruta on buscar els documents a carregar</param>
    Public Sub buscarDocs(ByVal path As String)
        Dim carpeta As New DirectoryInfo(path)

        llistatDocs.Clear()
        Try
            For Each D As FileInfo In carpeta.GetFiles("*.pdf")
                If D.Name <> "@Recycle" Then
                    llistatDocs.Add(D)
                End If
            Next
        Catch
        End Try
    End Sub

    ''' <summary>Funcio que crea la ruta correcta per al fitxer pendent</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data en que es gestiona el fitxer</param>
    ''' <param name="FileName"><c>String</c>: Nom actual del fitxer</param>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    ''' <returns><c>String</c>: ruta on guardar el pdf pendent de revisar</returns>
    Private Function CrearRutaPendent(ByVal data As Date, ByVal FileName As String, ByVal idAlbaran As String) As String
        Dim dataString As String

        dataString = data.Year.ToString

        If data.Month < 10 Then
            dataString = dataString + "0" + data.Month.ToString
        Else
            dataString = dataString + data.Month.ToString
        End If

        If data.Day < 10 Then
            dataString = dataString + "0" + data.Day.ToString
        Else
            dataString = dataString + data.Day.ToString
        End If

        Return (rutaPendents & dataString & "-" & idAlbaran & "-" & FileName)
    End Function

    ''' <summary>Funcio que crea la ruta correcta per al fitxer gestionat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="exercici"><c>String</c>: Exercici de l'albaran</param>
    ''' <param name="operacio"><c>String</c>: Operacio de l'albaran</param>
    ''' <param name="venedor"><c>String</c>: Venedor de l'albaran, la ruta</param>
    ''' <param name="albaran"><c>String</c>: Numero d'albaran</param>
    ''' <param name="pagina"><c>String</c>: Pagina</param>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    ''' <returns><c>String</c>: ruta on guardar el fitxer gestionat</returns>
    Private Function CrearRutaGestionat(ByVal exercici As String, ByVal operacio As String, ByVal venedor As String, ByVal albaran As String, ByVal pagina As String, ByVal idAlbaran As String) As String
        Dim path As String

        'path = rutaGestionats & exercici & "-" & operacio & "-" & venedor & "-" & albaran & "-" & pagina & "-" & idAlbaran & ".pdf"
        path = rutaGestionats & exercici & "-" & operacio & "-" & venedor & "-" & albaran & "-" & pagina & ".pdf"
        While File.Exists(path)
            pagina = pagina + 1
            path = rutaGestionats & exercici & "-" & operacio & "-" & venedor & "-" & albaran & "-" & pagina & ".pdf"
        End While

        Return path
    End Function

    ''' <summary>Funcio que calcula si un fitxer esta correctament gestionat o no</summary>
    ''' <remarks>Actualment no es te en compte la data de l'albaran
    ''' </remarks>
    ''' <param name="exercici"><c>String</c>: Exercici de l'albaran</param>
    ''' <param name="operacio"><c>String</c>: Operacio de l'albaran</param>
    ''' <param name="venedor"><c>String</c>: Venedor de l'albaran, la ruta</param>
    ''' <param name="albaran"><c>String</c>: Numero d'albaran</param>
    ''' <param name="dataAlbaran"><c>String</c>: Data de l'albaran</param>
    ''' <returns><c>Boolean</c>: Si es els camps minims son correctes o no</returns>
    Public Function isGestionat(ByVal exercici As String, ByVal operacio As String, ByVal venedor As String, ByVal albaran As String, ByVal dataAlbaran As String) As Boolean
        'If IsNumeric(exercici) And IsNumeric(operacio) And IsNumeric(venedor) And IsNumeric(albaran) And IsDate(dataAlbaran) Then
        'Return True
        'End If

        If IsNumeric(exercici) And IsNumeric(operacio) And IsNumeric(venedor) And IsNumeric(albaran) Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>Funcio que limpia els errors d'escaneig al principi o final del texte del peu</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte procedent del OCR</param>
    ''' <returns><c>String</c>: Texte corretgit</returns>
    Private Function limpiarErrorsEscaneig(ByVal pageText As String) As String
        Dim s As String

        s = pageText.Replace("t[", "[[")
        s = s.Replace("[t", "[[")
        s = s.Replace("|[", "[[")
        s = s.Replace("[|", "[[")
        s = s.Replace("[1", "[[")
        s = s.Replace("([", "[[")
        s = s.Replace("[(", "[[")
        s = s.Replace("([", "[[")
        s = s.Replace("[f", "[[")
        s = s.Replace("f[", "[[")
        s = s.Replace("f|", "[[")
        s = s.Replace("|f", "[[")
        s = s.Replace("t|", "[[")
        s = s.Replace("|t", "[[")
        s = s.Replace("[I", "[[")
        s = s.Replace("I[", "[[")
        's = s.Replace("<<", "[[")
        s = s.Replace("<[", "[[")
        s = s.Replace("[<", "[[")
        s = s.Replace("ft", "[[")

        s = s.Replace("fI2", "[[2")

        s = s.Replace("])", "]]")
        s = s.Replace(")]", "]]")
        s = s.Replace("]J", "]]")
        s = s.Replace("J]", "]]")
        s = s.Replace("]3", "]]")
        s = s.Replace("]}", "]]")
        s = s.Replace("}]", "]]")
        s = s.Replace("I]", "]]")
        s = s.Replace("]I", "]]")
        's = s.Replace(">>", "]]")
        s = s.Replace("]>", "]]")
        s = s.Replace(">]", "]]")

        Return s
    End Function

    ''' <summary>Funcio que gestiona un albaran de manera automatica</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Ftx"><c>FileInfo</c>: Ruta del fitxer a gestionar</param>
    Public Sub gestionarAlbaran(ByVal Ftx As FileInfo)
        If DB.test Then
            MessageBox.Show(Ftx.FullName, "Error", MessageBoxButtons.OK)
        End If

        Dim its As ITextExtractionStrategy = New iTextSharp.text.pdf.parser.LocationTextExtractionStrategy()
        Dim reader As PdfReader = New PdfReader(Ftx.FullName)
        Dim pageText As String

        Dim exercici As String = "2017"
        Dim operacio As String = "00"
        Dim venedor As String = ""
        Dim albaran As String = ""
        Dim dataAlbaran As String = ""
        Dim textOriginal As String = ""
        Dim textCap As String = ""
        Dim pagina As String = "1"
        Dim gestionat As String = "0"
        Dim ruta As String = Ftx.FullName

        Dim idAlbaran As Integer
        Dim Sql As String

        pageText = PdfTextExtractor.GetTextFromPage(reader, 1, its)
        reader.Close()

        If DB.test Then
            MessageBox.Show(pageText, "Error", MessageBoxButtons.OK)
        End If

        'limpio errors d'escaneig
        pageText = limpiarErrorsEscaneig(pageText)

        textOriginal = tincTextPeu(pageText)
        textCap = tincTextPeu(pageText, "<<", ">>")

        If textOriginal <> "" Or textCap <> "" Then
            ' tinc text agrupat
            exercici = buscarExerciciTextPeu(textOriginal, pageText, textCap)
            operacio = buscarOperacioTextPeu(textOriginal, pageText, textCap)
            venedor = buscarVenedorTextPeu(textOriginal, pageText, textCap)
            albaran = buscarNumAlbaranTextPeu(textOriginal, pageText, textCap)
            pagina = buscarPagina(pageText)
            dataAlbaran = buscarDataAlbaran(pageText, Ftx.CreationTime)

            If isGestionat(exercici, operacio, venedor, albaran, dataAlbaran) Then
                gestionat = "1"
            Else
                gestionat = "0"
            End If

        Else
            ' no tinc texte agrupat. buscat la vida
            exercici = buscarExercici(pageText)
            operacio = buscarOperacio(pageText)
            venedor = buscarVenedor(pageText)
            albaran = buscarNumAlbaran(pageText)
            pagina = buscarPagina(pageText)
            dataAlbaran = buscarDataAlbaran(pageText, Ftx.CreationTime)
            textOriginal = ""
            gestionat = "0"
        End If

        'insert a la BD
        Sql = "INSERT INTO albarans (exercici, operacio, venedor, albaran, pagina, dataAlbaran, dataAlbaranFiltro, textOriginal, ruta, gestionat, textPdf) VALUES (" &
        "'" & DB.cleanSql(exercici) & "', " &
        "'" & DB.cleanSql(operacio) & "', " &
        "'" & DB.cleanSql(venedor) & "', " &
        "'" & DB.cleanSql(albaran) & "', " &
        "'" & pagina & "', " &
        "'" & dataAlbaran & "', " &
        "'" & DB.convertDate(dataAlbaran) & "', " &
        "'" & DB.cleanSql(textOriginal) & "', " &
        "'" & DB.cleanSql(ruta) & "', " &
        "'" & gestionat & "', " &
        "'" & DB.cleanSql(pageText) & "') "

        If DB.test Then
            MessageBox.Show(Sql, "Error", MessageBoxButtons.OK)
        End If

        DB.doOperation(Sql)

        'busco id del registre creat
        idAlbaran = DB.getLastId("albarans")

        'carrego lo pdf a la base de dades
        carregarPdf(ruta, idAlbaran.ToString())

        'creo la nova ruta
        If gestionat = "1" Then
            ruta = CrearRutaGestionat(exercici, operacio, venedor, albaran, pagina, idAlbaran.ToString())
        Else
            ruta = CrearRutaPendent(Ftx.CreationTime, Ftx.Name, idAlbaran.ToString())
        End If

        'moc fitxer
        mourePdf(Ftx.FullName, ruta, idAlbaran.ToString())

    End Sub


    ''' <summary>Funcio que modifica un albaran. informacio be del formulari manual</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="exercici"><c>String</c>: Exercici de l'albaran</param>
    ''' <param name="operacio"><c>String</c>: Operacio de l'albaran</param>
    ''' <param name="venedor"><c>String</c>: Venedor de l'albaran, la ruta</param>
    ''' <param name="albaran"><c>String</c>: Numero d'albaran</param>
    ''' <param name="pagina"><c>String</c>: Pagina</param>
    ''' <param name="dataAlbaran"><c>String</c>: Data de l'albaran</param>
    ''' <param name="textOriginal"><c>String</c>: Texte original</param>
    ''' <param name="ruta"><c>String</c>: Ruta de l'albaran</param>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    Public Sub modificarAlbaran(ByVal exercici As String, ByVal operacio As String, ByVal venedor As String, ByVal albaran As String, ByVal pagina As String, ByVal dataAlbaran As String, ByVal textOriginal As String, ByVal ruta As String, ByVal idAlbaran As String)
        Dim Sql As String
        Dim novaRuta As String
        Dim gestionat As String

        If isGestionat(exercici, operacio, venedor, albaran, dataAlbaran) Then
            gestionat = "1"
        Else
            gestionat = "0"
        End If

        If gestionat = "1" Then
            'si be de la carpeta original, es mou
            'si be de pendents, es mou
            'si esta gestionat i es canvien les dades, es mou
            novaRuta = CrearRutaGestionat(exercici, operacio, venedor, albaran, pagina, idAlbaran)
            If novaRuta <> ruta Then
                'moc fitxer
                mourePdf(ruta, novaRuta, idAlbaran)
            End If
        End If



        'insert a la BD
        Sql = "UPDATE albarans SET " &
        "exercici = '" & DB.cleanSql(exercici) & "', " &
        "operacio = '" & DB.cleanSql(operacio) & "', " &
        "venedor = '" & DB.cleanSql(venedor) & "', " &
        "albaran = '" & DB.cleanSql(albaran) & "', " &
        "pagina = '" & pagina & "', " &
        "dataAlbaran = '" & dataAlbaran & "', " &
        "dataAlbaranFiltro = '" & DB.convertDate(dataAlbaran) & "', " &
        "textOriginal = '" & DB.cleanSql(textOriginal) & "', " &
        "gestionat = '" & gestionat & "', " &
        "dataModificat = '" & DB.convertDateTime(Now) & "' " &
        "WHERE id=" & idAlbaran

        Log.Add("Modifico albaran. sql: " & Sql, "DEBUG")
        If DB.test Then
            MessageBox.Show(Sql, "Error", MessageBoxButtons.OK)
        End If

        DB.doOperation(Sql)
    End Sub


    ''' <summary>Funcio localitza el texte del peu de l'albaran</summary>
    ''' <remarks>Localitza el texte del peu de l'albaran o retorna buit en cas contrari
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: texte del peu o cadena buida</returns>
    Public Function tincTextPeu(ByVal pageText As String, Optional ByVal inici As String = "[[", Optional ByVal final As String = "]]") As String
        Dim position As Integer
        Dim positionFin As Integer
        Dim pos As Integer
        Dim text As String = ""
        Dim caracterFinal As String = final(0)

        'limpio errors escaneig
        pageText = limpiarErrorsEscaneig(pageText)

        position = pageText.IndexOf(inici)

        If position > -1 Then
            'tinc posicio de texte agrupat. el secciono. [[201700016118077]]
            positionFin = pageText.IndexOf(final)

            If positionFin > -1 Then
                positionFin = positionFin + 1
                If (positionFin - position) >= 17 And (positionFin - position) <= 19 Then 'detecta correctament lo final. poso un caracter menos per si de cas
                    text = pageText.Substring(position, positionFin - position)

                    'controlo que la cadena acabe be
                    If text.IndexOf(final) = -1 Then
                        text = text & final
                    End If

                Else ' no detecta correctament lo final.
                    ' podria ser que el final estes per davant del inici, o sigue, la cadena esta talla i la part final esta per davant de la inicial
                    '7023000000]]
                    '[[201
                    ' o podria ser que lo final tingues linies entre mig

                    'MessageBox.Show("codi invertit. position: " & position & ", pos final: " & positionFin & ", lenght: " & pageText.Length, "Error", MessageBoxButtons.OK)
                    'agafo la primera part
                    pos = position
                    While pos < pageText.Length
                        'MessageBox.Show("text: " & text & ", pos: " & pos & ", char: " & pageText(pos), "Error", MessageBoxButtons.OK)
                        If pageText(pos) <> Chr(10) Then
                            If IsNumeric(pageText(pos)) Then
                                text = text & pageText(pos)
                            End If
                        Else
                            pos = pageText.Length
                        End If

                        pos = pos + 1
                    End While

                    'busco la segona part
                    pos = positionFin - (17 - text.Length)
                    If pos < 0 Then
                        pos = 0
                    End If

                    While pos < pageText.Length
                        'MessageBox.Show("text: " & text & ", pos: " & pos & ", char: " & pageText(pos), "Error", MessageBoxButtons.OK)
                        If pageText(pos) <> caracterFinal Then
                            If IsNumeric(pageText(pos)) Then
                                text = text & pageText(pos)
                            End If
                        Else
                            pos = pageText.Length
                        End If

                        pos = pos + 1
                    End While

                    text = inici & text & final

                    If text.Length < 17 Then
                        If DB.test Then
                            MessageBox.Show("caracter restants: " & (pageText.Length - position), "Error", MessageBoxButtons.OK)
                        End If

                        If pageText.Length >= position + 17 Then ' si hi ha prou lletres
                            text = pageText.Substring(position, 17) & final
                        Else
                            text = ""
                        End If
                    End If
                End If

            Else
                'si no troba els simbols del final, intento fer la gestio en los 20 seguents caracters.
                If DB.test Then
                    MessageBox.Show("caracter restants: " & (pageText.Length - position), "Error", MessageBoxButtons.OK)
                End If

                If pageText.Length >= position + 17 Then ' si hi ha prou lletres
                    text = pageText.Substring(position, 17) & final
                End If
            End If
        End If

        'limpiar el peu
        If text <> "" Then
            text = text.Replace("\n", "")
            text = text.Replace(" ", "")
            text = text.Replace(Chr(10), "")
            text = text.Replace(Chr(13), "")
            text = text.Replace("O", "0")

            'trec lletres del mig, per si de cas
            For pos = 0 To (Asc("Z") - Asc("A"))
                text = text.Replace(Chr(pos + Asc("a")), "")
                text = text.Replace(Chr(pos + Asc("A")), "")
            Next

        End If

        Return text
    End Function

    ''' <summary>Funcio que busca l'exercici dins el text de la pagina</summary>
    ''' <remarks>s'utilitza en cas de no tenir texte del peu de pagina
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la data de l'exercici o cadena buina</returns>
    Public Function buscarExercici(ByVal pageText As String) As String
        Dim position As Integer
        Dim any As Integer = Now.Year - 1

        If Now.Month > 6 Then
            any = Now.Year
        End If

        While any <= Now.Year
            If DB.test Then
                MessageBox.Show("busco any: " & any.ToString(), "Error", MessageBoxButtons.OK)
            End If

            position = pageText.IndexOf(any.ToString())
            While position > 0 'indexof retorna -1 si no troba i 0 si la cadena esta buida. com la operacio mai estara a la posicio 0, aprofito i poso aixo
                If DB.test Then
                    MessageBox.Show("posicio: " & position & " hi ha " & pageText(position - 1) & " i " & pageText(position + 2), "Error", MessageBoxButtons.OK)
                End If

                If pageText(position + 2) = "/n" And pageText(position - 1) = "/n" Then
                    Return any.ToString()
                End If
                If pageText(position + 2) = " " And pageText(position - 1) = " " Then
                    Return any.ToString()
                End If

                position = pageText.IndexOf(any.ToString(), position + 1)
            End While

            any = any + 1
        End While

        Return Now.Year.ToString
    End Function

    ''' <summary>Funcio que busca l'operacio dins el text de la pagina</summary>
    ''' <remarks>'utilitza en cas de no tenir texte del peu de pagina
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: l'operacio o cadena buida</returns>
    Public Function buscarOperacio(ByVal pageText As String) As String
        Dim position As Integer

        For Each ope As String In OPERACIONS_VALIDES
            position = pageText.IndexOf(ope)
            While position > 0 And position < (pageText.Length - 2) 'indexof retorna -1 si no troba i 0 si la cadena esta buida. com la operacio mai estara a la posicio 0, aprofito i poso aixo
                If DB.test Then
                    MessageBox.Show("posicio: " & position & " hi ha " & pageText(position - 1) & " i " & pageText(position + 2), "Error", MessageBoxButtons.OK)
                End If

                If pageText(position + 2) = "/n" And pageText(position - 1) = "/n" Then
                    Return ope
                End If
                If pageText(position + 2) = " " And pageText(position - 1) = " " Then
                    Return ope
                End If

                position = pageText.IndexOf(ope, position + 1)
            End While
        Next

        Return "00"
    End Function

    ''' <summary>Funcio que busca el venedor dins el text de la pagina</summary>
    ''' <remarks>'utilitza en cas de no tenir texte del peu de pagina
    ''' <para>imposible de detectar dins de tot el text</para>
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: el venedor o cadena buida</returns>
    Public Function buscarVenedor(ByVal pageText As String) As String
        Dim position As Integer

        For Each ope As String In RUTES_VALIDES
            position = pageText.IndexOf(ope)
            While position > 0 And position < (pageText.Length - 4) 'indexof retorna -1 si no troba i 0 si la cadena esta buida. com la operacio mai estara a la posicio 0, aprofito i poso aixo
                If DB.test Then
                    MessageBox.Show("posicio: " & position & " hi ha " & pageText(position - 1) & " i " & pageText(position + 2), "Error", MessageBoxButtons.OK)
                End If

                If pageText(position + 3) = "/n" And pageText(position - 1) = "/n" Then
                    Return ope
                End If
                If pageText(position + 3) = " " And pageText(position - 1) = " " Then
                    Return ope
                End If

                position = pageText.IndexOf(ope, position + 1)
            End While
        Next

        Return ""
    End Function

    ''' <summary>Funcio que busca la pagina dins el text de la pagina</summary>
    ''' <remarks>'utilitza en cas de no tenir texte del peu de pagina
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la pagina o cadena buida</returns>
    Public Function buscarPagina(ByVal pageText As String) As String
        Dim position As Integer
        Dim pagina As Integer = 1

        For pagina = 1 To 3
            If DB.test Then
                MessageBox.Show("busco pagina " & pagina.ToString(), "Error", MessageBoxButtons.OK)
            End If

            position = pageText.IndexOf("Pag: " & pagina.ToString())

            If position > -1 Then
                Return pagina.ToString()
            End If
        Next

        Return "1"
    End Function

    ''' <summary>Funcio que busca el num de l'albaran dins el text de la pagina</summary>
    ''' <remarks>'utilitza en cas de no tenir texte del peu de pagina
    ''' <para>imposible de detectar dins de tot el text</para>
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: el num de l'albaran o cadena buida</returns>
    Public Function buscarNumAlbaran(ByVal pageText As String) As String


        Return ""
    End Function

    ''' <summary>Funcio que busca la data de l'albaran dins el text de la pagina</summary>
    ''' <remarks>'utilitza en cas de no tenir texte del peu de pagina
    ''' </remarks>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la data de l'albaran o cadena buida</returns>
    Public Function buscarDataAlbaran(ByVal pageText As String, ByVal DataCreacio As Date) As String
        Dim position As Integer
        Dim data As String = ""
        Dim actData As Date = Nothing
        Dim newData As Date

        position = pageText.IndexOf("/")
        While position > 0 And position < (pageText.Length - 8) 'indexof retorna -1 si no troba i 0 si la cadena esta buida. com la operacio mai estara a la posicio 0, aprofito i poso aixo
            If DB.test Then
                MessageBox.Show("posicio: " & position & " hi ha " & pageText(position + 3), "Error", MessageBoxButtons.OK)
            End If

            If position >= 2 Then ' control que la / tingue 2 caracters per davant, sino, peta
                If pageText(position + 3) = "/" Then
                    data = pageText.Substring(position - 2, 10)
                    ' correccio error O per 0
                    data = data.Replace("O", "0")
                    data = data.Replace("o", "0")

                    If DB.test Then
                        MessageBox.Show("trobat: " & data, "Error", MessageBoxButtons.OK)
                    End If

                    If data <> "00/00/0000" Then
                        If IsDate(data) Then
                            If DB.test Then
                                MessageBox.Show("data correcta: " & data, "Error", MessageBoxButtons.OK)
                            End If

                            newData = data
                            If newData < DataCreacio And newData.Year >= (Now.Year - 1) Then

                                If actData = Nothing Then
                                    actData = newData
                                Else
                                    If newData < actData Then
                                        actData = newData
                                    End If
                                End If
                            End If
                        Else
                            If DB.test Then
                                MessageBox.Show("data incorrecta: " & data, "Error", MessageBoxButtons.OK)
                            End If
                        End If
                    End If
                End If
            End If


            position = pageText.IndexOf("/", position + 8)
        End While

        If actData <> Nothing Then
            Return actData.ToShortDateString()
        End If

        Return ""
    End Function


    Public Function decidirDosTexts(ByVal Peu As String, ByVal Cap As String, ByVal longitud As Integer) As String
        ' miro continguts
        If Peu <> "" And Cap = "" Then
            Return Peu
        End If

        If Peu = "" And Cap <> "" Then
            Return Cap
        End If

        If Peu <> "" And Cap <> "" Then
            If Peu = Cap Then
                Return Peu
            Else
                'miro per longitud
                If Peu.Length = longitud And Cap.Length <> longitud Then
                    Return Peu
                End If

                If Peu.Length <> longitud And Cap.Length = longitud Then
                    Return Cap
                End If

                If Peu.Length = longitud And Cap.Length = longitud Then
                    'si tenen igual longitud, retorno el peu perque sempre se sol llegir millor
                    Return Peu
                End If
            End If
        End If

        Return ""
    End Function

    Public Function esDadaValida(ByVal Dada As String, ByVal Tipo As String) As Boolean
        Dim any As Integer = 2016

        If Tipo = "OPERACIO" Then
            For Each Str As String In OPERACIONS_VALIDES
                If Str = Dada Then
                    Return True
                End If
            Next
        End If

        If Tipo = "EXERCICI" Then
            While any <= Now.Year
                If any = Dada Then
                    Return True
                End If

                any = any + 1
            End While
        End If

        If Tipo = "RUTA" Then
            For Each Str As String In RUTES_VALIDES
                If Str = Dada Then
                    Return True
                End If
            Next
        End If

        Return False
    End Function


    ''' <summary>Funcio que busca l'exercici de l'albaran dins el peu de la pagina</summary>
    ''' <remarks>En cas de no trobar-ho al text del peu de la pagina, crida la funcio que ho busca a tot el text escanejat
    ''' </remarks>
    ''' <param name="Text"><c>String</c>: Texte del peu de la pagina</param>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: l'exercici de l'albaran o cadena buida</returns>
    Public Function buscarExerciciTextPeu(ByVal Text As String, ByVal pageText As String, ByVal TextCap As String) As String
        Dim sPeu As String = ""
        Dim sCap As String = ""

        If Text.Length() > 6 Then
            sPeu = Text.Substring(2, 4)
            If IsNumeric(sPeu) Then
                If esDadaValida(sPeu, "EXERCICI") = False Then
                    sPeu = ""
                End If
            Else
                sPeu = ""
            End If
        End If

        If TextCap.Length() > 6 Then
            sCap = TextCap.Substring(2, 4)
            If IsNumeric(sCap) Then
                If esDadaValida(sCap, "EXERCICI") = False Then
                    sCap = ""
                End If
            Else
                sCap = ""
            End If
        End If

        sPeu = decidirDosTexts(sPeu, sCap, 4)

        If sPeu <> "" Then
            Return sPeu
        End If

        Return buscarExercici(pageText)
    End Function

    ''' <summary>Funcio que busca la operacio de l'albaran dins el peu de la pagina</summary>
    ''' <remarks>En cas de no trobar-ho al text del peu de la pagina, crida la funcio que ho busca a tot el text escanejat
    ''' </remarks>
    ''' <param name="Text"><c>String</c>: Texte del peu de la pagina</param>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la operacio de l'albaran o cadena buida</returns>
    Public Function buscarOperacioTextPeu(ByVal Text As String, ByVal pageText As String, ByVal TextCap As String) As String
        Dim sPeu As String = ""
        Dim sCap As String = ""

        If Text.Length() > 8 Then
            sPeu = Text.Substring(6, 2)
            If IsNumeric(sPeu) Then
                If esDadaValida(sPeu, "OPERACIO") = False Then
                    sPeu = ""
                End If
            Else
                sPeu = ""
            End If
        End If

        If TextCap.Length() > 8 Then
            sCap = TextCap.Substring(6, 2)
            If IsNumeric(sCap) Then
                If esDadaValida(sCap, "OPERACIO") = False Then
                    sCap = ""
                End If
            Else
                sCap = ""
            End If
        End If

        sPeu = decidirDosTexts(sPeu, sCap, 2)

        If sPeu <> "" Then
            Return sPeu
        End If

        Return buscarOperacio(pageText)
    End Function

    ''' <summary>Funcio que busca la ruta del venedor de l'albaran dins el peu de la pagina</summary>
    ''' <remarks>En cas de no trobar-ho al text del peu de la pagina, crida la funcio que ho busca a tot el text escanejat
    ''' </remarks>
    ''' <param name="Text"><c>String</c>: Texte del peu de la pagina</param>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la ruta del venedor de l'albaran o cadena buida</returns>
    Public Function buscarVenedorTextPeu(ByVal Text As String, ByVal pageText As String, ByVal TextCap As String) As String
        Dim sPeu As String = ""
        Dim sCap As String = ""

        If Text.Length() > 11 Then
            sPeu = Text.Substring(8, 3)
            If IsNumeric(sPeu) Then
                If esDadaValida(sPeu, "RUTA") = False Then
                    sPeu = ""
                End If
            Else
                sPeu = ""
            End If
        End If

        If TextCap.Length() > 11 Then
            sCap = TextCap.Substring(8, 3)
            If IsNumeric(sCap) Then
                If esDadaValida(sCap, "RUTA") = False Then
                    sCap = ""
                End If
            Else
                sCap = ""
            End If
        End If

        sPeu = decidirDosTexts(sPeu, sCap, 3)

        If sPeu <> "" Then
            Return sPeu
        End If

        Return buscarVenedor(pageText)
    End Function

    ''' <summary>Funcio que busca el num de l'albaran dins el peu de la pagina</summary>
    ''' <remarks>En cas de no trobar-ho al text del peu de la pagina, crida la funcio que ho busca a tot el text escanejat
    ''' </remarks>
    ''' <param name="Text"><c>String</c>: Texte del peu de la pagina</param>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: el num de l'albaran o cadena buida</returns>
    Public Function buscarNumAlbaranTextPeu(ByVal Text As String, ByVal pageText As String, ByVal TextCap As String) As String
        Dim sPeu As String = ""
        Dim sCap As String = ""
        Dim position As Integer

        If Text.Length() > 17 Then
            position = Text.IndexOf("]")
            If position > -1 Then
                sPeu = Text.Substring(11, position - 11)
                If IsNumeric(sPeu) = False Then
                    sPeu = ""
                End If
            End If
        End If

        If TextCap.Length() > 17 Then
            position = TextCap.IndexOf(">")
            If position > -1 Then
                sCap = TextCap.Substring(11, position - 11)
                If IsNumeric(sCap) = False Then
                    sCap = ""
                End If
            End If
        End If

        sPeu = decidirDosTexts(sPeu, sCap, 6)

        If sPeu <> "" Then
            Return sPeu
        End If

        Return buscarNumAlbaran(pageText)
    End Function

    ''' <summary>Funcio que busca la pagina dins el peu de la pagina</summary>
    ''' <remarks>En cas de no trobar-ho al text del peu de la pagina, crida la funcio que ho busca a tot el text escanejat
    ''' </remarks>
    ''' <param name="Text"><c>String</c>: Texte del peu de la pagina</param>
    ''' <param name="pageText"><c>String</c>: Texte escanejat per OCR</param>
    ''' <returns><c>String</c>: la pagina o cadena buida</returns>
    Public Function buscarPaginaTextPeu(ByVal Text As String, ByVal pageText As String, ByVal TextCap As String, Optional ByVal caracterFinal As String = "]") As String
        Dim sPeu As String = ""
        Dim sCap As String = ""
        Dim position As Integer

        If Text.Length() > 18 Then
            position = Text.IndexOf("]")
            If position > -1 Then
                sPeu = Text.Substring(11, position - 11)
                If IsNumeric(sPeu) = False Then
                    sPeu = ""
                End If
            End If
        End If

        If TextCap.Length() > 18 Then
            position = TextCap.IndexOf(">")
            If position > -1 Then
                sCap = TextCap.Substring(11, position - 11)
                If IsNumeric(sCap) = False Then
                    sCap = ""
                End If
            End If
        End If

        sPeu = decidirDosTexts(sPeu, sCap, 1)

        If sPeu <> "" Then
            Return sPeu
        End If

        Return buscarPagina(pageText)
    End Function



    ''' <summary>Funcio que torna a crear el pdf en cas que este no estigue. retorna la ruta</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    ''' <returns><c>String</c>: ruta de l'albaran</returns>
    Public Function guardarPdf(ByVal idAlbaran As String) As String
        Dim Sql As String
        Dim ds As DataSet

        Dim aBytDocumento() As Byte = Nothing
        Dim oFileStream As FileStream

        'buscar albaran
        Sql = "SELECT a.ruta AS 'Ruta'" &
    ", a.pdf AS 'Pdf'" &
    " FROM albarans a WHERE " &
    " a.id = " & idAlbaran

        ds = DB.doSelect(Sql)

        'check la ruta
        If ds.Tables(0).Rows.Count > 0 Then
            If Not File.Exists(ds.Tables(0).Rows(0)(0)) Then
                'guardar el pdf
                Try
                    aBytDocumento = CType(ds.Tables(0).Rows(0)(1), Byte())

                    oFileStream = New FileStream(ds.Tables(0).Rows(0)(0), FileMode.CreateNew, FileAccess.Write)
                    oFileStream.Write(aBytDocumento, 0, aBytDocumento.Length)
                    oFileStream.Close()
                Catch
                End Try
            End If
        End If

        Return ds.Tables(0).Rows(0)(0)
    End Function

    ''' <summary>Funcio que mou el pdf de carpeta</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="rutaActual"><c>String</c>: Ruta actual del fitxer pdf</param>
    ''' <param name="rutaNova"><c>String</c>: Nova ruta del fitxer</param>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    Public Sub mourePdf(ByVal rutaActual As String, ByVal rutaNova As String, ByVal idAlbaran As String)
        Dim Sql As String

        'moc fitxer
        If DB.test Then
            MessageBox.Show("movent " & rutaActual & " a " & rutaNova, "Error", MessageBoxButtons.OK)
        End If

        'guardo la nova ruta a la db
        Sql = "UPDATE albarans SET ruta='" & DB.cleanSql(rutaNova) & "' WHERE id=" & idAlbaran
        If DB.test Then
            MessageBox.Show("Sql " & Sql, "Error", MessageBoxButtons.OK)
        End If

        DB.doOperation(Sql)

        If DB.test = False Then
            Try
                File.Move(rutaActual, rutaNova)
            Catch
            End Try
        End If
    End Sub

    ''' <summary>Funcio que carrega el pdf a la bd</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="ruta"><c>String</c>: Ruta actual del fitxer pdf</param>
    ''' <param name="idAlbaran"><c>String</c>: ID de la taula on esta guardada la informacio de l'albaran</param>
    Public Sub carregarPdf(ByVal ruta As String, ByVal idAlbaran As String)
        Dim Sql As String
        Dim data() As Byte

        'carrego lo pdf a la base de dades
        Data = File.ReadAllBytes(ruta)

        Sql = "UPDATE albarans SET pdf = @content WHERE id=" & idAlbaran
        DB.updateBlob(Sql, "@content", Data)

    End Sub


    ''' <summary>Funcio que busca totes les rutes disponibles</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatRutes()
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT DISTINCT a.venedor AS 'Ruta' FROM albarans a ORDER BY a.venedor"
        ds = DB.doSelect(Sql)

        Return ds
    End Function

    ''' <summary>Funcio que busca els albarans segons els filtros</summary>
    ''' <remarks> per al funcionament avançat del programa
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="albaran"><c>String</c>: Numero d'albaran</param>
    ''' <param name="venedor"><c>String</c>: Venedor de l'albaran, la ruta</param>
    ''' <param name="gestionat"><c>String</c>: Si l'albaran esta gestionat o no</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatAlbaransPendents(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Albaran As String, ByVal Venedor As String, ByVal gestionat As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT a.id AS 'ID'" &
            ", a.exercici AS 'Exercici'" &
            ", a.operacio AS 'Operacio'" &
            ", a.venedor AS 'Ruta'" &
            ", a.albaran AS 'Albaran'" &
            ", a.pagina AS 'Pagina'" &
            ", a.dataAlbaran AS 'Data albaran'" &
            ", a.textOriginal AS 'Text original'" &
            ", a.ruta AS 'Fitxer'" &
            ", a.textPdf AS 'textPdf'" &
            " FROM albarans a WHERE"

        If Albaran <> "" Then
            Sql = Sql & " a.albaran LIKE '%" & Albaran & "%'"
            Sql = Sql & " ORDER BY a.id DESC"

        Else
            If gestionat = "0" Then
                Sql = Sql & " a.gestionat = '" & gestionat & "'"
                Sql = Sql & " ORDER BY a.id DESC LIMIT 50"

            Else
                Sql = Sql & " a.dataAlbaranFiltro >= '" & DB.convertDate(DataInici) & "'" &
                    " AND a.dataAlbaranFiltro <= '" & DB.convertDate(DataFinal) & "'"

                If Venedor <> "Tots" Then
                    Sql = Sql & " AND a.venedor = '" & Venedor & "'"
                End If

                If gestionat <> "2" Then
                    Sql = Sql & " AND a.gestionat = '" & gestionat & "'"
                End If

                Sql = Sql & " ORDER BY a.albaran"
            End If
        End If

        'If DB.test Then
        'MessageBox.Show("Query: " & Sql, "Error", MessageBoxButtons.OK)
        'End If

        ds = DB.doSelect(Sql)

        Return ds
    End Function


    ''' <summary>Funcio que busca els albarans segons els filtros de la busqueda avançada</summary>
    ''' <remarks> per al funcionament avançat del programa
    ''' </remarks>
    ''' <param name="data"><c>String</c>: Data albaran</param>
    ''' <param name="text"><c>String</c>: text dins el camp textPdf</param>
    ''' <param name="gestionat"><c>String</c>: Si l'albaran esta gestionat o no</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatAlbaransBA(ByVal data As String, ByVal text As String, ByVal gestionat As String)
        Dim Sql As String
        Dim where As String = ""
        Dim ds As DataSet

        Sql = "SELECT a.id AS 'ID'" &
            ", a.exercici AS 'Exercici'" &
            ", a.operacio AS 'Operacio'" &
            ", a.venedor AS 'Ruta'" &
            ", a.albaran AS 'Albaran'" &
            ", a.pagina AS 'Pagina'" &
            ", a.dataAlbaran AS 'Data albaran'" &
            ", a.textOriginal AS 'Text original'" &
            ", a.ruta AS 'Fitxer'" &
            ", a.textPdf AS 'textPdf'" &
            " FROM albarans a"

        If data <> "" Then
            where = where & " a.dataAlbaranFiltro = '" & data & "'"
        End If

        If gestionat <> "" Then
            If where <> "" Then
                where = where & " AND"
            End If
            where = where & " a.gestionat = '" & gestionat & "'"
        End If

        If text <> "" Then
            If where <> "" Then
                where = where & " AND"
            End If
            where = where & " a.textPdf LIKE '%" & text & "%'"
        End If

        If where <> "" Then
            Sql = Sql & " WHERE" & where
        End If

        Sql = Sql & " ORDER BY a.albaran"

        'If DB.test Then
        'MessageBox.Show("Query: " & Sql, "Error", MessageBoxButtons.OK)
        'End If

        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que busca els albarans segons els filtros</summary>
    ''' <remarks> per al funcionament basic del programa
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="albaran"><c>String</c>: Numero d'albaran</param>
    ''' <param name="venedor"><c>String</c>: Venedor de l'albaran, la ruta</param>
    ''' <param name="gestionat"><c>String</c>: Si l'albaran esta gestionat o no</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatAlbaransPendentsBasic(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Albaran As String, ByVal Venedor As String, ByVal gestionat As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT a.id AS 'ID'" &
            ", a.exercici AS 'Exercici'" &
            ", a.operacio AS 'Operacio'" &
            ", a.venedor AS 'Ruta'" &
            ", a.albaran AS 'Albaran'" &
            ", a.pagina AS 'Pagina'" &
            ", a.dataAlbaran AS 'Data albaran'" &
            " FROM albarans a WHERE"

        If Albaran <> "" Then
            Sql = Sql & " a.albaran LIKE '%" & Albaran & "%'"
            Sql = Sql & " ORDER BY a.id DESC"

        Else
            If gestionat = "0" Then
                Sql = Sql & " a.gestionat = '" & gestionat & "'"
                Sql = Sql & " ORDER BY a.id DESC LIMIT 50"

            Else
                'Sql = Sql & " a.dataAlbaranFiltro >= '" & DB.convertDate(DataInici) & "'" &
                '    " AND a.dataAlbaranFiltro <= '" & DB.convertDate(DataFinal) & "'"

                Sql = Sql & " a.dataEntrat >= '" & DB.convertDate(DataInici) & "'" &
                    " AND a.dataEntrat <= '" & DB.convertDate(DataFinal.AddDays(1)) & "'"

                If Venedor <> "Tots" Then
                    Sql = Sql & " AND a.venedor = '" & Venedor & "'"
                End If

                If gestionat <> "2" Then
                    Sql = Sql & " AND a.gestionat = '" & gestionat & "'"
                End If

                Sql = Sql & " ORDER BY a.albaran"
            End If
        End If

        'If DB.test Then
        'MessageBox.Show("Query: " & Sql, "Error", MessageBoxButtons.OK)
        'End If

        ds = DB.doSelect(Sql)

        Return ds
    End Function

    ''' <summary>Funcio que les estadistiques d'una data</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataEscaneix"><c>Date</c>: Data en que es van escanejar els albarans</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function BuscarTotals(ByVal DataEscaneix As Date)
        Dim Sql As String
        Dim ds As DataSet
        Dim dataFin As Date = DataEscaneix.AddDays(1)

        Sql = "SELECT COUNT(a.id) AS 'Total'" &
            ", (SELECT COUNT(a.id) FROM albarans a WHERE gestionat = '0') AS 'pendent' " &
            ", (SELECT COUNT(a.id) FROM albarans a WHERE dataEntrat >= '" & DB.convertDate(DataEscaneix) & "' AND dataEntrat <= '" & DB.convertDate(dataFin) & "') AS 'escanejats' " &
            ", (SELECT COUNT(a.id) FROM albarans a WHERE dataEntrat >= '" & DB.convertDate(DataEscaneix) & "' AND dataEntrat <= '" & DB.convertDate(dataFin) & "' AND dataModificat IS NULL AND gestionat = '1') AS 'ok' " &
            ", (SELECT COUNT(a.id) FROM albarans a WHERE dataEntrat >= '" & DB.convertDate(DataEscaneix) & "' AND dataEntrat <= '" & DB.convertDate(dataFin) & "' AND dataModificat IS NOT NULL AND gestionat = '1') AS 'modificat' " &
            ", (SELECT COUNT(a.id) FROM albarans a WHERE dataEntrat >= '" & DB.convertDate(DataEscaneix) & "' AND dataEntrat <= '" & DB.convertDate(dataFin) & "' AND gestionat = '0') AS 'ko' " &
            "FROM albarans a"

        If DB.test Then
            MessageBox.Show("Query: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DB.doSelect(Sql)

        Return ds
    End Function

    ''' <summary>Funcio que recalcula les dates dels filtros a la taula</summary>
    ''' <remarks>NO S'USA. es va crear per a facilitar un proces automatic durant la creacio del programa.
    ''' </remarks>
    Public Sub ferDadesFiltro()
        Dim Sql As String
        Dim ds As DataSet
        Dim row As Integer

        Sql = "SELECT a.id, a.dataAlbaran FROM albarans a WHERE a.dataAlbaranFiltro IS NULL AND a.dataAlbaran IS NOT NULL AND a.dataAlbaran != ''"
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < ds.Tables(0).Rows.Count
                Sql = "UPDATE albarans SET dataAlbaranFiltro='" & DB.convertDate(ds.Tables(0).Rows(row)(1)) & "' WHERE id=" & ds.Tables(0).Rows(row)(0)
                If DB.test Then
                    MessageBox.Show(Sql, "Error", MessageBoxButtons.OK)
                End If

                DB.doOperation(Sql)
                row = row + 1
            End While
        End If

    End Sub

    ''' <summary>Funcio que recalcula de manera automa tots els albarans pendents</summary>
    ''' <remarks>NO S'USA. es va crear per a facilitar un proces automatic durant la creacio del programa.
    ''' </remarks>
    ''' <param name="Progres"><c>ProgressBar</c>: barra que mostra el progres del proces</param>
    Public Sub recalcularPendents(ByVal Progres As ProgressBar)
        Dim Sql As String
        Dim ds As DataSet
        Dim row As Integer

        Dim textPeu As String
        Dim textCap As String
        Dim textPdf As String
        Dim exercici As String
        Dim operacio As String
        Dim venedor As String
        Dim albaran As String
        Dim pagina As String
        Dim dataAlbaran As String

        Sql = "SELECT a.id, a.textPdf, a.ruta FROM albarans a WHERE a.gestionat = '0'"
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Progres.Maximum = ds.Tables(0).Rows.Count
            Progres.Value = 0

            row = 0
            While row < ds.Tables(0).Rows.Count
                textPdf = ds.Tables(0).Rows(row)(1)
                textPeu = tincTextPeu(textPdf)
                textCap = tincTextPeu(textPdf, "<<", ">>")

                If textPeu <> "" Or textCap <> "" Then
                    ' gestio del text del peu
                    exercici = buscarExerciciTextPeu(textPeu, textPdf, textCap)
                    operacio = buscarOperacioTextPeu(textPeu, textPdf, textCap)
                    venedor = buscarVenedorTextPeu(textPeu, textPdf, textCap)
                    albaran = buscarNumAlbaranTextPeu(textPeu, textPdf, textCap)

                Else
                    'gestio sense text del peu
                    exercici = buscarExercici(textPdf)
                    operacio = buscarOperacio(textPdf)
                    venedor = buscarVenedor(textPdf)
                    albaran = buscarNumAlbaran(textPdf)
                End If

                pagina = buscarPagina(textPdf)
                dataAlbaran = buscarDataAlbaran(textPdf, Now)

                modificarAlbaran(exercici, operacio, venedor, albaran, pagina, dataAlbaran, textPeu, ds.Tables(0).Rows(row)(2), ds.Tables(0).Rows(row)(0))

                Progres.Value = Progres.Value + 1
                row = row + 1
            End While
        End If
    End Sub

    ''' <summary>Funcio que recalcula la ruta dels albarans gestionats</summary>
    ''' <remarks>NO S'USA. es va crear per a facilitar un proces automatic durant la creacio del programa.
    ''' </remarks>
    ''' <param name="Progres"><c>ProgressBar</c>: barra que mostra el progres del proces</param>
    Public Sub recalcularRutaGestionats(ByVal Progres As ProgressBar)
        Dim Sql As String
        Dim ds As DataSet
        Dim row As Integer

        Dim idAlbaran As String
        Dim exercici As String
        Dim operacio As String
        Dim venedor As String
        Dim albaran As String
        Dim pagina As String

        Dim ruta As String
        Dim novaRuta As String

        Sql = "SELECT a.id, a.exercici, a.operacio, a.venedor, a.albaran, a.pagina, a.ruta FROM albarans a WHERE a.gestionat = '1'"
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Progres.Maximum = ds.Tables(0).Rows.Count
            Progres.Value = 0

            row = 0
            While row < ds.Tables(0).Rows.Count
                idAlbaran = ds.Tables(0).Rows(row)(0)
                exercici = ds.Tables(0).Rows(row)(1)
                operacio = ds.Tables(0).Rows(row)(2)
                venedor = ds.Tables(0).Rows(row)(3)
                albaran = ds.Tables(0).Rows(row)(4)
                pagina = ds.Tables(0).Rows(row)(5)
                ruta = ds.Tables(0).Rows(row)(6)

                novaRuta = CrearRutaGestionat(exercici, operacio, venedor, albaran, pagina, idAlbaran)

                If ruta <> novaRuta Then
                    mourePdf(ruta, novaRuta, idAlbaran)
                End If

                Progres.Value = Progres.Value + 1
                row = row + 1
            End While
        End If
    End Sub

End Class
