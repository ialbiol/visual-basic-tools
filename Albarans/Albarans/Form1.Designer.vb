﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FAlbarans
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Y:")
        Me.TC1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.BReferRutes = New System.Windows.Forms.Button()
        Me.BPRecalcularTot = New System.Windows.Forms.Button()
        Me.BDataFiltro = New System.Windows.Forms.Button()
        Me.BPCarregarPdf = New System.Windows.Forms.Button()
        Me.WBPPdf = New System.Windows.Forms.WebBrowser()
        Me.TPTextPdf = New System.Windows.Forms.RichTextBox()
        Me.LPRuta = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BPCalcular = New System.Windows.Forms.Button()
        Me.BPGuardar = New System.Windows.Forms.Button()
        Me.LPText = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TPData = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TPPagina = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TPAlbaran = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TPVenedor = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TPOperacio = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TPExercici = New System.Windows.Forms.TextBox()
        Me.DGPendents = New System.Windows.Forms.DataGridView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.BCGestioTot = New System.Windows.Forms.Button()
        Me.BCGestioCarpeta = New System.Windows.Forms.Button()
        Me.WBCarpeta = New System.Windows.Forms.WebBrowser()
        Me.TVCarpetes = New System.Windows.Forms.TreeView()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.LTotalErrors = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.LTotalModificats = New System.Windows.Forms.Label()
        Me.LTotalOk = New System.Windows.Forms.Label()
        Me.LTotalEscaneix = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.LTotals = New System.Windows.Forms.Label()
        Me.MCTotals = New System.Windows.Forms.MonthCalendar()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TBBAlbaran = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CBBGestionats = New System.Windows.Forms.ComboBox()
        Me.CBBRuta = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TLoadAll = New System.Windows.Forms.Timer(Me.components)
        Me.TDelete = New System.Windows.Forms.Timer(Me.components)
        Me.TSetFocus = New System.Windows.Forms.Timer(Me.components)
        Me.PBCarrega = New System.Windows.Forms.ProgressBar()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.LOperacio = New System.Windows.Forms.Label()
        Me.PBasic = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.BGuardarBasic = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TPDataBasic = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TPPaginaBasic = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TPAlbaranBasic = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TPVenedorBasic = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TPOperacioBasic = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TPExerciciBasic = New System.Windows.Forms.TextBox()
        Me.WBPBasic = New System.Windows.Forms.WebBrowser()
        Me.DGPEndentsBasic = New System.Windows.Forms.DataGridView()
        Me.Pbuscar = New System.Windows.Forms.Panel()
        Me.MCBData = New System.Windows.Forms.MonthCalendar()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PBusquedaAvan = New System.Windows.Forms.Panel()
        Me.TBAGestionat = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TBAText = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TBAData = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.LCText = New System.Windows.Forms.Label()
        Me.TC1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGPendents, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.PBasic.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DGPEndentsBasic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbuscar.SuspendLayout()
        Me.PBusquedaAvan.SuspendLayout()
        Me.SuspendLayout()
        '
        'TC1
        '
        Me.TC1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TC1.Controls.Add(Me.TabPage2)
        Me.TC1.Controls.Add(Me.TabPage4)
        Me.TC1.Controls.Add(Me.TabPage1)
        Me.TC1.Location = New System.Drawing.Point(221, 8)
        Me.TC1.Name = "TC1"
        Me.TC1.SelectedIndex = 0
        Me.TC1.Size = New System.Drawing.Size(1125, 687)
        Me.TC1.TabIndex = 0
        Me.TC1.Visible = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.LCText)
        Me.TabPage2.Controls.Add(Me.BReferRutes)
        Me.TabPage2.Controls.Add(Me.BPRecalcularTot)
        Me.TabPage2.Controls.Add(Me.BDataFiltro)
        Me.TabPage2.Controls.Add(Me.BPCarregarPdf)
        Me.TabPage2.Controls.Add(Me.WBPPdf)
        Me.TabPage2.Controls.Add(Me.TPTextPdf)
        Me.TabPage2.Controls.Add(Me.LPRuta)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.BPCalcular)
        Me.TabPage2.Controls.Add(Me.BPGuardar)
        Me.TabPage2.Controls.Add(Me.LPText)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.TPData)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.TPPagina)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.TPAlbaran)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.TPVenedor)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.TPOperacio)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.TPExercici)
        Me.TabPage2.Controls.Add(Me.DGPendents)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1117, 659)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Albarans"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'BReferRutes
        '
        Me.BReferRutes.Location = New System.Drawing.Point(1021, 477)
        Me.BReferRutes.Margin = New System.Windows.Forms.Padding(2)
        Me.BReferRutes.Name = "BReferRutes"
        Me.BReferRutes.Size = New System.Drawing.Size(91, 29)
        Me.BReferRutes.TabIndex = 25
        Me.BReferRutes.Text = "Refer Rutes"
        Me.BReferRutes.UseVisualStyleBackColor = True
        '
        'BPRecalcularTot
        '
        Me.BPRecalcularTot.Location = New System.Drawing.Point(1021, 338)
        Me.BPRecalcularTot.Margin = New System.Windows.Forms.Padding(2)
        Me.BPRecalcularTot.Name = "BPRecalcularTot"
        Me.BPRecalcularTot.Size = New System.Drawing.Size(91, 46)
        Me.BPRecalcularTot.TabIndex = 24
        Me.BPRecalcularTot.Text = "Recalcular Pendents"
        Me.BPRecalcularTot.UseVisualStyleBackColor = True
        '
        'BDataFiltro
        '
        Me.BDataFiltro.Location = New System.Drawing.Point(1021, 440)
        Me.BDataFiltro.Margin = New System.Windows.Forms.Padding(2)
        Me.BDataFiltro.Name = "BDataFiltro"
        Me.BDataFiltro.Size = New System.Drawing.Size(91, 29)
        Me.BDataFiltro.TabIndex = 23
        Me.BDataFiltro.Text = "Data filtro"
        Me.BDataFiltro.UseVisualStyleBackColor = True
        '
        'BPCarregarPdf
        '
        Me.BPCarregarPdf.Location = New System.Drawing.Point(1021, 393)
        Me.BPCarregarPdf.Margin = New System.Windows.Forms.Padding(2)
        Me.BPCarregarPdf.Name = "BPCarregarPdf"
        Me.BPCarregarPdf.Size = New System.Drawing.Size(91, 29)
        Me.BPCarregarPdf.TabIndex = 22
        Me.BPCarregarPdf.Text = "Carregar PDF"
        Me.BPCarregarPdf.UseVisualStyleBackColor = True
        '
        'WBPPdf
        '
        Me.WBPPdf.Location = New System.Drawing.Point(2, 200)
        Me.WBPPdf.Margin = New System.Windows.Forms.Padding(2)
        Me.WBPPdf.MinimumSize = New System.Drawing.Size(16, 17)
        Me.WBPPdf.Name = "WBPPdf"
        Me.WBPPdf.Size = New System.Drawing.Size(697, 457)
        Me.WBPPdf.TabIndex = 21
        '
        'TPTextPdf
        '
        Me.TPTextPdf.Location = New System.Drawing.Point(716, 514)
        Me.TPTextPdf.Margin = New System.Windows.Forms.Padding(2)
        Me.TPTextPdf.Name = "TPTextPdf"
        Me.TPTextPdf.Size = New System.Drawing.Size(396, 138)
        Me.TPTextPdf.TabIndex = 20
        Me.TPTextPdf.Text = ""
        '
        'LPRuta
        '
        Me.LPRuta.AutoSize = True
        Me.LPRuta.Location = New System.Drawing.Point(769, 488)
        Me.LPRuta.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LPRuta.Name = "LPRuta"
        Me.LPRuta.Size = New System.Drawing.Size(13, 15)
        Me.LPRuta.TabIndex = 19
        Me.LPRuta.Text = "//"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(714, 488)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Ruta"
        '
        'BPCalcular
        '
        Me.BPCalcular.Location = New System.Drawing.Point(1021, 291)
        Me.BPCalcular.Margin = New System.Windows.Forms.Padding(2)
        Me.BPCalcular.Name = "BPCalcular"
        Me.BPCalcular.Size = New System.Drawing.Size(91, 29)
        Me.BPCalcular.TabIndex = 17
        Me.BPCalcular.Text = "Calcular"
        Me.BPCalcular.UseVisualStyleBackColor = True
        '
        'BPGuardar
        '
        Me.BPGuardar.Location = New System.Drawing.Point(724, 448)
        Me.BPGuardar.Margin = New System.Windows.Forms.Padding(2)
        Me.BPGuardar.Name = "BPGuardar"
        Me.BPGuardar.Size = New System.Drawing.Size(91, 29)
        Me.BPGuardar.TabIndex = 16
        Me.BPGuardar.Text = "Guardar"
        Me.BPGuardar.UseVisualStyleBackColor = True
        '
        'LPText
        '
        Me.LPText.AutoSize = True
        Me.LPText.Location = New System.Drawing.Point(984, 248)
        Me.LPText.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LPText.Name = "LPText"
        Me.LPText.Size = New System.Drawing.Size(19, 15)
        Me.LPText.TabIndex = 14
        Me.LPText.Text = "[[]]"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(984, 218)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(30, 15)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Text"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(722, 399)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 15)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Data Albaran"
        '
        'TPData
        '
        Me.TPData.Location = New System.Drawing.Point(806, 397)
        Me.TPData.Margin = New System.Windows.Forms.Padding(2)
        Me.TPData.Name = "TPData"
        Me.TPData.Size = New System.Drawing.Size(142, 21)
        Me.TPData.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(722, 363)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 15)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Pagina"
        '
        'TPPagina
        '
        Me.TPPagina.Location = New System.Drawing.Point(806, 360)
        Me.TPPagina.Margin = New System.Windows.Forms.Padding(2)
        Me.TPPagina.Name = "TPPagina"
        Me.TPPagina.Size = New System.Drawing.Size(142, 21)
        Me.TPPagina.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(722, 323)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 15)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Albaran"
        '
        'TPAlbaran
        '
        Me.TPAlbaran.Location = New System.Drawing.Point(806, 321)
        Me.TPAlbaran.Margin = New System.Windows.Forms.Padding(2)
        Me.TPAlbaran.Name = "TPAlbaran"
        Me.TPAlbaran.Size = New System.Drawing.Size(142, 21)
        Me.TPAlbaran.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(722, 286)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Ruta"
        '
        'TPVenedor
        '
        Me.TPVenedor.Location = New System.Drawing.Point(806, 283)
        Me.TPVenedor.Margin = New System.Windows.Forms.Padding(2)
        Me.TPVenedor.Name = "TPVenedor"
        Me.TPVenedor.Size = New System.Drawing.Size(142, 21)
        Me.TPVenedor.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(722, 251)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Operacio"
        '
        'TPOperacio
        '
        Me.TPOperacio.Location = New System.Drawing.Point(806, 248)
        Me.TPOperacio.Margin = New System.Windows.Forms.Padding(2)
        Me.TPOperacio.Name = "TPOperacio"
        Me.TPOperacio.Size = New System.Drawing.Size(142, 21)
        Me.TPOperacio.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(722, 218)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Exercici"
        '
        'TPExercici
        '
        Me.TPExercici.Location = New System.Drawing.Point(806, 215)
        Me.TPExercici.Margin = New System.Windows.Forms.Padding(2)
        Me.TPExercici.Name = "TPExercici"
        Me.TPExercici.Size = New System.Drawing.Size(142, 21)
        Me.TPExercici.TabIndex = 1
        '
        'DGPendents
        '
        Me.DGPendents.AllowUserToAddRows = False
        Me.DGPendents.AllowUserToDeleteRows = False
        Me.DGPendents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGPendents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGPendents.Location = New System.Drawing.Point(0, 0)
        Me.DGPendents.Margin = New System.Windows.Forms.Padding(2)
        Me.DGPendents.MultiSelect = False
        Me.DGPendents.Name = "DGPendents"
        Me.DGPendents.ReadOnly = True
        Me.DGPendents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGPendents.Size = New System.Drawing.Size(1112, 196)
        Me.DGPendents.TabIndex = 0
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.BCGestioTot)
        Me.TabPage4.Controls.Add(Me.BCGestioCarpeta)
        Me.TabPage4.Controls.Add(Me.WBCarpeta)
        Me.TabPage4.Controls.Add(Me.TVCarpetes)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1117, 659)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Carpetes pendents"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'BCGestioTot
        '
        Me.BCGestioTot.Location = New System.Drawing.Point(638, 497)
        Me.BCGestioTot.Margin = New System.Windows.Forms.Padding(2)
        Me.BCGestioTot.Name = "BCGestioTot"
        Me.BCGestioTot.Size = New System.Drawing.Size(128, 40)
        Me.BCGestioTot.TabIndex = 3
        Me.BCGestioTot.Text = "Gestionar tot"
        Me.BCGestioTot.UseVisualStyleBackColor = True
        '
        'BCGestioCarpeta
        '
        Me.BCGestioCarpeta.Location = New System.Drawing.Point(445, 497)
        Me.BCGestioCarpeta.Margin = New System.Windows.Forms.Padding(2)
        Me.BCGestioCarpeta.Name = "BCGestioCarpeta"
        Me.BCGestioCarpeta.Size = New System.Drawing.Size(122, 40)
        Me.BCGestioCarpeta.TabIndex = 2
        Me.BCGestioCarpeta.Text = "Gestionar Carpeta"
        Me.BCGestioCarpeta.UseVisualStyleBackColor = True
        '
        'WBCarpeta
        '
        Me.WBCarpeta.Location = New System.Drawing.Point(296, 2)
        Me.WBCarpeta.Margin = New System.Windows.Forms.Padding(2)
        Me.WBCarpeta.MinimumSize = New System.Drawing.Size(16, 17)
        Me.WBCarpeta.Name = "WBCarpeta"
        Me.WBCarpeta.Size = New System.Drawing.Size(807, 485)
        Me.WBCarpeta.TabIndex = 1
        '
        'TVCarpetes
        '
        Me.TVCarpetes.Location = New System.Drawing.Point(2, 2)
        Me.TVCarpetes.Margin = New System.Windows.Forms.Padding(2)
        Me.TVCarpetes.Name = "TVCarpetes"
        TreeNode1.Name = "Nodo0"
        TreeNode1.Text = "Y:"
        Me.TVCarpetes.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode1})
        Me.TVCarpetes.Size = New System.Drawing.Size(290, 650)
        Me.TVCarpetes.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.LTotalErrors)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.LTotalModificats)
        Me.TabPage1.Controls.Add(Me.LTotalOk)
        Me.TabPage1.Controls.Add(Me.LTotalEscaneix)
        Me.TabPage1.Controls.Add(Me.Label24)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.LTotals)
        Me.TabPage1.Controls.Add(Me.MCTotals)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1117, 659)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Estadistiques"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'LTotalErrors
        '
        Me.LTotalErrors.AutoSize = True
        Me.LTotalErrors.Location = New System.Drawing.Point(145, 367)
        Me.LTotalErrors.Name = "LTotalErrors"
        Me.LTotalErrors.Size = New System.Drawing.Size(14, 15)
        Me.LTotalErrors.TabIndex = 35
        Me.LTotalErrors.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(29, 367)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(43, 15)
        Me.Label26.TabIndex = 34
        Me.Label26.Text = "Errors:"
        '
        'LTotalModificats
        '
        Me.LTotalModificats.AutoSize = True
        Me.LTotalModificats.Location = New System.Drawing.Point(145, 327)
        Me.LTotalModificats.Name = "LTotalModificats"
        Me.LTotalModificats.Size = New System.Drawing.Size(14, 15)
        Me.LTotalModificats.TabIndex = 33
        Me.LTotalModificats.Text = "0"
        '
        'LTotalOk
        '
        Me.LTotalOk.AutoSize = True
        Me.LTotalOk.Location = New System.Drawing.Point(145, 288)
        Me.LTotalOk.Name = "LTotalOk"
        Me.LTotalOk.Size = New System.Drawing.Size(14, 15)
        Me.LTotalOk.TabIndex = 32
        Me.LTotalOk.Text = "0"
        '
        'LTotalEscaneix
        '
        Me.LTotalEscaneix.AutoSize = True
        Me.LTotalEscaneix.Location = New System.Drawing.Point(145, 245)
        Me.LTotalEscaneix.Name = "LTotalEscaneix"
        Me.LTotalEscaneix.Size = New System.Drawing.Size(14, 15)
        Me.LTotalEscaneix.TabIndex = 31
        Me.LTotalEscaneix.Text = "0"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(29, 327)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(66, 15)
        Me.Label24.TabIndex = 30
        Me.Label24.Text = "Modificats:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(29, 288)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(69, 15)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "OK Directe:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(29, 244)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(99, 15)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "Total escanejats:"
        '
        'LTotals
        '
        Me.LTotals.AutoSize = True
        Me.LTotals.Location = New System.Drawing.Point(249, 42)
        Me.LTotals.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LTotals.Name = "LTotals"
        Me.LTotals.Size = New System.Drawing.Size(43, 15)
        Me.LTotals.TabIndex = 27
        Me.LTotals.Text = "Totals:"
        '
        'MCTotals
        '
        Me.MCTotals.Location = New System.Drawing.Point(12, 36)
        Me.MCTotals.MinDate = New Date(2017, 8, 8, 0, 0, 0, 0)
        Me.MCTotals.Name = "MCTotals"
        Me.MCTotals.TabIndex = 17
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(13, 12)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(86, 15)
        Me.Label21.TabIndex = 16
        Me.Label21.Text = "Data Escaneix"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(5, 40)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 15)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Num Albaran"
        '
        'TBBAlbaran
        '
        Me.TBBAlbaran.Location = New System.Drawing.Point(8, 62)
        Me.TBBAlbaran.Margin = New System.Windows.Forms.Padding(2)
        Me.TBBAlbaran.Name = "TBBAlbaran"
        Me.TBBAlbaran.Size = New System.Drawing.Size(192, 21)
        Me.TBBAlbaran.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 365)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 15)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Gestionats?"
        '
        'CBBGestionats
        '
        Me.CBBGestionats.FormattingEnabled = True
        Me.CBBGestionats.Items.AddRange(New Object() {"Nomes els pendents de revisar", "Nomes els correctes", "Tots els albarans"})
        Me.CBBGestionats.Location = New System.Drawing.Point(11, 390)
        Me.CBBGestionats.Margin = New System.Windows.Forms.Padding(2)
        Me.CBBGestionats.Name = "CBBGestionats"
        Me.CBBGestionats.Size = New System.Drawing.Size(189, 23)
        Me.CBBGestionats.TabIndex = 4
        '
        'CBBRuta
        '
        Me.CBBRuta.FormattingEnabled = True
        Me.CBBRuta.Items.AddRange(New Object() {"Nomes els correctes", "Nomes els pendents de revisar", "Tots els albarans"})
        Me.CBBRuta.Location = New System.Drawing.Point(11, 326)
        Me.CBBRuta.Margin = New System.Windows.Forms.Padding(2)
        Me.CBBRuta.Name = "CBBRuta"
        Me.CBBRuta.Size = New System.Drawing.Size(189, 23)
        Me.CBBRuta.TabIndex = 10
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 301)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 15)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Ruta"
        '
        'TLoadAll
        '
        Me.TLoadAll.Interval = 3600000
        '
        'TDelete
        '
        Me.TDelete.Interval = 500
        '
        'TSetFocus
        '
        Me.TSetFocus.Interval = 500
        '
        'PBCarrega
        '
        Me.PBCarrega.Location = New System.Drawing.Point(5, 654)
        Me.PBCarrega.Margin = New System.Windows.Forms.Padding(2)
        Me.PBCarrega.Name = "PBCarrega"
        Me.PBCarrega.Size = New System.Drawing.Size(208, 19)
        Me.PBCarrega.Step = 1
        Me.PBCarrega.TabIndex = 11
        Me.PBCarrega.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(9, 100)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 15)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Data Escaneix"
        '
        'LOperacio
        '
        Me.LOperacio.AutoSize = True
        Me.LOperacio.Location = New System.Drawing.Point(5, 634)
        Me.LOperacio.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LOperacio.Name = "LOperacio"
        Me.LOperacio.Size = New System.Drawing.Size(11, 15)
        Me.LOperacio.TabIndex = 14
        Me.LOperacio.Text = "-"
        Me.LOperacio.Visible = False
        '
        'PBasic
        '
        Me.PBasic.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PBasic.Controls.Add(Me.Panel1)
        Me.PBasic.Controls.Add(Me.WBPBasic)
        Me.PBasic.Controls.Add(Me.DGPEndentsBasic)
        Me.PBasic.Location = New System.Drawing.Point(220, 7)
        Me.PBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.PBasic.Name = "PBasic"
        Me.PBasic.Size = New System.Drawing.Size(1125, 696)
        Me.PBasic.TabIndex = 15
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.BGuardarBasic)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.TPDataBasic)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.TPPaginaBasic)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.TPAlbaranBasic)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.TPVenedorBasic)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.TPOperacioBasic)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.TPExerciciBasic)
        Me.Panel1.Location = New System.Drawing.Point(829, 251)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(291, 425)
        Me.Panel1.TabIndex = 37
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label20.Location = New System.Drawing.Point(68, 14)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(161, 18)
        Me.Label20.TabIndex = 49
        Me.Label20.Text = "MODIFICAR ALBARAN"
        '
        'BGuardarBasic
        '
        Me.BGuardarBasic.Location = New System.Drawing.Point(103, 356)
        Me.BGuardarBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.BGuardarBasic.Name = "BGuardarBasic"
        Me.BGuardarBasic.Size = New System.Drawing.Size(91, 29)
        Me.BGuardarBasic.TabIndex = 48
        Me.BGuardarBasic.Text = "Guardar"
        Me.BGuardarBasic.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(23, 244)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 15)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "Data Albaran"
        '
        'TPDataBasic
        '
        Me.TPDataBasic.Location = New System.Drawing.Point(107, 242)
        Me.TPDataBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPDataBasic.Name = "TPDataBasic"
        Me.TPDataBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPDataBasic.TabIndex = 46
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(23, 208)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(46, 15)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Pagina"
        '
        'TPPaginaBasic
        '
        Me.TPPaginaBasic.Location = New System.Drawing.Point(107, 205)
        Me.TPPaginaBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPPaginaBasic.Name = "TPPaginaBasic"
        Me.TPPaginaBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPPaginaBasic.TabIndex = 44
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(23, 168)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(49, 15)
        Me.Label15.TabIndex = 43
        Me.Label15.Text = "Albaran"
        '
        'TPAlbaranBasic
        '
        Me.TPAlbaranBasic.Location = New System.Drawing.Point(107, 166)
        Me.TPAlbaranBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPAlbaranBasic.Name = "TPAlbaranBasic"
        Me.TPAlbaranBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPAlbaranBasic.TabIndex = 42
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(23, 131)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(33, 15)
        Me.Label16.TabIndex = 41
        Me.Label16.Text = "Ruta"
        '
        'TPVenedorBasic
        '
        Me.TPVenedorBasic.Location = New System.Drawing.Point(107, 128)
        Me.TPVenedorBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPVenedorBasic.Name = "TPVenedorBasic"
        Me.TPVenedorBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPVenedorBasic.TabIndex = 40
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(23, 96)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 15)
        Me.Label17.TabIndex = 39
        Me.Label17.Text = "Operacio"
        '
        'TPOperacioBasic
        '
        Me.TPOperacioBasic.Location = New System.Drawing.Point(107, 93)
        Me.TPOperacioBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPOperacioBasic.Name = "TPOperacioBasic"
        Me.TPOperacioBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPOperacioBasic.TabIndex = 38
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(23, 63)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 15)
        Me.Label18.TabIndex = 37
        Me.Label18.Text = "Exercici"
        '
        'TPExerciciBasic
        '
        Me.TPExerciciBasic.Location = New System.Drawing.Point(107, 60)
        Me.TPExerciciBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.TPExerciciBasic.Name = "TPExerciciBasic"
        Me.TPExerciciBasic.Size = New System.Drawing.Size(142, 21)
        Me.TPExerciciBasic.TabIndex = 36
        '
        'WBPBasic
        '
        Me.WBPBasic.Location = New System.Drawing.Point(6, 250)
        Me.WBPBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.WBPBasic.MinimumSize = New System.Drawing.Size(16, 17)
        Me.WBPBasic.Name = "WBPBasic"
        Me.WBPBasic.Size = New System.Drawing.Size(818, 426)
        Me.WBPBasic.TabIndex = 36
        '
        'DGPEndentsBasic
        '
        Me.DGPEndentsBasic.AllowUserToAddRows = False
        Me.DGPEndentsBasic.AllowUserToDeleteRows = False
        Me.DGPEndentsBasic.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGPEndentsBasic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGPEndentsBasic.Location = New System.Drawing.Point(5, 6)
        Me.DGPEndentsBasic.Margin = New System.Windows.Forms.Padding(2)
        Me.DGPEndentsBasic.MultiSelect = False
        Me.DGPEndentsBasic.Name = "DGPEndentsBasic"
        Me.DGPEndentsBasic.ReadOnly = True
        Me.DGPEndentsBasic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGPEndentsBasic.Size = New System.Drawing.Size(1116, 240)
        Me.DGPEndentsBasic.TabIndex = 22
        '
        'Pbuscar
        '
        Me.Pbuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbuscar.Controls.Add(Me.MCBData)
        Me.Pbuscar.Controls.Add(Me.Label19)
        Me.Pbuscar.Controls.Add(Me.Label8)
        Me.Pbuscar.Controls.Add(Me.TBBAlbaran)
        Me.Pbuscar.Controls.Add(Me.Label14)
        Me.Pbuscar.Controls.Add(Me.Label10)
        Me.Pbuscar.Controls.Add(Me.CBBGestionats)
        Me.Pbuscar.Controls.Add(Me.Label13)
        Me.Pbuscar.Controls.Add(Me.CBBRuta)
        Me.Pbuscar.Location = New System.Drawing.Point(5, 5)
        Me.Pbuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.Pbuscar.Name = "Pbuscar"
        Me.Pbuscar.Size = New System.Drawing.Size(208, 431)
        Me.Pbuscar.TabIndex = 16
        '
        'MCBData
        '
        Me.MCBData.Location = New System.Drawing.Point(8, 124)
        Me.MCBData.Name = "MCBData"
        Me.MCBData.TabIndex = 15
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label19.Location = New System.Drawing.Point(21, 7)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(151, 18)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "BUSCAR ALBARANS"
        '
        'PBusquedaAvan
        '
        Me.PBusquedaAvan.Controls.Add(Me.TBAGestionat)
        Me.PBusquedaAvan.Controls.Add(Me.Label28)
        Me.PBusquedaAvan.Controls.Add(Me.TBAText)
        Me.PBusquedaAvan.Controls.Add(Me.Label27)
        Me.PBusquedaAvan.Controls.Add(Me.TBAData)
        Me.PBusquedaAvan.Controls.Add(Me.Label25)
        Me.PBusquedaAvan.Location = New System.Drawing.Point(5, 441)
        Me.PBusquedaAvan.Name = "PBusquedaAvan"
        Me.PBusquedaAvan.Size = New System.Drawing.Size(208, 179)
        Me.PBusquedaAvan.TabIndex = 17
        Me.PBusquedaAvan.Visible = False
        '
        'TBAGestionat
        '
        Me.TBAGestionat.Location = New System.Drawing.Point(7, 143)
        Me.TBAGestionat.Name = "TBAGestionat"
        Me.TBAGestionat.Size = New System.Drawing.Size(194, 21)
        Me.TBAGestionat.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(10, 123)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(57, 15)
        Me.Label28.TabIndex = 4
        Me.Label28.Text = "gestionat"
        '
        'TBAText
        '
        Me.TBAText.Location = New System.Drawing.Point(7, 89)
        Me.TBAText.Name = "TBAText"
        Me.TBAText.Size = New System.Drawing.Size(194, 21)
        Me.TBAText.TabIndex = 3
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(10, 69)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(26, 15)
        Me.Label27.TabIndex = 2
        Me.Label27.Text = "text"
        '
        'TBAData
        '
        Me.TBAData.Location = New System.Drawing.Point(7, 31)
        Me.TBAData.Name = "TBAData"
        Me.TBAData.Size = New System.Drawing.Size(194, 21)
        Me.TBAData.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(10, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(76, 15)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "data albaran"
        '
        'LCText
        '
        Me.LCText.AutoSize = True
        Me.LCText.Location = New System.Drawing.Point(984, 271)
        Me.LCText.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LCText.Name = "LCText"
        Me.LCText.Size = New System.Drawing.Size(19, 15)
        Me.LCText.TabIndex = 26
        Me.LCText.Text = "[[]]"
        '
        'FAlbarans
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1356, 711)
        Me.Controls.Add(Me.PBusquedaAvan)
        Me.Controls.Add(Me.Pbuscar)
        Me.Controls.Add(Me.LOperacio)
        Me.Controls.Add(Me.PBCarrega)
        Me.Controls.Add(Me.TC1)
        Me.Controls.Add(Me.PBasic)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FAlbarans"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Albarans"
        Me.TC1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DGPendents, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.PBasic.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGPEndentsBasic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbuscar.ResumeLayout(False)
        Me.Pbuscar.PerformLayout()
        Me.PBusquedaAvan.ResumeLayout(False)
        Me.PBusquedaAvan.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TC1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents DGPendents As System.Windows.Forms.DataGridView
    Friend WithEvents TVCarpetes As System.Windows.Forms.TreeView
    Friend WithEvents WBCarpeta As System.Windows.Forms.WebBrowser
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TPPagina As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TPAlbaran As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TPVenedor As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TPOperacio As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TPExercici As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TPData As System.Windows.Forms.TextBox
    Friend WithEvents LPText As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents BPCalcular As System.Windows.Forms.Button
    Friend WithEvents BPGuardar As System.Windows.Forms.Button
    Friend WithEvents LPRuta As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TBBAlbaran As System.Windows.Forms.TextBox
    Friend WithEvents BCGestioCarpeta As System.Windows.Forms.Button
    Friend WithEvents TPTextPdf As System.Windows.Forms.RichTextBox
    Friend WithEvents WBPPdf As System.Windows.Forms.WebBrowser
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CBBGestionats As System.Windows.Forms.ComboBox
    Friend WithEvents BCGestioTot As System.Windows.Forms.Button
    Friend WithEvents BPCarregarPdf As System.Windows.Forms.Button
    Friend WithEvents CBBRuta As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents BDataFiltro As System.Windows.Forms.Button
    Friend WithEvents TLoadAll As System.Windows.Forms.Timer
    Friend WithEvents TDelete As System.Windows.Forms.Timer
    Friend WithEvents TSetFocus As System.Windows.Forms.Timer
    Friend WithEvents PBCarrega As System.Windows.Forms.ProgressBar
    Friend WithEvents BPRecalcularTot As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents LOperacio As System.Windows.Forms.Label
    Friend WithEvents BReferRutes As System.Windows.Forms.Button
    Friend WithEvents PBasic As System.Windows.Forms.Panel
    Friend WithEvents WBPBasic As System.Windows.Forms.WebBrowser
    Friend WithEvents DGPEndentsBasic As System.Windows.Forms.DataGridView
    Friend WithEvents Pbuscar As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents MCBData As System.Windows.Forms.MonthCalendar
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents BGuardarBasic As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TPDataBasic As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TPPaginaBasic As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TPAlbaranBasic As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TPVenedorBasic As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TPOperacioBasic As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TPExerciciBasic As System.Windows.Forms.TextBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents LTotalErrors As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents LTotalModificats As System.Windows.Forms.Label
    Friend WithEvents LTotalOk As System.Windows.Forms.Label
    Friend WithEvents LTotalEscaneix As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents LTotals As System.Windows.Forms.Label
    Friend WithEvents MCTotals As System.Windows.Forms.MonthCalendar
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents PBusquedaAvan As System.Windows.Forms.Panel
    Friend WithEvents TBAGestionat As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TBAText As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TBAData As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents LCText As System.Windows.Forms.Label

End Class
