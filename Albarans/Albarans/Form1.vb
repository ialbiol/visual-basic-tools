﻿Imports System.IO
Imports System.Text

''' <summary>Formulari principal del programa</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>controller <c>Controller</c></term><description>Objecte en tota la logica de funcionament del programa</description>
'''   </item>
'''   <item>
'''      <term>IDAlbaran <c>String = False</c></term><description>Guarda el ID del albaran que esta modificant</description>
'''   </item>
'''   <item>
'''      <term>allok <c>Boolean = False</c></term><description>Variable que controla si tot esta ok per a poder carregar dades o esta pendent d'algun camp</description>
'''   </item>
'''   <item>
'''      <term>modeBasic <c>Boolean = True</c></term><description>Controla si l'aplicacio esta en mode Basic o Avançat</description>
'''   </item>
'''   <item>
'''      <term>rutaBasic <c>String = ""</c></term><description>Variable que guarda la ruta de l'albaran en mode Basic</description>
'''   </item>
'''   <item>
'''      <term>textPeuBasic <c>String = ""</c></term><description>Variable que guarda el text del peu de l'albaran en mode Basic</description>
'''   </item>
'''   <item>
'''      <term>editRow <c>Integer</c></term><description>Index de la fila del DataGrid que s'esta editant</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FAlbarans

    Dim controller As New ControllerClass
    Dim IDAlbaran As String = ""
    Dim allok As Boolean = False
    Dim modeBasic As Boolean = True
    Dim rutaBasic As String = ""
    Dim textPeuBasic As String = ""
    Dim editRow As Integer


#Region "events formulari"

    ''' <summary>Event formulari Close</summary>
    ''' <remarks>Gestiona les ultimes accions que ha de fer el controller
    ''' </remarks>
    Private Sub FAlbarans_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        controller.Dispose()
    End Sub

    ''' <summary>Event formulari Load</summary>
    ''' <remarks>Gestiona les primeres accions que ha de fer el controller
    ''' </remarks>
    Private Sub FAlbarans_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'test inicial
        If controller.TestInici() Then
            'gestiono albarans
            'controller.processAlbarans()

            'poso valors als filtros
            posarValors()

            If modeBasic = False Then
                'mostro llistat de carpetes pendents
                mostrarCarpetesPendents()
            End If

            allok = True

            'mostro llistat albarans
            mostrarPendents()

            TLoadAll.Enabled = True
        End If
    End Sub

#End Region


#Region "events segona pestaña del mode avançat"

    ''' <summary>Event After Select del TreeView</summary>
    ''' <remarks>Mostra el llistat de carpetes i fitxers pendents de gestionar. Mostra un albaran escanejat al clicar-lo
    ''' </remarks>
    Private Sub TVCarpetes_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TVCarpetes.AfterSelect
        Dim ruta As String

        If TVCarpetes.SelectedNode.Level = 2 Then
            ruta = TVCarpetes.SelectedNode.FullPath

            WBCarpeta.Navigate(ruta)
        End If
    End Sub

    ''' <summary>Event boto gestionar carpeta click</summary>
    ''' <remarks>Importa i gestiona els pdf de dins una sola carpeta. S'utilitza per a debugar l'aplicacio
    ''' </remarks>
    Private Sub BCGestioCarpeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCGestioCarpeta.Click
        ' gestiono la carpeta seleccionada al tree view
        If TVCarpetes.SelectedNode.Level = 1 Then
            controller.Log.Add("Gestiono la carpeta : " & TVCarpetes.SelectedNode.FullPath, "INFO")
            PBCarrega.Visible = True
            LOperacio.Visible = True
            LOperacio.Text = "Gestiono la carpeta"
            controller.processAlbarans(TVCarpetes.SelectedNode.FullPath, PBCarrega)
            mostrarCarpetesPendents()
            PBCarrega.Visible = False
            LOperacio.Visible = False
        Else
            controller.Log.Add("Intento gestionar una carpeta, pero no n'he seleccionat cap", "ERROR")
            MessageBox.Show("Selecciona la carpeta que vols gestionar", "Error", MessageBoxButtons.OK)
        End If

    End Sub

    ''' <summary>Event boto gestionar tot click</summary>
    ''' <remarks>Importa i gestiona els pdf de totes les carpetes. S'utilitza per a no esperar al timer
    ''' </remarks>
    Private Sub BCGestioTot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCGestioTot.Click
        controller.Log.Add("Demano gestionar totes les carpetes", "INFO")
        PBCarrega.Visible = True
        LOperacio.Visible = True
        LOperacio.Text = "gestiono totes les carpetes"
        controller.processAlbarans(PBCarrega)
        mostrarCarpetesPendents()
        PBCarrega.Visible = False
        LOperacio.Visible = False
    End Sub

#End Region

#Region "events mode avançat"

    ''' <summary>Event DataGrid Click</summary>
    ''' <remarks>Mostra la informacio de l'albaran clicat 
    ''' </remarks>
    Private Sub DGPendents_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGPendents.CellClick
        mostrarInfoAlbaran(e.RowIndex)
    End Sub


    ''' <summary>Event Exercici keypress</summary>
    ''' <remarks>Quan es modifiquen les dades de l'albaran, es recalcula el text del peu
    ''' </remarks>
    Private Sub TPExercici_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TPExercici.KeyUp
        calcularTextPeu()
    End Sub

    ''' <summary>Event Operacio keypress</summary>
    ''' <remarks>Quan es modifiquen les dades de l'albaran, es recalcula el text del peu 
    ''' </remarks>
    Private Sub TPOperacio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TPOperacio.KeyUp
        calcularTextPeu()
    End Sub

    ''' <summary>Event venedor keypress </summary>
    ''' <remarks>Quan es modifiquen les dades de l'albaran, es recalcula el text del peu 
    ''' </remarks>
    Private Sub TPVenedor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TPVenedor.KeyUp
        calcularTextPeu()
    End Sub

    ''' <summary>Event Num albaran keypress </summary>
    ''' <remarks>Quan es modifiquen les dades de l'albaran, es recalcula el text del peu 
    ''' </remarks>
    Private Sub TPAlbaran_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TPAlbaran.KeyUp
        calcularTextPeu()
    End Sub

    ''' <summary>Event pagina keypress</summary>
    ''' <remarks>Quan es modifiquen les dades de l'albaran, es recalcula el text del peu 
    ''' </remarks>
    Private Sub TPPagina_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TPPagina.KeyUp
        calcularTextPeu()
    End Sub


    ''' <summary>Event boto calcular click</summary>
    ''' <remarks>Intenta recalcular els diferents campsa partir del text escanejat.
    ''' </remarks>
    Private Sub BPCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BPCalcular.Click
        LPText.Text = controller.tincTextPeu(TPTextPdf.Text)
        LCText.Text = controller.tincTextPeu(TPTextPdf.Text, "<<", ">>")
        If LPText.Text <> "" Or LCText.Text <> "" Then
            ' gestio del text del peu
            TPExercici.Text = controller.buscarExerciciTextPeu(LPText.Text, TPTextPdf.Text, LCText.Text)
            TPOperacio.Text = controller.buscarOperacioTextPeu(LPText.Text, TPTextPdf.Text, LCText.Text)
            TPVenedor.Text = controller.buscarVenedorTextPeu(LPText.Text, TPTextPdf.Text, LCText.Text)
            TPAlbaran.Text = controller.buscarNumAlbaranTextPeu(LPText.Text, TPTextPdf.Text, LCText.Text)

        Else
            'gestio sense text del peu
            TPExercici.Text = controller.buscarExercici(TPTextPdf.Text)
            TPOperacio.Text = controller.buscarOperacio(TPTextPdf.Text)
            TPVenedor.Text = controller.buscarVenedor(TPTextPdf.Text)
            TPAlbaran.Text = controller.buscarNumAlbaran(TPTextPdf.Text)
        End If

        TPPagina.Text = controller.buscarPagina(TPTextPdf.Text)
        TPData.Text = controller.buscarDataAlbaran(TPTextPdf.Text, Now)
        calcularTextPeu()

        TPExercici.Focus()
        TPExercici.SelectAll()
    End Sub

    ''' <summary>Event boto guardar click </summary>
    ''' <remarks>Activa el timer que espera que es limpie el camp del navegador que mostra el pdf, per a poder guardar les modificacions de l'albaran i moure el fitxer on toca
    ''' </remarks>
    Private Sub BPGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BPGuardar.Click
        WBPPdf.Navigate(controller.rutaClear)
        TDelete.Enabled = True
    End Sub


    ''' <summary>Event boto carregar pdf</summary>
    ''' <remarks>Torna a carregar el fitxer pdf a la base de dades. s'utilitza per a debugar o corregir algun error
    ''' </remarks>
    Private Sub BPCarregarPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BPCarregarPdf.Click
        controller.carregarPdf(LPRuta.Text, IDAlbaran)
    End Sub

    ''' <summary>Event boto data filtro click</summary>
    ''' <remarks>si s'han de fer modificacions a totes les dades dela base de dades, utilitzo este boto per a fer-ho
    ''' </remarks>
    Private Sub BDataFiltro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BDataFiltro.Click
        controller.ferDadesFiltro()
    End Sub

    ''' <summary>Event boto recalcular tot</summary>
    ''' <remarks>quan es millora l'algoritme de busqueda de dades, s'utilitza este boto per a que es recalculen tots els albarans pendents de gestionar
    ''' </remarks>
    Private Sub BPRecalcularTot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BPRecalcularTot.Click
        PBCarrega.Visible = True
        LOperacio.Visible = True
        LOperacio.Text = "recalculo els no gestionats"
        controller.recalcularPendents(PBCarrega)
        PBCarrega.Visible = False
        LOperacio.Visible = False
    End Sub

    ''' <summary>Event boto recalcular les rutes</summary>
    ''' <remarks>quan es modifica la estructura dels noms dels fitxers dels albarans, es crida este boto per a que es recalculen totes
    ''' </remarks>
    Private Sub BReferRutes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BReferRutes.Click
        PBCarrega.Visible = True
        LOperacio.Visible = True
        LOperacio.Text = "recalculo les rutes"
        controller.recalcularRutaGestionats(PBCarrega)
        PBCarrega.Visible = False
        LOperacio.Visible = False
    End Sub

#End Region

#Region "events tercera pestanya mode avançat"

    ''' <summary>Event date calendar data changed </summary>
    ''' <remarks>Mostra les estadstiques de la data seleccionada
    ''' </remarks>
    Private Sub MCTotals_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MCTotals.DateChanged
        mostrarEstadistiques()
    End Sub

#End Region

#Region "filtros de busqueda"

    ''' <summary>Event Num albaran keypress </summary>
    ''' <remarks>Torna a carregar les dades buscant nomes per numero d'albaran, sense tenir en compte les dates, al pulsar enter.
    ''' <para>Si s'intrudueix la paraula "ivan" es pasa del mode basic al avançat</para> 
    ''' </remarks>
    Private Sub TBBAlbaran_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBBAlbaran.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If TBBAlbaran.Text = "ivan" Then
                canviMode()
                TBBAlbaran.Text = ""
            Else
                mostrarPendents()
            End If
        End If
    End Sub

    ''' <summary>Event data escaneix changed</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'escaneix 
    ''' </remarks>
    Private Sub MCBData_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MCBData.DateChanged
        mostrarPendents()
    End Sub

    ''' <summary>Event ruta changed</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova ruta
    ''' </remarks>
    Private Sub CBBRuta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBBRuta.SelectedIndexChanged
        mostrarPendents()
    End Sub

    ''' <summary>Event gestionats changed</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova seleccio
    ''' <para>si es selecciona "nomes pendents", l'aplicacio entra en mode edicio continua d'albarans, per a facilitar la seua modificacio</para>
    ''' </remarks>
    Private Sub CBBGestionats_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBBGestionats.SelectedIndexChanged
        mostrarPendents()
    End Sub

#End Region


#Region "filtros de busqueda avançada"

    ''' <summary>Event data key press</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'escaneix en busqueda avançada 
    ''' </remarks>
    Private Sub TBAData_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBAData.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            mostrarBusquedaAvant()
        End If
    End Sub

    ''' <summary>Event text key press</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'escaneix en busqueda avançada 
    ''' </remarks>
    Private Sub TBAText_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBAText.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            mostrarBusquedaAvant()
        End If
    End Sub

    ''' <summary>Event gestionat key press</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'escaneix en busqueda avançada 
    ''' </remarks>
    Private Sub TBAGestionat_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBAGestionat.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            mostrarBusquedaAvant()
        End If
    End Sub

#End Region

#Region "events mode basic"

    ''' <summary>Event DataGrid Click </summary>
    ''' <remarks>Mostra la informacio de l'albaran clicat 
    ''' </remarks>
    Private Sub DGPEndentsBasic_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGPEndentsBasic.CellClick
        mostrarInfoAlbaran(e.RowIndex)
    End Sub

    ''' <summary>Event boto guardar</summary>
    ''' <remarks>Activa el timer que espera que es limpie el camp del navegador que mostra el pdf, per a poder guardar les modificacions de l'albaran i moure el fitxer on toca 
    ''' </remarks>
    Private Sub BGuardarBasic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGuardarBasic.Click
        WBPBasic.Navigate(controller.rutaClear)
        TDelete.Enabled = True
    End Sub

#End Region

#Region "funcions propies"

    ''' <summary>Funcio que calcula si una fila del DataGrid es correcta o no </summary>
    ''' <remarks>Gestiona el canvi de colors de les files
    ''' </remarks>
    ''' <param name="row"><c>Integer</c>: Fila a comprobar</param>
    ''' <returns><c>Boolean</c>: retona si la linia es correcta o no</returns>
    Private Function isRowOk(ByVal row As Integer) As Boolean
        Dim i As Integer
        Dim res As Boolean = True

        For i = 1 To DGPEndentsBasic.ColumnCount - 2
            If DGPEndentsBasic.Item(i, row).Value.ToString = "" Then
                res = False
            End If
        Next

        Return res

    End Function

    ''' <summary>Funcio que mostra les dades dels albarans segons els filtros </summary>
    ''' <remarks>Omple el dataGrid en les dades de la base de dades 
    ''' </remarks>
    Private Sub mostrarPendents()
        Dim ds As DataSet
        Dim i As Integer

        If allok Then
            If modeBasic = False Then
                ds = controller.llistatAlbaransPendents(MCBData.SelectionStart, MCBData.SelectionStart, TBBAlbaran.Text, CBBRuta.SelectedItem.ToString(), CBBGestionats.SelectedIndex.ToString())
                DGPendents.DataSource = ds
                DGPendents.DataMember = "Tabla"

            Else
                ds = controller.llistatAlbaransPendentsBasic(MCBData.SelectionStart, MCBData.SelectionStart, TBBAlbaran.Text, CBBRuta.SelectedItem.ToString(), CBBGestionats.SelectedIndex.ToString())
                DGPEndentsBasic.DataSource = ds
                DGPEndentsBasic.DataMember = "Tabla"

                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To DGPEndentsBasic.Rows.Count - 1
                        If isRowOk(i) = False Then
                            DGPEndentsBasic.Rows(i).DefaultCellStyle.BackColor = Color.Salmon
                            'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                        End If
                    Next
                End If
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                If CBBGestionats.SelectedIndex = 0 Then
                    mostrarInfoAlbaran(0)
                Else
                    limpiarInfoAlbaran()
                End If
            Else
                limpiarInfoAlbaran()
            End If
        End If
    End Sub

    ''' <summary>Funcio que omple les dades del TreeView </summary>
    ''' <remarks>Busca les carpetes i els documents pendents de gestionar i els mostra 
    ''' </remarks>
    Private Sub mostrarCarpetesPendents()
        Dim cont As Integer = 0

        TVCarpetes.Nodes(0).Nodes.Clear()

        controller.buscarCarpetes()
        For Each V As String In controller.llistatCarpetes
            TVCarpetes.Nodes(0).Nodes.Add(New TreeNode(V))

            controller.buscarDocs(controller.rutaAlbarans & V)
            For Each F As FileInfo In controller.llistatDocs
                TVCarpetes.Nodes(0).Nodes(cont).Nodes.Add(New TreeNode(F.Name))
            Next

            cont = cont + 1
        Next
    End Sub

    ''' <summary>Funcio que mostra les dades dels albarans segons els filtros de la busqueda avançada </summary>
    ''' <remarks>Omple el dataGrid en les dades de la base de dades 
    ''' </remarks>
    Private Sub mostrarBusquedaAvant()
        Dim ds As DataSet

        If allok Then
            ds = controller.llistatAlbaransBA(TBAData.Text, TBAText.Text, TBAGestionat.Text)
            DGPendents.DataSource = ds
            DGPendents.DataMember = "Tabla"

            limpiarInfoAlbaran()
        End If
    End Sub

    ''' <summary>Funcio que omple els desplegables</summary>
    ''' <remarks> 
    ''' </remarks>
    Private Sub posarValors()
        Dim dsVenedor As DataSet
        Dim row As Integer

        'omplo el desplegable dels venedors / rutes
        dsVenedor = controller.llistatRutes()

        CBBRuta.Items.Clear()
        CBBRuta.Items.Add("Tots")
        If dsVenedor.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsVenedor.Tables(0).Rows.Count
                CBBRuta.Items.Add(dsVenedor.Tables(0).Rows(row)(0))
                row = row + 1
            End While
        End If
        CBBRuta.SelectedIndex = 0

        CBBGestionats.SelectedIndex = 2

        'DTBInici.Value = Now.AddDays(-1)
        'DTBInici.MaxDate = Now.AddDays(-1)

        'DTBFinal.Value = Now.AddDays(-1)
        'DTBFinal.MaxDate = Now.AddDays(-1)

    End Sub

    ''' <summary>Funcio que omple els camps editables a partir dels DataGrid</summary>
    ''' <remarks>Mostra la informacio de l'albaran seleccionat al dataGrid per a poder editar-lo o simplement vore'l 
    ''' </remarks>
    ''' <param name="row"><c>Integer</c>: Fila del dataGrid a mostrar</param>
    Private Sub mostrarInfoAlbaran(ByVal row As Integer)

        editRow = row

        If modeBasic = False Then
            IDAlbaran = DGPendents.Item(0, row).Value.ToString

            TPExercici.Text = DGPendents.Item(1, row).Value.ToString
            TPOperacio.Text = DGPendents.Item(2, row).Value.ToString
            TPVenedor.Text = DGPendents.Item(3, row).Value.ToString
            TPAlbaran.Text = DGPendents.Item(4, row).Value.ToString
            TPPagina.Text = DGPendents.Item(5, row).Value.ToString
            TPData.Text = DGPendents.Item(6, row).Value.ToString
            LPText.Text = DGPendents.Item(7, row).Value.ToString
            LPRuta.Text = DGPendents.Item(8, row).Value.ToString
            TPTextPdf.Text = DGPendents.Item(9, row).Value.ToString

            If Not File.Exists(LPRuta.Text) Then
                controller.guardarPdf(IDAlbaran)
            End If

            WBPPdf.Navigate(LPRuta.Text)

            TPVenedor.Focus()
            TPVenedor.SelectAll()
            TSetFocus.Enabled = True
        Else
            IDAlbaran = DGPEndentsBasic.Item(0, row).Value.ToString

            TPExerciciBasic.Text = DGPEndentsBasic.Item(1, row).Value.ToString
            TPOperacioBasic.Text = DGPEndentsBasic.Item(2, row).Value.ToString
            TPVenedorBasic.Text = DGPEndentsBasic.Item(3, row).Value.ToString
            TPAlbaranBasic.Text = DGPEndentsBasic.Item(4, row).Value.ToString
            TPPaginaBasic.Text = DGPEndentsBasic.Item(5, row).Value.ToString
            TPDataBasic.Text = DGPEndentsBasic.Item(6, row).Value.ToString

            rutaBasic = controller.guardarPdf(IDAlbaran)

            WBPBasic.Navigate(rutaBasic)

            TPVenedorBasic.Focus()
            TPVenedorBasic.SelectAll()
            TSetFocus.Enabled = True
        End If
        controller.Log.Add("Mostro albaran id: " & IDAlbaran, "INFO")
    End Sub

    ''' <summary>Funcio que limpia els camps editables</summary>
    ''' <remarks>
    ''' </remarks>
    Private Sub limpiarInfoAlbaran()
        IDAlbaran = 0

        If modeBasic = False Then
            TPExercici.Text = ""
            TPOperacio.Text = ""
            TPVenedor.Text = ""
            TPAlbaran.Text = ""
            TPPagina.Text = ""
            TPData.Text = ""
            LPText.Text = "-"
            LPRuta.Text = "-"
            TPTextPdf.Text = ""
            WBPPdf.Navigate(controller.rutaClear)
        Else
            TPExerciciBasic.Text = ""
            TPOperacioBasic.Text = ""
            TPVenedorBasic.Text = ""
            TPAlbaranBasic.Text = ""
            TPPaginaBasic.Text = ""
            TPDataBasic.Text = ""
            rutaBasic = ""
            WBPBasic.Navigate(controller.rutaClear)
        End If
    End Sub

    ''' <summary>Funcio que calcula el texte del peu de l'albaran</summary>
    ''' <remarks>Especifica per al mode avançat de l'aplicacio
    ''' </remarks>
    Private Sub calcularTextPeu()
        LPText.Text = "[["

        If TPExercici.Text = "" Then
            LPText.Text = LPText.Text & "    "
        Else
            LPText.Text = LPText.Text & TPExercici.Text
        End If

        If TPOperacio.Text = "" Then
            LPText.Text = LPText.Text & "  "
        Else
            LPText.Text = LPText.Text & TPOperacio.Text
        End If

        If TPVenedor.Text = "" Then
            LPText.Text = LPText.Text & "   "
        Else
            LPText.Text = LPText.Text & TPVenedor.Text
        End If

        If TPAlbaran.Text = "" Then
            LPText.Text = LPText.Text & "     "
        Else
            LPText.Text = LPText.Text & TPAlbaran.Text
        End If

        If TPPagina.Text = "" Then
            LPText.Text = LPText.Text & "  "
        Else
            LPText.Text = LPText.Text & TPPagina.Text
        End If

        LPText.Text = LPText.Text & "]]"
    End Sub

    ''' <summary>Funcio que calcula el texte del peu de l'albaran </summary>
    ''' <remarks>Especifica per al mode basic de l'aplicacio 
    ''' </remarks>
    Private Sub calcularTextPeuBasic()
        textPeuBasic = "[["

        If TPExerciciBasic.Text = "" Then
            textPeuBasic = textPeuBasic & "    "
        Else
            textPeuBasic = textPeuBasic & TPExerciciBasic.Text
        End If

        If TPOperacioBasic.Text = "" Then
            textPeuBasic = textPeuBasic & "  "
        Else
            textPeuBasic = textPeuBasic & TPOperacioBasic.Text
        End If

        If TPVenedorBasic.Text = "" Then
            textPeuBasic = textPeuBasic & "   "
        Else
            textPeuBasic = textPeuBasic & TPVenedorBasic.Text
        End If

        If TPAlbaranBasic.Text = "" Then
            textPeuBasic = textPeuBasic & "     "
        Else
            textPeuBasic = textPeuBasic & TPAlbaranBasic.Text
        End If

        If TPPaginaBasic.Text = "" Then
            textPeuBasic = textPeuBasic & "  "
        Else
            textPeuBasic = textPeuBasic & TPPaginaBasic.Text
        End If

        textPeuBasic = textPeuBasic & "]]"
    End Sub

    ''' <summary>Funcio que canvia el mode de l'aplicacio</summary>
    ''' <remarks>Mostra i oculta els camps corresponents
    ''' </remarks>
    Private Sub canviMode()
        If modeBasic = False Then
            ' paso a mode basic
            controller.Log.Add("CANVIO A MODE BASIC", "DEBUG")
            modeBasic = True

        Else
            ' paso a mode avançat
            controller.Log.Add("CANVIO A MODE AVANÇAT!!!", "DEBUG")
            modeBasic = False
            mostrarCarpetesPendents()

        End If

        PBasic.Visible = modeBasic
        TC1.Visible = Not (modeBasic)
        PBusquedaAvan.Visible = Not (modeBasic)
    End Sub

    ''' <summary>Funcio que mostra les estadistiques segons el filtro</summary>
    ''' <remarks>Mostra les estadistiques de la data seleccionada
    ''' </remarks>
    Private Sub mostrarEstadistiques()
        Dim dstotal As DataSet

        dstotal = controller.BuscarTotals(MCTotals.SelectionStart)
        LTotals.Text = "total: " & dstotal.Tables(0).Rows(0)(0) & ", pendent: " & dstotal.Tables(0).Rows(0)(1)

        LTotalEscaneix.Text = dstotal.Tables(0).Rows(0)(2)
        LTotalOk.Text = dstotal.Tables(0).Rows(0)(3)
        LTotalModificats.Text = dstotal.Tables(0).Rows(0)(4)
        LTotalErrors.Text = dstotal.Tables(0).Rows(0)(5)
    End Sub

#End Region

#Region "events timers"

    ''' <summary>Event timer load all </summary>
    ''' <remarks>Activa la carrega completa dels albarans.
    ''' <para>l'objecte webBrowser que mostra el pdf te la mania de tardar un temps en lliberar el fitxer que esta mostrant, aixi que fa falta este timer per a esperar-lo</para>
    ''' </remarks>
    Private Sub TLoadAll_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TLoadAll.Tick
        PBCarrega.Visible = True
        LOperacio.Visible = True
        LOperacio.Text = "Carregant tots els albarans"
        controller.processAlbaransTimer(PBCarrega)
        mostrarCarpetesPendents()
        PBCarrega.Visible = False
        LOperacio.Visible = False
    End Sub

    ''' <summary>Event timer delete</summary>
    ''' <remarks>Activa la modificacio a la base de dades de l'albaran, aixi com tambe si el pdf s'ha de moure o renombrar
    ''' <para>l'objecte webBrowser que mostra el pdf te la mania de tardar un temps en lliberar el fitxer que esta mostrant, aixi que fa falta este timer per a esperar-lo</para> 
    ''' </remarks>
    Private Sub TDelete_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TDelete.Tick
        TDelete.Enabled = False

        If modeBasic = False Then
            controller.Log.Add("Modifico albaran id: " & IDAlbaran, "EDIT")
            controller.Log.Add("Exercici: de " & DGPendents.Item(1, editRow).Value.ToString & " a " & TPExercici.Text, "EDIT")
            controller.Log.Add("Operacio: de " & DGPendents.Item(2, editRow).Value.ToString & " a " & TPOperacio.Text, "EDIT")
            controller.Log.Add("Venedor: de " & DGPendents.Item(3, editRow).Value.ToString & " a " & TPVenedor.Text, "EDIT")
            controller.Log.Add("Albaran: de " & DGPendents.Item(4, editRow).Value.ToString & " a " & TPAlbaran.Text, "EDIT")
            controller.Log.Add("Pagina: de " & DGPendents.Item(5, editRow).Value.ToString & " a " & TPPagina.Text, "EDIT")
            controller.Log.Add("Data: de " & DGPendents.Item(6, editRow).Value.ToString & " a " & TPData.Text, "EDIT")
            controller.Log.Add("Text peu: de " & DGPendents.Item(7, editRow).Value.ToString & " a " & LPText.Text, "EDIT")
            controller.Log.Add("Ruta: de " & DGPendents.Item(8, editRow).Value.ToString & " a " & LPRuta.Text, "EDIT")

            controller.modificarAlbaran(TPExercici.Text, TPOperacio.Text, TPVenedor.Text, TPAlbaran.Text, TPPagina.Text, TPData.Text, LPText.Text, LPRuta.Text, IDAlbaran)

        Else

            calcularTextPeuBasic()

            controller.Log.Add("Modifico albaran id: " & IDAlbaran, "EDIT")
            controller.Log.Add("Exercici: de " & DGPEndentsBasic.Item(1, editRow).Value.ToString & " a " & TPExerciciBasic.Text, "EDIT")
            controller.Log.Add("Operacio: de " & DGPEndentsBasic.Item(2, editRow).Value.ToString & " a " & TPOperacioBasic.Text, "EDIT")
            controller.Log.Add("Venedor: de " & DGPEndentsBasic.Item(3, editRow).Value.ToString & " a " & TPVenedorBasic.Text, "EDIT")
            controller.Log.Add("Albaran: de " & DGPEndentsBasic.Item(4, editRow).Value.ToString & " a " & TPAlbaranBasic.Text, "EDIT")
            controller.Log.Add("Pagina: de " & DGPEndentsBasic.Item(5, editRow).Value.ToString & " a " & TPPaginaBasic.Text, "EDIT")
            controller.Log.Add("Data: de " & DGPEndentsBasic.Item(6, editRow).Value.ToString & " a " & TPDataBasic.Text, "EDIT")
            controller.Log.Add("Text peu: de RES a " & textPeuBasic, "EDIT")
            controller.Log.Add("Ruta: de RES a " & rutaBasic, "EDIT")

            controller.modificarAlbaran(TPExerciciBasic.Text, TPOperacioBasic.Text, TPVenedorBasic.Text, TPAlbaranBasic.Text, TPPaginaBasic.Text, TPDataBasic.Text, textPeuBasic, rutaBasic, IDAlbaran)
        End If

        mostrarPendents()
    End Sub

    ''' <summary>Event timer set focus</summary>
    ''' <remarks>Posa el foco en el camp a editar
    ''' <para>l'objecte webBrowser que mostra el pdf te la mania de tardar un temps en lliberar el fitxer que esta mostrant, aixi que fa falta este timer per a esperar-lo</para> 
    ''' </remarks>
    Private Sub TSetFocus_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSetFocus.Tick
        TSetFocus.Enabled = False

        If modeBasic = False Then
            TPVenedor.Focus()
            TPVenedor.SelectAll()
        Else
            TPVenedorBasic.Focus()
            TPVenedorBasic.SelectAll()
        End If
    End Sub

#End Region


End Class
