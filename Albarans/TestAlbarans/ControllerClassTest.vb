﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports WindowsApplication1



'''<summary>
'''Se trata de una clase de prueba para ControllerClassTest y se pretende que
'''contenga todas las pruebas unitarias ControllerClassTest.
'''</summary>
<TestClass()> _
Public Class ControllerClassTest


    Private testContextInstance As TestContext
    Private pageText(10, 8) As String

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Omplo els valors a testejar
    '''</summary>
    Private Sub OmplirDades()
        ' tot ok i clar
        pageText(0, 0) = "ODi" & Chr(10) &
        "_______()" & Chr(10) &
        "<<201700036902388>>" & Chr(10) &
        "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
        "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201700036902388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(0, 1) = "[[201700036902388]]]" 'text peu
        pageText(0, 2) = "<<201700036902388>>>" 'text cap
        pageText(0, 3) = "2017" 'exercici
        pageText(0, 4) = "902388" 'num albaran
        pageText(0, 5) = "00" 'operacio
        pageText(0, 6) = "1" 'pagina
        pageText(0, 7) = "036" 'venedor



        ' textes tallats i invertits
        pageText(1, 0) = "ODi" & Chr(10) &
        "_______() 02378>>" & Chr(10) &
        "<<2017000329" & Chr(10) &
        "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
        "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00 02378]]" & Chr(10) &
"[[2017000329" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(1, 1) = "[[201700032902378]]"
        pageText(1, 2) = "<<201700032902378>>"
        pageText(1, 3) = "2017" 'exercici
        pageText(1, 4) = "902378" 'num albaran
        pageText(1, 5) = "00" 'operacio
        pageText(1, 6) = "1" 'pagina
        pageText(1, 7) = "032" 'venedor


        ' textes tallats
        pageText(2, 0) = "ODi" & Chr(10) &
    "_______() <<20160" & Chr(10) &
    "0016802388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"15/08/2016        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00 [[20160" & Chr(10) &
"0016802388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(2, 1) = "[[201600016802388]]]"
        pageText(2, 2) = "<<201600016802388>>>"
        pageText(2, 3) = "2016" 'exercici
        pageText(2, 4) = "802388" 'num albaran
        pageText(2, 5) = "00" 'operacio
        pageText(2, 6) = "1" 'pagina
        pageText(2, 7) = "016" 'venedor



        'un text tallat i l'altre no
        pageText(3, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<20170003" & Chr(10) &
    "6902388>>Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"2016        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"<[201700036902388]>" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(3, 1) = "[[201700036902388]]]"
        pageText(3, 2) = "<<201700036902388>>>"
        pageText(3, 3) = "2017" 'exercici
        pageText(3, 4) = "902388" 'num albaran
        pageText(3, 5) = "00" 'operacio
        pageText(3, 6) = "1" 'pagina
        pageText(3, 7) = "036" 'venedor


        'sense text al cap
        pageText(4, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201700036902388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(4, 1) = "[[201700036902388]]]"
        pageText(4, 2) = ""
        pageText(4, 3) = "2017" 'exercici
        pageText(4, 4) = "902388" 'num albaran
        pageText(4, 5) = "00" 'operacio
        pageText(4, 6) = "1" 'pagina
        pageText(4, 7) = "036" 'venedor


        ' sense text al peu
        pageText(5, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<201700036902388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(5, 1) = ""
        pageText(5, 2) = "<<201700036902388>>>"
        pageText(5, 3) = "2017" 'exercici
        pageText(5, 4) = "902388" 'num albaran
        pageText(5, 5) = "00" 'operacio
        pageText(5, 6) = "1" 'pagina
        pageText(5, 7) = "036" 'venedor



        'cap incorrecte
        pageText(6, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<20170kasdlan388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201700036902388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(6, 1) = "[[201700036902388]]]"
        pageText(6, 2) = "<<20170kasdlan388>>>"
        pageText(6, 3) = "2017" 'exercici
        pageText(6, 4) = "902388" 'num albaran
        pageText(6, 5) = "00" 'operacio
        pageText(6, 6) = "1" 'pagina
        pageText(6, 7) = "036" 'venedor



        ' peu incorrecte
        pageText(7, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<201700036902388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201706902388]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(7, 1) = "[[201706902388]]]"
        pageText(7, 2) = "<<201700036902388>>>"
        pageText(7, 3) = "2017" 'exercici
        pageText(7, 4) = "902388" 'num albaran
        pageText(7, 5) = "00" 'operacio
        pageText(7, 6) = "1" 'pagina
        pageText(7, 7) = "036" 'venedor




        pageText(8, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<201700036902388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201700036902388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(8, 1) = "[[201700036902388]]]"
        pageText(8, 2) = "<<201700036902388>>>"
        pageText(8, 3) = "2017" 'exercici
        pageText(8, 4) = "902388" 'num albaran
        pageText(8, 5) = "00" 'operacio
        pageText(8, 6) = "1" 'pagina
        pageText(8, 7) = "036" 'venedor



        pageText(9, 0) = "ODi" & Chr(10) &
    "_______()" & Chr(10) &
    "<<201700036902388>>" & Chr(10) &
    "Datos(cliente)" & Chr(10) &
"20420:  HOTEL(SAN)" & Chr(10) &
    "FRANCISCO()" & Chr(10) &
"c/ANP05TA, 5" & Chr(10) &
"Albarán n° Fecha albat84CPc&9°" & Chr(10) &
"Cola OPE Entregado" & Chr(10) &
"por: ," & Chr(10) &
"        o()" & Chr(10) &
"        Ref.Descripción()" & Chr(10) &
"Lote Cantidad Precio" & Chr(10) &
"%IVA Importe" & Chr(10) &
"        Cajas(Kg / Und)" & Chr(10) &
"600168 ht surimi palitó cangrejo" & Chr(10) &
"c/9k 0,0 ,00" & Chr(10) &
"2,05 10 ,00" & Chr(10) &
"Caducidad:08/09/2018" & Chr(10) &
"        Lote155()" & Chr(10) &
"% %" & Chr(10) &
"Base Imponible IVA Total IVA R.E. Totál R.E. Importe Conforme cliente:" & Chr(10) &
"        Total()" & Chr(10) &
"202,00 4 8,08 0,5 ,00 590,72" & Chr(10) &
"346,04 10 34,60 1,4 ,00" & Chr(10) &
"[[201700036902388]]" & Chr(10) &
"        y()" & Chr(10) &
"Con domiciliación bancaria" & Chr(10) &
"S1D NoEI" & Chr(10)

        pageText(9, 1) = "[[201700036902388]]]"
        pageText(9, 2) = "<<201700036902388>>>"
        pageText(9, 3) = "2017" 'exercici
        pageText(9, 4) = "902388" 'num albaran
        pageText(9, 5) = "00" 'operacio
        pageText(9, 6) = "1" 'pagina
        pageText(9, 7) = "036" 'venedor
    End Sub


    '''<summary>
    '''Una prueba de tincTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub decidirDosTexts()
        Dim target As ControllerClass = New ControllerClass()
        Dim actual As String

        actual = target.decidirDosTexts("", "2017", 4)
        Assert.AreEqual("2017", actual)

        actual = target.decidirDosTexts("2017", "", 4)
        Assert.AreEqual("2017", actual)

        actual = target.decidirDosTexts("2017", "2017", 4)
        Assert.AreEqual("2017", actual)

        actual = target.decidirDosTexts("2016", "2017", 4)
        Assert.AreEqual("2016", actual)

        actual = target.decidirDosTexts("2017", "2016", 4)
        Assert.AreEqual("2017", actual)

        actual = target.decidirDosTexts("201", "2017", 4)
        Assert.AreEqual("2017", actual)

        actual = target.decidirDosTexts("2017", "201", 4)
        Assert.AreEqual("2017", actual)

    End Sub

    '''<summary>
    '''Una prueba de tincTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub tincTextPeuTest()
        Dim target As ControllerClass = New ControllerClass()
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.tincTextPeu(pageText(count, 0))
                Assert.AreEqual(pageText(count, 1), actual, "Falla count=" & count)

                actual = target.tincTextPeu(pageText(count, 0), "<<", ">>")
                Assert.AreEqual(pageText(count, 2), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarExercici
    '''</summary>
    <TestMethod()> _
    Public Sub buscarExerciciTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarExercici(pageText(count, 0))
                Assert.AreEqual(pageText(count, 3), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarExerciciTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub buscarExerciciTextPeuTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarExerciciTextPeu(pageText(count, 1), pageText(count, 0), pageText(count, 2))
                Assert.AreEqual(pageText(count, 3), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarNumAlbaranTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub buscarNumAlbaranTextPeuTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarNumAlbaranTextPeu(pageText(count, 1), pageText(count, 0), pageText(count, 2))
                Assert.AreEqual(pageText(count, 4), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarOperacio
    '''</summary>
    <TestMethod()> _
    Public Sub buscarOperacioTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarOperacio(pageText(count, 0))
                Assert.AreEqual(pageText(count, 5), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarOperacioTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub buscarOperacioTextPeuTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarOperacioTextPeu(pageText(count, 1), pageText(count, 0), pageText(count, 2))
                Assert.AreEqual(pageText(count, 5), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarPagina
    '''</summary>
    <TestMethod()> _
    Public Sub buscarPaginaTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarPagina(pageText(count, 0))
                Assert.AreEqual(pageText(count, 6), actual, "Falla count=" & count)
            End If
        Next

    End Sub

    '''<summary>
    '''Una prueba de buscarVenedorTextPeu
    '''</summary>
    <TestMethod()> _
    Public Sub buscarVenedorTextPeuTest()
        Dim target As ControllerClass = New ControllerClass() ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        Dim count As Integer

        OmplirDades()

        For count = 0 To 9
            If pageText(count, 0) <> "" Then
                actual = target.buscarVenedorTextPeu(pageText(count, 1), pageText(count, 0), pageText(count, 2))
                Assert.AreEqual(pageText(count, 7), actual, "Falla count=" & count)
            End If
        Next

    End Sub

End Class
