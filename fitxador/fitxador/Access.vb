﻿Imports System.Data.OleDb

''' <summary>Clase per a gestionar la conexio i peticions a una base de dades ACCESS</summary>
''' <remarks>configuracio:
''' <list type="table">
'''   <listheader>
'''      <term>Variable privada</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>test <c>Boolean = False</c></term><description>Indica si la clase esta en mode test (<c>True</c>) o no (<c>False</c>). 
'''      <para>En mode test la clase es conecta a una base de dades de test i mostra una serie de misatges quan executes l'aplicacio</para>
'''      <para>Esta variable es pot usar a altres clases per a afegir mes missatges de control, per exemple:</para>
'''      <para><example><code>
'''        If BD.test then
'''           MessageBox("sql: " + sql, "Debug", MessageBoxButtons.OK)
'''        End If
'''      </code></example></para></description>
'''   </item>
'''   <item>
'''      <term>dbProvider <c>String</c></term><description>El proveedor a utilitzar</description>
'''   </item>
'''   <item>
'''      <term>dbDataSource <c>String = "3306"</c></term><description>Nom del fitxer a obrir</description>
'''   </item>
'''   <item>
'''      <term>dbPass <c>String</c></term><description>Password de la base de dades</description>
'''   </item>
'''   <item>
'''      <term>conexion <c>OleDbConnection</c></term><description>Es la clase que gestiona tota la coneccio. Be de la dll importada al principi de tot</description>
'''   </item>
'''</list>
''' </remarks>

Public Class Access
    Public test As Boolean = False 
    'Public test As Boolean = True 

    'acces 2007 -> Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\myFolder\myAccessFile.accdb;Jet OLEDB:Database Password=MyDbPassword;
    Private dbProvider As String = "PROVIDER=Microsoft.Jet.OLEDB.4.0;"
    Private dbDataSource As String = "Data Source = 'C:\Archivos de programa/Access Manager/DB/NITGENDBAC.mdb';"
    'Private dbDataSource As String = "Data Source = 'X:\NITGENDBAC.mdb';"
    Private dbPass As String = "Jet OLEDB:Database Password=nac3000;"

    'NGAC_LOG
    'logindex
    'nodeindex
    'logtime -
    'userid -
    'nodeid -
    'authtype
    'authresult
    'openresult
    'functionno
    'slogtime
    'checked
    'reserved

    'NGAC_TERMINAL
    'nodeid
    'nodename
    'description

    'NGAC_USERINFO
    'userid
    'username
    'groupid



    Private conexion As OleDbConnection


    ''' <summary>Funcio que conecta a la base de dades</summary>
    ''' <remarks>Mostra un missatge d'error en cas de trobar algun problema, aixi com una excepcio
    ''' </remarks>
    Public Function connect()
        Try
            If test Then
                conexion = New OleDbConnection(dbProvider & dbDataSource & dbPass)
            Else
                conexion = New OleDbConnection(dbProvider & dbDataSource & dbPass)
            End If

            conexion.Open()

            Return True
        Catch ex As Exception
            MsgBox("Error al conectar a la BD access " & vbCrLf & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    ''' <summary>Funcio que converteix una data al format de la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: objecte en la data a convertir</param>
    ''' <returns><c>String</c>: data convertida a punt per a insertar-la en una sentencia SQL</returns>
    Public Function convertDate(ByVal data As Date) As String
        Dim dataString As String

        dataString = "#" & data.Month & "/" & data.Day & "/" & data.Year & "#"

        Return dataString

    End Function

    ''' <summary>Funcio que escapa els caracters que donen problemes a una sentencia SQL</summary>
    ''' <remarks> Utilitzar sempre en tots els camps de text
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sentencia SQL o Texte a depurar</param>
    ''' <returns><c>String</c>: Texte en los caracters escapats</returns>
    Public Function cleanSql(ByVal Sql As String) As String
        Sql = Sql.Replace("'", "''")
        Sql = Sql.Replace("\", "\\")

        Return Sql
    End Function

    ''' <summary>Funcio que executa una consulta "SELECT" a la base de dades</summary>
    ''' <remarks> 
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sentencia SQL a executar</param>
    ''' <returns><c>DataSet / Boolean</c>: retorna <c>False</c> si detecta algun error o un <c>DataSet</c> en les dades</returns>
    Public Function doSelect(ByVal Sql As String) 'As DataSet
        Dim consultaSQL As OleDbCommand = New OleDbCommand(Sql, conexion)
        Dim ds As DataSet = New DataSet()
        Dim DataAdapter As OleDbDataAdapter = New OleDbDataAdapter()

        Try
            DataAdapter.SelectCommand = consultaSQL
            DataAdapter.Fill(ds, "Tabla")
            Return ds
            'DataGridView1.DataSource = ds
            'DataGridView1.DataMember = "Tabla"

            'bePanelNumRegistros.Text = "| Nº registros: " & CStr(ds.Tables(0).Rows.Count)

            '-------------------

            'Dim t1 As DataTable = ds.Tables(Table_)
            'Dim row As DataRow
            'Dim Item(2) As String

            'For Each row In t1.Rows
            'Item(0) = row(0)
            'Item(1) = row(1)
            'Dim NextListItem As New ListViewItem(Item)

            'ListView1.Items.Add(NextListItem)
            'Next
        Catch ex As Exception
            MsgBox("Error al ejecutar consulta SQL: " & vbCrLf & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)

            Return False
        End Try
    End Function

    ''' <summary>Funcio que executa una operacio de "INSERT", "UPDATE" o "DELETE" a la base de dades</summary>
    ''' <remarks> 
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sql en la sentencia a executar</param>
    ''' <returns><c>Integer</c>: Numero de registres afectats</returns>
    Public Function doOperation(ByVal Sql As String) As Integer
        Dim consultaSQL As OleDbCommand = New OleDbCommand(Sql, conexion)
        Dim numRegistrosAfectados As Integer
        Dim comandoSQL As New OleDbCommand

        Try
            comandoSQL.Connection = conexion
            comandoSQL.CommandText = Sql
            numRegistrosAfectados = comandoSQL.ExecuteNonQuery()

            Return numRegistrosAfectados
        Catch ex As Exception
            MsgBox("Error al ejecutar consulta SQL: " & vbCrLf & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)

            Return 0
        End Try
    End Function

End Class
