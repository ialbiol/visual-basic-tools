﻿Imports System.IO

''' <summary>Clase que gestiona tota la logica del programa i del proces d'importacio de dades</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>DBMysql <c>mysql</c></term><description>Objecte que gestiona la conexio en la base de dades MySQL</description>
'''   </item>
'''   <item>
'''      <term>DBAccess <c>Access</c></term><description>Objecte que gestiona la conexio en la base de dades Access</description>
'''   </item>
'''   <item>
'''      <term>Fitxades <c>FitxadaController</c></term><description>Objecte que gestiona les fitxades</description>
'''   </item>
'''   <item>
'''      <term>Log <c>LogClass</c></term><description>Objecte que gestiona el fitxer de log</description>
'''   </item>
'''   <item>
'''      <term>PathRegistres <c>String = Application.StartupPath() + "\Registres\"</c></term><description>Ruta de la carpeta on guardar el registres dels treballadors</description>
'''   </item>
'''   <item>
'''      <term>TemplateRegistre <c>Application.StartupPath() + "\Registres\Template.xlsx"</c></term><description>Ruta de la plantilla dels registres dels treballadors</description>
'''   </item>
'''   <item>
'''      <term>COL_USERID <c>Integer = 0</c></term><description>Constant que indica l'index de la columna user_Id dins la query que busca els treballadors</description>
'''   </item>
'''   <item>
'''      <term>COL_USERNAME <c>Integer = 1</c></term><description>Constant que indica l'index de la columna user_Name dins la query que busca els treballadors</description>
'''   </item>
'''   <item>
'''      <term>COL_LOGTIME <c>Integer = 0</c></term><description>Constant que indica l'index de la columna Log_Time dins la query que busca les fitxades</description>
'''   </item>
'''   <item>
'''      <term>COL_NODEID <c>Integer = 1</c></term><description>Constant que indica l'index de la columna Node_Id dins la query que busca les fitxades</description>
'''   </item>
'''</list>
''' </remarks>

Public Class Controller

    Private DBMysql As New mysql
    Private DBAccess As New Access
    Public Fitxades As New FitxadaController
    Public Log As New LogClass

    Private PathRegistres As String = Application.StartupPath() & "\Registres\"
    Private TemplateRegistre As String = Application.StartupPath() & "\Registres\Template.xlsx"

    Private COL_USERID = 0 'numero de columna
    Private COL_USERNAME = 1 'numero de columna

    Private COL_LOGTIME = 0 'numero de columna
    Private COL_NODEID = 1 'numero de columna


    ''' <summary>Funcio que busca la ultima data que tenim controlat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: Ultima data gestionada</returns>
    Private Function LastDate() As String
        Dim ds As DataSet

        ' busco la data mes gran dins la taula dels logs de mysql
        ds = DBMysql.doSelect("SELECT data FROM registre ORDER BY data DESC LIMIT 1")

        If ds.Tables(0).Rows.Count > 0 Then
            Return (ds.Tables(0).Rows(0)(0))
        Else
            'Return ("17/07/2017") 'data inici
            Return ("30/06/2017") 'data inici
        End If
    End Function

    ''' <summary>Funcio que busca la ultima data de l'access</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: Ultima data gestionada</returns>
    Private Function LastDateAccess() As String
        Dim ds As DataSet

        ' busco la data mes gran dins la taula dels logs d'access
        ds = DBAccess.doSelect("SELECT TOP 1 logtime FROM NGAC_LOG ORDER BY logtime DESC")

        If ds.Tables(0).Rows.Count > 0 Then
            Return (ds.Tables(0).Rows(0)(0))
        Else
            Return Now.ToShortDateString() 'data inici
        End If

    End Function


    ''' <summary>Funcio que busca el llistat de treballadors a l'access</summary>
    ''' <remarks>busca a l'access els treballadors que han fitxat des de la data d'inici hasta avui
    ''' </remarks>
    ''' <param name="dataInici"><c>Date</c>: Data inicial a buscar</param>
    ''' <returns><c>DataSet</c>: dades trobades</returns>
    Private Function buscarTreballadorsAccess(ByVal dataInici As Date)
        Dim sql As String
        Dim ds As DataSet

        sql = "SELECT DISTINCT NGAC_USERINFO.userid, NGAC_USERINFO.username "
        sql = sql & "FROM NGAC_LOG INNER JOIN NGAC_USERINFO ON NGAC_LOG.userid = NGAC_USERINFO.userid "
        sql = sql & "WHERE NGAC_LOG.logtime >= " & DBAccess.convertDate(dataInici) & " "
        sql = sql & "ORDER BY NGAC_USERINFO.userid, NGAC_USERINFO.username"

        If DBAccess.test Then
            sql = "SELECT DISTINCT TOP 1 NGAC_USERINFO.userid, NGAC_USERINFO.username "
            sql = sql & "FROM NGAC_LOG INNER JOIN NGAC_USERINFO ON NGAC_LOG.userid = NGAC_USERINFO.userid "
            sql = sql & "WHERE NGAC_LOG.logtime >= " & DBAccess.convertDate(dataInici) & " "
            'sql = sql & "WHERE NGAC_LOG.logtime >= #7/18/2017# " '"#" & data.Month & "/" & data.Day & "/" & data.Year & "#"
            'sql = sql & "AND NGAC_USERINFO.userid = '000300000000000' " 'kevin - mati i extres
            'sql = sql & "AND NGAC_USERINFO.userid = '000800000000000' " 'ivan - normal
            sql = sql & "AND NGAC_USERINFO.userid = '002800000000000' " 'bernardo - normal
            'sql = sql & "AND NGAC_USERINFO.userid = '002500000000000' " 'casals - oficines en 2 fitxades
            'sql = sql & "AND NGAC_USERINFO.userid = '008200000000000' " 'vizcarro - nit
            'sql = sql & "AND NGAC_USERINFO.userid = '002000000000000' " 'videllet - nit
            sql = sql & "ORDER BY NGAC_USERINFO.userid, NGAC_USERINFO.username"

            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Carrego dades: busco treballadors " & sql, "CARREGA")
        ds = DBAccess.doSelect(sql)
        Return ds
    End Function

    ''' <summary>Funcio que busca la ultima fitxada del treballador al MySQL</summary>
    ''' <remarks>Esta funcio torna un objecte fitxada i la inserta al llistat de fitxades
    ''' </remarks>
    ''' <param name="codiTreballador"><c>String</c>: codi del treballador a buscar</param>
    ''' <param name="nomTreballador"><c>String</c>: Nom del treballador a buscar</param>
    ''' <returns><c>FitxadaClass</c>: ultima fitxada o nothing</returns>
    Private Function buscarUltimaFitxadaMySql(ByVal codiTreballador As String, ByVal nomTreballador As String) As FitxadaClass
        Dim sql As String
        Dim ds As DataSet
        Dim fitxa As FitxadaClass

        If DBMysql.test Then
            MessageBox.Show("codiTreballador: " & codiTreballador & " nomTreballador: " & nomTreballador, "Error", MessageBoxButtons.OK)
        End If

        sql = "SELECT id FROM registre WHERE "
        sql = sql & "codiTreballador = '" & DBMysql.cleanSql(codiTreballador) & "'"
        sql = sql & " AND treballador = '" & DBMysql.cleanSql(nomTreballador) & "'"
        sql = sql & " ORDER BY data DESC LIMIT 1"

        Log.Add("Carrego dades: sql buscar ultima fitxada mysql: " & sql, "CARREGA")

        If DBMysql.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBMysql.doSelect(sql)

        If ds.Tables(0).Rows.Count > 0 Then ' hi han dades
            fitxa = SearchById(ds.Tables(0).Rows(0)(0))
            Fitxades.SetUltimaFitxada(fitxa)

            Return fitxa
        End If

        Return Nothing
    End Function


    ''' <summary>Funcio que busca les dades a l'access per al treballador</summary>
    ''' <remarks>busco dos dies antes d'avui, o sigue now-1 a les 00:00:00, sino no podre carregar correctament la sortida del turno de nit
    ''' </remarks>
    ''' <param name="codiTreballador"><c>String</c>: codi del treballador a buscar</param>
    ''' <param name="nomTreballador"><c>String</c>: Nom del treballador a buscar</param>
    ''' <param name="dataInici"><c>Date</c>: Data inicial a buscar</param>
    ''' <returns><c>DataSet</c>: dades trobades</returns>
    Private Function buscarFitxadesAccess(ByVal codiTreballador As String, ByVal nomTreballador As String, ByVal dataInici As DateTime)
        Dim ds As DataSet
        Dim sql As String
        Dim dataFin As Date = Now.AddDays(-1)

        'sql = "SELECT NGAC_USERINFO.userid, NGAC_USERINFO.username, NGAC_USERINFO.groupid, NGAC_LOG.nodeid, NGAC_LOG.logtime"
        sql = "SELECT NGAC_LOG.logtime, NGAC_LOG.nodeid"
        sql = sql & " FROM NGAC_LOG INNER JOIN NGAC_USERINFO ON NGAC_LOG.userid = NGAC_USERINFO.userid WHERE"
        sql = sql & " NGAC_USERINFO.userid = '" & DBAccess.cleanSql(codiTreballador) & "'"
        sql = sql & " AND NGAC_USERINFO.username = '" & DBAccess.cleanSql(nomTreballador) & "'"
        sql = sql & " AND NGAC_LOG.logtime > #" & dataInici.Month & "/" & dataInici.Day & "/" & dataInici.Year & " " & dataInici.Hour & ":" & dataInici.Minute & ":" & dataInici.Second & "#"
        'sql = sql & " AND NGAC_LOG.logtime <= #" & dataFin.Month & "/" & dataFin.Day & "/" & dataFin.Year & " 00:00:00#"
        sql = sql & " ORDER BY NGAC_LOG.logtime"

        Log.Add("Carrego dades: sql buscar fitxades access: " & sql, "CARREGA")

        If DBAccess.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBAccess.doSelect(sql)

        Return ds
    End Function


    ''' <summary>Funcio que guarda les dades importades al MySQL</summary>
    ''' <remarks>
    ''' </remarks>
    Private Sub guardarDadesMysql()
        Dim row As Integer
        Dim fitxa As FitxadaClass

        For row = 0 To Fitxades.Count - 1
            fitxa = Fitxades.GetItem(row)
            If fitxa.Id = 0 Then
                InsertarFitxada(fitxa)

            Else
                ModificarFitxada(fitxa)

            End If

        Next

    End Sub



    ''' <summary>Funcio que gestiona la importacio de dades de l'access al MySQL</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dataMysql"><c>Date</c>: Data inicial a gestionar</param>
    Private Sub carregaInfoNova(ByVal dataMysql As Date)
        Dim dsTreballadors As DataSet
        Dim row As Integer
        Dim rowHores As Integer
        Dim ultimaFitxada As DateTime
        Dim ds As DataSet
        Dim ultimaFitxa As FitxadaClass

        'Dim data As Date = dataMysql
        'Dim sql As String


        dsTreballadors = buscarTreballadorsAccess(dataMysql)
        Log.Add("Carrego dades: trobo " & dsTreballadors.Tables(0).Rows.Count & " treballadors", "CARREGA")

        If dsTreballadors.Tables(0).Rows.Count > 0 Then ' hi han treballadors
            For row = 0 To dsTreballadors.Tables(0).Rows.Count - 1
                'limpio dades
                Fitxades.nouTreballador(dsTreballadors.Tables(0).Rows(row)(COL_USERID), dsTreballadors.Tables(0).Rows(row)(COL_USERNAME))

                'busco ultima fitxada del treballador
                ultimaFitxa = buscarUltimaFitxadaMySql(dsTreballadors.Tables(0).Rows(row)(COL_USERID), dsTreballadors.Tables(0).Rows(row)(COL_USERNAME))
                If IsNothing(ultimaFitxa) Then
                    ultimaFitxada = dataMysql
                Else
                    ultimaFitxada = ultimaFitxa.getUltimaFitxada()
                    If ultimaFitxada = Nothing Then
                        ultimaFitxada = dataMysql
                    End If
                End If

                Log.Add("Carrego dades: ultima fitxada de " & dsTreballadors.Tables(0).Rows(row)(COL_USERNAME) & " gestionada es " & ultimaFitxada.ToString(), "CARREGA")

                If DBMysql.test Then
                    MessageBox.Show("ultimaFitxada: " & ultimaFitxada, "Error", MessageBoxButtons.OK)
                End If

                'busco les fitxades del treballador desde la ultima hasta ahir
                ds = buscarFitxadesAccess(dsTreballadors.Tables(0).Rows(row)(COL_USERID), dsTreballadors.Tables(0).Rows(row)(COL_USERNAME), ultimaFitxada)
                Log.Add("Carrego dades: trobo " & ds.Tables(0).Rows.Count & " fitxades pendents", "CARREGA")

                If ds.Tables(0).Rows.Count > 0 Then ' hi han dades

                    For rowHores = 0 To ds.Tables(0).Rows.Count - 1
                        Fitxades.AddHora(ds.Tables(0).Rows(rowHores)(COL_LOGTIME))
                    Next

                    Fitxades.checkUltimaData() ' per si de cas queda algo a mitges
                    Fitxades.PostProcess() ' faig el post process per a comprobar que no hi hagi algun error detectable
                    guardarDadesMysql() ' guardo les dades a la BD
                End If

            Next
        End If

    End Sub



    ''' <summary>Funcio que fa el check inicial</summary>
    ''' <remarks>Es crida sempre al iniciar l'aplicacio
    ''' </remarks>
    ''' <returns><c>Boolean</c>: Retorna True si tot s'ha inicialitzat correctament</returns>
    Public Function checkBegin() As Boolean

        ' inicio el log
        Log.Init()

        'conecto a la base de dades mysql
        If Not DBMysql.connect() Then
            Log.Add("No puc inicial DB MySQL", "ERROR")
            Return False
        End If

        'conecto a la base de dades access
        If Not DBAccess.connect() Then
            Log.Add("No puc inicial DB Access", "ERROR")
            Return False
        End If

        If Not Directory.Exists(PathRegistres) Then
            Directory.CreateDirectory(PathRegistres)
        End If

        Return True
    End Function

    ''' <summary>Funcio que tanca l'objecte</summary>
    ''' <remarks>Es crida sempre antes de tancar l'aplicacio
    ''' </remarks>
    Public Sub Dispose()
        Log.Add("Aplicacio finalitzada")

        Log.Close()
    End Sub

    ''' <summary>Funcio que busca si hi han dades noves a carregar de l'access al MySQL</summary>
    ''' <remarks>En cas de trobar dades pendent d'importar, inicia el proces
    ''' </remarks>
    Public Sub carregarInfo()
        ' busco la ultima data que existeix a l'access
        Dim dateAccess As Date = LastDateAccess()
        If DBMysql.test Then
            MessageBox.Show("data access: " & dateAccess.ToShortDateString(), "Info", MessageBoxButtons.OK)
        End If

        ' busco la ultima data gestionada a mysql
        Dim dateMysql As Date = LastDate()
        If DBMysql.test Then
            MessageBox.Show("data mysql: " & dateMysql.ToShortDateString(), "Info", MessageBoxButtons.OK)
        End If

        If dateAccess.DayOfYear > dateMysql.DayOfYear Then
            dateMysql = dateMysql.AddDays(1)
            Log.Add("Carrego dades des del " & dateMysql.ToShortDateString(), "CARREGA")
            carregaInfoNova(dateMysql)
        End If
    End Sub

    ''' <summary>Funcio que seteja el DataGrid on posar les dades que s'estan gestionant</summary>
    ''' <remarks>NO S'USA!! s'ha deixat la funcio per si s'ha de modificar el proces
    ''' </remarks>
    ''' <param name="DGDebug"><c>DataGridView</c>: DataGridView on mostrar les dades que s'estan gestionant</param>
    Public Sub debug(ByVal DGDebug As DataGridView)
        Fitxades.debug(DGDebug)


    End Sub



    ''' <summary>Funcio que busca el codi d'un treballador a partir del nom</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Treballador"><c>String</c>: Nom del treballador</param>
    ''' <returns><c>String</c>: Codi del treballador o cadena buida si no el troba</returns>
    Public Function BuscarCodiTreballador(ByVal Treballador As String) As String
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT r.codiTreballador FROM registre r WHERE " &
            "r.treballador = '" & Treballador & "'"
        Sql = Sql & " ORDER BY r.codiTreballador LIMIT 1"
        ds = DBMysql.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0)
        Else
            Return ""
        End If

    End Function

    ''' <summary>Funcio que busca si una data i un treballador ja tenen una fitxada creada o no</summary>
    ''' <remarks>Serveix per a gestionar errors al crear fitxades noves de manera manual
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data de la fitxada</param>
    ''' <param name="Treballador"><c>String</c>: Nom del treballador</param>
    ''' <returns><c>Boolean</c>: Si existeix la fitxada a la base de dades o no</returns>
    Public Function BuscarData(ByVal Data As Date, ByVal Treballador As String) As Boolean
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT COUNT(r.data) as 'Num' FROM registre r WHERE " &
            "r.treballador = '" & Treballador & "'" &
            " AND r.data = '" & DBMysql.convertDate(Data) & "'"

        If DBMysql.test Then
            MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBMysql.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) = 1 Then
                Return True
            End If
        End If

        Return False
    End Function

    ''' <summary>Funcio que retorna un llistat de treballadors</summary>
    ''' <remarks>Busca els treballadors que tenen dades entre les dates indicades
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial a filtrar</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final a filtrar</param>
    ''' <returns><c>DataSet</c>: dades trobades</returns>
    Public Function llistatTreballadors(ByVal DataInici As Date, ByVal DataFinal As Date)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT DISTINCT r.treballador FROM registre r WHERE " &
            "r.data >= '" & DBMysql.convertDate(DataInici) & "'" &
            " AND r.data <= '" & DBMysql.convertDate(DataFinal) & "'"
        Sql = Sql & " ORDER BY r.treballador"
        ds = DBMysql.doSelect(Sql)

        Return ds
    End Function

    ''' <summary>Funcio que busca un llistat de fitxades segons filtros</summary>
    ''' <remarks>Es la funcio que omple el DataGrid del formulari principal
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial a filtrar</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final a filtrar</param>
    ''' <param name="Treballador"><c>String</c>: Nom del treballador a filtrar</param>
    ''' <returns><c>DataSet</c>: dades trobades</returns>
    Public Function llistatFitxades(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Treballador As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT r.id AS 'Id'"
        Sql = Sql & ", r.treballador AS 'Treballador'"

        Sql = Sql & ", (CASE WHEN r.diaSemana = 0 THEN 'diumenge' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 1 THEN 'dilluns' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 2 THEN 'dimarts' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 3 THEN 'dimecres' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 4 THEN 'dijous' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 5 THEN 'divendres' ELSE "
        Sql = Sql & "CASE WHEN r.diaSemana = 6 THEN 'dissabte' ELSE '' "
        Sql = Sql & "END END END END END END END) AS 'Dia semana'"  '2

        Sql = Sql & ", r.data AS 'Data'"
        Sql = Sql & ", r.entrada AS 'Entrada'"
        Sql = Sql & ", r.sortida AS 'Sortida'" '5
        Sql = Sql & ", r.entrada2 AS 'Entrada 2'"
        Sql = Sql & ", r.sortida2 AS 'Sortida 2'"
        Sql = Sql & ", r.totalHores AS 'Total hores'"
        Sql = Sql & ", r.horesNormals AS 'Hores normals'"
        Sql = Sql & ", r.horesExtres AS 'Hores extres'" '10
        Sql = Sql & ", r.vacances AS 'Vacances'"
        Sql = Sql & ", r.baixa AS 'Baixa'"
        Sql = Sql & ", r.modificat AS 'Error Fitxada'"
        Sql = Sql & ", r.error AS 'Error Calcul'"

        Sql = Sql & "FROM registre r WHERE"
        Sql = Sql & " r.data >= '" & DBMysql.convertDate(DataInici) & "'"
        Sql = Sql & " AND r.data <= '" & DBMysql.convertDate(DataFinal) & "'"
        If Treballador <> "Tots" Then
            Sql = Sql & " AND r.treballador = '" & Treballador & "'"
        End If
        Sql = Sql & " ORDER BY r.treballador, r.data"

        If DBMysql.test Then
            MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBMysql.doSelect(Sql)

        Return ds
    End Function

    ''' <summary>Funcio que busca els totals segons els filtros</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial a filtrar</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final a filtrar</param>
    ''' <param name="Treballador"><c>String</c>: Nom del treballador a filtrar</param>
    ''' <returns><c>DataSet</c>: dades trobades</returns>
    Public Function llistatTotals(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Treballador As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT SEC_TO_TIME( SUM(time_to_sec(r.totalHores))) AS 'Total hores'"
        Sql = Sql & ", SEC_TO_TIME( SUM(time_to_sec(r.horesNormals))) AS 'Hores normals'"
        Sql = Sql & ", SEC_TO_TIME( SUM(time_to_sec(r.horesExtres))) AS 'Hores extres'"
        Sql = Sql & ", COUNT(r.id) AS 'Total dies'"
        Sql = Sql & ", SUM(r.vacances) AS 'Total vacances'"
        Sql = Sql & ", SUM(r.modificat) AS 'Total modificat'" '5
        Sql = Sql & ", SUM(r.error) AS 'Total error'"
        Sql = Sql & ", SUM(r.baixa) AS 'Total baixa'"
        Sql = Sql & "FROM registre r WHERE"
        Sql = Sql & " r.data >= '" & DBMysql.convertDate(DataInici) & "'"
        Sql = Sql & " AND r.data <= '" & DBMysql.convertDate(DataFinal) & "'"
        If Treballador <> "Tots" Then
            Sql = Sql & " AND r.treballador = '" & Treballador & "'"
        End If

        If DBMysql.test Then
            MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBMysql.doSelect(Sql)

        Return ds
    End Function


    ''' <summary>Funcio que busca una fitxada per l'ID de la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Id"><c>String</c>: ID de la fitxada a buscar</param>
    ''' <returns><c>FitxadaClass</c>: Fitxada trobada o una en blanc en cas contrari</returns>
    Public Function SearchById(ByVal Id As String) As FitxadaClass
        Dim fitxa As New FitxadaClass
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT id"
        Sql = Sql & ", codiTreballador"
        Sql = Sql & ", data"
        Sql = Sql & ", treballador"
        Sql = Sql & ", entrada"
        Sql = Sql & ", sortida" '5
        Sql = Sql & ", entrada2"
        Sql = Sql & ", sortida2"
        Sql = Sql & ", vacances"
        Sql = Sql & ", modificat"
        Sql = Sql & ", dataModificat" '10
        Sql = Sql & ", error"
        Sql = Sql & ", totalHores"
        Sql = Sql & ", horesNormals"
        Sql = Sql & ", horesExtres"
        Sql = Sql & ", diaSemana" '15
        Sql = Sql & ", notes"
        Sql = Sql & ", log"
        Sql = Sql & ", baixa"
        Sql = Sql & " FROM registre r WHERE"
        Sql = Sql & " r.id = '" & Id & "'"


        If DBMysql.test Then
            MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DBMysql.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            fitxa.Id = ds.Tables(0).Rows(0)(0)
            fitxa.CodiTreballador = ds.Tables(0).Rows(0)(1)
            fitxa.Data = ds.Tables(0).Rows(0)(2)
            fitxa.Treballador = ds.Tables(0).Rows(0)(3)
            fitxa.Entrada = ds.Tables(0).Rows(0)(4)
            fitxa.Sortida = ds.Tables(0).Rows(0)(5)
            fitxa.Entrada2 = ds.Tables(0).Rows(0)(6)
            fitxa.Sortida2 = ds.Tables(0).Rows(0)(7)
            fitxa.Vacances = ds.Tables(0).Rows(0)(8)
            fitxa.Modificat = ds.Tables(0).Rows(0)(9)
            fitxa.DataModificat = ds.Tables(0).Rows(0)(10)
            fitxa.Fail = ds.Tables(0).Rows(0)(11)
            fitxa.totalHores = ds.Tables(0).Rows(0)(12)
            fitxa.horesNormals = ds.Tables(0).Rows(0)(13)
            fitxa.horesExtres = ds.Tables(0).Rows(0)(14)
            fitxa.diaSemana = ds.Tables(0).Rows(0)(15)
            If IsDBNull(ds.Tables(0).Rows(0)(16)) = False Then
                fitxa.Notes = ds.Tables(0).Rows(0)(16)
            End If
            fitxa.Log = ds.Tables(0).Rows(0)(17)
            fitxa.Baixa = ds.Tables(0).Rows(0)(18)
        End If

            Return fitxa

    End Function

    ''' <summary>Funcio que guarda les modificacions d'una fitxada a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte fitxada a guardar</param>
    Public Sub ModificarFitxada(ByVal fitxa As FitxadaClass)
        Dim sql As String

        sql = "UPDATE registre SET "
        'sql = sql & "codiTreballador = '" & fitxa.CodiTreballador & "'"
        sql = sql & "data = '" & DBMysql.convertDate(fitxa.Data) & "'"
        'sql = sql & ", treballador = '" & fitxa.Treballador & "'"
        If Fitxades.dataOk(fitxa.Entrada) Then
            sql = sql & ", entrada = '" & DBMysql.convertDateTime(fitxa.Entrada) & "'"
        Else
            sql = sql & ", entrada = '0001-01-01 00:00:00'"
        End If

        If Fitxades.dataOk(fitxa.Sortida) Then
            sql = sql & ", sortida = '" & DBMysql.convertDateTime(fitxa.Sortida) & "'"
        Else
            sql = sql & ", sortida = '0001-01-01 00:00:00'"
        End If

        If Fitxades.dataOk(fitxa.Entrada2) Then
            sql = sql & ", entrada2 = '" & DBMysql.convertDateTime(fitxa.Entrada2) & "'"
        Else
            sql = sql & ", entrada2 = '0001-01-01 00:00:00'"
        End If

        If Fitxades.dataOk(fitxa.Sortida2) Then
            sql = sql & ", sortida2 = '" & DBMysql.convertDateTime(fitxa.Sortida2) & "'"
        Else
            sql = sql & ", sortida2 = '0001-01-01 00:00:00'"
        End If

        sql = sql & ", vacances = '" & fitxa.Vacances & "'"
        sql = sql & ", modificat = '" & fitxa.Modificat & "'"
        sql = sql & ", dataModificat = '" & DBMysql.convertDateTime(fitxa.DataModificat) & "'"
        sql = sql & ", error = '" & fitxa.Fail & "'"
        sql = sql & ", totalHores = '" & fitxa.totalHores.ToString() & "'"
        sql = sql & ", horesNormals = '" & fitxa.horesNormals.ToString() & "'"
        sql = sql & ", horesExtres = '" & fitxa.horesExtres.ToString() & "'"
        sql = sql & ", diaSemana = '" & fitxa.diaSemana.ToString() & "'"
        sql = sql & ", notes = '" & fitxa.Notes & "'"
        sql = sql & ", baixa = '" & fitxa.Baixa & "'"

        sql = sql & " WHERE id = " & fitxa.Id

        If DBMysql.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Modifico dades. sql: " & sql, "DEBUG")

        DBMysql.doOperation(sql)
    End Sub

    ''' <summary>Funcio que crea una nova fitxada a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte fitxada a guardar</param>
    Public Sub InsertarFitxada(ByVal fitxa As FitxadaClass)
        Dim sql As String

        sql = "INSERT INTO registre ("
        sql = sql & "codiTreballador"
        sql = sql & ", data"
        sql = sql & ", treballador"
        sql = sql & ", entrada"
        sql = sql & ", sortida" '5
        sql = sql & ", entrada2"
        sql = sql & ", sortida2"
        sql = sql & ", vacances"
        sql = sql & ", modificat"
        sql = sql & ", dataModificat" '10
        sql = sql & ", error"
        sql = sql & ", totalHores"
        sql = sql & ", horesNormals"
        sql = sql & ", horesExtres"
        sql = sql & ", diaSemana" '15
        sql = sql & ", notes"
        sql = sql & ", log"
        sql = sql & ", baixa"

        sql = sql & ") VALUES ("

        sql = sql & "'" & fitxa.CodiTreballador & "', "
        sql = sql & "'" & DBMysql.convertDate(fitxa.Data) & "', "
        sql = sql & "'" & fitxa.Treballador & "', "

        If Fitxades.dataOk(fitxa.Entrada) Then
            sql = sql & "'" & DBMysql.convertDateTime(fitxa.Entrada) & "', "
        Else
            sql = sql & "'0001-01-01 00:00:00', "
        End If

        If Fitxades.dataOk(fitxa.Sortida) Then
            sql = sql & "'" & DBMysql.convertDateTime(fitxa.Sortida) & "', " '5
        Else
            sql = sql & "'0001-01-01 00:00:00', " '5
        End If

        If Fitxades.dataOk(fitxa.Entrada2) Then
            sql = sql & "'" & DBMysql.convertDateTime(fitxa.Entrada2) & "', "
        Else
            sql = sql & "'0001-01-01 00:00:00', "
        End If

        If Fitxades.dataOk(fitxa.Sortida2) Then
            sql = sql & "'" & DBMysql.convertDateTime(fitxa.Sortida2) & "', "
        Else
            sql = sql & "'0001-01-01 00:00:00', "
        End If

        sql = sql & "'" & fitxa.Vacances & "', "
        sql = sql & "'" & fitxa.Modificat & "', "
        sql = sql & "'" & DBMysql.convertDateTime(fitxa.DataModificat) & "', " '10
        sql = sql & "'" & fitxa.Fail & "', "
        sql = sql & "'" & fitxa.totalHores.ToString() & "', "
        sql = sql & "'" & fitxa.horesNormals.ToString() & "', "
        sql = sql & "'" & fitxa.horesExtres.ToString() & "', "
        sql = sql & "'" & fitxa.diaSemana.ToString() & "', " '15
        sql = sql & "'" & fitxa.Notes & "', "
        sql = sql & "'" & fitxa.Log & "' , "
        sql = sql & "'" & fitxa.Baixa & "'"
        sql = sql & ")"

        If DBMysql.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Inserto dades. sql: " & sql, "DEBUG")

        DBMysql.doOperation(sql)

    End Sub



    ''' <summary>Funcio que construix la casella de l'excel que correspon a una fila i columna</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="row"><c>Integer</c>: Fila de l'excel</param>
    ''' <param name="col"><c>Integer</c>: Columna de l'excel</param>
    ''' <returns><c>String</c>: conversio en format "lletra""numero"</returns>
    Private Function Casella(ByVal row As Integer, ByVal col As Integer) As String
        Return Chr(col + Asc("A")) & (row + 1).ToString
    End Function

    ''' <summary>Funcio que crea i guarda el registre d'un treballador segons filtro</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial a filtrar</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final a filtrar</param>
    ''' <param name="Treballador"><c>String</c>: Nom del treballador a filtrar</param>
    Public Sub GuardarRegistre(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Treballador As String)
        Dim actPath As String = PathRegistres & DataInici.Year.ToString & "-" & DataInici.Month.ToString & "\"
        Dim fname As String = ""
        Dim ds As DataSet
        Dim row As Integer = 0
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        Dim d As Date
        Dim excelRow As Integer = 9

        Dim ts As TimeSpan

        If DataInici.Month < 10 Then
            actPath = PathRegistres & DataInici.Year.ToString & "-0" & DataInici.Month.ToString & "\"
        End If

        If Not Directory.Exists(actPath) Then
            Directory.CreateDirectory(actPath)
        End If

        fname = actPath & Treballador & ".xlsx"

        '1- busco les dades del treballador
        ds = llistatFitxades(DataInici, DataFinal, Treballador)

        '2- obrec excel
        'Start a new workbook in Excel
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.workbooks.open(TemplateRegistre)
        'oBook = oExcel.Workbooks.Add

        'Add data to cells of the first worksheet in the new workbook
        oSheet = oBook.Worksheets(1)

        '3- poso dades fixes
        'treballador
        oSheet.Range("F3").Value = ds.Tables(0).Rows(row)(1)

        'cif
        oSheet.Range("F4").Value = ""

        'data
        d = ds.Tables(0).Rows(row)(3)
        oSheet.Range("F6").Value = d.ToString("MMM-yyyy")

        '4- poso dades
        While row < ds.Tables(0).Rows.Count
            d = ds.Tables(0).Rows(row)(3)
            excelRow = d.Day + 8

            'entrada
            If ds.Tables(0).Rows(row)(4) <> Nothing Then
                oSheet.Range("B" & excelRow.ToString()).Value = ds.Tables(0).Rows(row)(4).ToString()
            End If

            'sortida
            If ds.Tables(0).Rows(row)(5) <> Nothing Then
                oSheet.Range("C" & excelRow.ToString()).Value = ds.Tables(0).Rows(row)(5).ToString()
            End If

            'entrada2
            If ds.Tables(0).Rows(row)(6) <> Nothing Then
                oSheet.Range("D" & excelRow.ToString()).Value = ds.Tables(0).Rows(row)(6).ToString()
            End If

            'sortida2
            If ds.Tables(0).Rows(row)(7) <> Nothing Then
                oSheet.Range("E" & excelRow.ToString()).Value = ds.Tables(0).Rows(row)(7).ToString()
            End If

            'total hores
            ts = ds.Tables(0).Rows(row)(8)
            oSheet.Range("F" & excelRow.ToString()).Value = ts.ToString()

            'normals. no son vacances ni baixa
            If ds.Tables(0).Rows(row)(11) = 0 And ds.Tables(0).Rows(row)(12) = 0 Then
                ts = ds.Tables(0).Rows(row)(9)
                oSheet.Range("G" & excelRow.ToString()).Value = ts.ToString()
            End If

            'extres
            ts = ds.Tables(0).Rows(row)(10)
            oSheet.Range("H" & excelRow.ToString()).Value = ts.ToString()

            'vacances
            If ds.Tables(0).Rows(row)(11) = 1 Then
                ts = ds.Tables(0).Rows(row)(9)
                oSheet.Range("I" & excelRow.ToString()).Value = ts.ToString()
            End If

            'baixa
            If ds.Tables(0).Rows(row)(12) = 1 Then
                ts = ds.Tables(0).Rows(row)(9)
                oSheet.Range("J" & excelRow.ToString()).Value = ts.ToString()
            End If

            row = row + 1
        End While

        'Save the Workbook and Quit Excel
        oBook.SaveAs(fname)
        oExcel.Quit()

    End Sub

    ''' <summary>Funcio que crea i guarda el registre de tots els treballadors segons filtro</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial a filtrar</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final a filtrar</param>
    Public Sub GuardarTotsRegistres(ByVal DataInici As Date, ByVal DataFinal As Date)
        Dim ds As DataSet
        Dim row As Integer

        ' 1- busco tots els treballadors
        ds = llistatTreballadors(DataInici, DataFinal)

        ' 2- creo els registre
        If ds.Tables(0).Rows.Count > 0 Then ' hi han treballadors
            For row = 0 To ds.Tables(0).Rows.Count - 1
                GuardarRegistre(DataInici, DataFinal, ds.Tables(0).Rows(row)(0))
            Next
        End If

    End Sub

End Class
