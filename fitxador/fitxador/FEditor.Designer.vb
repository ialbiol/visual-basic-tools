﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FEditor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DTSortida = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DTData = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DTSortida2 = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.DTEntrada2 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.RTNotes = New System.Windows.Forms.RichTextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.LDiaSemana = New System.Windows.Forms.Label()
        Me.LDataModificat = New System.Windows.Forms.Label()
        Me.LModificat = New System.Windows.Forms.Label()
        Me.LError = New System.Windows.Forms.Label()
        Me.LHoresExtres = New System.Windows.Forms.Label()
        Me.LHoresNormals = New System.Windows.Forms.Label()
        Me.LTotalHores = New System.Windows.Forms.Label()
        Me.DTEntrada = New System.Windows.Forms.DateTimePicker()
        Me.LLog = New System.Windows.Forms.RichTextBox()
        Me.CBTreballador = New System.Windows.Forms.ComboBox()
        Me.BGuardar = New System.Windows.Forms.Button()
        Me.BCancelar = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.BTreureError = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BDinar2 = New System.Windows.Forms.Button()
        Me.BDinar1 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.BHores9 = New System.Windows.Forms.Button()
        Me.BHores8 = New System.Windows.Forms.Button()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.BVacances9 = New System.Windows.Forms.Button()
        Me.BVacances8 = New System.Windows.Forms.Button()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.BBaixa9 = New System.Windows.Forms.Button()
        Me.BBaixa8 = New System.Windows.Forms.Button()
        Me.LVacances = New System.Windows.Forms.Label()
        Me.LBaixa = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Hora entrada"
        '
        'DTSortida
        '
        Me.DTSortida.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.DTSortida.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTSortida.Location = New System.Drawing.Point(138, 131)
        Me.DTSortida.Name = "DTSortida"
        Me.DTSortida.Size = New System.Drawing.Size(200, 21)
        Me.DTSortida.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 137)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 15)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Hora sortida"
        '
        'DTData
        '
        Me.DTData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTData.Location = New System.Drawing.Point(138, 41)
        Me.DTData.Name = "DTData"
        Me.DTData.Size = New System.Drawing.Size(200, 21)
        Me.DTData.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(29, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Data"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(29, 185)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(70, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Treballador"
        '
        'DTSortida2
        '
        Me.DTSortida2.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.DTSortida2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTSortida2.Location = New System.Drawing.Point(485, 131)
        Me.DTSortida2.Name = "DTSortida2"
        Me.DTSortida2.Size = New System.Drawing.Size(200, 21)
        Me.DTSortida2.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(376, 137)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 15)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Hora sortida 2"
        '
        'DTEntrada2
        '
        Me.DTEntrada2.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.DTEntrada2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTEntrada2.Location = New System.Drawing.Point(485, 88)
        Me.DTEntrada2.Name = "DTEntrada2"
        Me.DTEntrada2.Size = New System.Drawing.Size(200, 21)
        Me.DTEntrada2.TabIndex = 12
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(376, 94)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 15)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Hora entrada 2"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(29, 240)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 15)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Vacances"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, 295)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 15)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Notes"
        '
        'RTNotes
        '
        Me.RTNotes.Location = New System.Drawing.Point(138, 295)
        Me.RTNotes.Name = "RTNotes"
        Me.RTNotes.Size = New System.Drawing.Size(547, 96)
        Me.RTNotes.TabIndex = 18
        Me.RTNotes.Text = ""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(29, 416)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 15)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Error:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(162, 416)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 15)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "Modificat:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(328, 416)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(89, 15)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "Data modificat:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(376, 185)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 15)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Total hores"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(536, 240)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 15)
        Me.Label15.TabIndex = 23
        Me.Label15.Text = "Hores normals"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(376, 240)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 15)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Hores extres"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(376, 46)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(74, 15)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "Dia semana"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(29, 461)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 15)
        Me.Label18.TabIndex = 26
        Me.Label18.Text = "Log"
        '
        'LDiaSemana
        '
        Me.LDiaSemana.AutoSize = True
        Me.LDiaSemana.Location = New System.Drawing.Point(482, 47)
        Me.LDiaSemana.Name = "LDiaSemana"
        Me.LDiaSemana.Size = New System.Drawing.Size(11, 15)
        Me.LDiaSemana.TabIndex = 27
        Me.LDiaSemana.Text = "-"
        '
        'LDataModificat
        '
        Me.LDataModificat.AutoSize = True
        Me.LDataModificat.Location = New System.Drawing.Point(419, 416)
        Me.LDataModificat.Name = "LDataModificat"
        Me.LDataModificat.Size = New System.Drawing.Size(11, 15)
        Me.LDataModificat.TabIndex = 30
        Me.LDataModificat.Text = "-"
        '
        'LModificat
        '
        Me.LModificat.AutoSize = True
        Me.LModificat.Location = New System.Drawing.Point(226, 416)
        Me.LModificat.Name = "LModificat"
        Me.LModificat.Size = New System.Drawing.Size(11, 15)
        Me.LModificat.TabIndex = 29
        Me.LModificat.Text = "-"
        '
        'LError
        '
        Me.LError.AutoSize = True
        Me.LError.Location = New System.Drawing.Point(69, 416)
        Me.LError.Name = "LError"
        Me.LError.Size = New System.Drawing.Size(11, 15)
        Me.LError.TabIndex = 28
        Me.LError.Text = "-"
        '
        'LHoresExtres
        '
        Me.LHoresExtres.AutoSize = True
        Me.LHoresExtres.Location = New System.Drawing.Point(482, 240)
        Me.LHoresExtres.Name = "LHoresExtres"
        Me.LHoresExtres.Size = New System.Drawing.Size(14, 15)
        Me.LHoresExtres.TabIndex = 33
        Me.LHoresExtres.Text = "0"
        '
        'LHoresNormals
        '
        Me.LHoresNormals.AutoSize = True
        Me.LHoresNormals.Location = New System.Drawing.Point(642, 240)
        Me.LHoresNormals.Name = "LHoresNormals"
        Me.LHoresNormals.Size = New System.Drawing.Size(14, 15)
        Me.LHoresNormals.TabIndex = 32
        Me.LHoresNormals.Text = "0"
        '
        'LTotalHores
        '
        Me.LTotalHores.AutoSize = True
        Me.LTotalHores.Location = New System.Drawing.Point(482, 185)
        Me.LTotalHores.Name = "LTotalHores"
        Me.LTotalHores.Size = New System.Drawing.Size(14, 15)
        Me.LTotalHores.TabIndex = 31
        Me.LTotalHores.Text = "0"
        '
        'DTEntrada
        '
        Me.DTEntrada.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.DTEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTEntrada.Location = New System.Drawing.Point(138, 89)
        Me.DTEntrada.Name = "DTEntrada"
        Me.DTEntrada.Size = New System.Drawing.Size(200, 21)
        Me.DTEntrada.TabIndex = 38
        '
        'LLog
        '
        Me.LLog.Location = New System.Drawing.Point(32, 487)
        Me.LLog.Name = "LLog"
        Me.LLog.ReadOnly = True
        Me.LLog.Size = New System.Drawing.Size(306, 189)
        Me.LLog.TabIndex = 45
        Me.LLog.Text = ""
        '
        'CBTreballador
        '
        Me.CBTreballador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.CBTreballador.Enabled = False
        Me.CBTreballador.FormattingEnabled = True
        Me.CBTreballador.Location = New System.Drawing.Point(138, 182)
        Me.CBTreballador.Name = "CBTreballador"
        Me.CBTreballador.Size = New System.Drawing.Size(200, 23)
        Me.CBTreballador.TabIndex = 46
        '
        'BGuardar
        '
        Me.BGuardar.Location = New System.Drawing.Point(165, 707)
        Me.BGuardar.Name = "BGuardar"
        Me.BGuardar.Size = New System.Drawing.Size(102, 32)
        Me.BGuardar.TabIndex = 48
        Me.BGuardar.Text = "Guardar"
        Me.BGuardar.UseVisualStyleBackColor = True
        '
        'BCancelar
        '
        Me.BCancelar.Location = New System.Drawing.Point(511, 707)
        Me.BCancelar.Name = "BCancelar"
        Me.BCancelar.Size = New System.Drawing.Size(102, 32)
        Me.BCancelar.TabIndex = 47
        Me.BCancelar.Text = "Cancelar"
        Me.BCancelar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(359, 461)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(337, 215)
        Me.TabControl1.TabIndex = 49
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label26)
        Me.TabPage4.Controls.Add(Me.Label27)
        Me.TabPage4.Controls.Add(Me.Label28)
        Me.TabPage4.Controls.Add(Me.BTreureError)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(329, 187)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Errors"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(39, 79)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(116, 15)
        Me.Label26.TabIndex = 49
        Me.Label26.Text = "i calculara les hores"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(39, 61)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(143, 15)
        Me.Label27.TabIndex = 48
        Me.Label27.Text = "Treura la marca del error"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(39, 25)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(211, 15)
        Me.Label28.TabIndex = 47
        Me.Label28.Text = "Utilitzar quan les dades son correctes"
        '
        'BTreureError
        '
        Me.BTreureError.Location = New System.Drawing.Point(42, 121)
        Me.BTreureError.Name = "BTreureError"
        Me.BTreureError.Size = New System.Drawing.Size(102, 32)
        Me.BTreureError.TabIndex = 46
        Me.BTreureError.Text = "Treure error"
        Me.BTreureError.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.BDinar2)
        Me.TabPage1.Controls.Add(Me.BDinar1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(329, 187)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Hores dinar"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(39, 79)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(152, 15)
        Me.Label19.TabIndex = 45
        Me.Label19.Text = "i posara 1 o 2 hores al mig"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(39, 61)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(204, 15)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Moura l'hora de sortida a la sortida2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(269, 15)
        Me.Label1.TabIndex = 43
        Me.Label1.Text = "Utilitzar quan les hores de dinar no s'han indicat"
        '
        'BDinar2
        '
        Me.BDinar2.Location = New System.Drawing.Point(191, 121)
        Me.BDinar2.Name = "BDinar2"
        Me.BDinar2.Size = New System.Drawing.Size(102, 32)
        Me.BDinar2.TabIndex = 42
        Me.BDinar2.Text = "2 Hores Dinar"
        Me.BDinar2.UseVisualStyleBackColor = True
        '
        'BDinar1
        '
        Me.BDinar1.Location = New System.Drawing.Point(42, 121)
        Me.BDinar1.Name = "BDinar1"
        Me.BDinar1.Size = New System.Drawing.Size(102, 32)
        Me.BDinar1.TabIndex = 41
        Me.BDinar1.Text = "1 Hora Dinar"
        Me.BDinar1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.BHores9)
        Me.TabPage2.Controls.Add(Me.BHores8)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(329, 187)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Hores"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(39, 79)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(203, 15)
        Me.Label20.TabIndex = 50
        Me.Label20.Text = "i de 15:00 hasta les hores indicades"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(39, 61)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(258, 15)
        Me.Label21.TabIndex = 49
        Me.Label21.Text = "Creará una entrada a les 8:00, hasta les 13:00"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(39, 25)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(216, 15)
        Me.Label22.TabIndex = 48
        Me.Label22.Text = "Utilitzar quan no hi ha hores indicades"
        '
        'BHores9
        '
        Me.BHores9.Location = New System.Drawing.Point(191, 121)
        Me.BHores9.Name = "BHores9"
        Me.BHores9.Size = New System.Drawing.Size(102, 32)
        Me.BHores9.TabIndex = 47
        Me.BHores9.Text = "9 hores"
        Me.BHores9.UseVisualStyleBackColor = True
        '
        'BHores8
        '
        Me.BHores8.Location = New System.Drawing.Point(42, 121)
        Me.BHores8.Name = "BHores8"
        Me.BHores8.Size = New System.Drawing.Size(102, 32)
        Me.BHores8.TabIndex = 46
        Me.BHores8.Text = "8 hores"
        Me.BHores8.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label23)
        Me.TabPage3.Controls.Add(Me.Label24)
        Me.TabPage3.Controls.Add(Me.Label25)
        Me.TabPage3.Controls.Add(Me.BVacances9)
        Me.TabPage3.Controls.Add(Me.BVacances8)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(329, 187)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Vacances"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(39, 79)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(203, 15)
        Me.Label23.TabIndex = 50
        Me.Label23.Text = "i de 15:00 hasta les hores indicades"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(39, 61)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(258, 15)
        Me.Label24.TabIndex = 49
        Me.Label24.Text = "Creará una entrada a les 8:00, hasta les 13:00"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(39, 25)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(227, 15)
        Me.Label25.TabIndex = 48
        Me.Label25.Text = "Utilitzar per a indicar un dia de vacances"
        '
        'BVacances9
        '
        Me.BVacances9.Location = New System.Drawing.Point(191, 121)
        Me.BVacances9.Name = "BVacances9"
        Me.BVacances9.Size = New System.Drawing.Size(102, 50)
        Me.BVacances9.TabIndex = 47
        Me.BVacances9.Text = "9 hores vacances"
        Me.BVacances9.UseVisualStyleBackColor = True
        '
        'BVacances8
        '
        Me.BVacances8.Location = New System.Drawing.Point(42, 121)
        Me.BVacances8.Name = "BVacances8"
        Me.BVacances8.Size = New System.Drawing.Size(102, 50)
        Me.BVacances8.TabIndex = 46
        Me.BVacances8.Text = "8 hores vacances"
        Me.BVacances8.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label29)
        Me.TabPage5.Controls.Add(Me.Label30)
        Me.TabPage5.Controls.Add(Me.Label31)
        Me.TabPage5.Controls.Add(Me.BBaixa9)
        Me.TabPage5.Controls.Add(Me.BBaixa8)
        Me.TabPage5.Location = New System.Drawing.Point(4, 24)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(329, 187)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Baixes"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(39, 79)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(203, 15)
        Me.Label29.TabIndex = 55
        Me.Label29.Text = "i de 15:00 hasta les hores indicades"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(39, 61)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(258, 15)
        Me.Label30.TabIndex = 54
        Me.Label30.Text = "Creará una entrada a les 8:00, hasta les 13:00"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(39, 25)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(206, 15)
        Me.Label31.TabIndex = 53
        Me.Label31.Text = "Utilitzar per a indicar un dia de baixa"
        '
        'BBaixa9
        '
        Me.BBaixa9.Location = New System.Drawing.Point(191, 121)
        Me.BBaixa9.Name = "BBaixa9"
        Me.BBaixa9.Size = New System.Drawing.Size(102, 50)
        Me.BBaixa9.TabIndex = 52
        Me.BBaixa9.Text = "9 hores baixa"
        Me.BBaixa9.UseVisualStyleBackColor = True
        '
        'BBaixa8
        '
        Me.BBaixa8.Location = New System.Drawing.Point(42, 121)
        Me.BBaixa8.Name = "BBaixa8"
        Me.BBaixa8.Size = New System.Drawing.Size(102, 50)
        Me.BBaixa8.TabIndex = 51
        Me.BBaixa8.Text = "8 hores baixa"
        Me.BBaixa8.UseVisualStyleBackColor = True
        '
        'LVacances
        '
        Me.LVacances.AutoSize = True
        Me.LVacances.Location = New System.Drawing.Point(119, 240)
        Me.LVacances.Name = "LVacances"
        Me.LVacances.Size = New System.Drawing.Size(11, 15)
        Me.LVacances.TabIndex = 50
        Me.LVacances.Text = "-"
        '
        'LBaixa
        '
        Me.LBaixa.AutoSize = True
        Me.LBaixa.Location = New System.Drawing.Point(284, 240)
        Me.LBaixa.Name = "LBaixa"
        Me.LBaixa.Size = New System.Drawing.Size(11, 15)
        Me.LBaixa.TabIndex = 52
        Me.LBaixa.Text = "-"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(194, 240)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(38, 15)
        Me.Label34.TabIndex = 51
        Me.Label34.Text = "Baixa"
        '
        'FEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 760)
        Me.Controls.Add(Me.LBaixa)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.LVacances)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.BGuardar)
        Me.Controls.Add(Me.BCancelar)
        Me.Controls.Add(Me.CBTreballador)
        Me.Controls.Add(Me.LLog)
        Me.Controls.Add(Me.DTEntrada)
        Me.Controls.Add(Me.LHoresExtres)
        Me.Controls.Add(Me.LHoresNormals)
        Me.Controls.Add(Me.LTotalHores)
        Me.Controls.Add(Me.LDataModificat)
        Me.Controls.Add(Me.LModificat)
        Me.Controls.Add(Me.LError)
        Me.Controls.Add(Me.LDiaSemana)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.RTNotes)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.DTSortida2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.DTEntrada2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DTData)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DTSortida)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FEditor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Editor"
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DTSortida As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DTData As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents DTSortida2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents DTEntrada2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents RTNotes As System.Windows.Forms.RichTextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents LDiaSemana As System.Windows.Forms.Label
    Friend WithEvents LDataModificat As System.Windows.Forms.Label
    Friend WithEvents LModificat As System.Windows.Forms.Label
    Friend WithEvents LError As System.Windows.Forms.Label
    Friend WithEvents LHoresExtres As System.Windows.Forms.Label
    Friend WithEvents LHoresNormals As System.Windows.Forms.Label
    Friend WithEvents LTotalHores As System.Windows.Forms.Label
    Friend WithEvents DTEntrada As System.Windows.Forms.DateTimePicker
    Friend WithEvents LLog As System.Windows.Forms.RichTextBox
    Friend WithEvents CBTreballador As System.Windows.Forms.ComboBox
    Friend WithEvents BGuardar As System.Windows.Forms.Button
    Friend WithEvents BCancelar As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BDinar2 As System.Windows.Forms.Button
    Friend WithEvents BDinar1 As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents BHores9 As System.Windows.Forms.Button
    Friend WithEvents BHores8 As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents BVacances9 As System.Windows.Forms.Button
    Friend WithEvents BVacances8 As System.Windows.Forms.Button
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents BTreureError As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents BBaixa9 As System.Windows.Forms.Button
    Friend WithEvents BBaixa8 As System.Windows.Forms.Button
    Friend WithEvents LVacances As System.Windows.Forms.Label
    Friend WithEvents LBaixa As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
End Class
