﻿
''' <summary>Formulari d'edicio de dades</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>controller <c>Controller</c></term><description>Objecte en tota la logica de funcionament del programa</description>
'''   </item>
'''   <item>
'''      <term>Fitxada <c>FitxadaClass</c></term><description>Objecte base que representa una fitxada</description>
'''   </item>
'''   <item>
'''      <term>errorData <c>Boolean = False</c></term><description>Indica si hi ha algun error en les dades modificades</description>
'''   </item>
'''   <item>
'''      <term>mostrant <c>Boolean = False</c></term><description>Controla si s'esta al proces de mostrar dades o no. serveix per a eliminar events innecesaris</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FEditor

    Dim controller As Controller
    Dim Fitxada As FitxadaClass

    Dim errorData As Boolean = False
    Dim mostrant As Boolean = False

#Region "events formulari"

    ''' <summary>Event formulari Close</summary>
    ''' <remarks>Gestiona les ultimes accions que ha de fer el controller
    ''' </remarks>
    Private Sub FEditor_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: CANCELO EDICIO!!", "EDIT RESULT")
    End Sub

    ''' <summary>Event formulari Load</summary>
    ''' <remarks>Gestiona les primeres accions que ha de fer el controller
    ''' </remarks>
    Private Sub FEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        omplirCB()

        mostrarFitxa()
    End Sub

#End Region

#Region "events modificar dades"

    ''' <summary>Event Data Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la data de la fitxada
    ''' </remarks>
    Private Sub DTData_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTData.ValueChanged
        If mostrant = False Then
            If controller.BuscarData(DTData.Value, CBTreballador.SelectedItem.ToString) = True Then
                MessageBox.Show("La data seleccionada ja existeix per a este treballador.", "Info", MessageBoxButtons.OK)
                errorData = True
            Else
                errorData = False
                controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: camp DATA de " & Fitxada.Data.ToShortDateString() & " a " & DTData.Value.ToShortDateString(), "EDIT")

                Fitxada = controller.Fitxades.ModificarData(Fitxada, DTData.Value)

                mostrarFitxa()
            End If
        End If

    End Sub

    ''' <summary>Event Entrada Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia l'entrada de la fitxada
    ''' </remarks>
    Private Sub DTEntrada_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTEntrada.ValueChanged
        If mostrant = False Then
            controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: camp ENTRADA de " & Fitxada.Entrada.ToString() & " a " & DTEntrada.Value.ToString(), "EDIT")
            Fitxada.Entrada = DTEntrada.Value
            calculateHores()
        End If
    End Sub

    ''' <summary>Event Sortida Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la sortida de la fitxada
    ''' </remarks>
    Private Sub DTSortida_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTSortida.ValueChanged
        If mostrant = False Then
            controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: camp SORTIDA de " & Fitxada.Sortida.ToString() & " a " & DTSortida.Value.ToString(), "EDIT")
            Fitxada.Sortida = DTSortida.Value
            calculateHores()
        End If
    End Sub

    ''' <summary>Event Entrada2 Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la segona entrada de la fitxada
    ''' </remarks>
    Private Sub DTEntrada2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTEntrada2.ValueChanged
        If mostrant = False Then
            controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: camp ENTRADA2 de " & Fitxada.Entrada2.ToString() & " a " & DTEntrada2.Value.ToString(), "EDIT")
            Fitxada.Entrada2 = DTEntrada2.Value
            calculateHores()
        End If
    End Sub

    ''' <summary>Event Sortida2 Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la segona sortida de la fitxada
    ''' </remarks>
    Private Sub DTSortida2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTSortida2.ValueChanged
        If mostrant = False Then
            controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: camp SORTIDA2 de " & Fitxada.Sortida2.ToString() & " a " & DTSortida2.Value.ToString(), "EDIT")
            Fitxada.Sortida2 = DTSortida2.Value
            calculateHores()
        End If
    End Sub

#End Region

#Region "events modificacions a les dades"

    ''' <summary>Event Dinar en 1 hora Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar una hora per a dinar
    ''' </remarks>
    Private Sub BDinar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BDinar1.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.FerDinar(Fitxada, 1)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto DINAR 1 HORA de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event Dinar en 2 hores Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar dos hores per a dinar
    ''' </remarks>
    Private Sub BDinar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BDinar2.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.FerDinar(Fitxada, 2)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto DINAR 2 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 8 hores treballades Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 8 hores treballades
    ''' </remarks>
    Private Sub BHores8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BHores8.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarHores(Fitxada, 8)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto 8 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 9 hores treballades Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 9 hores treballades
    ''' </remarks>
    Private Sub BHores9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BHores9.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarHores(Fitxada, 9)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto 9 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 8 hores vacances Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 8 hores vacances
    ''' </remarks>
    Private Sub BVacances8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BVacances8.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarVacances(Fitxada, 8)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto VACANCES 8 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 9 hores vacances Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 9 hores vacances
    ''' </remarks>
    Private Sub BVacances9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BVacances9.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarVacances(Fitxada, 9)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto VACANCES 9 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 8 hores baixa Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 8 hores baixa
    ''' </remarks>
    Private Sub BBaixa8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBaixa8.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarBaixa(Fitxada, 8)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto BAIXA 8 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event 9 hores baixa Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar 9 hores baixa
    ''' </remarks>
    Private Sub BBaixa9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BBaixa9.Click
        Dim str As String

        str = Fitxada.DatesToString()
        Fitxada = controller.Fitxades.PosarBaixa(Fitxada, 9)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto BAIXA 9 HORES de " & str & " a " & Fitxada.DatesToString(), "EDIT")

        mostrarFitxa()
    End Sub

    ''' <summary>Event boto no hi ha error Click</summary>
    ''' <remarks>Gestiona les operacions per a indicar que la fitxada no te cap error
    ''' </remarks>
    Private Sub BTreureError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTreureError.Click
        Fitxada = controller.Fitxades.TreureError(Fitxada)
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: boto TREURE ERROR", "EDIT")

        mostrarFitxa()
    End Sub

#End Region


#Region "events operacions en les dades"

    ''' <summary>Event boto guardar dades Click</summary>
    ''' <remarks>Gestiona les operacions per a guardar o modificar les dades si no hi ha cap error
    ''' </remarks>
    Private Sub BGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGuardar.Click
        If errorData Then
            MessageBox.Show("La data seleccionada ja existeix per a este treballador.", "Info", MessageBoxButtons.OK)
        Else
            controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: GUARDO DADES!!", "EDIT RESULT")
            Fitxada = controller.Fitxades.MarcarModificat(Fitxada)

            If Fitxada.Id = 0 Then
                controller.InsertarFitxada(Fitxada)
            Else
                controller.ModificarFitxada(Fitxada)
            End If

            Me.Close()
        End If

    End Sub

    ''' <summary>Event boto cancelar edicio Click</summary>
    ''' <remarks>Gestiona les operacions per a cancelar l'edicio
    ''' </remarks>
    Private Sub BCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCancelar.Click
        controller.Log.Add("Modifico la fitxada ID[" & Fitxada.Id & "]: CANCELO EDICIO!!", "EDIT RESULT")
        Me.Close()
    End Sub

#End Region

#Region "funcions propies"

    ''' <summary>Funcio que mostra les dades de la fitxada</summary>
    ''' <remarks>Omple els camps per a poder editar les dades de la fitxada
    ''' </remarks>
    Private Sub mostrarFitxa()
        Dim diaSemana = New String() {"diumenge", "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte"}
        Dim siNo = New String() {"No", "Si"}

        mostrant = True

        DTData.Value = Fitxada.Data

        If Fitxada.Entrada <> Nothing Then
            DTEntrada.Value = Fitxada.Entrada
        Else
            DTEntrada.Value = Fitxada.Data
        End If

        If Fitxada.Sortida <> Nothing Then
            DTSortida.Value = Fitxada.Sortida
        Else
            DTSortida.Value = Fitxada.Data
        End If

        If Fitxada.Entrada2 <> Nothing Then
            DTEntrada2.Value = Fitxada.Entrada2
        Else
            DTEntrada2.Value = Fitxada.Data
        End If

        If Fitxada.Sortida2 <> Nothing Then
            DTSortida2.Value = Fitxada.Sortida2
        Else
            DTSortida2.Value = Fitxada.Data
        End If

        'TCodiTreballador.Text = Fitxada.CodiTreballador
        'TTreballador.Text = Fitxada.Treballador
        CBTreballador.SelectedItem = Fitxada.Treballador
        LVacances.Text = siNo(Fitxada.Vacances)
        LBaixa.Text = siNo(Fitxada.Baixa)
        RTNotes.Text = Fitxada.Notes


        LDiaSemana.Text = diaSemana(Fitxada.diaSemana)
        LError.Text = siNo(Fitxada.Fail)
        LModificat.Text = siNo(Fitxada.Modificat)
        LDataModificat.Text = Fitxada.DataModificat.ToString()
        LTotalHores.Text = Math.Round(Fitxada.totalHores.TotalHours, 2, MidpointRounding.AwayFromZero)
        LHoresNormals.Text = Math.Round(Fitxada.horesNormals.TotalHours, 2, MidpointRounding.AwayFromZero)
        LHoresExtres.Text = Math.Round(Fitxada.horesExtres.TotalHours, 2, MidpointRounding.AwayFromZero)
        LLog.Text = Fitxada.Log.Replace("| ", Microsoft.VisualBasic.ChrW(Keys.Return))

        mostrant = False
    End Sub

    ''' <summary>Funcio inicial per a indicar el Id de la fitxada a modificar</summary>
    ''' <remarks>Esta funcio es crida antes de mostrar el formulari i despues de la seua creacio per a indicar quina fitxada s'ha de modificar
    ''' </remarks>
    ''' <param name="Id"><c>String</c>: ID de la fitxada a modificar</param>
    ''' <param name="control"><c>Controller</c>: Objecte controller que executara totes les operacions</param>
    Public Sub SetIdFitxada(ByVal Id As String, ByVal control As Controller)
        controller = control
        Fitxada = controller.SearchById(Id)
    End Sub

    ''' <summary>Funcio inicial per a indicar l'objecte fitxada a modificar</summary>
    ''' <remarks>Esta funcio es crida antes de mostrar el formulari i despues de la seua creacio per a indicar quina fitxada s'ha de modificar
    ''' </remarks>
    ''' <param name="Fitxa"><c>FitxadaClass</c>: Objecte de la fitxada a modificar / crear</param>
    ''' <param name="control"><c>Controller</c>: Objecte controller que executara totes les operacions</param>
    Public Sub SetFitxada(ByVal Fitxa As FitxadaClass, ByVal control As Controller)
        Fitxada = Fitxa
        controller = control
    End Sub

    ''' <summary>Funcio que busca les dades dels desplegables i els ompli</summary>
    ''' <remarks>modifica les dades dels desplegables segons sigue necesari
    ''' </remarks>
    Private Sub omplirCB()
        Dim dsTreballador As DataSet
        Dim row As Integer

        'omplo el desplegable dels treballadors
        dsTreballador = controller.llistatTreballadors(Now.AddMonths(-1), Now)

        CBTreballador.Items.Clear()
        If dsTreballador.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsTreballador.Tables(0).Rows.Count
                CBTreballador.Items.Add(dsTreballador.Tables(0).Rows(row)(0))
                row = row + 1
            End While

        Else
            CBTreballador.Items.Add("Tots")
        End If

        CBTreballador.SelectedIndex = 0
    End Sub

    ''' <summary>Funcio que recalcula les hores treballades i extres</summary>
    ''' <remarks>Recalcula les hores treballades i extres segons les entrades i sortides
    ''' </remarks>
    Private Sub calculateHores()
        Fitxada = controller.Fitxades.SetOk(Fitxada)
        Fitxada = controller.Fitxades.calculateHores(Fitxada)
        mostrarFitxa()
    End Sub

#End Region

End Class