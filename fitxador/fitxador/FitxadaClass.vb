﻿
''' <summary>Classe base que representa una fitxada</summary>
''' <remarks>Propietats:
''' <list type="table">
'''   <listheader>
'''      <term>Propietat</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>Id <c>Integer</c></term><description>Identificador unic de la fitxada dins la taula de la base de dades</description>
'''   </item>
'''   <item>
'''      <term>CodiTreballador <c>String</c></term><description>Codi del fitxador del treballador en format de 15 digits</description>
'''   </item>
'''   <item>
'''      <term>Data <c>Date</c></term><description>Data en que es va fer la fitxada</description>
'''   </item>
'''   <item>
'''      <term>Treballador <c>String</c></term><description>Nom del treballador</description>
'''   </item>
'''   <item>
'''      <term>Entrada <c>DateTime</c></term><description>Dia i hora de la primera entrada. Recorda que podem tenir entrades i sortides a diferents dies</description>
'''   </item>
'''    <item>
'''      <term>Sortida <c>DateTime</c></term><description>Dia i hora de la primera sortida</description>
'''   </item>
'''    <item>
'''      <term>Entrada2 <c>DateTime</c></term><description>Dia i hora de la segona entrada</description>
'''   </item>
'''    <item>
'''      <term>Sortida2 <c>DateTime</c></term><description>Dia i hora de la segona sortida</description>
'''   </item>
'''    <item>
'''      <term>Vacances <c>Integer</c></term><description>Indica si el treballador esta de vacances (<c>1</c>) o no (<c>0</c>)</description>
'''   </item>
'''    <item>
'''      <term>Modificat <c>Integer</c></term><description>Indica si la fitxada s'ha modificat (<c>1</c>) o no (<c>0</c>)</description>
'''   </item>
'''    <item>
'''      <term>DataModificat <c>DateTime</c></term><description>Quan s'ha modificat la fitxada per ultima vegada</description>
'''   </item>
'''    <item>
'''      <term>Fail <c>Integer</c></term><description>Indica si la fitxada te un error (<c>1</c>) o no (<c>0</c>)</description>
'''   </item>
'''    <item>
'''      <term>totalHores <c>TimeSpan</c></term><description>Total hores treballades</description>
'''   </item>
'''    <item>
'''      <term>horesNormals <c>TimeSpan</c></term><description>Total hores normals</description>
'''   </item>
'''    <item>
'''      <term>horesExtres <c>TimeSpan</c></term><description>Total hores extres</description>
'''   </item>
'''    <item>
'''      <term>diaSemana <c>Integer</c></term><description>Representacio del dia de la semana de la data de la fitxada. 0-> Diumenge, 6-> Dissabte</description>
'''   </item>
'''    <item>
'''      <term>Notes <c>String</c></term><description>Notes propies sobre la fitxada. Comentaris</description>
'''   </item>
'''    <item>
'''      <term>Log <c>String</c></term><description>Registre en totes les entrades importades corresponent a la fitxada</description>
'''   </item>
'''    <item>
'''      <term>Pas <c>Integer</c></term><description>Al importar les dades de l'access necesito saber a quin pas estic (primera entrada, primera sortida, segona entrada o segona sortida), per a insertar les dades correctament</description>
'''   </item>
'''    <item>
'''      <term>TurnoNit <c>Boolean</c></term><description>Controla si es detecta que el treballador fa el turno de nit o no. Serveix per a facilitar l'entrada de dades</description>
'''   </item>
'''     <item>
'''      <term>Baixa <c>Integer</c></term><description>Indica si el treballador esta de baixa (<c>1</c>) o no (<c>0</c>)</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FitxadaClass

    Private s_id As Integer = 0

    Public Property Id() As Integer
        Get
            Return s_id
        End Get
        Set(ByVal id As Integer)
            s_id = id
        End Set
    End Property

    Private s_codiTreballador As String = ""

    Public Property CodiTreballador() As String
        Get
            Return s_codiTreballador
        End Get
        Set(ByVal codiTreballador As String)
            s_codiTreballador = codiTreballador
        End Set
    End Property

    Private s_data As Date = Nothing

    Public Property Data() As Date
        Get
            Return s_data
        End Get
        Set(ByVal data As Date)
            s_data = data
        End Set
    End Property

    Private s_treballador As String = ""

    Public Property Treballador() As String
        Get
            Return s_treballador
        End Get
        Set(ByVal treballador As String)
            s_treballador = treballador
        End Set
    End Property

    Private s_entrada As DateTime = Nothing

    Public Property Entrada() As DateTime
        Get
            Return s_entrada
        End Get
        Set(ByVal entrada As DateTime)
            s_entrada = entrada
        End Set
    End Property

    Private s_sortida As DateTime = Nothing

    Public Property Sortida() As DateTime
        Get
            Return s_sortida
        End Get
        Set(ByVal sortida As DateTime)
            s_sortida = sortida
        End Set
    End Property

    Private s_entrada2 As DateTime = Nothing

    Public Property Entrada2() As DateTime
        Get
            Return s_entrada2
        End Get
        Set(ByVal entrada2 As DateTime)
            s_entrada2 = entrada2
        End Set
    End Property

    Private s_sortida2 As DateTime = Nothing

    Public Property Sortida2() As DateTime
        Get
            Return s_sortida2
        End Get
        Set(ByVal sortida2 As DateTime)
            s_sortida2 = sortida2
        End Set
    End Property

    Private s_vacances As Integer = 0

    Public Property Vacances() As Integer
        Get
            Return s_vacances
        End Get
        Set(ByVal vacances As Integer)
            s_vacances = vacances
        End Set
    End Property

    Private s_modificat As Integer = 0

    Public Property Modificat() As Integer
        Get
            Return s_modificat
        End Get
        Set(ByVal modificat As Integer)
            s_modificat = modificat
        End Set
    End Property

    Private s_dataModificat As DateTime = Nothing

    Public Property DataModificat() As DateTime
        Get
            Return s_dataModificat
        End Get
        Set(ByVal dataModificat As DateTime)
            s_dataModificat = dataModificat
        End Set
    End Property

    Private s_error As Integer = 0

    Public Property Fail() As Integer
        Get
            Return s_error
        End Get
        Set(ByVal fail As Integer)
            s_error = fail
        End Set
    End Property

    Private s_totalHores As TimeSpan = Nothing

    Public Property totalHores() As TimeSpan
        Get
            Return s_totalHores
        End Get
        Set(ByVal totalHores As TimeSpan)
            s_totalHores = totalHores
        End Set
    End Property


    Private s_horesNormals As TimeSpan = Nothing

    Public Property horesNormals() As TimeSpan
        Get
            Return s_horesNormals
        End Get
        Set(ByVal horesNormals As TimeSpan)
            s_horesNormals = horesNormals
        End Set
    End Property


    Private s_horesExtres As TimeSpan = Nothing

    Public Property horesExtres() As TimeSpan
        Get
            Return s_horesExtres
        End Get
        Set(ByVal horesExtres As TimeSpan)
            s_horesExtres = horesExtres
        End Set
    End Property


    Private s_diaSemana As Integer = 0

    Public Property diaSemana() As Integer
        Get
            Return s_diaSemana
        End Get
        Set(ByVal diaSemana As Integer)
            s_diaSemana = diaSemana
        End Set
    End Property


    Private s_notes As String
    Public Property Notes() As String
        Get
            Return s_notes
        End Get
        Set(ByVal value As String)
            s_notes = value
        End Set
    End Property


    Private s_log As String = ""

    Public Property Log() As String
        Get
            Return s_log
        End Get
        Set(ByVal value As String)
            s_log = value
        End Set
    End Property



    Private s_pas As Integer = 0
    Public Property Pas() As Integer
        Get
            Return s_pas
        End Get
        Set(ByVal value As Integer)
            s_pas = value
        End Set
    End Property


    Private s_turnoNit As Boolean = False
    Public Property TurnoNit() As Boolean
        Get
            Return s_turnoNit
        End Get
        Set(ByVal value As Boolean)
            s_turnoNit = value
        End Set
    End Property


    Private s_baixa As Integer = 0
    Public Property Baixa() As Integer
        Get
            Return s_baixa
        End Get
        Set(ByVal value As Integer)
            s_baixa = value
        End Set
    End Property


    ''' <summary>Funcio que neteja els valors de totes les propietats</summary>
    ''' <remarks>Inicialitza totes les propietats de l'objecte al seu valor per defecte
    ''' </remarks>
    Public Sub Clear()
        s_id = 0
        s_codiTreballador = ""
        s_data = Nothing
        s_treballador = ""
        s_entrada = Nothing
        s_sortida = Nothing
        s_entrada2 = Nothing
        s_sortida2 = Nothing
        s_vacances = 0
        s_modificat = 0
        s_dataModificat = Nothing
        s_error = 0
        s_totalHores = Nothing
        s_horesNormals = Nothing
        s_horesExtres = Nothing
        s_diaSemana = 0
        s_notes = ""
        s_log = ""
        s_pas = 0
        s_turnoNit = False
        s_baixa = 0
    End Sub

    ''' <summary>Funcio que busca la ultima fitxada</summary>
    ''' <remarks>Retorna nothing en cas que no trobe cap entrada ni sortida
    ''' </remarks>
    ''' <returns><c>DateTime</c>: ultima fitxada</returns>
    Public Function getUltimaFitxada() As DateTime
        If s_sortida2 <> Nothing Then ' intento sortida 2
            Return s_sortida2
        End If

        If s_entrada2 <> Nothing Then ' intento entrada 2
            Return s_entrada2
        End If

        If s_sortida <> Nothing Then ' intento sortida
            Return s_sortida
        End If

        If s_entrada <> Nothing Then ' intento entrada
            Return s_entrada
        End If

        Return Nothing
    End Function

    ''' <summary>Funcio que mostra les dades en un message box</summary>
    ''' <remarks>S'utilitza per a debugar l'aplicacio
    ''' </remarks>
    Public Sub Show()
        Dim str As String = ""

        str = str & "treballador: " & s_treballador
        str = str & "| entrada: " & s_entrada
        str = str & "| sortida: " & s_sortida
        str = str & "| entrada2: " & s_entrada2
        str = str & "| sortida2: " & s_sortida2
        str = str & "| error: " & s_error
        str = str & "| log: " & s_log

        MessageBox.Show(str, "Error", MessageBoxButtons.OK)
    End Sub

    ''' <summary>Funcio que converteix les dades en una cadena de text</summary>
    ''' <remarks>retorna un string en los valors de les entrades i sortides. Se utilitza als logs per a debugar l'aplicacio
    ''' </remarks>
    ''' <returns><c>String</c>: dada convertida a string</returns>
    Public Function DatesToString() As String
        Dim str As String = ""

        str = str & "entrada: " & s_entrada.ToString()
        str = str & "| sortida: " & s_sortida.ToString()
        str = str & "| entrada2: " & s_entrada2.ToString()
        str = str & "| sortida2: " & s_sortida2.ToString()

        Return str
    End Function

End Class
