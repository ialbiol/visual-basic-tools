﻿
''' <summary>Clase que controla les operacions objectes base <c>FitxadaClass</c></summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>test <c>Boolean = False</c></term><description>Indica si l'objecte esta en mode test o no. En mode test es mostren missatges que ajuden a debugar l'aplicacio</description>
'''   </item>
'''   <item>
'''      <term>fitxades <c>List(Of FitxadaClass)</c></term><description>Llistat de les fitxades del treballador gestionades per l'importador</description>
'''   </item>
'''   <item>
'''      <term>codiTreballador <c>String</c></term><description>Codi del treballador al que pertanyen les fitxades</description>
'''   </item>
'''   <item>
'''      <term>Treballador <c>String</c></term><description>Nom del treballador al que pertanyen les fitxades</description>
'''   </item>
'''    <item>
'''      <term>MIN_HORES_ENTRE_LECTURES <c>integer = 1</c></term><description>Constant en lo numero minim d'hores que han de pasar entre lectures. Les lectures dins d'este marge s'ignoren</description>
'''   </item>
'''    <item>
'''      <term>MIN_MINUTS_DINAR <c>Integer = 30</c></term><description>Constant en los minuts minums per a dinar</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FitxadaController
    Public test As Boolean = False 'test mode.
    'Public test As Boolean = True 'test mode.

    Private fitxades As New List(Of FitxadaClass)

    Private codiTreballador As String
    Private Treballador As String

    Private MIN_HORES_ENTRE_LECTURES = 1
    Private MIN_MINUTS_DINAR = 30 'num minuts minim dinar


    ''' <summary>Funcio que crea una fitxada nova i la posa al llistat de fitxades</summary>
    ''' <remarks>No es posa la data com a entrada perque sino despues detecta que la primera fitxada es un error
    ''' </remarks>
    ''' <param name="data"><c>DateTime</c>: Dia de la fitxada</param>
    ''' <returns><c>FitxadaClass</c>: objecte de la nova fitxada</returns>
    Private Function createFitxa(ByVal Data As DateTime) As FitxadaClass
        Dim f As New FitxadaClass
        f.Clear()
        f.CodiTreballador = codiTreballador
        f.Treballador = Treballador
        f.Data = Data
        f.diaSemana = Data.DayOfWeek
        'no es posa la data com a entrada perque sino despues detecta que la primera fitxada es un error

        fitxades.Add(f)

        Return f
    End Function

    ''' <summary>Funcio que busca la ultima fitxada del llistat o en crea una de nova</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>DateTime</c>: Data de la fitxada</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la ultima fitxada</returns>
    Private Function getLast(ByVal data As DateTime) As FitxadaClass
        If fitxades.Count > 0 Then
            Return fitxades(fitxades.Count - 1)
        Else
            Return createFitxa(data)
        End If
    End Function

    ''' <summary>Funcio que calcula les hores treballades en los diferents tipos</summary>
    ''' <remarks>Marca la fitxada com a Fail si detecta algun error
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function calculateHores(ByVal fitxa As FitxadaClass, Optional ByVal checkError As Boolean = True) As FitxadaClass
        Dim horesNormals As TimeSpan = New TimeSpan(8, 0, 0)

        ' control que estan les sortides de les entrades, sino, fail
            If fitxa.Entrada <> Nothing And fitxa.Sortida = Nothing Then
                fitxa.Fail = 1
            End If

            If fitxa.Entrada2 <> Nothing And fitxa.Sortida2 = Nothing Then
                fitxa.Fail = 1
            End If

        ' si no tinc fail
        If fitxa.Fail = 0 Then
            ' calculo les hores treballades
            If fitxa.Entrada <> Nothing And fitxa.Sortida <> Nothing Then
                fitxa.totalHores = fitxa.Sortida - fitxa.Entrada
            End If

            If fitxa.Entrada2 <> Nothing And fitxa.Sortida2 <> Nothing Then
                fitxa.totalHores = fitxa.totalHores + (fitxa.Sortida2 - fitxa.Entrada2)
            End If

            ' calculo les extres
            If fitxa.diaSemana = 0 Or fitxa.diaSemana = 6 Then ' domenge i dbt. tot extra
                fitxa.horesExtres = fitxa.totalHores
            Else

                If fitxa.totalHores.TotalHours > 8 Then
                    fitxa.horesNormals = horesNormals
                    fitxa.horesExtres = fitxa.totalHores - horesNormals
                Else
                    fitxa.horesNormals = fitxa.totalHores
                End If
            End If

            ' si treballa menos de 8 hores i no son extres -> error
            If checkError Then
                If fitxa.totalHores.TotalHours < 8 And fitxa.horesExtres.TotalHours = Nothing Then
                    fitxa.Fail = 1
                End If
            End If

        End If

        Return fitxa
    End Function






    ''' <summary>Funcio que limpia el controlador</summary>
    ''' <remarks>Limpia el llistat de fitxades
    ''' </remarks>
    Public Sub Clear()
        fitxades.Clear()
    End Sub

    ''' <summary>Funcio que inicialitza el proces d'importacio</summary>
    ''' <remarks>Tambe reseteja tot l'objecte i limpia el llistat de fitxades
    ''' </remarks>
    ''' <param name="Codi"><c>String</c>: Codi del nou treballador a gestionar</param>
    ''' <param name="Nom"><c>String</c>: Nom del nou treballador a gestionar</param>
    Public Sub nouTreballador(ByVal Codi As String, ByVal Nom As String)
        Clear()
        codiTreballador = Codi
        Treballador = Nom
    End Sub


    ''' <summary>Funcio que afegeix una nova data i hora a les fitxades</summary>
    ''' <remarks>Esta es la funcio principal del proces d'importacio de dates. S'ocupa de decidir si la nova Data es una entrada, una sortida, pertant a un altre dia o si s'ha d'ignorar
    ''' </remarks>
    ''' <param name="data"><c>DateTime</c>: Data i hora a gestionar</param>
    Public Sub AddHora(ByVal Data As DateTime)
        Dim hores07_30 As TimeSpan = New TimeSpan(7, 30, 0)
        Dim hora13_30 As TimeSpan = New TimeSpan(13, 30, 0)
        Dim hora17_00 As TimeSpan = New TimeSpan(17, 0, 0)
        Dim hora21_30 As TimeSpan = New TimeSpan(21, 30, 0)
        Dim hores24_00 As TimeSpan = New TimeSpan(24, 0, 0)

        Dim fitxa As FitxadaClass
        Dim ok As Boolean = False

        fitxa = getLast(Data)
        If test Then
            fitxa.Show()
        End If

        ' arrivat aqui poden pasar varies coses
        If fitxa.TurnoNit = False And fitxa.Pas >= 2 Then
            If fitxa.Data.DayOfYear = Data.DayOfYear Then

                ' tinc sortida2 i la nova data te menos 1 hora separacio. actualitzo sortida
                If fitxa.Pas = 4 And fitxa.Entrada2 <> Nothing And fitxa.Sortida2 <> Nothing And fitxa.Sortida2.AddHours(MIN_HORES_ENTRE_LECTURES) > Data Then
                    fitxa.Sortida2 = Data
                    fitxa.Pas = 4
                    ok = True
                End If


                ' no tinc sortida2 i la nova data te mes 1 hora separacio. set
                If fitxa.Pas = 3 And fitxa.Entrada2 <> Nothing And fitxa.Sortida2 = Nothing And fitxa.Entrada2.AddMinutes(MIN_MINUTS_DINAR) < Data Then
                    fitxa.Sortida2 = Data
                    fitxa.Pas = 4
                    ok = True
                End If

                '1- que hagi jornada partida -> la data es la mateixa i la hora mes gran que les 13:30 i menor que les 17:00
                If Data.TimeOfDay >= hora13_30 And Data.TimeOfDay <= hora17_00 And (fitxa.Sortida - fitxa.Entrada) < hores07_30 Then
                    ' no tinc entrada2. set
                    If fitxa.Entrada2 = Nothing Then
                        fitxa.Entrada2 = Data
                        fitxa.Pas = 3
                        ok = True
                    End If
                End If


                '2- que sigue lo turno de nit del dia seguent -> la data es la mateixa, pero la hora es mes gran de les 21:30
                'If Data.TimeOfDay >= hora21_30 Then
                ' no tinc entrada2. set
                'If fitxa.Entrada2 = Nothing Then
                'MessageBox.Show("estic a les 21:30", "Error", MessageBoxButtons.OK)
                'fitxa.Entrada2 = Data
                'fitxa.Pas = 3
                'ok = True
                'End If
                'End If

            Else '3- que sigue lo dia seguent -> la data no es la mateixa. la data sempre sera una entrada
                ' dia diferent, creo una nova fitxada

                fitxa = calculateHores(fitxa)
                fitxa = createFitxa(Data) 'el pas esta a 0, aixi que mes avall es posara la data d'entrada
                ok = True
            End If
        End If

        'gestio fitxa nova al turno de nit
        If fitxa.TurnoNit = True And fitxa.Pas = 2 Then
            ' tinc sortida i la nova data te mes 1 hora separacio. fitxa nova, que es la entrada del dia
            If fitxa.Pas = 2 And fitxa.Sortida <> Nothing And fitxa.Sortida.AddHours(MIN_HORES_ENTRE_LECTURES) < Data And fitxa.Entrada2 = Nothing Then
                fitxa = calculateHores(fitxa)
                fitxa = createFitxa(Data) 'el pas esta a 0, aixi que mes avall es posara la data d'entrada
                ok = True
            End If
        End If

        ' tinc sortida i la nova data te menos 1 hora separacio. actualitzo sortida. sempre agafo la ultima fitxada a la sortida
        If fitxa.Pas = 2 And fitxa.Sortida <> Nothing And fitxa.Sortida.AddHours(MIN_HORES_ENTRE_LECTURES) > Data And fitxa.Entrada2 = Nothing Then
            fitxa.Sortida = Data
            fitxa.Pas = 2
            ok = True
        End If

        ' no tinc sortida i la nova data te mes 1 hora separacio i entrada2 no existeix. set. nomes agafo la primera lectura a les entrades
        If fitxa.Pas = 1 And fitxa.Sortida = Nothing And fitxa.Entrada.AddHours(MIN_HORES_ENTRE_LECTURES) < Data And fitxa.Entrada2 = Nothing Then ' no tinc sortida i la nova data te mes 1 hora separacio. set
            If Data.DayOfYear <> fitxa.Entrada.DayOfYear Then
                ' miro si la primera hora es la sortida del turno de nit del dia anterior
                If (Data - fitxa.Entrada) > hores24_00 Then
                    'ignoro la entrada anterior i uso l'actual com a data de la fitxa
                    fitxa.Data = Data
                    fitxa.Entrada = Data
                    fitxa.Pas = 1
                    ok = True
                Else
                    'si hi han menos de 24 hores de diferencia, la sortida es bona. es turno de nit
                    fitxa.Sortida = Data
                    fitxa.Pas = 2
                    fitxa.TurnoNit = True
                    ok = True
                End If
            Else
                fitxa.Sortida = Data
                fitxa.Pas = 2
                ok = True
            End If
        End If

        ' no tinc entrada. set. esta per seguretat, pero mai s'hauria d'usar
        If fitxa.Pas = 0 And fitxa.Entrada = Nothing Then
            fitxa.Entrada = Data
            fitxa.Pas = 1
            ok = True
        End If

        ' faig el log
        fitxa.Log = fitxa.Log & Data.ToString() & " | "

        If ok = False Then 'error
            fitxa.Fail = 1
        End If

    End Sub

    ''' <summary>Funcio que gestiona els calculs de la ultima data introduida en el proces d'importacio</summary>
    ''' <remarks>Es crida al finalitzar el proces d'importacio per si hi ha algun calcul pendent de fer
    ''' </remarks>
    Public Sub checkUltimaData()
        Dim fitxa As FitxadaClass

        If fitxades.Count > 0 Then
            fitxa = getLast(Now)
            If test Then
                fitxa.Show()
            End If

            fitxa = calculateHores(fitxa)
        End If

    End Sub

    ''' <summary>Funcio que ha de gestionar el post procesament en el proces d'importacio de dates</summary>
    ''' <remarks>TODO - hauria de comprobar les dates dins la semana si son homogenies
    ''' </remarks>
    Public Sub PostProcess()
        ' check si les hores dins una semana cambien molt -> marcar com error



    End Sub


    ''' <summary>Funcio que retorna el numero de fitxades gestionades durant el proces d'importacio</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Integer</c>: numero de fitxades gestionades</returns>
    Public Function Count() As Integer
        Return fitxades.Count
    End Function

    ''' <summary>Funcio que busca una fitxada dins del llistat de fitxades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="item"><c>Integer</c>: Index de la fitxada a retornar</param>
    ''' <returns><c>FitxadaClass</c>: Objecte fitxada demanat</returns>
    Public Function GetItem(ByVal item As Integer) As FitxadaClass
        If item >= 0 And item < fitxades.Count Then
            Return fitxades(item)
        End If

        Return Nothing
    End Function

    ''' <summary>Funcio que inserta la ultima fitxada del treballador que ja esta gestionada</summary>
    ''' <remarks>Esta ultima fitxada es recupera per si la seguent hora a gestionar es una continuacio de lo que ja tenim gestionat
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada</param>
    Public Sub SetUltimaFitxada(ByVal fitxa As FitxadaClass)
        Dim hores24_00 As TimeSpan = New TimeSpan(24, 0, 0)

        fitxa.Pas = 1

        If fitxa.Sortida <> Nothing And fitxa.Entrada2 = Nothing Then
            fitxa.Pas = 2

            If fitxa.Sortida.DayOfYear <> fitxa.Entrada.DayOfYear Then
                ' miro si la primera hora es la sortida del turno de nit del dia anterior
                If (fitxa.Sortida - fitxa.Entrada) < hores24_00 Then
                    'si hi han menos de 24 hores de diferencia, la sortida es bona. es turno de nit
                    fitxa.TurnoNit = True
                End If
            End If

        End If

        If fitxa.Entrada2 <> Nothing And fitxa.Sortida2 = Nothing Then
            fitxa.Pas = 3
        End If

        If fitxa.Entrada2 <> Nothing And fitxa.Sortida2 <> Nothing Then
            fitxa.Pas = 4
        End If

        fitxades.Add(fitxa)

    End Sub


    ''' <summary>Funcio per a mostrar les dades en proces en un dataGrid</summary>
    ''' <remarks>NO S'USA!!! es va fer per a debugar el proces d'importacio. Es mante per a futures modificacions
    ''' </remarks>
    ''' <param name="DGDebug"><c>DataGridView</c>: DataGrid on mostrar les dades</param>
    Public Sub debug(ByVal DGDebug As DataGridView)
        Dim row As Integer = 0

        DGDebug.RowCount = fitxades.Count + 1
        DGDebug.ColumnCount = 10

        For Each d As FitxadaClass In fitxades
            DGDebug.Item(0, row).Value = d.CodiTreballador
            DGDebug.Item(1, row).Value = "" 'd.Treballador
            DGDebug.Item(2, row).Value = d.Entrada
            DGDebug.Item(3, row).Value = d.Sortida
            DGDebug.Item(4, row).Value = d.Entrada2
            DGDebug.Item(5, row).Value = d.Sortida2
            DGDebug.Item(6, row).Value = d.totalHores
            DGDebug.Item(7, row).Value = d.horesNormals
            DGDebug.Item(8, row).Value = d.horesExtres
            DGDebug.Item(9, row).Value = d.Fail & " | " & d.Log

            row = row + 1
        Next

    End Sub

    ''' <summary>Funcio que indica si una data es correcta</summary>
    ''' <remarks>Al modificar una entrada o sortida, si es posa l'hora, minuts i segons a 0, s'indica que s'ha eliminat la data
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data a comrpobar</param>
    ''' <returns><c>Boolean</c>: indica si la data es correcta o no</returns>
    Public Function dataOk(ByVal data As Date) As Boolean
        If data.Hour = 0 And data.Minute = 0 And data.Second = 0 Then
            Return False
        End If

        Return True
    End Function


    ''' <summary>Funcio que marca la fitxada com a modificada</summary>
    ''' <remarks>Sempre que es modifica una entrada o sortida, la fitxada es marca com a modificada manualment i es guarda la ultima vegada que s'ha modificat
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function MarcarModificat(ByVal fitxa As FitxadaClass) As FitxadaClass
        fitxa.Modificat = 1
        fitxa.DataModificat = Now

        Return fitxa
    End Function

    ''' <summary>Funcio que indica que la fitxada no te cap error</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function SetOk(ByVal fitxa As FitxadaClass) As FitxadaClass
        fitxa.Fail = 0

        Return fitxa
    End Function

    ''' <summary>Funcio que modifica la fitxada per incloure hores de dinar</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <param name="hores"><c>Integer</c>: Hores de dinar a incloure</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function FerDinar(ByVal fitxa As FitxadaClass, ByVal Hores As Integer) As FitxadaClass

        fitxa = SetOk(fitxa)
        fitxa = MarcarModificat(fitxa)

        If fitxa.Sortida2 = Nothing Then
            fitxa.Sortida2 = fitxa.Sortida
        End If

        fitxa.Sortida = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 13:00:00"
        If Hores = 1 Then
            fitxa.Entrada2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 14:00:00"
        Else
            fitxa.Entrada2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 15:00:00"
        End If

        fitxa = calculateHores(fitxa)

        Return fitxa
    End Function

    ''' <summary>Funcio que afegeix hores totals a la fitxada</summary>
    ''' <remarks>S'utilitza en fitxades noves per a facilitar la entrada de dades
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <param name="Hores"><c>Integer</c>: Hores a incloure</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function PosarHores(ByVal fitxa As FitxadaClass, ByVal Hores As Integer) As FitxadaClass
        fitxa = SetOk(fitxa)
        fitxa = MarcarModificat(fitxa)

        fitxa.Entrada = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 08:00:00"
        fitxa.Sortida = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 13:00:00"
        fitxa.Entrada2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 15:00:00"
        If Hores = 8 Then
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 18:00:00"
        Else
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 19:00:00"
        End If

        fitxa = calculateHores(fitxa)

        Return fitxa
    End Function

    ''' <summary>Funcio que posa hores de vacances a la fitxada</summary>
    ''' <remarks>S'utilitza a la modificacio o creacio de fitxades per a facilitar la entrada de dades
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <param name="Hores"><c>Integer</c>: Hores a incloure</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function PosarVacances(ByVal fitxa As FitxadaClass, ByVal Hores As Integer) As FitxadaClass
        fitxa = SetOk(fitxa)
        fitxa = MarcarModificat(fitxa)

        fitxa.Vacances = 1
        fitxa.Entrada = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 08:00:00"
        fitxa.Sortida = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 13:00:00"
        fitxa.Entrada2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 15:00:00"
        If Hores = 8 Then
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 18:00:00"
        Else
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 19:00:00"
        End If

        fitxa = calculateHores(fitxa)

        Return fitxa
    End Function

    ''' <summary>Funcio que posa hores de baixa a la fitxada</summary>
    ''' <remarks>S'utilitza a la modificacio o creacio de fitxades per a facilitar l'entrada de dades
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <param name="Hores"><c>Integer</c>: Hores a incloure</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function PosarBaixa(ByVal fitxa As FitxadaClass, ByVal Hores As Integer) As FitxadaClass
        fitxa = SetOk(fitxa)
        fitxa = MarcarModificat(fitxa)

        fitxa.Baixa = 1
        fitxa.Entrada = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 08:00:00"
        fitxa.Sortida = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 13:00:00"
        fitxa.Entrada2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 15:00:00"
        If Hores = 8 Then
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 18:00:00"
        Else
            fitxa.Sortida2 = fitxa.Data.Day & "/" & fitxa.Data.Month & "/" & fitxa.Data.Year & " 19:00:00"
        End If

        fitxa = calculateHores(fitxa)

        Return fitxa
    End Function

    ''' <summary>Funcio que treu els errors de la fitxada i recalcula els totals de les hores</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function TreureError(ByVal fitxa As FitxadaClass) As FitxadaClass
        fitxa = SetOk(fitxa)

        fitxa = calculateHores(fitxa, False)

        Return fitxa
    End Function

    ''' <summary>Funcio que modifica la data de la fitxada</summary>
    ''' <remarks>Tambe modifica totes les dates de les entrades i sortides en correspondencia. Mante les sortides del dia seguent
    ''' </remarks>
    ''' <param name="fitxa"><c>FitxadaClass</c>: Objecte en la fitxada a modificar</param>
    ''' <param name="data"><c>String</c>: Nova data</param>
    ''' <returns><c>FitxadaClass</c>: Objecte en la fitxada modificada</returns>
    Public Function ModificarData(ByVal fitxa As FitxadaClass, ByVal Data As Date) As FitxadaClass
        Dim nextDay As Date = Data.AddDays(1)

        fitxa = SetOk(fitxa)
        fitxa = MarcarModificat(fitxa)

        fitxa.Data = Data.Date
        fitxa.diaSemana = Data.DayOfWeek

        'check que les hores tinguen la mateixa data o lo dia seguent
        If fitxa.Entrada <> Nothing And fitxa.Entrada.Date <> fitxa.Data.Date Then
            fitxa.Entrada = fitxa.Data.ToShortDateString & " " & fitxa.Entrada.ToShortTimeString
        End If

        If fitxa.Sortida <> Nothing Then
            If fitxa.Sortida.Date <> fitxa.Data.Date Then
                If fitxa.Sortida.TimeOfDay > fitxa.Entrada.TimeOfDay Then 'mateix dia
                    fitxa.Sortida = fitxa.Data.ToShortDateString & " " & fitxa.Sortida.ToShortTimeString
                Else ' dia seguent
                    fitxa.Sortida = nextDay.ToShortDateString & " " & fitxa.Sortida.ToShortTimeString
                End If
            End If
        End If

        If fitxa.Entrada2 <> Nothing Then
            If fitxa.Entrada2.Date <> fitxa.Data.Date Then
                If fitxa.Entrada2.TimeOfDay > fitxa.Sortida.TimeOfDay Then 'mateix dia
                    fitxa.Entrada2 = fitxa.Data.ToShortDateString & " " & fitxa.Entrada2.ToShortTimeString
                Else ' dia seguent
                    fitxa.Entrada2 = nextDay.ToShortDateString & " " & fitxa.Entrada2.ToShortTimeString
                End If
            End If
        End If

        If fitxa.Sortida2 <> Nothing Then
            If fitxa.Sortida2.Date <> fitxa.Data.Date Then
                If fitxa.Sortida2.TimeOfDay > fitxa.Entrada2.TimeOfDay Then 'mateix dia
                    fitxa.Sortida2 = fitxa.Data.ToShortDateString & " " & fitxa.Sortida2.ToShortTimeString
                Else ' dia seguent
                    fitxa.Sortida2 = nextDay.ToShortDateString & " " & fitxa.Sortida2.ToShortTimeString
                End If
            End If
        End If

        fitxa = calculateHores(fitxa)

        Return fitxa
    End Function


End Class
