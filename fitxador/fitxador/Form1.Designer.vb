﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FFitxador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BExcelTots = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BCrear = New System.Windows.Forms.Button()
        Me.BExcel = New System.Windows.Forms.Button()
        Me.CBTreballador = New System.Windows.Forms.ComboBox()
        Me.DTFinal = New System.Windows.Forms.DateTimePicker()
        Me.DTInici = New System.Windows.Forms.DateTimePicker()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DGDades = New System.Windows.Forms.DataGridView()
        Me.Status1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLTotalHores = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLHoresNormals = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLHoresExtres = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel7 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLDiesTreballats = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel8 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel9 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLDiesVacances = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel10 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel11 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLDiesBaixa = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DGDades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Status1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.BExcelTots)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.BCrear)
        Me.Panel1.Controls.Add(Me.BExcel)
        Me.Panel1.Controls.Add(Me.CBTreballador)
        Me.Panel1.Controls.Add(Me.DTFinal)
        Me.Panel1.Controls.Add(Me.DTInici)
        Me.Panel1.Location = New System.Drawing.Point(1, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(239, 807)
        Me.Panel1.TabIndex = 0
        '
        'BExcelTots
        '
        Me.BExcelTots.Location = New System.Drawing.Point(25, 668)
        Me.BExcelTots.Name = "BExcelTots"
        Me.BExcelTots.Size = New System.Drawing.Size(186, 34)
        Me.BExcelTots.TabIndex = 8
        Me.BExcelTots.Text = "Exportar tots a excel"
        Me.BExcelTots.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 182)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 18)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Treballador"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Data final"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Data inici"
        '
        'BCrear
        '
        Me.BCrear.Location = New System.Drawing.Point(25, 727)
        Me.BCrear.Name = "BCrear"
        Me.BCrear.Size = New System.Drawing.Size(186, 34)
        Me.BCrear.TabIndex = 4
        Me.BCrear.Text = "Afegir dia"
        Me.BCrear.UseVisualStyleBackColor = True
        '
        'BExcel
        '
        Me.BExcel.Location = New System.Drawing.Point(25, 611)
        Me.BExcel.Name = "BExcel"
        Me.BExcel.Size = New System.Drawing.Size(186, 34)
        Me.BExcel.TabIndex = 3
        Me.BExcel.Text = "Exportar actual a excel"
        Me.BExcel.UseVisualStyleBackColor = True
        '
        'CBTreballador
        '
        Me.CBTreballador.FormattingEnabled = True
        Me.CBTreballador.Location = New System.Drawing.Point(8, 203)
        Me.CBTreballador.Name = "CBTreballador"
        Me.CBTreballador.Size = New System.Drawing.Size(218, 26)
        Me.CBTreballador.TabIndex = 2
        '
        'DTFinal
        '
        Me.DTFinal.Location = New System.Drawing.Point(7, 123)
        Me.DTFinal.Name = "DTFinal"
        Me.DTFinal.Size = New System.Drawing.Size(219, 24)
        Me.DTFinal.TabIndex = 1
        '
        'DTInici
        '
        Me.DTInici.Location = New System.Drawing.Point(8, 46)
        Me.DTInici.Name = "DTInici"
        Me.DTInici.Size = New System.Drawing.Size(218, 24)
        Me.DTInici.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.DGDades)
        Me.Panel2.Controls.Add(Me.Status1)
        Me.Panel2.Location = New System.Drawing.Point(246, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1153, 807)
        Me.Panel2.TabIndex = 1
        '
        'DGDades
        '
        Me.DGDades.AllowUserToAddRows = False
        Me.DGDades.AllowUserToDeleteRows = False
        Me.DGDades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGDades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGDades.Location = New System.Drawing.Point(3, 3)
        Me.DGDades.MultiSelect = False
        Me.DGDades.Name = "DGDades"
        Me.DGDades.ReadOnly = True
        Me.DGDades.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DGDades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGDades.Size = New System.Drawing.Size(1145, 779)
        Me.DGDades.TabIndex = 1
        '
        'Status1
        '
        Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.TLTotalHores, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3, Me.TLHoresNormals, Me.ToolStripStatusLabel4, Me.ToolStripStatusLabel5, Me.TLHoresExtres, Me.ToolStripStatusLabel6, Me.ToolStripStatusLabel7, Me.TLDiesTreballats, Me.ToolStripStatusLabel8, Me.ToolStripStatusLabel9, Me.TLDiesVacances, Me.ToolStripStatusLabel10, Me.ToolStripStatusLabel11, Me.TLDiesBaixa})
        Me.Status1.Location = New System.Drawing.Point(0, 783)
        Me.Status1.Name = "Status1"
        Me.Status1.Size = New System.Drawing.Size(1151, 22)
        Me.Status1.TabIndex = 0
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(70, 17)
        Me.ToolStripStatusLabel1.Text = "Total Hores:"
        '
        'TLTotalHores
        '
        Me.TLTotalHores.Name = "TLTotalHores"
        Me.TLTotalHores.Size = New System.Drawing.Size(13, 17)
        Me.TLTotalHores.Text = "0"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel2.Text = "|"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(87, 17)
        Me.ToolStripStatusLabel3.Text = "Hores normals:"
        '
        'TLHoresNormals
        '
        Me.TLHoresNormals.Name = "TLHoresNormals"
        Me.TLHoresNormals.Size = New System.Drawing.Size(13, 17)
        Me.TLHoresNormals.Text = "0"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel4.Text = "|"
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(74, 17)
        Me.ToolStripStatusLabel5.Text = "Hores extres:"
        '
        'TLHoresExtres
        '
        Me.TLHoresExtres.Name = "TLHoresExtres"
        Me.TLHoresExtres.Size = New System.Drawing.Size(13, 17)
        Me.TLHoresExtres.Text = "0"
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel6.Text = "|"
        '
        'ToolStripStatusLabel7
        '
        Me.ToolStripStatusLabel7.Name = "ToolStripStatusLabel7"
        Me.ToolStripStatusLabel7.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel7.Text = "Dies treballats:"
        '
        'TLDiesTreballats
        '
        Me.TLDiesTreballats.Name = "TLDiesTreballats"
        Me.TLDiesTreballats.Size = New System.Drawing.Size(13, 17)
        Me.TLDiesTreballats.Text = "0"
        '
        'ToolStripStatusLabel8
        '
        Me.ToolStripStatusLabel8.Name = "ToolStripStatusLabel8"
        Me.ToolStripStatusLabel8.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel8.Text = "|"
        '
        'ToolStripStatusLabel9
        '
        Me.ToolStripStatusLabel9.Name = "ToolStripStatusLabel9"
        Me.ToolStripStatusLabel9.Size = New System.Drawing.Size(83, 17)
        Me.ToolStripStatusLabel9.Text = "Dies vacances:"
        '
        'TLDiesVacances
        '
        Me.TLDiesVacances.Name = "TLDiesVacances"
        Me.TLDiesVacances.Size = New System.Drawing.Size(13, 17)
        Me.TLDiesVacances.Text = "0"
        '
        'ToolStripStatusLabel10
        '
        Me.ToolStripStatusLabel10.Name = "ToolStripStatusLabel10"
        Me.ToolStripStatusLabel10.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel10.Text = "|"
        '
        'ToolStripStatusLabel11
        '
        Me.ToolStripStatusLabel11.Name = "ToolStripStatusLabel11"
        Me.ToolStripStatusLabel11.Size = New System.Drawing.Size(62, 17)
        Me.ToolStripStatusLabel11.Text = "Dies baixa:"
        '
        'TLDiesBaixa
        '
        Me.TLDiesBaixa.Name = "TLDiesBaixa"
        Me.TLDiesBaixa.Size = New System.Drawing.Size(13, 17)
        Me.TLDiesBaixa.Text = "0"
        '
        'FFitxador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1411, 814)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FFitxador"
        Me.Text = "Fitxador"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DGDades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Status1.ResumeLayout(False)
        Me.Status1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BCrear As System.Windows.Forms.Button
    Friend WithEvents BExcel As System.Windows.Forms.Button
    Friend WithEvents CBTreballador As System.Windows.Forms.ComboBox
    Friend WithEvents DTFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTInici As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DGDades As System.Windows.Forms.DataGridView
    Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLTotalHores As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLHoresNormals As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLHoresExtres As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel7 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLDiesTreballats As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel8 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel9 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLDiesVacances As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents BExcelTots As System.Windows.Forms.Button
    Friend WithEvents ToolStripStatusLabel10 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel11 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLDiesBaixa As System.Windows.Forms.ToolStripStatusLabel

End Class
