﻿
''' <summary>Formulari principal del programa</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>controller <c>Controller</c></term><description>Objecte en tota la logica de funcionament del programa</description>
'''   </item>
'''   <item>
'''      <term>allok <c>Boolean = False</c></term><description>Variable que controla si tot esta ok per a poder carregar dades o esta pendent d'algun camp</description>
'''   </item>
'''   <item>
'''      <term>IdFitxada <c>String</c></term><description>Guarda el ID de la fitxada que s'esta modificant</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FFitxador

    Dim controller As New Controller

    Dim allok = False

    Dim IdFitxada As String

#Region "events formulari"

    ''' <summary>Event formulari Close</summary>
    ''' <remarks>Gestiona les ultimes accions que ha de fer el controller
    ''' </remarks>
    Private Sub FFitxador_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        controller.Dispose()
    End Sub


    ''' <summary>Event formulari Load</summary>
    ''' <remarks>Gestiona les primeres accions que ha de fer el controller
    ''' </remarks>
    Private Sub FFitxador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If controller.checkBegin() Then
            ' setejo valors inicials
            DTInici.Value = Now
            DTInici.MaxDate = Now

            DTFinal.Value = Now
            DTFinal.MaxDate = Now

            ' carrego info pendnet
            controller.carregarInfo()

            ' carrego dades desplegables
            carregarDades()

            'mostro dades
            allok = True
            mostrarDades()
        End If

    End Sub

#End Region

#Region "filtros de busqueda"

    ''' <summary>Event Data inici del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'inici
    ''' </remarks>
    Private Sub DTInici_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTInici.ValueChanged
        controller.Log.Add("Canvio filtro Data Inici a " & DTInici.Value.ToShortDateString(), "INFO")
        carregarDades()
        mostrarDades()
    End Sub

    ''' <summary>Event Data final del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data final
    ''' </remarks>
    Private Sub DTFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTFinal.ValueChanged
        controller.Log.Add("Canvio filtro Data Final a " & DTFinal.Value.ToShortDateString(), "INFO")
        carregarDades()
        mostrarDades()
    End Sub

    ''' <summary>Event combo box del llistat de treballadors del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova dada
    ''' </remarks>
    Private Sub CBTreballador_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBTreballador.SelectedIndexChanged
        controller.Log.Add("Canvio filtro del treballador a " & CBTreballador.SelectedItem.ToString(), "INFO")
        mostrarDades()
    End Sub

#End Region

#Region "modificar dades"

    ''' <summary>Event doble click al llistat de dades</summary>
    ''' <remarks>s'inicia la modificacio de les dades obrint el formulari FEditor
    ''' </remarks>
    Private Sub DGDades_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGDades.CellDoubleClick
        Dim eform As FEditor

        ' mostrar dades per a modificar
        If e.RowIndex >= 0 Then
            IdFitxada = DGDades.Item(0, e.RowIndex).Value.ToString
            controller.Log.Add("Obro fitxa en id " & IdFitxada, "EDIT")

            eform = New FEditor
            eform.SetIdFitxada(IdFitxada, controller)
            eform.ShowDialog()

            IdFitxada = ""
            mostrarDades()
        End If
    End Sub

#End Region


#Region "botons exportacio excel"

    ''' <summary>Event boto exportar en excel el treballador actual</summary>
    ''' <remarks>Exporta el registre del treballador actual
    ''' </remarks>
    Private Sub BExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BExcel.Click
        ' fer excel
        controller.Log.Add("Creo excel de " & CBTreballador.SelectedItem.ToString() & " des de " & DTInici.Value.ToShortDateString() & " a " & DTFinal.Value.ToShortDateString(), "INFO")

        controller.GuardarRegistre(DTInici.Value, DTFinal.Value, CBTreballador.SelectedItem.ToString)

    End Sub

    ''' <summary>Event boto exportar en excel tots els treballadors dins dels dies actuals</summary>
    ''' <remarks>Exporta el registre de tots els treballadors dins de les dates del filtre actual
    ''' </remarks>
    Private Sub BExcelTots_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BExcelTots.Click
        ' fer excel
        controller.Log.Add("Creo tots excels des de " & DTInici.Value.ToShortDateString() & " a " & DTFinal.Value.ToShortDateString(), "INFO")

        controller.GuardarTotsRegistres(DTInici.Value, DTFinal.Value)
    End Sub

#End Region

#Region "botons creacio de nova fitxada"

    ''' <summary>Event click boto crear nova fitxada</summary>
    ''' <remarks>s'inicia la creacio de una nova dada obrint el formulari FEditor
    ''' </remarks>
    Private Sub BCrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCrear.Click
        Dim Fitxada As FitxadaClass = New FitxadaClass
        Dim eform As FEditor

        If CBTreballador.SelectedItem.ToString = "Tots" Then
            MessageBox.Show("Selecciona una data i un treballador per a crear una nova entrada", "Info", MessageBoxButtons.OK)
        Else
            ' crear dia
            controller.Log.Add("Creo fitxada nova de " & CBTreballador.SelectedItem.ToString(), "INFO")

            Fitxada.Data = DTFinal.Value.Date
            Fitxada.Treballador = CBTreballador.SelectedItem.ToString()
            Fitxada.CodiTreballador = controller.BuscarCodiTreballador(Fitxada.Treballador)

            eform = New FEditor
            eform.SetFitxada(Fitxada, controller)
            eform.ShowDialog()

            mostrarDades()
        End If

    End Sub

#End Region

#Region "funcions propies"

    ''' <summary>Funcio que busca les dades dels desplegables i els ompli</summary>
    ''' <remarks>modifica les dades dels desplegables segons les dates dels filtros indicats
    ''' </remarks>
    Private Sub carregarDades()
        Dim dsTreballador As DataSet
        Dim row As Integer

        allok = False

        'omplo el desplegable dels treballadors
        dsTreballador = controller.llistatTreballadors(DTInici.Value, DTFinal.Value)

        CBTreballador.Items.Clear()
        If dsTreballador.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsTreballador.Tables(0).Rows.Count
                CBTreballador.Items.Add(dsTreballador.Tables(0).Rows(row)(0))
                row = row + 1
            End While

        Else
            CBTreballador.Items.Add("Tots")
        End If

        CBTreballador.SelectedIndex = 0


        allok = True
    End Sub

    ''' <summary>Funcio que mostra les dades de les fitxades segons el filtros actuals</summary>
    ''' <remarks>omple els elements DataGrid en les dades procedents de la base de dades segons els filtros actuals
    ''' </remarks>
    Private Sub mostrarDades()
        Dim dsFitxades As DataSet
        Dim dsTotals As DataSet
        Dim i As Integer

        If allok Then
            ' mostro lo llistat de fitxades
            dsFitxades = controller.llistatFitxades(DTInici.Value, DTFinal.Value, CBTreballador.SelectedItem.ToString)
            DGDades.DataSource = dsFitxades
            DGDades.DataMember = "Tabla"

            If DGDades.Rows.Count > 0 Then
                For i = 0 To DGDades.Rows.Count - 1

                    'marco dissabtes i domenges
                    If DGDades.Item(2, i).Value = "dissabte" Or DGDades.Item(2, i).Value = "diumenge" Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.Beige
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If

                    'marco dilluns
                    If DGDades.Item(2, i).Value = "divendres" Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.LightGray
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If

                    'marco modficats
                    If DGDades.Item(13, i).Value = 1 Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.LightSkyBlue
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If

                    'marco vacances
                    If DGDades.Item(11, i).Value = 1 Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.LightGreen
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If

                    'marco baixa
                    If DGDades.Item(12, i).Value = 1 Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.Orange
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If

                    'marco errors
                    If DGDades.Item(14, i).Value = 1 Then
                        DGDades.Rows(i).DefaultCellStyle.BackColor = Color.Salmon
                        'DGPEndentsBasic.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                    End If
                Next
            End If

            'mostro els totals
            dsTotals = controller.llistatTotals(DTInici.Value, DTFinal.Value, CBTreballador.SelectedItem.ToString)

            TLTotalHores.Text = "0"
            TLHoresNormals.Text = "0"
            TLHoresExtres.Text = "0"
            TLDiesTreballats.Text = "0"
            TLDiesVacances.Text = "0"
            TLDiesBaixa.Text = "0"

            If dsTotals.Tables(0).Rows.Count > 0 And IsDBNull(dsTotals.Tables(0).Rows(0)(0)) = False Then
                TLTotalHores.Text = Math.Round(dsTotals.Tables(0).Rows(0)(0).TotalHours(), 2, MidpointRounding.AwayFromZero)
                TLHoresNormals.Text = Math.Round(dsTotals.Tables(0).Rows(0)(1).TotalHours(), 2, MidpointRounding.AwayFromZero)
                TLHoresExtres.Text = Math.Round(dsTotals.Tables(0).Rows(0)(2).TotalHours(), 2, MidpointRounding.AwayFromZero)

                TLDiesTreballats.Text = dsTotals.Tables(0).Rows(0)(3).ToString()
                TLDiesVacances.Text = dsTotals.Tables(0).Rows(0)(4).ToString()
                TLDiesBaixa.Text = dsTotals.Tables(0).Rows(0)(7).ToString()
            End If

        End If
    End Sub

#End Region

End Class
