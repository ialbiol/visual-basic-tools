﻿Imports System
Imports System.IO
Imports System.Windows.Forms

''' <summary>Clase per a gestionar els fitxers de log</summary>
''' <remarks>configuracio:
''' <list type="table">
'''   <listheader>
'''      <term>Variable privada</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>test <c>Boolean = False</c></term><description>Indica si la clase esta en mode test (<c>True</c>) o no (<c>False</c>). 
'''      <para>En mode test la clase mostra una serie de misatges quan executes l'aplicacio</para>
'''      <para>Esta variable es pot usar a altres clases per a afegir mes missatges de control, per exemple:</para>
'''      <para><example><code>
'''        If LOG.test then
'''           MessageBox("Text: " + text, "Debug", MessageBoxButtons.OK)
'''        End If
'''      </code></example></para></description>
'''   </item>
'''   <item>
'''      <term>objStreamWriter <c>StreamWriter</c></term><description>Es l'objecte que gestiona la escritura al fitxer de log</description>
'''   </item>
'''   <item>
'''      <term>userName <c>String</c></term><description>Nom de l'usuari que esta executant l'aplicacio. l'agafa del sistema</description>
'''   </item>
'''</list>
''' </remarks>

Public Class LogClass

    Private objStreamWriter As StreamWriter
    Private userName As String

    Private test As Boolean = False

    ''' <summary>Funcio que inicia el log</summary>
    ''' <remarks>Crea el nom del fitxer que toca segons parametres i la data d'avui. Tambe comproba que la ruta existeix i sino la crea
    ''' </remarks>
    ''' <param name="Name"><c>Optional String = "Log.log"</c>: Nom del fitxer de log despues de la data, que es posa sempre de manera automatica</param>
    ''' <param name="Path"><c>Optional String = ""</c>: Nom de la carpeta on guardar els logs. Per defecte usa la carpeta "log" dins la carpeta on esta l'aplicacio</param>
    Public Sub Init(Optional ByVal Name As String = "Log.log", Optional ByVal Path As String = "")
        Dim appPath As String = Application.StartupPath()
        Dim LogPath As String = ""

        userName = GetUserName()

        If Path = "" Then
            Path = appPath & "\Log"
        End If

        LogPath = Path & "\" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Name

        If test Then
            MessageBox.Show("path: " & LogPath, "Error", MessageBoxButtons.OK)
        End If

        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

        objStreamWriter = New StreamWriter(LogPath, True)
        objStreamWriter.AutoFlush = True

        Add("Log iniciat")
    End Sub

    ''' <summary>Funcio que inserta un texte al log</summary>
    ''' <remarks>De manera automatica inclou l'hora i l'usuari al texte enviat. El Level es posa antes del text
    ''' </remarks>
    ''' <param name="text"><c>String</c>: Texte a afegir al log</param>
    ''' <param name="Level"><c>Optional String = ""</c>: Nivell del log. serveix per a poder separar diferents tipus de missatges de manera visual</param>
    Public Sub Add(ByVal text As String, Optional ByVal Level As String = "")
        Dim str As String = ""

        str = Now.ToShortTimeString() & " "

        If Level <> "" Then
            str = str & Level & " "
        End If

        str = str & "USER: " & userName & " | "
        str = str & text

        If test Then
            MessageBox.Show("texte al log: " & str, "Error", MessageBoxButtons.OK)
        End If

        objStreamWriter.WriteLine(str)
    End Sub

    ''' <summary>Funcio que tanca el log</summary>
    ''' <remarks>S'ha de cridar sempre al tancar l'aplicacio
    ''' </remarks>
    Public Sub Close()
        objStreamWriter.Close()
    End Sub


    ''' <summary>Funcio que busca el nom de l'usuari al sistema</summary>
    ''' <remarks>Retorna el nom de l'usuari loguejat en aquell moment
    ''' </remarks>
    Private Function GetUserName() As String
        If TypeOf My.User.CurrentPrincipal Is 
          Security.Principal.WindowsPrincipal Then
            ' The application is using Windows authentication.
            ' The name format is DOMAIN\USERNAME.
            Dim parts() As String = Split(My.User.Name, "\")
            Dim username As String = parts(1)
            Return username
        Else
            ' The application is using custom authentication.
            Return My.User.Name
        End If
    End Function

End Class
