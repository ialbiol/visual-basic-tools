﻿Imports MySql.Data.MySqlClient


''' <summary>Clase per a gestionar la conexio i peticions a una base de dades MySql</summary>
''' <remarks>configuracio:
''' <list type="table">
'''   <listheader>
'''      <term>Variable privada</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>test <c>Boolean = False</c></term><description>Indica si la clase esta en mode test (<c>True</c>) o no (<c>False</c>). 
'''      <para>En mode test la clase es conecta a una base de dades de test i mostra una serie de misatges quan executes l'aplicacio</para>
'''      <para>Esta variable es pot usar a altres clases per a afegir mes missatges de control, per exemple:</para>
'''      <para><example><code>
'''        If BD.test then
'''           MessageBox("sql: " + sql, "Debug", MessageBoxButtons.OK)
'''        End If
'''      </code></example></para></description>
'''   </item>
'''   <item>
'''      <term>Server <c>String</c></term><description>Es la IP del servidor MySql on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>Port <c>String = "3306"</c></term><description>Es el port del servidor MySql on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>User <c>String</c></term><description>Es l'usuari del servidor MySql on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>Pass <c>String</c></term><description>Es el password del servidor MySql on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>DBName <c>String</c></term><description>Es el nom de la base de dades on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>DBNameDev <c>String</c></term><description>Es el nom de la base de dades de proves on te vols conectar</description>
'''   </item>
'''   <item>
'''      <term>conexion <c>MySqlConnection</c></term><description>Es la clase que gestiona tota la coneccio. Be de la dll de MySql importada al principi de tot</description>
'''   </item>
'''</list>
''' </remarks>

Public Class mysql
    Public test As Boolean = False
    'Public test As Boolean = True

    Private Server As String = "192.168.0.151"
    Private Port As String = "3306"
    Private User As String = "ticket"
    Private Pass As String = "Tw826zF"
    Private DBName As String = "Fitxador"
    Private DBNameDev As String = "Fitxadordev"

    'registre:
    'id
    'codiTreballador
    'data
    'treballador
    'entrada
    'sortida
    'entrada2
    'sortida2
    'vacances
    'modificat
    'dataModificat
    'error
    'totalHores	
    'horesNormals	
    'horesExtres	
    'diaSemana
    'notes
    'log
    'baixa


    Private conexion As MySqlConnection


    ''' <summary>Funcio que conecta a la base de dades MySql</summary>
    ''' <remarks>Mostra un missatge d'error en cas de trobar algun problema, aixi com una excepcio
    ''' </remarks>
    Public Function connect()
        Try
            conexion = New MySqlConnection()

            If test Then
                conexion.ConnectionString =
                "server=" & Server & ";" &
                "user id=" & User & ";" &
                "password=" & Pass & ";" &
                "port=" & Port & ";" &
                "database=" & DBNameDev & ";"
            Else
                conexion.ConnectionString =
                "server=" & Server & ";" &
                "user id=" & User & ";" &
                "password=" & Pass & ";" &
                "port=" & Port & ";" &
                "database=" & DBName & ";"
            End If

            conexion.Open()

            Return True
        Catch ex As Exception
            MsgBox("Error al conectar al servidor MySQL " & vbCrLf & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
            Return False
        End Try
    End Function

    ''' <summary>Funcio que converteix una data al format de la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>String</c>: Cadena de texte en la data a convertir</param>
    ''' <returns><c>String</c>: data convertida a punt per a insertar-la en una sentencia SQL</returns>
    Public Function convertDate(ByVal data As String) As String
        Dim d As Date

        If data = "" Then
            Return ""
        Else
            If IsDate(data) Then
                d = data
                Return convertDate(d)
            Else
                Return ""
            End If
        End If

    End Function

    ''' <summary>Funcio que converteix una data al format de la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: objecte en la data a convertir</param>
    ''' <returns><c>String</c>: data convertida a punt per a insertar-la en una sentencia SQL</returns>
    Public Function convertDate(ByVal data As Date) As String
        Dim dataString As String

        dataString = data.Year.ToString + "/"

        If data.Month < 10 Then
            dataString = dataString + "0" + data.Month.ToString + "/"
        Else
            dataString = dataString + data.Month.ToString + "/"
        End If

        If data.Day < 10 Then
            dataString = dataString + "0" + data.Day.ToString
        Else
            dataString = dataString + data.Day.ToString
        End If

        Return dataString

    End Function

    ''' <summary>Funcio que converteix una data al format de la base de dades, incloent l'hora</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Objecte en la data a convertir</param>
    ''' <returns><c>String</c>: data convertida a punt per a insertar-la en una sentencia SQL</returns>
    Public Function convertDateTime(ByVal data As Date) As String
        Dim dataString As String

        dataString = convertDate(data)

        dataString = dataString + " "

        If data.Hour < 10 Then
            dataString = dataString + "0" + data.Hour.ToString + ":"
        Else
            dataString = dataString + data.Hour.ToString + ":"
        End If

        If data.Minute < 10 Then
            dataString = dataString + "0" + data.Minute.ToString + ":"
        Else
            dataString = dataString + data.Minute.ToString + ":"
        End If

        If data.Second < 10 Then
            dataString = dataString + "0" + data.Second.ToString
        Else
            dataString = dataString + data.Second.ToString
        End If

        Return dataString

    End Function

    ''' <summary>Funcio que retorna el numero de registres que retorna una consulta</summary>
    ''' <remarks> No l'he usat mai. es va copiar el primer dia i esta per si algun dia fa falta
    ''' </remarks>
    ''' <param name="dr"><c>MySqlDataReader</c>: Objecte en les dades que retorna la consulta</param>
    ''' <returns><c>Integer</c>: Numero de registres dins l'objecte</returns>
    Private Function numeroRegistrosConsulta(ByVal dr As MySqlDataReader) As Integer
        Dim numeroRegistros As Integer = 0
        Do While dr.Read
            numeroRegistros = numeroRegistros + 1
        Loop
        numeroRegistrosConsulta = numeroRegistros
    End Function

    ''' <summary>Funcio que retorna el numero de taules d'una base de dades</summary>
    ''' <remarks> No l'he usat mai. es va copiar el primer dia i esta per si algun dia fa falta
    ''' </remarks>
    ''' <returns><c>Integer</c>: Numero de taules dins la base de dades</returns>
    Private Function numeroTablas() As Integer
        Dim consultaSQL As MySqlCommand = New MySqlCommand("show tables", conexion)
        Dim dr As MySqlDataReader = consultaSQL.ExecuteReader()
        numeroTablas = numeroRegistrosConsulta(dr)
        dr.Close()
    End Function

    ''' <summary>Funcio que escapa els caracters que donen problemes a una sentencia SQL</summary>
    ''' <remarks> Utilitzar sempre en tots els camps de text
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sentencia SQL o Texte a depurar</param>
    ''' <returns><c>String</c>: Texte en los caracters escapats</returns>
    Public Function cleanSql(ByVal Sql As String) As String
        Sql = Sql.Replace("'", "''")
        Sql = Sql.Replace("\", "\\")

        Return Sql
    End Function

    ''' <summary>Funcio que busca l'ultim ID d'una taula</summary>
    ''' <remarks> Utilitzar en compte despues de fer els inserts si treballa mes d'una persona al mateix temps
    ''' </remarks>
    ''' <param name="table"><c>String</c>: Nom de la taula a buscar</param>
    ''' <returns><c>Integer</c>: ID de l'ultim registre</returns>
    Public Function getLastId(ByVal table As String) As Integer
        Dim ds As DataSet

        ds = doSelect("SELECT id FROM " & table & " ORDER BY id DESC LIMIT 1")

        If ds.Tables(0).Rows.Count > 0 Then
            Return (ds.Tables(0).Rows(0)(0))
        Else
            Return (0)
        End If
    End Function

    ''' <summary>Funcio que executa una consulta "SELECT" a la base de dades</summary>
    ''' <remarks> 
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sentencia SQL a executar</param>
    ''' <returns><c>DataSet / Boolean</c>: retorna <c>False</c> si detecta algun error o un <c>DataSet</c> en les dades</returns>
    Public Function doSelect(ByVal Sql As String) 'As DataSet
        Dim consultaSQL As MySqlCommand = New MySqlCommand(Sql, conexion)
        Dim ds As DataSet = New DataSet()
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()

        Try
            DataAdapter1.SelectCommand = consultaSQL
            DataAdapter1.Fill(ds, "Tabla")
            Return ds
            'DataGridView1.DataSource = ds
            'DataGridView1.DataMember = "Tabla"

            'bePanelNumRegistros.Text = "| Nº registros: " & CStr(ds.Tables(0).Rows.Count)
        Catch ex As MySqlException
            MsgBox("Error al ejecutar consulta SQL: " & vbCrLf & vbCrLf & ex.ErrorCode & " " & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)

            Return False
        End Try
    End Function

    ''' <summary>Funcio que executa una operacio de "INSERT", "UPDATE" o "DELETE" a la base de dades</summary>
    ''' <remarks> 
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sql en la sentencia a executar</param>
    ''' <returns><c>Integer</c>: Numero de registres afectats</returns>
    Public Function doOperation(ByVal Sql As String) As Integer
        Dim consultaSQL As MySqlCommand = New MySqlCommand(Sql, conexion)
        Dim numRegistrosAfectados As Integer
        Dim comandoSQL As New MySqlCommand

        Try
            comandoSQL.Connection = conexion
            comandoSQL.CommandText = Sql
            numRegistrosAfectados = comandoSQL.ExecuteNonQuery()

            Return numRegistrosAfectados
        Catch ex As MySqlException
            MsgBox("Error al ejecutar consulta SQL: " & vbCrLf & vbCrLf & ex.ErrorCode & " " & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)

            Return 0
        End Try
    End Function

    ''' <summary>Funcio que modifica un camp BLOB per a guardar un fitxer</summary>
    ''' <remarks> Es va crear per a guardar els pdf dels albarans i es va quedar per si de cas. Utilitza un parametre per indicar el valor del camp a la sentencia SQL
    ''' </remarks>
    ''' <param name="Sql"><c>String</c>: Sentencia SQL a executar. inclou un parametre per indicar el valor BLOB</param>
    ''' <param name="Param"><c>String</c>: Nom del parametre de la sentencia SQL per a posar el BLOB</param>
    ''' <param name="Value"><c>Byte()</c>: Array de BYTE en la informacio a carregar</param>
    ''' <returns><c>Integer</c>: numero de registres afectats o 0 en cas d'error</returns>
    Public Function updateBlob(ByVal Sql As String, ByVal Param As String, ByVal Value As Byte()) As Integer
        Dim consultaSQL As MySqlCommand = New MySqlCommand(Sql, conexion)
        Dim numRegistrosAfectados As Integer
        Dim comandoSQL As New MySqlCommand

        Try
            comandoSQL.Connection = conexion
            comandoSQL.CommandText = Sql
            comandoSQL.Parameters.AddWithValue(Param, Value)
            numRegistrosAfectados = comandoSQL.ExecuteNonQuery()

            Return numRegistrosAfectados
        Catch ex As MySqlException
            MsgBox("Error al ejecutar consulta SQL: " & vbCrLf & vbCrLf & ex.ErrorCode & " " & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)

            Return 0
        End Try
    End Function

End Class
