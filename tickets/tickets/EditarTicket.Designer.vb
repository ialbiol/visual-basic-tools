﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FEditarTicket
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DTEData = New System.Windows.Forms.DateTimePicker()
        Me.LEData = New System.Windows.Forms.Label()
        Me.LERuta = New System.Windows.Forms.Label()
        Me.TECif = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TECifNom = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TEImport = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CBERuta = New System.Windows.Forms.ComboBox()
        Me.CBEVenedors = New System.Windows.Forms.ComboBox()
        Me.BGuardar = New System.Windows.Forms.Button()
        Me.BCancelar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'DTEData
        '
        Me.DTEData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTEData.Location = New System.Drawing.Point(184, 49)
        Me.DTEData.Name = "DTEData"
        Me.DTEData.Size = New System.Drawing.Size(302, 24)
        Me.DTEData.TabIndex = 1
        '
        'LEData
        '
        Me.LEData.AutoSize = True
        Me.LEData.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LEData.Location = New System.Drawing.Point(56, 57)
        Me.LEData.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LEData.Name = "LEData"
        Me.LEData.Size = New System.Drawing.Size(39, 18)
        Me.LEData.TabIndex = 46
        Me.LEData.Text = "Data"
        '
        'LERuta
        '
        Me.LERuta.AutoSize = True
        Me.LERuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LERuta.Location = New System.Drawing.Point(56, 96)
        Me.LERuta.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LERuta.Name = "LERuta"
        Me.LERuta.Size = New System.Drawing.Size(39, 18)
        Me.LERuta.TabIndex = 45
        Me.LERuta.Text = "Ruta"
        '
        'TECif
        '
        Me.TECif.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TECif.Location = New System.Drawing.Point(184, 218)
        Me.TECif.Margin = New System.Windows.Forms.Padding(2)
        Me.TECif.Name = "TECif"
        Me.TECif.Size = New System.Drawing.Size(302, 24)
        Me.TECif.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(56, 221)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(105, 18)
        Me.Label11.TabIndex = 44
        Me.Label11.Text = "Cif establiment"
        '
        'TECifNom
        '
        Me.TECifNom.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TECifNom.Location = New System.Drawing.Point(184, 257)
        Me.TECifNom.Margin = New System.Windows.Forms.Padding(2)
        Me.TECifNom.Name = "TECifNom"
        Me.TECifNom.Size = New System.Drawing.Size(302, 24)
        Me.TECifNom.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(54, 260)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 18)
        Me.Label10.TabIndex = 42
        Me.Label10.Text = "Nom establiment"
        '
        'TEImport
        '
        Me.TEImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TEImport.Location = New System.Drawing.Point(184, 176)
        Me.TEImport.Margin = New System.Windows.Forms.Padding(2)
        Me.TEImport.Name = "TEImport"
        Me.TEImport.Size = New System.Drawing.Size(302, 24)
        Me.TEImport.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(56, 179)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 18)
        Me.Label9.TabIndex = 39
        Me.Label9.Text = "Import"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(56, 138)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 18)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Venedor"
        '
        'CBERuta
        '
        Me.CBERuta.FormattingEnabled = True
        Me.CBERuta.Location = New System.Drawing.Point(184, 93)
        Me.CBERuta.Name = "CBERuta"
        Me.CBERuta.Size = New System.Drawing.Size(302, 26)
        Me.CBERuta.TabIndex = 2
        '
        'CBEVenedors
        '
        Me.CBEVenedors.FormattingEnabled = True
        Me.CBEVenedors.Location = New System.Drawing.Point(184, 135)
        Me.CBEVenedors.Name = "CBEVenedors"
        Me.CBEVenedors.Size = New System.Drawing.Size(302, 26)
        Me.CBEVenedors.TabIndex = 3
        '
        'BGuardar
        '
        Me.BGuardar.Location = New System.Drawing.Point(59, 387)
        Me.BGuardar.Name = "BGuardar"
        Me.BGuardar.Size = New System.Drawing.Size(138, 35)
        Me.BGuardar.TabIndex = 47
        Me.BGuardar.Text = "Guardar"
        Me.BGuardar.UseVisualStyleBackColor = True
        '
        'BCancelar
        '
        Me.BCancelar.Location = New System.Drawing.Point(348, 387)
        Me.BCancelar.Name = "BCancelar"
        Me.BCancelar.Size = New System.Drawing.Size(138, 35)
        Me.BCancelar.TabIndex = 48
        Me.BCancelar.Text = "Cancelar"
        Me.BCancelar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(54, 306)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(491, 18)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "COMPTE! Als textes apreta enter per a validar-los, sino no es modificaran"
        '
        'FEditarTicket
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 444)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BCancelar)
        Me.Controls.Add(Me.BGuardar)
        Me.Controls.Add(Me.CBEVenedors)
        Me.Controls.Add(Me.CBERuta)
        Me.Controls.Add(Me.DTEData)
        Me.Controls.Add(Me.LEData)
        Me.Controls.Add(Me.LERuta)
        Me.Controls.Add(Me.TECif)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TECifNom)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.TEImport)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FEditarTicket"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Editar ticket"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DTEData As System.Windows.Forms.DateTimePicker
    Friend WithEvents LEData As System.Windows.Forms.Label
    Friend WithEvents LERuta As System.Windows.Forms.Label
    Friend WithEvents TECif As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TECifNom As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TEImport As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CBERuta As System.Windows.Forms.ComboBox
    Friend WithEvents CBEVenedors As System.Windows.Forms.ComboBox
    Friend WithEvents BGuardar As System.Windows.Forms.Button
    Friend WithEvents BCancelar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
