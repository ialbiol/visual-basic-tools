﻿
''' <summary>Formulari d'edicio de dades</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>controller <c>TicketListClass</c></term><description>Objecte en tota la logica de funcionament del programa</description>
'''   </item>
'''   <item>
'''      <term>Tiquet <c>TicketClass</c></term><description>Objecte base que representa un tiquet</description>
'''   </item>
'''   <item>
'''      <term>errorData <c>Boolean = False</c></term><description>Indica si hi ha algun error en les dades modificades</description>
'''   </item>
'''   <item>
'''      <term>mostrant <c>Boolean = False</c></term><description>Controla si s'esta al proces de mostrar dades o no. serveix per a eliminar events innecesaris</description>
'''   </item>
'''</list>
''' </remarks>

Public Class FEditarTicket

    Dim controller As TicketListClass
    Dim Tiquet As TicketClass

    Dim errorData As Boolean = False
    Dim mostrant As Boolean = False

#Region "events formulari"

    ''' <summary>Event formulari Close</summary>
    ''' <remarks>Gestiona les ultimes accions que ha de fer el controller
    ''' </remarks>
    Private Sub FEditarTicket_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'fin
        controller.Log.Add("Modifico lo tiquet ID[" & Tiquet.Id & "]: CANCELO EDICIO!!", "EDIT RESULT")
    End Sub

    ''' <summary>Event formulari Load</summary>
    ''' <remarks>Gestiona les primeres accions que ha de fer el controller
    ''' </remarks>
    Private Sub FEditarTicket_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'load

        omplirCB()

        mostrarDada()
    End Sub

#End Region

#Region "events modificar dades"

    ''' <summary>Event Data Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la data del tiquet
    ''' </remarks>
    Private Sub DTEData_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTEData.ValueChanged
        If mostrant = False Then
            checkError()
            controller.Log.Add("Modifico data de " & Tiquet.Data.ToString() & " a " & DTEData.Value.ToString() & ". error: " & errorData.ToString(), "EDIT")

            Tiquet.Data = DTEData.Value
        End If
    End Sub

    ''' <summary>Event Ruta Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia la ruta del tiquet
    ''' </remarks>
    Private Sub CBERuta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBERuta.SelectedIndexChanged
        If mostrant = False Then
            controller.Log.Add("Modifico ruta de " & Tiquet.Ruta & " a " & CBERuta.SelectedItem.ToString(), "EDIT")

            Tiquet.Ruta = CBERuta.SelectedItem.ToString()
        End If
    End Sub

    ''' <summary>Event venedor Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia el venedor del tiquet
    ''' </remarks>
    Private Sub CBEVenedors_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBEVenedors.SelectedIndexChanged
        If mostrant = False Then
            checkError()
            controller.Log.Add("Modifico treballador de " & Tiquet.VEN_Nom & " a " & CBEVenedors.SelectedItem.ToString() & ". error: " & errorData.ToString(), "EDIT")

            Tiquet.VEN_Nom = CBEVenedors.SelectedItem.ToString()
            Tiquet.IdVenedor = controller.FindVenedor(CBEVenedors.SelectedItem.ToString(), "")
        End If
    End Sub

    ''' <summary>Event Import Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia l'import del tiquet
    ''' </remarks>
    Private Sub TEImport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TEImport.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If mostrant = False Then
                checkError()
                controller.Log.Add("Modifico import de " & Tiquet.Import & " a " & TEImport.Text & ". error: " & errorData.ToString(), "EDIT")

                Tiquet.Import = TEImport.Text
            End If
        End If
    End Sub

    ''' <summary>Event cif Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia el cif de l'establiment del tiquet
    ''' </remarks>
    Private Sub TECif_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TECif.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If mostrant = False Then
                controller.Log.Add("Modifico cif de l'establiment de " & Tiquet.CIF_Cif & " a " & TECif.Text, "EDIT")

                Tiquet.CIF_Cif = TECif.Text
                Tiquet = controller.FindCif(Tiquet)

                mostrarDada()
            End If
        End If
    End Sub

    ''' <summary>Event Cif_Nom Changed</summary>
    ''' <remarks>Gestiona les operacions i comprobacions a realitzar si es canvia el nom de l'establiment del tiquet
    ''' </remarks>
    Private Sub TECifNom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TECifNom.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If mostrant = False Then
                controller.Log.Add("Modifico nom de l'establiment de " & Tiquet.CIF_Nom & " a " & TECifNom.Text, "EDIT")

                Tiquet.CIF_Nom = TECifNom.Text
                controller.updateEstabliment(Tiquet)

            End If
        End If
    End Sub

#End Region

#Region "events operacions en les dades"

    ''' <summary>Event boto guardar dades Click</summary>
    ''' <remarks>Gestiona les operacions per a guardar o modificar les dades si no hi ha cap error
    ''' </remarks>
    Private Sub BGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGuardar.Click
        If errorData Then
            MessageBox.Show("La data seleccionada ja existeix per a este treballador.", "Info", MessageBoxButtons.OK)
        Else
            controller.Log.Add("Modifico el tiquet ID[" & Tiquet.Id & "]: GUARDO DADES!!", "EDIT RESULT")
            Tiquet = controller.MarcarModificat(Tiquet)

            If Tiquet.Id = "0" Then
                controller.Insert(Tiquet)
            Else
                controller.Update(Tiquet)
            End If
            controller.buscarImatgeCamara(Tiquet)

            Me.Close()
        End If
    End Sub

    ''' <summary>Event boto cancelar edicio Click</summary>
    ''' <remarks>Gestiona les operacions per a cancelar l'edicio
    ''' </remarks>
    Private Sub BCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCancelar.Click
        controller.Log.Add("Modifico el tiquet ID[" & Tiquet.Id & "]: CANCELO EDICIO!!", "EDIT RESULT")
        Me.Close()
    End Sub

#End Region

#Region "funcions propies"

    ''' <summary>Funcio que controla si hi ha algun error a les dades modificades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Boolean</c>: si hi ha algun error o no</returns>
    Private Function checkError() As Boolean
        errorData = False

        If TEImport.Text = "" Then
            errorData = True
        End If

        If errorData = False And CBEVenedors.SelectedItem.ToString() <> "" Then
            errorData = controller.existeix(DTEData.Value, CBEVenedors.SelectedItem.ToString(), Tiquet.Id)
        Else
            errorData = True
        End If


        If errorData Then
            MessageBox.Show("La data seleccionada ja existeix per a este treballador. l'import no pot estar en blanc", "Info", MessageBoxButtons.OK)
        End If

        Return errorData
    End Function

    ''' <summary>Funcio que mostra les dades del tiquet</summary>
    ''' <remarks>Omple els camps per a poder editar les dades
    ''' </remarks>
    Private Sub mostrarDada()
        mostrant = True

        DTEData.Value = Tiquet.DataText
        CBERuta.SelectedItem = Tiquet.Ruta
        CBEVenedors.SelectedItem = Tiquet.VEN_Nom
        TEImport.Text = Tiquet.Import
        TECif.Text = Tiquet.CIF_Cif
        TECifNom.Text = Tiquet.CIF_Nom

        mostrant = False
    End Sub

    ''' <summary>Funcio que busca les dades dels desplegables i els ompli</summary>
    ''' <remarks>modifica les dades dels desplegables segons sigue necesari
    ''' </remarks>
    Private Sub omplirCB()
        Dim dsVenedor As DataSet
        Dim row As Integer

        mostrant = True

        'omplo el desplegable dels venedors
        dsVenedor = controller.llistatNifVenedors()

        CBEVenedors.Items.Clear()
        CBEVenedors.Items.Add("")
        If dsVenedor.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsVenedor.Tables(0).Rows.Count
                CBEVenedors.Items.Add(dsVenedor.Tables(0).Rows(row)(0))
                row = row + 1
            End While
        End If
        'CBEVenedors.SelectedIndex = 0

        CBERuta.Items.Add("")
        For Each V As String In controller.LlistatVenedors
            CBERuta.Items.Add(V)
            CBERuta.Items.Add(V & "-apo")
            CBERuta.Items.Add(V & "-rep")
        Next
        'CBERuta.SelectedIndex = 0

        mostrant = False
    End Sub

    ''' <summary>Funcio inicial per a indicar el Id del tiquet a modificar</summary>
    ''' <remarks>Esta funcio es crida antes de mostrar el formulari i despues de la seua creacio per a indicar quin tiquet s'ha de modificar
    ''' </remarks>
    ''' <param name="Id"><c>String</c>: ID del tiquet a modificar</param>
    ''' <param name="control"><c>Controller</c>: Objecte controller que executara totes les operacions</param>
    Public Sub SetIdTiquet(ByVal Id As String, ByVal control As TicketListClass)
        controller = control
        Tiquet = controller.SearchById(Id)
    End Sub

    ''' <summary>Funcio inicial per a indicar l'objecte tiquet a modificar</summary>
    ''' <remarks>Esta funcio es crida antes de mostrar el formulari i despues de la seua creacio per a indicar quin tiquet s'ha de modificar
    ''' </remarks>
    ''' <param name="Tiqt"><c>TicketClass</c>: Objecte del tiquet a modificar / crear</param>
    ''' <param name="control"><c>Controller</c>: Objecte controller que executara totes les operacions</param>
    Public Sub SetTiquet(ByVal Tiqt As TicketClass, ByVal control As TicketListClass)
        Tiquet = Tiqt
        controller = control
    End Sub

#End Region

End Class