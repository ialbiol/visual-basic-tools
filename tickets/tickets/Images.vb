﻿
''' <summary>Classe que realitza les operacions basiques a una imatge</summary>
''' <remarks>funcions:
''' <list type="table">
'''   <listheader>
'''      <term>Funcio</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>redimensionarImatge <c>(ByVal rutaImatge As String)</c></term><description>Redimensiona una imatge a un tamany manejable</description>
'''   </item>
'''   <item>
'''      <term>girarImatge <c>(ByVal rutaImatge As String)</c></term><description>Girar una imatge 90 graus</description>
'''   </item>
'''   <item>
'''      <term>ImatgeHoritzontal <c>(ByVal rutaImatge As String)</c></term><description>Posa la imatge en horitzontal, girant-la si fa falta</description>
'''   </item>
'''   <item>
'''      <term>ImatgeVertical <c>(ByVal rutaImatge As String)</c></term><description>Posa la imatge en vertical, girant-la si fa falta</description>
'''   </item>
'''</list>
''' </remarks>
Public Class ImagesClass

    ''' <summary>Funcio redimensiona una imatge</summary>
    ''' <remarks>la mida mes gran pasa a ser de 600 i l'altra es proporcional
    ''' </remarks>
    ''' <param name="rutaImatge"><c>String</c>: Ruta de la imatge</param>
    Public Sub redimensionarImatge(ByVal rutaImatge As String)
        Dim imagenOriginal As Bitmap
        Dim imagenRedimensionada As Bitmap
        Dim h As Integer
        Dim w As Integer

        If My.Computer.FileSystem.FileExists(rutaImatge) Then
            'Carga de la imagen original
            imagenOriginal = New Bitmap(rutaImatge)

            'calculo noves mides
            If imagenOriginal.Width > imagenOriginal.Height Then
                w = 600
                h = (w * imagenOriginal.Height) / imagenOriginal.Width
            Else
                h = 600
                w = (h * imagenOriginal.Width) / imagenOriginal.Height
            End If

            'creamos una imagen con las dimensiones que se desean
            'en este caso la creamos de 100x100 pixels
            imagenRedimensionada = New Bitmap(w, h)

            'creamos un objeto graphics desde la nueva imagen
            Using gr As Graphics = Graphics.FromImage(imagenRedimensionada)

                'en la nueva imagen "pintamos" la antigua imagen con las dimensiones de la nueva imagen
                gr.DrawImage(imagenOriginal, 0, 0, imagenRedimensionada.Width, imagenRedimensionada.Height)

            End Using

            'se guarda la nueva imagen
            imagenOriginal.Dispose()
            Try
                imagenRedimensionada.Save(rutaImatge)
            Catch
            End Try
            imagenRedimensionada.Dispose()
        End If
    End Sub

    ''' <summary>Funcio gira una imatge 90 graus</summary>
    ''' <remarks>sempre gira 90 graus a la dreta
    ''' </remarks>
    ''' <param name="rutaImatge"><c>String</c>: Ruta de la imatge</param>
    Public Sub girarImatge(ByVal rutaImatge As String)
        Dim imagenOriginal As Bitmap

        If My.Computer.FileSystem.FileExists(rutaImatge) Then
            'Carga de la imagen original
            imagenOriginal = New Bitmap(rutaImatge)
            imagenOriginal.RotateFlip(RotateFlipType.Rotate90FlipNone)

            'se guarda la nueva imagen
            Try
                imagenOriginal.Save(rutaImatge)
            Catch
            End Try
            imagenOriginal.Dispose()
        End If
    End Sub


    ''' <summary>Funcio posa la imatge en horitzontal</summary>
    ''' <remarks>comproba que la mida mes gran sigue la horitzontal i sino, gira la imatge
    ''' </remarks>
    ''' <param name="rutaImatge"><c>String</c>: Ruta de la imatge</param>
    Public Sub ImatgeHoritzontal(ByVal rutaImatge As String)
        Dim imagenOriginal As Bitmap

        If My.Computer.FileSystem.FileExists(rutaImatge) Then
            'Carga de la imagen original
            imagenOriginal = New Bitmap(rutaImatge)

            If imagenOriginal.Height > imagenOriginal.Width Then
                imagenOriginal.RotateFlip(RotateFlipType.Rotate90FlipNone)
            End If

            'se guarda la nueva imagen
            Try
                imagenOriginal.Save(rutaImatge)
            Catch
            End Try
            imagenOriginal.Dispose()
        End If
    End Sub


    ''' <summary>Funcio posa la imatge en vertical</summary>
    ''' <remarks>comproba que la mida mes gran sigue la vertical i sino, gira la imatge
    ''' </remarks>
    ''' <param name="rutaImatge"><c>String</c>: Ruta de la imatge</param>
    Public Sub ImatgeVertical(ByVal rutaImatge As String)
        Dim imagenOriginal As Bitmap

        If My.Computer.FileSystem.FileExists(rutaImatge) Then
            'Carga de la imagen original
            imagenOriginal = New Bitmap(rutaImatge)

            If imagenOriginal.Width > imagenOriginal.Height Then
                imagenOriginal.RotateFlip(RotateFlipType.Rotate90FlipNone)
            End If

            'se guarda la nueva imagen
            Try
                imagenOriginal.Save(rutaImatge)
            Catch
            End Try
            imagenOriginal.Dispose()
        End If
    End Sub

End Class
