﻿Imports System.Net
Imports System.Net.Sockets

Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Text

''' <summary>Clase que gestiona la comunicacio en un servidor de correu IMAP</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>test <c>Boolean = False</c></term><description>Indica si la clase esta en mode test (<c>True</c>) o no (<c>False</c>). 
'''      <para>En mode test la clase mostra una serie de misatges quan executes l'aplicacio</para>
'''      <para>Esta variable es pot usar a altres clases per a afegir mes missatges de control, per exemple:</para>
'''      <para><example><code>
'''        If IMAP.test then
'''           MessageBox("sql: " + sql, "Debug", MessageBoxButtons.OK)
'''        End If
'''      </code></example></para></description>
'''   </item>
'''   <item>
'''      <term>host <c>String</c></term><description>Direccio del servidor de correu</description>
'''   </item>
'''   <item>
'''      <term>user <c>String</c></term><description>Usuari d'identificacio al servidor de correu</description>
'''   </item>
'''   <item>
'''      <term>pass <c>String</c></term><description>Password d'identificacio al servidor de correu</description>
'''   </item>
'''   <item>
'''      <term>_imapClient <c>TcpClient</c></term><description>Objecte TCP que gestiona la comunicacio</description>
'''   </item>
'''   <item>
'''      <term>_imapNs <c>NetworkStream</c></term><description>Objecte que gestiona la informacio rebuda i enviada al servidor</description>
'''   </item>
'''   <item>
'''      <term>_imapSw <c>StreamWriter</c></term><description>Objecte que gestiona la informacio a enviar al servidor</description>
'''   </item>
'''   <item>
'''      <term>_imapSr <c>StreamReader</c></term><description>Objecte que gestiona la informacio rebuda del servidor</description>
'''   </item>
'''   <item>
'''      <term>Conected <c>Boolean</c></term><description>Propietat que indica si s'ha establert la comunicacio o no</description>
'''   </item>
'''   <item>
'''      <term>Count <c>Integer</c></term><description>Propietat en lo numero d'emails que hi ha a la carpeta inbox</description>
'''   </item>
'''   <item>
'''      <term>Unread <c>Integer</c></term><description>Propietat en lo numero d'emails sense llegir</description>
'''   </item>
'''   <item>
'''      <term>Emails <c>List(Of ImapEmailClass)</c></term><description>Propietat en lo llistat dels emails seleccionats</description>
'''   </item>
'''</list>
''' </remarks>

Public Class ImapClass
    'Private test = True
    Private test = False


    Private host As String = "correoweb.lluiscongelats.com"
    Private user As String = "tiquets@lluiscongelats.com"
    Private pass As String = "Pepo1234"


    Private _imapClient As TcpClient
    Private _imapNs As NetworkStream
    Private _imapSw As StreamWriter
    Private _imapSr As StreamReader


    Private s_conected As Boolean = False
    Public ReadOnly Property Conected() As Boolean
        Get
            Return s_conected
        End Get
    End Property


    Private s_count As Integer
    Public Property Count() As Integer
        Get
            Return s_count
        End Get
        Set(ByVal value As Integer)
            s_count = value
        End Set
    End Property


    Private s_unread As Integer
    Public Property Unread() As Integer
        Get
            Return s_unread
        End Get
        Set(ByVal value As Integer)
            s_unread = value
        End Set
    End Property


    Private s_emails As List(Of ImapEmailClass)
    Public ReadOnly Property Emails() As List(Of ImapEmailClass)
        Get
            Return s_emails
        End Get
    End Property


    ''' <summary>Funcio que gestiona la recepcio de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: String en la resposta rebuda sense formatar</returns>
    Private Function Response() As String
        Dim data(_imapClient.ReceiveBufferSize) As Byte
        Dim ret As Integer
        Dim res As String

        ret = _imapNs.Read(data, 0, data.Length)

        res = Encoding.ASCII.GetString(data).TrimEnd()

        If test Then
            MessageBox.Show("receivedBufferSize: " & _imapClient.ReceiveBufferSize & " | response: " & res, "Debug", MessageBoxButtons.OK)
        End If

        Return res
    End Function

    ''' <summary>Funcio que envia una ordre al servidor</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Command"><c>String</c>: Comanda a enviar al servidor</param>
    ''' <returns><c>String</c>: String en la resposta del servidor sense formatar</returns>
    Private Function SendCommand(ByVal Command As String) As String
        Dim res As String = ""

        If test Then
            MessageBox.Show("send command: " & Command, "Debug", MessageBoxButtons.OK)
        End If

        If s_conected Then
            _imapSw.WriteLine(Command)
            _imapSw.Flush()

            res = Response()
        End If

        Return res
    End Function

    ''' <summary>Funcio que inicialitza la coneccio en lo servidor</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="hostname"><c>String</c>: Direccio del servidor</param>
    ''' <param name="port"><c>Optional Integer = 143</c>: Port de coneccio</param>
    ''' <returns><c>Boolean</c>: si s'ha pogut conectar correctament o no</returns>
    Private Function InitializeConnection(ByVal hostname As String, Optional ByVal port As Integer = 143) As Boolean
        Dim res As String
        Dim ret As Boolean = False

        Try
            _imapClient = New TcpClient(hostname, port)
            _imapClient.ReceiveBufferSize = 500000
            _imapNs = _imapClient.GetStream()
            _imapSw = New StreamWriter(_imapNs)
            _imapSr = New StreamReader(_imapNs)

            res = Response()

            res = res.Substring(0, 4)

            If res.LastIndexOf("OK") > -1 Then
                s_conected = True
                ret = True
            Else
                MessageBox.Show("not conected to server: " & res, "Error", MessageBoxButtons.OK)
            End If

        Catch ex As Exception
            MessageBox.Show("exception: " & ex.Message, "Error", MessageBoxButtons.OK)
        End Try

        Return ret
    End Function

    ''' <summary>Funcio que finalitza la coneccio en lo servidor</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Boolean</c>: si la operacio s'ha realitzat correctament o no</returns>
    Private Function FinalizeConnection() As Boolean
        Dim res As String
        Dim ret As Boolean = False

        _imapSw.WriteLine("$ LOGOUT")
        _imapSw.Flush()

        res = Response()

        If res.LastIndexOf("OK") > -1 Then
            ret = True
        Else
            MessageBox.Show("not disconected to server: " & res, "Error", MessageBoxButtons.OK)
        End If

        Return ret
    End Function

    ''' <summary>Funcio que autentifica un usuari al servidor</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="username"><c>String</c>: Nom d'usuari</param>
    ''' <param name="password"><c>String</c>: Clau d'acces</param>
    ''' <returns><c>Boolean</c>: si la operacio s'ha realitzat correctament o no</returns>
    Private Function AuthenticateUser(ByVal username As String, ByVal password As String) As Boolean
        Dim res As String
        Dim ret As Boolean = False

        res = SendCommand("$ LOGIN " + username + " " + password)

        If res.Length > 0 Then
            res = res.Substring(0, 4)

            If res.LastIndexOf("OK") > -1 Then
                ret = True
            Else
                MessageBox.Show("not authenticated to server: " & res, "Error", MessageBoxButtons.OK)
            End If
        End If

        Return ret
    End Function

    ''' <summary>Funcio que busca el numero d'emails al inbox</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Integer</c>: el numero d'emails trobats</returns>
    Private Function MailCount() As Integer
        Dim res As String
        Dim m As Match

        res = SendCommand("$ STATUS INBOX (messages)")
        m = Regex.Match(res, "[0-9]*[0-9]")

        Return Convert.ToInt32(m.ToString())
    End Function

    ''' <summary>Funcio que busca el numero d'emails no llegits al inbox</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Integer</c>: el numero d'emails trobats</returns>
    Private Function MailUnreadCount() As Integer
        Dim res As String
        Dim m As Match

        res = SendCommand("$ STATUS INBOX (unseen)")
        m = Regex.Match(res, "[0-9]*[0-9]")

        Return Convert.ToInt32(m.ToString())
    End Function

    ''' <summary>Funcio que busca dins el llistat d'emails l'email indicat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="id"><c>Integer</c>: ID del email a buscar</param>
    ''' <returns><c>ImapEmailClass</c>: Objecte email trobat o creat</returns>
    Private Function searchMessage(ByVal id As Integer) As ImapEmailClass
        Dim num As Integer = 0
        Dim email As ImapEmailClass

        If s_emails.Count > 0 Then
            While num < s_emails.Count
                If s_emails(num).Id = id Then
                    ' he trobat el que busco
                    Return s_emails(num)
                End If
                num = num + 1
            End While
        End If

        ' si no trobo el email o no ne hi han, el creo. no tindra capçalera
        email = New ImapEmailClass
        email.Id = id
        s_emails.Add(email)

        Return email
    End Function

    ''' <summary>Funcio que recull i gestiona la capçalera d'un correu</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="index"><c>Integer</c>: ID de l'email a buscar</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Private Function getMessageHeaders(ByVal index As Integer) As Boolean
        Dim res As String
        Dim lines As String()
        Dim num As Integer
        Dim email As New ImapEmailClass

        res = SendCommand("$ FETCH " & index & " (body[header.fields (from subject date)])")
        If res.LastIndexOf("OK") > -1 Then
            email.Id = index
            lines = res.Split(Chr(10))

            For num = 0 To lines.Count - 1
                'busco el from
                If lines(num).IndexOf("From:") > -1 Then
                    email.From = lines(num).Substring(5)
                End If

                'busco el subject
                If lines(num).IndexOf("Subject:") > -1 Then
                    email.Subject = lines(num).Substring(8)
                End If

                'busco la data
                If lines(num).IndexOf("Date:") > -1 Then
                    res = lines(num).Substring(11, 21)
                    email.ArrivalDate = res.Trim(" ")
                End If
            Next

            s_emails.Add(email)

            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>Funcio que recull i gestiona el cos d'un email</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="index"><c>Integer</c>: ID de l'email a buscar</param>
    Private Sub getMessageBody(ByVal index As Integer)
        Dim res As String
        Dim lines As String()
        Dim aux As String()
        Dim num As Integer = 0
        Dim contentType As String = ""
        Dim email As ImapEmailClass
        Dim attach As ImapEmailAttachmentClass


        email = searchMessage(index)

        res = SendCommand("$ FETCH " & index & " body[text]") '"$ UID FETCH " uid " body[text]"

        lines = res.Split(Chr(10))

        While num < lines.Count

            'gestiono el text del body
            If contentType = "text/plain" Then
                If lines(num).Length > 1 AndAlso lines(num).Substring(0, 2) = "--" Then
                    contentType = ""
                Else
                    email.Body = email.Body & Chr(10) & lines(num)
                End If
            End If

            'gestiono el attachment
            If contentType = "application/octet-stream" Then
                If lines(num).Length > 1 AndAlso lines(num).Substring(0, 2) = "--" Then
                    email.Attachments.Add(attach)

                Else
                    'contingut
                    If lines(num).IndexOf("Content-") = -1 Then
                        If lines(num).Trim() <> " " And lines(num).Trim() <> ")" Then
                            attach.Data = attach.Data & lines(num).Trim()
                        Else
                            If lines(num).Trim() = ")" Then 'final de tot
                                contentType = ""
                                email.Attachments.Add(attach)
                            End If
                        End If
                    End If

                    'busco el filename
                    If lines(num).IndexOf("filename") > -1 Then
                        res = lines(num).Substring(lines(num).IndexOf("filename") + 10)
                        aux = res.Split("/")
                        res = aux(aux.Count - 1)
                        res = res.Substring(0, res.Length - 2)
                        attach.Filename = res

                        num = num + 1 ' la seguent linia esta en blanc
                    End If

                End If
            End If

            'canvi de content-type
            If lines(num).IndexOf("Content-Type:") > -1 Then
                res = lines(num).Substring(13)
                If res.IndexOf("text/plain") > -1 Then
                    contentType = "text/plain"
                    num = num + 1 ' la seguent linia esta en blanc
                End If
                If res.IndexOf("application/octet-stream") > -1 Then
                    contentType = "application/octet-stream"
                    attach = New ImapEmailAttachmentClass
                    attach.Type = "application/octet-stream"
                End If
            End If

            num = num + 1
        End While

        If contentType = "application/octet-stream" Then
            email.Attachments.Add(attach)
        End If

    End Sub

    ''' <summary>Funcio que afegeix un flag al correu</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="index"><c>Integer</c>: ID de l'email a buscar</param>
    ''' <param name="flag"><c>String</c>: Flag a incloure al email</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Private Function addflag(ByVal index As Integer, ByVal flag As String)
        If flag = "Seen" Or flag = "Unseen" Or flag = "Deleted" Or flag = "Answered" Or flag = "Flagged" Or flag = "Draft" Then

            Return SendCommand("$ STORE " & index & " +flags (\" & flag & ")")

        Else

            Return "Error, flag don't exists"

        End If
    End Function

    ''' <summary>Funcio que busca les capçaleres d'un llistat de correu</summary>
    ''' <remarks>El llistat es com arriva el resultat d'una cerca al servidor:
    ''' <para>id de correus separats per un espai en blanc</para>
    ''' </remarks>
    ''' <param name="llistat"><c>String</c>: Llistat dels emails a gestionar</param>
    Private Sub processMessages(ByVal llistat As String)
        Dim refs As String()
        Dim num As Integer

        refs = llistat.split(" ")
        For num = 0 To refs.Count - 1
            If IsNumeric(refs(num)) Then
                getMessageHeaders(refs(num))
                'getMessageBody(refs(num))
            End If
        Next

    End Sub

    ''' <summary>Funcio que borra un correu del servidor</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="index"><c>Integer</c>: ID de l'email a buscar</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Private Function deleteMessage(ByVal index As Integer) As Boolean
        Dim res As String

        addflag(index, "Deleted")
        res = SendCommand("$ EXPUNGE")

        If res.LastIndexOf("OK") > -1 Then
            Return True
        End If

        Return False
    End Function


    ''' <summary>Funcio que conecta en lo servidor</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub connect()
        If InitializeConnection(host) Then
            If AuthenticateUser(user, pass) Then

                If s_emails Is Nothing Then
                    s_emails = New List(Of ImapEmailClass)
                End If
                s_emails.Clear()

            End If
        End If
    End Sub

    ''' <summary>Funcio que desconecta el servidor</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub disconnect()
        If FinalizeConnection() Then
            s_emails.Clear()

            s_conected = False
        End If
    End Sub

    ''' <summary>Funcio que selecciona una carpeta del servidor</summary>
    ''' <remarks>Sempre s'ha de seleccionar una carpeta antes de buscar els correus
    ''' </remarks>
    ''' <param name="folder"><c>String</c>: Nom de la carpeta</param>
    ''' <param name="LoadHeaders"><c>Optional Boolean = False</c>: Si es volen carregar les capçaleres o no</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Public Function SelectFolder(ByVal folder As String, Optional ByVal LoadHeaders As Boolean = False) As Boolean
        Dim res As String
        Dim ret As Boolean = False
        Dim num As Integer

        res = SendCommand("$ SELECT " & folder)

        If res.LastIndexOf("OK") > -1 Then

            s_emails.Clear()

            s_count = MailCount()
            s_unread = MailUnreadCount()

            If LoadHeaders Then
                For num = 1 To s_count
                    getMessageHeaders(num)
                Next
            End If

            ret = True
        Else
            MessageBox.Show("folder " & folder & " don't exists on server: " & res, "Error", MessageBoxButtons.OK)
        End If

        Return ret
    End Function

    ''' <summary>Funcio que busca els correus pel camp subject</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="text"><c>String</c>: Text a buscar</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Public Function searchMessageBySubject(ByVal text As String) As Boolean
        Dim res As String
        Dim lines As String()


        res = SendCommand("$ SEARCH SUBJECT """ & text & """")
        If res.LastIndexOf("OK") > -1 Then
            lines = res.Split(Chr(10))

            For num = 0 To lines.Count - 1
                If lines(num).IndexOf("SEARCH") > -1 Then
                    res = lines(num).Substring(9)
                    processMessages(res)
                End If
            Next

            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary>Funcio que busca els correus pel camp from</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="text"><c>String</c>: Text a buscar</param>
    ''' <returns><c>Boolean</c>: Si la operacio s'ha realitzat correctament</returns>
    Public Function searchMessageByFrom(ByVal text As String) As Boolean
        Dim res As String
        Dim lines As String()

        res = SendCommand("$ SEARCH FROM " & text)
        If res.LastIndexOf("OK") > -1 Then
            lines = res.Split(Chr(10))

            For num = 0 To lines.Count - 1
                If lines(num).IndexOf("SEARCH") > -1 Then
                    res = lines(num).Substring(9)
                    processMessages(res)
                End If
            Next

            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary>Funcio que busca un email a traves del seu id</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="id"><c>Integer</c>: ID de l'email a buscar</param>
    ''' <returns><c>ImapEmailClass</c>: retorna un objecte email en tota la informacio</returns>
    Public Function getMessage(ByVal id As Integer) As ImapEmailClass
        Dim email As ImapEmailClass

        getMessageBody(id)
        email = searchMessage(id)

        Return email
    End Function

    ''' <summary>Funcio que busca un email a traves d'un objecte ImapEmailClass precreat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="email"><c>ImapEmailClass</c>: Objecte email</param>
    ''' <returns><c>ImapEmailClass</c>: retorna un objecte email en tota la informacio</returns>
    Public Function getMessage(ByVal email As ImapEmailClass) As ImapEmailClass

        Return getMessage(email.Id)
    End Function

    '"$ FETCH " + index + "(unseen),body[text]"
    '"$ UID SEARCH UNSEEN"

End Class


''' <summary>Clase que gestiona l'estructura d'un correu IMAP</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>charsToTrim <c>Array Char</c></term><description>Llistat de caracters a eliminar dels textes</description>
'''   </item>
'''   <item>
'''      <term>Id <c>Integer</c></term><description>ID identificatiu de l'email</description>
'''   </item>
'''   <item>
'''      <term>From <c>String</c></term><description>Camp from del correu</description>
'''   </item>
'''   <item>
'''      <term>Subject <c>String</c></term><description>Camp Subject del correu</description>
'''   </item>
'''   <item>
'''      <term>ArrivalDate <c>DateTime</c></term><description>Data i hora a la que ha arivat el correu</description>
'''   </item>
'''   <item>
'''      <term>Body <c>String</c></term><description>Cos del correu</description>
'''   </item>
'''   <item>
'''      <term>BodyHtml <c>String</c></term><description>Cos del correu en format HTML</description>
'''   </item>
'''   <item>
'''      <term>Seen <c>Boolean</c></term><description>Si el correu esta marcat com a vist o no</description>
'''   </item>
'''   <item>
'''      <term>Attachments <c>List(Of ImapEmailAttachmentClass)</c></term><description>Propietat en lo llistat dels attachments de l'email</description>
'''   </item>
'''</list>
''' </remarks>

Public Class ImapEmailClass

    Private charsToTrim() As Char = {" "c, Chr(13)}

    Private s_id As Integer = 0
    Public Property Id() As Integer
        Get
            Return s_id
        End Get
        Set(ByVal value As Integer)
            s_id = value
        End Set
    End Property


    Private s_from As String = ""
    Public Property From() As String
        Get
            Return s_from
        End Get
        Set(ByVal value As String)
            s_from = value.Trim(charsToTrim)
        End Set
    End Property


    Private s_subject As String = ""
    Public Property Subject() As String
        Get
            Return s_subject
        End Get
        Set(ByVal value As String)
            s_subject = value.Trim(charsToTrim)
        End Set
    End Property


    Private s_date As Date = Nothing
    Public Property ArrivalDate() As Date
        Get
            Return s_date
        End Get
        Set(ByVal value As Date)
            s_date = value
        End Set
    End Property


    Private s_body As String = ""
    Public Property Body() As String
        Get
            Return s_body
        End Get
        Set(ByVal value As String)
            s_body = value
        End Set
    End Property


    Private s_bodyHtml As String = ""
    Public Property BodyHtml() As String
        Get
            Return s_bodyHtml
        End Get
        Set(ByVal value As String)
            s_bodyHtml = value
        End Set
    End Property


    Private s_seen As Boolean = False
    Public Property Seen() As Boolean
        Get
            Return s_seen
        End Get
        Set(ByVal value As Boolean)
            s_seen = value
        End Set
    End Property


    Private s_attachments As New List(Of ImapEmailAttachmentClass)
    Public ReadOnly Property Attachments() As List(Of ImapEmailAttachmentClass)
        Get
            Return s_attachments
        End Get
    End Property


    ''' <summary>Funcio que neteja els valors de totes les propietats</summary>
    ''' <remarks>Inicialitza totes les propietats de l'objecte al seu valor per defecte
    ''' </remarks>
    Public Sub Clear()
        s_id = 0
        s_from = ""
        s_subject = ""
        s_date = Nothing
        s_body = ""
        s_bodyHtml = ""
        s_seen = False
        s_attachments.Clear()

    End Sub

End Class



''' <summary>Clase que gestiona l'estructura d'un attachment d'un correu IMAP</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>Type <c>String</c></term><description>Mime type de l'attachment</description>
'''   </item>
'''   <item>
'''      <term>Filename <c>String</c></term><description>Nom del fitxer</description>
'''   </item>
'''   <item>
'''      <term>Data <c>String</c></term><description>Fitxer codificat en Base64</description>
'''   </item>
'''</list>
''' </remarks>

Public Class ImapEmailAttachmentClass

    Private s_type As String
    Public Property Type() As String
        Get
            Return s_type
        End Get
        Set(ByVal value As String)
            s_type = value
        End Set
    End Property


    Private s_fileName As String
    Public Property Filename() As String
        Get
            Return s_fileName
        End Get
        Set(ByVal value As String)
            s_fileName = value
        End Set
    End Property


    Private s_data As String = Nothing
    Public Property Data() As String
        Get
            Return s_data
        End Get
        Set(ByVal value As String)
            s_data = value
        End Set
    End Property

    ''' <summary>Funcio que guarda el fitxer a la ruta indicada</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="filename"><c>String</c>: Nom i ruta on guardar el fitxer</param>
    Public Sub SaveAs(ByVal filename As String)
        Dim aBytDocumento() As Byte = Nothing
        Dim oFileStream As FileStream

        If s_data <> Nothing Then
            If Not File.Exists(filename) Then
                Try
                    aBytDocumento = Convert.FromBase64String(s_data)

                    oFileStream = New FileStream(filename, FileMode.CreateNew, FileAccess.Write)
                    oFileStream.Write(aBytDocumento, 0, aBytDocumento.Length)
                    oFileStream.Close()
                Catch
                End Try
            Else
                MessageBox.Show("el fitxer " & filename & " ja existeix", "Error", MessageBoxButtons.OK)
            End If
        Else
            MessageBox.Show("l'attachment no conte dades", "Error", MessageBoxButtons.OK)
        End If
    End Sub

End Class