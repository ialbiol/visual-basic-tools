﻿Imports System.Collections.ObjectModel
Imports System.IO
Imports System.Collections.Generic
Imports PortableDeviceApiLib
Imports PortableDeviceTypesLib
Imports _tagpropertykey = PortableDeviceApiLib._tagpropertykey
Imports IPortableDeviceKeyCollection = PortableDeviceApiLib.IPortableDeviceKeyCollection
Imports IPortableDeviceValues = PortableDeviceApiLib.IPortableDeviceValues
Imports System.Runtime.InteropServices
Imports System.Reflection

Namespace PortableDevices

    ''' <summary>Clase per a gestionar la copia de fotos del mobil al pc</summary>
    ''' <remarks>configuracio:
    ''' <list type="table">
    '''   <listheader>
    '''      <term>Variable privada</term><description>Descripcio</description>
    '''   </listheader>
    '''   <item>
    '''      <term>pds <c>PortableDeviceCollection</c></term><description>coleccio de dispositius conectats al pc</description>
    '''   </item>
    '''   <item>
    '''      <term>DestinationPath <c>String</c></term><description>ruta on guardar el fitxer</description>
    '''   </item>
    '''</list>
    ''' </remarks>
    Public Class MTP

        Dim pds As New PortableDeviceCollection

        Private DestinationPath As String

        Public Sub copyImage(ByVal Destination As String)
            DestinationPath = Destination

            pds.Refresh()

            If pds.Count > 0 Then
                For Each device In pds

                    device.Connect()
                    If device.FriendlyName.IndexOf("S6") > -1 Then
                        Dim root = device.GetContents()

                        For Each item In root.Files
                            DisplayItem(device, item)
                        Next

                    End If

                    device.Disconnect()
                Next
            End If
        End Sub

        Private Sub DisplayItem(ByVal dev As PortableDevice, ByVal devobj As PortableDeviceObject)

            'MessageBox.Show("path: " & devobj.Name, "Error", MessageBoxButtons.OK)

            If devobj.IsFolder Then
                DisplayFolderContents(dev, CType(devobj, PortableDeviceFolder))
            End If
        End Sub

        Private Sub DisplayFolderContents(ByVal dev As PortableDevice, ByVal folder As PortableDeviceFolder)
            Dim str As String = "{0}     (a {1})"

            For Each item In folder.Files
                If item.Name = "Camera" Then
                    copyFile(dev, CType(item, PortableDeviceFolder))
                End If

                If item.IsFolder Then
                    DisplayFolderContents(dev, CType(item, PortableDeviceFolder))
                End If
            Next

        End Sub

        Private Sub copyFile(ByVal dev As PortableDevice, ByVal folder As PortableDeviceFolder)
            Dim name As String = ""
            Dim ftx As PortableDeviceObject

            For Each item In folder.Files
                If item.IsFile Then
                    If item.Name > name Then
                        name = item.Name
                        ftx = item
                    End If
                End If
            Next

            If name <> "" Then
                dev.DownloadFile(CType(ftx, PortableDeviceFile), DestinationPath)
            End If

        End Sub

    End Class



    ''' <summary>Clase per a gestionar el llistat de dispositius conectats al pc</summary>
    ''' <remarks>codi copiat de https://cgeers.wordpress.com/?s=WPD i traduit a vb
    ''' </remarks>
    Public Class PortableDeviceCollection
        Inherits Collection(Of PortableDevice)
        Private ReadOnly _deviceManager As PortableDeviceManager

        Public Sub New()
            Me._deviceManager = New PortableDeviceManager()
        End Sub

        Public Sub Refresh()
            Me._deviceManager.RefreshDeviceList()

            ' Determine how many WPD devices are connected
            Dim deviceIds = New String(0) {}
            Dim count As UInteger = 1
            Me._deviceManager.GetDevices(deviceIds(0), count)

            If count > 0 Then
                ' Retrieve the device id for each connected device
                deviceIds = New String(count - 1) {}
                Me._deviceManager.GetDevices(deviceIds(0), count)
                For Each deviceId In deviceIds
                    Add(New PortableDevice(deviceId))
                Next
            End If
        End Sub
    End Class



    ''' <summary>Clase per a gestionar un dispositiu conectat al pc</summary>
    ''' <remarks>codi copiat de https://cgeers.wordpress.com/?s=WPD i traduit a vb
    ''' </remarks>
    Public Class PortableDevice

        Private ReadOnly _device As PortableDeviceClass

        Public Sub New(ByVal deviceId As String)
            Me._device = New PortableDeviceClass
            Me.DeviceId = deviceId

            Connect()
            _name = GetName()

            Disconnect()
        End Sub

        Public Property DeviceId() As String
            Get
                Return m_DeviceId
            End Get
            Set(ByVal value As String)
                m_DeviceId = value
            End Set
        End Property
        Private m_DeviceId As String

        Private _name As String
        Public ReadOnly Property FriendlyName() As String
            Get
                If [String].IsNullOrEmpty(_name) Then
                    Connect()
                    _name = GetName()
                    Disconnect()
                End If
                Return _name
            End Get
        End Property

        Private Function GetName() As String
            If Not Me._isConnected Then
                Throw New InvalidOperationException("Not connected to device.")
            End If

            ' Retrieve the properties of the device
            Dim content As IPortableDeviceContent
            Dim properties As IPortableDeviceProperties
            Me._device.Content(content)
            content.Properties(properties)

            ' Retrieve the values for the properties
            Dim propertyValues As IPortableDeviceValues
            properties.GetValues("DEVICE", Nothing, propertyValues)

            ' Identify the property to retrieve
            Dim [property] = New _tagpropertykey()
            [property].fmtid = DeviceGUID.PDNameID
            'new Guid(0x26D4979A, 0xE643, 0x4626, 0x9E, 0x2B, 0x73, 0x6D, 0xC0, 0xC9, 0x2F, 0xDC);
            [property].pid = 12

            ' Retrieve the friendly name
            Dim propertyValue As String
            propertyValues.GetStringValue([property], propertyValue)

            Return propertyValue
        End Function

        Friend ReadOnly Property PortableDeviceClass() As PortableDeviceClass
            Get
                Return Me._device
            End Get
        End Property

        Private _isConnected As Boolean
        Public Sub Connect()
            If Me._isConnected Then
                Return
            End If

            Dim clientInfo = DirectCast(New PortableDeviceTypesLib.PortableDeviceValuesClass(), IPortableDeviceValues)
            Me._device.Open(Me.DeviceId, clientInfo)
            Me._isConnected = True
        End Sub

        Public Sub Disconnect()
            If Not Me._isConnected Then
                Return
            End If
            Me._device.Close()
            Me._isConnected = False
        End Sub

        Public Function GetContents() As PortableDeviceFolder
            Dim root = New PortableDeviceFolder("DEVICE", "DEVICE")

            Dim content As IPortableDeviceContent
            Me._device.Content(content)

            EnumerateContents(content, root)

            Return root
        End Function

        Private Shared Sub EnumerateContents(ByRef content As IPortableDeviceContent, ByVal parent As PortableDeviceFolder)
            ' Get the properties of the object
            Dim properties As IPortableDeviceProperties
            content.Properties(properties)

            ' Enumerate the items contained by the current object
            Dim objectIds As IEnumPortableDeviceObjectIDs
            content.EnumObjects(0, parent.Id, Nothing, objectIds)

            Dim fetched As UInteger = 0
            Do
                Dim objectId As String

                objectIds.[Next](1, objectId, fetched)
                If fetched > 0 Then
                    Dim currentObject = WrapObject(properties, objectId)

                    parent.Files.Add(currentObject)

                    If TypeOf currentObject Is PortableDeviceFolder Then
                        EnumerateContents(content, DirectCast(currentObject, PortableDeviceFolder))
                    End If
                End If
            Loop While fetched > 0
        End Sub

        Public Sub DownloadFile(ByVal file As PortableDeviceFile, ByVal saveToPath As String)
            Dim content As IPortableDeviceContent
            Me._device.Content(content)

            Dim resources As IPortableDeviceResources
            content.Transfer(resources)

            Dim wpdStream As PortableDeviceApiLib.IStream
            Dim optimalTransferSize As UInteger = 0

            Dim [property] = New _tagpropertykey()
            [property].fmtid = DeviceGUID.PDDefault
            'new Guid(0xE81E79BE, 0x34F0, 0x41BF, 0xB5, 0x3F, 0xF1, 0xA0, 0x6A, 0xE8, 0x78, 0x42);
            [property].pid = 0

            resources.GetStream(file.Id, [property], 0, optimalTransferSize, wpdStream)

            Dim sourceStream As System.Runtime.InteropServices.ComTypes.IStream = DirectCast(wpdStream, System.Runtime.InteropServices.ComTypes.IStream)

            Dim filename = Path.GetFileName(file.Id)
            'Dim targetStream As New FileStream(Path.Combine(saveToPath, filename), FileMode.Create, FileAccess.Write)
            Dim targetStream As New FileStream(saveToPath, FileMode.Create, FileAccess.Write)

            Dim buffer = New Byte(1023) {}
            Dim bytesRead As Integer
            Dim pRead As IntPtr

            pRead = Marshal.AllocHGlobal(4)
            Marshal.WriteInt32(pRead, bytesRead)
            Do
                sourceStream.Read(buffer, 1024, pRead)
                targetStream.Write(buffer, 0, 1024)

                bytesRead = Marshal.ReadInt32(pRead)
            Loop While bytesRead > 0

            Marshal.FreeHGlobal(pRead)
            targetStream.Close()
        End Sub

        Public Sub DeleteFile(ByVal file As PortableDeviceFile)
            Dim content As IPortableDeviceContent
            Me._device.Content(content)

            Dim vari = New PortableDeviceApiLib.tag_inner_PROPVARIANT()
            StringToPropVariant(file.Id, vari)

            Dim objectIds As PortableDeviceApiLib.IPortableDevicePropVariantCollection = TryCast(New PortableDeviceTypesLib.PortableDevicePropVariantCollection(), PortableDeviceApiLib.IPortableDevicePropVariantCollection)
            objectIds.Add(vari)

            content.Delete(0, objectIds, Nothing)
        End Sub

        Public Sub TransferContentToDevice(ByVal fileName As String, ByVal parentObjectId As String)
            Dim content As IPortableDeviceContent
            Me._device.Content(content)

            Dim values As IPortableDeviceValues = GetRequiredPropertiesForContentType(fileName, parentObjectId)

            Dim tempStream As PortableDeviceApiLib.IStream
            Dim optimalTransferSizeBytes As UInteger = 0
            content.CreateObjectWithPropertiesAndData(values, tempStream, optimalTransferSizeBytes, Nothing)

            Dim targetStream As System.Runtime.InteropServices.ComTypes.IStream = DirectCast(tempStream, System.Runtime.InteropServices.ComTypes.IStream)
            Try
                Using sourceStream = New FileStream(fileName, FileMode.Open, FileAccess.Read)
                    Dim buffer = New Byte(optimalTransferSizeBytes - 1) {}
                    Dim bytesRead As Integer
                    Do
                        bytesRead = sourceStream.Read(buffer, 0, CInt(optimalTransferSizeBytes))
                        Dim pcbWritten As IntPtr = IntPtr.Zero
                        targetStream.Write(buffer, CInt(optimalTransferSizeBytes), pcbWritten)
                    Loop While bytesRead > 0
                End Using
                targetStream.Commit(0)
            Finally
                Marshal.ReleaseComObject(tempStream)
            End Try
        End Sub

        Private Function GetRequiredPropertiesForContentType(ByVal fileName As String, ByVal parentObjectId As String) As IPortableDeviceValues
            Dim values As IPortableDeviceValues = TryCast(New PortableDeviceTypesLib.PortableDeviceValues(), IPortableDeviceValues)

            Dim WPD_OBJECT_PARENT_ID = New _tagpropertykey()
            WPD_OBJECT_PARENT_ID.fmtid = DeviceGUID.ObjectParentID
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 
            '         0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            WPD_OBJECT_PARENT_ID.pid = 3
            values.SetStringValue(WPD_OBJECT_PARENT_ID, parentObjectId)

            Dim fileInfo As New FileInfo(fileName)
            Dim WPD_OBJECT_SIZE = New _tagpropertykey()
            WPD_OBJECT_SIZE.fmtid = DeviceGUID.ObjectSize
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 
            '         0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            WPD_OBJECT_SIZE.pid = 11
            values.SetUnsignedLargeIntegerValue(WPD_OBJECT_SIZE, CULng(fileInfo.Length))

            Dim WPD_OBJECT_ORIGINAL_FILE_NAME = New _tagpropertykey()
            WPD_OBJECT_ORIGINAL_FILE_NAME.fmtid = DeviceGUID.OriginalFileName
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 
            '         0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            WPD_OBJECT_ORIGINAL_FILE_NAME.pid = 12
            values.SetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, Path.GetFileName(fileName))

            Dim WPD_OBJECT_NAME = New _tagpropertykey()
            WPD_OBJECT_NAME.fmtid = DeviceGUID.ObjectName
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 
            '         0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            WPD_OBJECT_NAME.pid = 4
            values.SetStringValue(WPD_OBJECT_NAME, Path.GetFileName(fileName))

            Return values
        End Function

        Private Shared Sub StringToPropVariant(ByVal value As String, ByRef propvarValue As PortableDeviceApiLib.tag_inner_PROPVARIANT)
            Dim pValues As PortableDeviceApiLib.IPortableDeviceValues = DirectCast(New PortableDeviceTypesLib.PortableDeviceValuesClass(), PortableDeviceApiLib.IPortableDeviceValues)

            Dim WPD_OBJECT_ID = New _tagpropertykey()
            WPD_OBJECT_ID.fmtid = DeviceGUID.ObjectName
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 0xDA,
            '         0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            WPD_OBJECT_ID.pid = 2

            pValues.SetStringValue(WPD_OBJECT_ID, value)

            pValues.GetValue(WPD_OBJECT_ID, propvarValue)
        End Sub

        Private Shared Function WrapObject(ByVal properties As IPortableDeviceProperties, ByVal objectId As String) As PortableDeviceObject
            Dim keys As IPortableDeviceKeyCollection
            properties.GetSupportedProperties(objectId, keys)

            Dim values As IPortableDeviceValues
            properties.GetValues(objectId, keys, values)

            ' Get the name of the object
            Dim name As String
            Dim [property] = New _tagpropertykey()
            [property].fmtid = DeviceGUID.ObjectName
            'new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            [property].pid = 4
            values.GetStringValue([property], name)

            ' Get the type of the object
            Dim contentType As Guid
            [property] = New _tagpropertykey()
            [property].fmtid = DeviceGUID.ObjectName
            ' new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC, 0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            [property].pid = 7
            values.GetGuidValue([property], contentType)

            Dim folderType = DeviceGUID.PDFolder
            ' new Guid(0x27E2E392, 0xA111, 0x48E0, 0xAB, 0x0C, 0xE1, 0x77, 0x05, 0xA0, 0x5F, 0x85);
            Dim functionalType = DeviceGUID.PDFunctionalType
            ' new Guid(0x99ED0160, 0x17FF, 0x4C44, 0x9D, 0x98, 0x1D, 0x7A, 0x6F, 0x94, 0x19, 0x21);

            If contentType = folderType OrElse contentType = functionalType Then
                Return New PortableDeviceFolder(objectId, name)
            End If

            Return New PortableDeviceFile(objectId, name)
        End Function


        Private NotInheritable Class DeviceGUID
            Private Sub New()
            End Sub
            Public Shared PDNameID As New Guid(&H26D4979A, &HE643, &H4626, &H9E, &H2B, &H73, _
             &H6D, &HC0, &HC9, &H2F, &HDC)
            Public Shared ObjectName As New Guid(&HEF6B490DUI, &H5CD8, &H437A, &HAF, &HFC, &HDA, _
             &H8B, &H60, &HEE, &H4A, &H3C)
            Public Shared PDFolder As New Guid(&H27E2E392, &HA111, &H48E0, &HAB, &HC, &HE1, _
             &H77, &H5, &HA0, &H5F, &H85)
            Public Shared PDFunctionalType As New Guid(&H99ED0160UI, &H17FF, &H4C44, &H9D, &H98, &H1D, _
             &H7A, &H6F, &H94, &H19, &H21)
            Public Shared PDDefault As New Guid(&HE81E79BEUI, &H34F0, &H41BF, &HB5, &H3F, &HF1, _
             &HA0, &H6A, &HE8, &H78, &H42)

            Public Shared ObjectParentID As New Guid(&HEF6B490DUI, &H5CD8, &H437A, &HAF, &HFC, &HDA, _
             &H8B, &H60, &HEE, &H4A, &H3C)
            Public Shared ObjectSize As New Guid(&HEF6B490DUI, &H5CD8, &H437A, &HAF, &HFC, &HDA, _
             &H8B, &H60, &HEE, &H4A, &H3C)
            Public Shared OriginalFileName As New Guid(&HEF6B490DUI, &H5CD8, &H437A, &HAF, &HFC, &HDA, _
             &H8B, &H60, &HEE, &H4A, &H3C)


        End Class

    End Class



    Public Enum ItemTypes
        Unknown = 0
        File = 1
        Folder = 2
    End Enum

    ''' <summary>Clase base per a gestionar els fitxers i carpetes dins del dispositiu</summary>
    ''' <remarks>codi copiat de https://cgeers.wordpress.com/?s=WPD i traduit a vb
    ''' </remarks>
    Public MustInherit Class PortableDeviceObject
        Protected Sub New(ByVal id As String, ByVal name As String)
            Me.Id = id
            Me.Name = name
        End Sub

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Private Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As String

        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Private Set(ByVal value As String)
                m_Name = value
            End Set
        End Property
        Private m_Name As String

        Public MustOverride Function IsFile() As Boolean

        Public MustOverride Function IsFolder() As Boolean

        Public Function GetItemType() As ItemTypes
            Return ItemType(Me)
        End Function

        Public Shared Function ItemType(ByVal obj As PortableDeviceObject) As ItemTypes
            If TypeOf obj Is PortableDeviceFolder Then
                Return ItemTypes.Folder
            End If
            If TypeOf obj Is PortableDeviceFile Then
                Return ItemTypes.File
            End If

            Return ItemTypes.Unknown

        End Function

    End Class



    ''' <summary>Clase per a gestionar uns carpeta dins del dispositiu</summary>
    ''' <remarks>codi copiat de https://cgeers.wordpress.com/?s=WPD i traduit a vb
    ''' </remarks>
    Public Class PortableDeviceFolder
        Inherits PortableDeviceObject
        Public Sub New(ByVal id As String, ByVal name As String)
            MyBase.New(id, name)
            Me.Files = New List(Of PortableDeviceObject)()
        End Sub

        Public Property Files() As IList(Of PortableDeviceObject)
            Get
                Return m_Files
            End Get
            Set(ByVal value As IList(Of PortableDeviceObject))
                m_Files = value
            End Set
        End Property
        Private m_Files As IList(Of PortableDeviceObject)

        Public Overrides Function IsFile() As Boolean
            Return False
        End Function

        Public Overrides Function IsFolder() As Boolean
            Return True
        End Function
    End Class



    ''' <summary>Clase per a gestionar un fitxer dins del dispositiu</summary>
    ''' <remarks>codi copiat de https://cgeers.wordpress.com/?s=WPD i traduit a vb
    ''' </remarks>
    Public Class PortableDeviceFile
        Inherits PortableDeviceObject
        Public Sub New(ByVal id As String, ByVal name As String)
            MyBase.New(id, name)
        End Sub

        Public Property Path() As String
            Get
                Return m_Path
            End Get
            Set(ByVal value As String)
                m_Path = value
            End Set
        End Property
        Private m_Path As String

        Public Overrides Function IsFile() As Boolean
            Return True
        End Function

        Public Overrides Function IsFolder() As Boolean
            Return False
        End Function
    End Class


End Namespace