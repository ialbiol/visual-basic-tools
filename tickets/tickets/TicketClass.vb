﻿
''' <summary>Classe base que representa un tiquet</summary>
''' <remarks>Propietats:
''' <list type="table">
'''   <listheader>
'''      <term>Propietat</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>Id <c>Integer</c></term><description>Identificador unic del tiquet dins la taula de la base de dades</description>
'''   </item>
'''   <item>
'''      <term>IdCif <c>String</c></term><description>Identificador unic del cif / establiment dins la taula de la base de dades</description>
'''   </item>
'''   <item>
'''      <term>CIF_Cif <c>String</c></term><description>Cif de l'establiment</description>
'''   </item>
'''   <item>
'''      <term>CIF_Nom <c>String</c></term><description>Nom de l'establiment</description>
'''   </item>
'''   <item>
'''      <term>IdVenedor <c>String</c></term><description>Identificador unic del venedor / treballador dins la taula de la base de dades</description>
'''   </item>
'''    <item>
'''      <term>VEN_Nom <c>String</c></term><description>Nom del venedor / treballador</description>
'''   </item>
'''    <item>
'''      <term>Data <c>DateTime</c></term><description>Data del tiquet</description>
'''   </item>
'''    <item>
'''      <term>DataText <c>String</c></term><description>Data del tiquet en format text</description>
'''   </item>
'''    <item>
'''      <term>Ruta <c>String</c></term><description>Ruta que realitzava el venedor</description>
'''   </item>
'''    <item>
'''      <term>NifVenedor <c>String</c></term><description>Nif del venedor. NO USAR. BORRAR??</description>
'''   </item>
'''    <item>
'''      <term>Import <c>String</c></term><description>Import del tiquet</description>
'''   </item>
'''    <item>
'''      <term>NumFactura <c>String</c></term><description>Numero de la factura on esta el tiquet</description>
'''   </item>
'''    <item>
'''      <term>CifModificat <c>Boolean</c></term><description>Indica si s'ha modificat el cif o no</description>
'''   </item>
'''    <item>
'''      <term>Modificat <c>Integer</c></term><description>Indica si la fitxada s'ha modificat (<c>1</c>) o no (<c>0</c>)</description>
'''   </item>
'''    <item>
'''      <term>DataModificat <c>DateTime</c></term><description>Quan s'ha modificat la fitxada per ultima vegada</description>
'''   </item>
'''</list>
''' </remarks>

Public Class TicketClass


    Private s_id As String
    Public Property Id() As String
        Get
            Return s_id
        End Get
        Set(ByVal value As String)
            s_id = value
        End Set
    End Property


    Private s_idCif As String
    Public Property IdCif() As String
        Get
            Return s_idCif
        End Get
        Set(ByVal value As String)
            s_idCif = value
        End Set
    End Property


    Private s_CIF_Cif As String
    Public Property CIF_Cif() As String
        Get
            Return s_CIF_Cif
        End Get
        Set(ByVal value As String)
            s_CIF_Cif = value

            s_cifModificat = True
        End Set
    End Property


    Private s_CIF_Nom As String
    Public Property CIF_Nom() As String
        Get
            Return s_CIF_Nom
        End Get
        Set(ByVal value As String)
            s_CIF_Nom = value

            s_cifModificat = True
        End Set
    End Property


    Private s_IdVenedor As String
    Public Property IdVenedor() As String
        Get
            Return s_IdVenedor
        End Get
        Set(ByVal value As String)
            s_IdVenedor = value
        End Set
    End Property


    Private s_VEN_Nom As String
    Public Property VEN_Nom() As String
        Get
            Return s_VEN_Nom
        End Get
        Set(ByVal value As String)
            s_VEN_Nom = value
        End Set
    End Property

    Private s_VEN_Nif As String
    Public Property VEN_Nif() As String
        Get
            Return s_VEN_Nif
        End Get
        Set(ByVal value As String)
            s_VEN_Nif = value
        End Set
    End Property


    Private s_data As Date
    Public Property Data() As Date
        Get
            Return s_data
        End Get
        Set(ByVal value As Date)
            s_data = value
            s_data_text = value.ToString()
        End Set
    End Property


    Private s_data_text As String
    Public Property DataText() As String
        Get
            Return s_data_text
        End Get
        Set(ByVal value As String)
            s_data_text = value
        End Set
    End Property


    Private s_ruta As String
    Public Property Ruta() As String
        Get
            Return s_ruta
        End Get
        Set(ByVal value As String)
            s_ruta = value
        End Set
    End Property


    Private s_import As String
    Public Property Import() As String
        Get
            Return s_import
        End Get
        Set(ByVal value As String)
            s_import = value.Replace(",", ".")
        End Set
    End Property


    Private s_num_factura As String
    Public Property NumFactura() As String
        Get
            Return s_num_factura
        End Get
        Set(ByVal value As String)
            s_num_factura = value
        End Set
    End Property


    Private s_modificat As Integer
    Public Property Modificat() As Integer
        Get
            Return s_modificat
        End Get
        Set(ByVal value As Integer)
            s_modificat = value
        End Set
    End Property


    Private s_dataModificat As Date
    Public Property DataModificat() As Date
        Get
            Return s_dataModificat
        End Get
        Set(ByVal value As Date)
            s_dataModificat = value
        End Set
    End Property


    Private s_cifModificat As Boolean
    Public Property CifModificat() As Boolean
        Get
            Return s_cifModificat
        End Get
        Set(ByVal value As Boolean)
            s_cifModificat = value
        End Set
    End Property


    ''' <summary>Funcio que neteja els valors de totes les propietats</summary>
    ''' <remarks>Inicialitza totes les propietats de l'objecte al seu valor per defecte
    ''' </remarks>
    Public Sub Clear()
        s_id = "0"
        s_idCif = ""
        s_CIF_Cif = ""
        s_CIF_Nom = ""
        s_IdVenedor = ""
        s_VEN_Nom = ""
        s_VEN_Nif = ""
        s_data = Nothing
        s_data_text = ""
        s_ruta = ""
        s_import = "0"
        s_num_factura = ""
        s_modificat = 0
        s_dataModificat = Nothing

        s_cifModificat = False

    End Sub


    ''' <summary>Funcio que converteix la data en una cadena de text en 0</summary>
    ''' <remarks> als mesos i dies menors de 10, els posa el cero davant
    ''' </remarks>
    ''' <returns><c>String</c>: ruta base</returns>
    Public Function getDataString(Optional ByVal separador As String = "") As String
        Dim str As String = ""

        str = s_data.Year.ToString & separador

        If s_data.Month < 10 Then
            str = str & "0" & s_data.Month.ToString & separador
        Else
            str = str & s_data.Month.ToString & separador
        End If

        If s_data.Day < 10 Then
            str = str & "0" & s_data.Day.ToString
        Else
            str = str & s_data.Day.ToString
        End If

        Return str
    End Function


    ''' <summary>Funcio que mostra les dades en un message box</summary>
    ''' <remarks>S'utilitza per a debugar l'aplicacio
    ''' </remarks>
    Public Sub Show()
        MessageBox.Show("dada: " & ToStr(), "Debug", MessageBoxButtons.OK)
    End Sub

    ''' <summary>Funcio que converteix les dades en una cadena de text</summary>
    ''' <remarks>retorna un string en los valors mes importants. Se utilitza als logs per a debugar l'aplicacio
    ''' </remarks>
    ''' <returns><c>String</c>: dada convertida a string</returns>
    Public Function ToStr() As String
        Dim str As String = ""

        str = str & "data: " & s_data.ToString() & " | "
        str = str & "venedor: " & s_VEN_Nom & " | "
        str = str & "cif: " & s_CIF_Cif & " | "
        str = str & "import: " & s_import & " | "

        Return str
    End Function

    ''' <summary>Funcio que construix la ruta i nom base de les imatges dels tiquets</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: ruta base</returns>
    Public Function getImagePath() As String
        Dim str As String = ""

        str = getDataString("-") & "/"

        Return str
    End Function

    ''' <summary>Funcio que retorna el text per a buscar els tiquets al correu</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: text</returns>
    Public Function getSearchSubject() As String
        Dim str As String = ""

        str = "0" & s_ruta & " " & getDataString()

        Return str
    End Function

End Class
