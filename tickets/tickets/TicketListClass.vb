﻿Imports System.IO

''' <summary>Clase que gestiona tota la logica del programa i del proces d'importacio de dades</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>DB <c>mysql</c></term><description>Objecte que gestiona la conexio en la base de dades MySQL</description>
'''   </item>
'''   <item>
'''      <term>Log <c>LogClass</c></term><description>Objecte que gestiona el fitxer de log</description>
'''   </item>
'''   <item>
'''      <term>LlistatVenedors <c>List(Of String)</c></term><description>Llistat de les rutes dels venedors</description>
'''   </item>
'''   <item>
'''      <term>LlistatImatges <c>List(Of String)</c></term><description>Llistat de les rutes de les imatges buscades</description>
'''   </item>
'''   <item>
'''      <term>LlistatImatgesIndex <c>Integer</c></term><description>Index del llistat d'imatges que s'esta mostrant</description>
'''   </item>
'''   <item>
'''      <term>path <c>String = "Z:\pockets""</c></term><description>Ruta dels fitxers a analitzar per a buscar els tiquets</description>
'''   </item>
'''   <item>
'''      <term>folderBaseName <c>String = "/pocket"</c></term><description>Ruta base on buscar els fitxers a analitzar</description>
'''   </item>
'''   <item>
'''      <term>subfolderBaseName <c>String = "/Liquidaciones"</c></term><description>Subcarpeta on buscar els fitxers a analitzar</description>
'''   </item>
'''   <item>
'''      <term>baseName <c>String = "/Liq "</c></term><description>Nom base dels fitxers a analitzar</description>
'''   </item>
'''   <item>
'''      <term>facturesPath <c>String = "Z:\FacturesTiquets/"</c></term><description>Ruta on es guarden les factures</description>
'''   </item>
'''   <item>
'''      <term>imatgesPath <c>String = "Z:\ImatgesTiquets/"</c></term><description>Ruta on es guarden les imatges dels tiquets</description>
'''   </item>
'''    <item>
'''      <term>COL_DATA_TIQUET <c> = 0</c></term><description>Constant en l'index del llistat de factures per a la columna Data tiquet</description>
'''   </item>
'''     <item>
'''      <term>COL_CIF_EMPRESA <c> = 1</c></term><description>Constant en l'index del llistat de factures per a la columna Cif empresa</description>
'''   </item>
'''     <item>
'''      <term>COL_NON_EMPRESA <c> = 2</c></term><description>Constant en l'index del llistat de factures per a la columna Nom empresa</description>
'''   </item>
'''     <item>
'''      <term>COL_IMPORT <c> = 3</c></term><description>Constant en l'index del llistat de factures per a la columna import</description>
'''   </item>
'''     <item>
'''      <term>COL_NUMFACTURA <c> = 4</c></term><description>Constant en l'index del llistat de factures per a la columna Num factura</description>
'''   </item>
'''     <item>
'''      <term>COL_BASE_IMPONIBLE <c> = 5</c></term><description>Constant en l'index del llistat de factures per a la columna base imponible</description>
'''   </item>
'''     <item>
'''      <term>COL_IVA <c> = 6</c></term><description>Constant en l'index del llistat de factures per a la columna iva</description>
'''   </item>
'''     <item>
'''      <term>COL_RUTA <c> = 7</c></term><description>Constant en l'index del llistat de factures per a la columna ruta</description>
'''   </item>
'''     <item>
'''      <term>COL_NIF_VENEDOR <c> = 8</c></term><description>Constant en l'index del llistat de factures per a la columna Nif venedor</description>
'''   </item>
'''</list>
''' </remarks>

Public Class TicketListClass

    Private DB As New mysql

    Public Log As New LogClass
    Public LlistatVenedors As New List(Of String)
    Private LlistatImatges As New List(Of String)
    Private LlistatImatgesIndex As Integer

    Private path As String = "Z:\pockets"
    Private folderBaseName As String = "/pocket"
    Private subfolderBaseName As String = "/Liquidaciones"
    Private baseName As String = "/Liq "

    Private facturesPath As String = "Z:\FacturesTiquets/"
    Private imatgesPath As String = "Z:\ImatgesTiquets/"
    Public imatgesPathLimpia As String = "Z:\ImatgesTiquets/clean.jpg"

    Private COL_DATA_TIQUET = 0 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_CIF_EMPRESA = 1 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_NON_EMPRESA = 2 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_IMPORT = 3 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_NUMFACTURA = 4 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_BASE_IMPONIBLE = 5 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_IVA = 6 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_RUTA = 7 'numero de columna del llistat que genera les factures dels tiquets
    Private COL_NIF_VENEDOR = 8 'numero de columna del llistat que genera les factures dels tiquets


    ''' <summary>Funcio que busca el id de la taula cifs del cif indicat. si no existeix, el crea i retorna el id</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="cif"><c>String</c>: Cif a buscar</param>
    ''' <param name="nom"><c>Optional String=""</c>: Nom establiment a buscar</param>
    ''' <returns><c>Integer</c>: id de la taula cifs del cif indicat</returns>
    Public Function FindCif(ByVal cif As String, Optional ByVal nom As String = "") As Integer
        Dim ds As DataSet

        ' busco el id del cif
        ds = DB.doSelect("SELECT id FROM cifs WHERE cif = '" & cif & "'")

        If ds.Tables(0).Rows.Count > 0 Then 'el cif existeix
            Return ds.Tables(0).Rows(0)(0)
        Else 'el cif no existeix. el creo
            If DB.doOperation("INSERT INTO cifs (cif, nom) values ('" & cif & "', '" & nom & "')") = 0 Then
                'error. no he pogut crear el cif
                MessageBox.Show("No puc crear el cif " & cif, "Error", MessageBoxButtons.OK)
                Return 0
            Else
                ' busco el id del cif
                ds = DB.doSelect("select id from cifs where cif = '" & cif & "'")
                Return ds.Tables(0).Rows(0)(0) 'no poso if perque l'acabo de crear i comprobar que esta ben creat
            End If
        End If

        Return 0
    End Function

    ''' <summary>Funcio que busca el id de la taula cifs del cif indicat. si no existeix, el crea i retorna el TicketClass.</summary>
    ''' <remarks> usat a l'editor
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte tiquet a modificar</param>
    ''' <returns><c>TicketClass</c>: Objecte tiquet modificat</returns>
    Public Function FindCif(ByVal dada As TicketClass) As TicketClass
        Dim ds As DataSet

        ' busco el id del cif
        ds = DB.doSelect("SELECT id, nom FROM cifs WHERE cif = '" & dada.CIF_Cif & "'")

        If ds.Tables(0).Rows.Count > 0 Then 'el cif existeix
            dada.IdCif = ds.Tables(0).Rows(0)(0)
            dada.CIF_Nom = ds.Tables(0).Rows(0)(1)

        Else 'el cif no existeix. el creo
            If DB.doOperation("INSERT INTO cifs (cif, nom) values ('" & dada.CIF_Cif & "', '" & dada.CIF_Nom & "')") = 0 Then
                'error. no he pogut crear el cif
                MessageBox.Show("No puc crear el cif " & dada.CIF_Cif, "Error", MessageBoxButtons.OK)

            Else
                ' busco el id del cif
                ds = DB.doSelect("select id from cifs where cif = '" & dada.CIF_Cif & "'")
                dada.IdCif = ds.Tables(0).Rows(0)(0) 'no poso if perque l'acabo de crear i comprobar que esta ben creat

            End If
        End If

        Return dada
    End Function


    ''' <summary>Funcio que busca el id de la taula venedors del nom indicat. si no existeix, retorna 0</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="nom"><c>Optional String = ""</c>: Nom del venedor a buscar</param>
    ''' <param name="ruta"><c>Optional String = ""</c>: Ruta del venedor a buscar</param>
    ''' <returns><c>Integer</c>: id de la taula venedors del nom indicat</returns>
    Public Function FindVenedor(Optional ByVal nom As String = "", Optional ByVal ruta As String = "", Optional ByVal nif As String = "") As Integer
        Dim ds As DataSet
        Dim sql As String

        ' busco el id del venedor
        If nom <> "" Then
            sql = "SELECT id FROM venedors WHERE UPPER(nom) = '" & nom.ToUpper() & "'"

            'If DB.test Then
            'MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
            'End If

            ds = DB.doSelect(sql)
        Else
            If ruta <> "" Then
                sql = "SELECT id FROM venedors WHERE ruta = '" & ruta & "'"

                'If DB.test Then
                '   MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
                'End If

                ds = DB.doSelect(sql)
            Else
                If nif <> "" Then
                    sql = "SELECT id FROM venedors WHERE nif = '" & nif & "'"

                    'If DB.test Then
                    '   MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
                    'End If

                    ds = DB.doSelect(sql)

                    If ds.Tables(0).Rows.Count > 0 Then 'el nif existeix
                        Return ds.Tables(0).Rows(0)(0)
                    Else 'el nif no existeix. el creo
                        If DB.doOperation("INSERT INTO venedors (nif) values ('" & nif & "')") = 0 Then
                            'error. no he pogut crear el nif
                            MessageBox.Show("No puc crear el nif " & nif, "Error", MessageBoxButtons.OK)
                            Return 0
                        Else
                            ' busco el id del nif
                            ds = DB.doSelect("select id FROM venedors WHERE nif = '" & nif & "'")
                            Return ds.Tables(0).Rows(0)(0) 'no poso if perque l'acabo de crear i comprobar que esta ben creat
                        End If
                    End If
                Else
                    Return 0
                End If
            End If
        End If

        If ds.Tables(0).Rows.Count > 0 Then 'el venedor existeix
            Return ds.Tables(0).Rows(0)(0)
        End If

        Return 0
    End Function

    ''' <summary>Funcio que busca el venedor de la taula venedors del id indicat. retorna el TicketClass.</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte tiquet a modificar</param>
    ''' <returns><c>TicketClass</c>: Objecte tiquet modificat</returns>
    Public Function FindVenedor(ByVal dada As TicketClass) As TicketClass
        Dim ds As DataSet

        ' busco el id del cif
        ds = DB.doSelect("SELECT nom, nif FROM venedors WHERE id = '" & dada.IdVenedor & "'")

        If ds.Tables(0).Rows.Count > 0 Then 'el cif existeix
            If IsDBNull(ds.Tables(0).Rows(0)(0)) = False Then
                dada.VEN_Nom = ds.Tables(0).Rows(0)(0)
            End If

            If IsDBNull(ds.Tables(0).Rows(0)(1)) = False Then
                dada.VEN_Nif = ds.Tables(0).Rows(0)(1)
            End If

        End If

        Return dada
    End Function


    ''' <summary>Funcio que busca la ultima data que tenim tickets controlats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: data ultim tiquet controlat</returns>
    Private Function LastDate() As String
        Dim ds As DataSet

        ' busco la data mes gran dins la taula dels tickets
        ds = DB.doSelect("SELECT dataText FROM tickets ORDER BY data DESC LIMIT 1")

        If ds.Tables(0).Rows.Count > 0 Then
            Return (ds.Tables(0).Rows(0)(0))
        Else
            Return ("19/06/2017") 'data primera carpeta
        End If
    End Function

    ''' <summary>Funcio que busca la ultima data de les carpetes</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: ultima data de les carpetes</returns>
    Private Function LastDateFolder() As String
        Dim carpeta As New DirectoryInfo(path + folderBaseName + LlistatVenedors(0) + subfolderBaseName)
        Dim word As Date = "19/06/2017"

        For Each D As DirectoryInfo In carpeta.GetDirectories()
            If word < D.CreationTime Then
                If D.CreationTime.DayOfYear < Now.DayOfYear Then
                    word = D.CreationTime
                End If
            End If
        Next
        Return (word)

    End Function



    ''' <summary>Funcio que busca totes les carpetes "pocketX" i crea un llistat</summary>
    ''' <remarks>
    ''' </remarks>
    Private Sub loadVenedors()
        Dim carpeta As New DirectoryInfo(path)
        Dim word As String

        For Each D As DirectoryInfo In carpeta.GetDirectories()
            If D.Name <> "DATIN" Then
                word = D.Name.Substring(6)

                LlistatVenedors.Add(word)
            End If
        Next
    End Sub

    ''' <summary>Funcio que crea la ruta correcta per al fitxer d'historia</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data a buscar</param>
    ''' <param name="Venedor"><c>String</c>: Ruta del venedor a buscar</param>
    ''' <returns><c>String</c>: ruta generada</returns>
    Public Function CrearRutaHistoric(ByVal data As Date, ByVal Venedor As String) As String
        Dim dataString As String

        dataString = data.Year.ToString

        If data.Month < 10 Then
            dataString = dataString + "0" + data.Month.ToString
        Else
            dataString = dataString + data.Month.ToString
        End If

        If data.Day < 10 Then
            dataString = dataString + "0" + data.Day.ToString
        Else
            dataString = dataString + data.Day.ToString
        End If

        Return (path + folderBaseName + Venedor + subfolderBaseName + baseName + dataString)
    End Function


    ''' <summary>Funcio que llig el fitxer d'historia, busca el ticket i el posa a la base de dades</summary>
    ''' <remarks>La seua funcio es analitzar el fitxer i omlir un objecte tiquet, que despues enviara a insertar a la base de dades
    ''' </remarks>
    ''' <param name="fitxer"><c>String</c>: Fitxer a analitzar</param>
    ''' <param name="Venedor"><c>String</c>: Ruta del venedor</param>
    ''' <param name="data"><c>Date</c>: Data a gestionar</param>
    Public Sub gestioFitxerHistoria(ByVal fitxer As String, ByVal Venedor As String, ByVal data As Date)
        Dim sr As StreamReader

        Dim line As String
        Dim cif As String
        Dim imp As String
        Dim nif As String = ""
        Dim dataStr As String()
        Dim charsToTrim() As Char = {"e"c, "u"c, "r"c, "o"c, "s"c, " "c, "€"c, "d"c, "n"c, "i"c}
        Dim trobat As Boolean = False

        Dim dada As New TicketClass
        Dim list As New List(Of TicketClass)
        Dim cont As Integer = -1
        Dim pos As Integer = 0

        ' llijo fitxer
        sr = New StreamReader(fitxer)

        Do While sr.Peek() >= 0
            ' per a cada linia
            line = sr.ReadLine()
            If Not line = "" Then
                'busco la paraula FOT
                If line.Contains("FOT") Then
                    ' dividijo la linia per parts
                    Dim words As String() = line.Split(New Char() {"|"c})

                    ' guardo la data
                    dataStr = words(0).Split(New Char() {" "c})

                    ' agafo la 3 part, que es la que te la info
                    line = words(2)

                    ' dividijo per : per a tenir el cif i el imp
                    Dim parts As String() = line.Split(New Char() {":"c})
                    cif = parts(1).Replace("imp", "")
                    cif = cif.Replace("-", "")
                    cif = cif.Replace(" ", "")
                    cif = cif.ToUpper()
                    imp = parts(2).Trim(charsToTrim)
                    imp = imp.Replace(",", ".") 'el decimal sempre es un . al insert
                    If parts.Count >= 4 Then
                        nif = parts(3)
                    End If

                    ' si tinc import i cif
                    If imp <> "" And cif <> "" Then
                        ' busco si tinc un tiquet en este nif
                        pos = -1
                        cont = 0
                        While cont < list.Count And pos = -1
                            If list(cont).VEN_Nif = nif Then
                                pos = cont
                            End If
                            cont = cont + 1
                        End While

                        ' no he trobat la dada. creo una nova
                        If pos = -1 Then
                            'valors inicials dada
                            dada = New TicketClass
                            dada.Clear()
                            dada.Data = data 'tambe posa el dataText
                            dada.Ruta = Venedor
                            If nif = "" Then
                                dada.IdVenedor = FindVenedor("", Venedor) 'busco el venedor per la ruta.
                            Else
                                dada.IdVenedor = FindVenedor("", "", nif) 'busco el venedor pel nif.
                            End If

                            If dada.IdVenedor > 0 Then ' si tinc id de venedor, busco les seues dades
                                dada = FindVenedor(dada)
                            End If

                            list.Add(dada)
                            pos = list.Count - 1
                        End If

                        'guardo dades. aixi sempre es crea l'ultim que es crea
                        trobat = True
                        list(pos).Data = dataStr(0)
                        list(pos).IdCif = FindCif(cif)
                        list(pos).Import = imp
                    End If
                End If
            End If
        Loop

        ' si no trobo el ticket, el creo com a no trobat
        If trobat = False Then
            ' creo el ticket a la taula
            dada = New TicketClass
            dada.Clear()
            dada.Data = data 'tambe posa el dataText
            dada.Ruta = Venedor
            dada.IdVenedor = FindVenedor("", Venedor) 'busco el venedor per la ruta.

            If dada.IdVenedor > 0 Then ' si tinc id de venedor, busco les seues dades
                dada = FindVenedor(dada)
            End If

            dada.IdCif = "1"
            dada.Import = "0"

            If existeix(dada.Data, "Tots", dada.Id, dada.VEN_Nif) = False Then
                Insert(dada) 'inserto un tiquet en blanc
            End If
        Else
            For pos = 0 To list.Count - 1
                ' miro que el tiquet no estigue ja
                If existeix(list(pos).Data, "Tots", list(pos).Id, list(pos).VEN_Nif) = False Then

                    ' creo el ticket a la taula
                    Insert(list(pos))

                    ' descarrego els tiquets del correu
                    getTiquetsFromEmail(list(pos))

                End If
            Next
        End If
    End Sub

    ''' <summary>Funcio que gestiona la carrega de tickets nous</summary>
    ''' <remarks>La seua funcio es buscar tots els fitxers d'historia d'una data en concret i si existixen enviarlos a analitzar
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data a gestionar</param>
    Public Sub carregarTicketsNousData(ByVal data As Date)
        Dim filePath As String
        Dim dada As New TicketClass

        ' per a cada venedor
        For Each Venedor As String In LlistatVenedors
            'la ruta 99 no la gestiono per aqui
            If Venedor <> "99" Then

                'miro si existeix la carpeta
                filePath = CrearRutaHistoric(data, Venedor)

                If Directory.Exists(filePath) Then
                    'miro si existeix el fitxer historia.txt
                    If My.Computer.FileSystem.FileExists(filePath + "/historia.txt") Then
                        gestioFitxerHistoria(filePath + "/historia.txt", Venedor, data)
                    End If

                    'miro si existeix el fitxer historia.txt
                    If My.Computer.FileSystem.FileExists(filePath + "/historia.apo") Then
                        gestioFitxerHistoria(filePath + "/historia.apo", Venedor + "-apo", data)
                    End If
                Else
                    'si el directori no existeix, creo un ticket buit

                    dada.Clear()
                    dada.Data = data 'tambe posa el dataText
                    dada.Ruta = Venedor
                    dada.IdVenedor = FindVenedor("", Venedor)
                    If dada.IdVenedor > 0 Then ' si tinc id de venedor, busco les seues dades
                        dada = FindVenedor(dada)
                    End If
                    dada.IdCif = "1"
                    dada.Import = "0"

                    If existeix(dada.Data, "Tots", dada.Id, dada.VEN_Nif) = False Then
                        Insert(dada)
                    End If
                End If

            End If
        Next
    End Sub

    ''' <summary>Funcio que gestiona les dates a carregar tiquets nous</summary>
    ''' <remarks>La seua funcio es recorrer totes les dates i cridar a la funcio carregarTicketsNousData
    ''' </remarks>
    ''' <param name="dateTicket"><c>Date</c>: Data a gestionar</param>
    Private Sub carregaTicketsNous(ByVal dateTicket As Date)
        Dim data As Date = dateTicket

        While data.DayOfYear < Now.DayOfYear
            carregarTicketsNousData(data)

            'gestiono el dia seguent hasta arrivar a avui, que em paro
            data = data.AddDays(1)
        End While

    End Sub



    ' funcions publiques

    ''' <summary>Funcio que fa el check inicial i carrega el llistat de venedors</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>Bolean</c>: Retorna True si tot s'ha inicialitzat correctament</returns>
    Function checkBegin() As Boolean
        ' inicio el log
        Log.Init()

        'miro si existeix la ruta per accedir als fitxers
        If Not Directory.Exists(path) Then
            Log.Add("No puc accedir a la ruta Z:\", "ERROR")
            MessageBox.Show("No puc accedir a la ruta Z:\", "Error", MessageBoxButtons.OK)
            Return False
        End If

        If Not Directory.Exists(facturesPath) Then
            Directory.CreateDirectory(facturesPath)
        End If

        If Not Directory.Exists(imatgesPath) Then
            Directory.CreateDirectory(imatgesPath)
        End If

        'conecto a la base de dades
        If Not DB.connect() Then
            Log.Add("No puc inicial DB MySQL", "ERROR")
            Return False
        End If

        loadVenedors()

        Return True
    End Function

    ''' <summary>Funcio que limpia i tanca l'objecte</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub Dispose()
        Log.Add("Aplicacio finalitzada")

        Log.Close()
    End Sub

    ''' <summary>Funcio que carrega els tickets pendents de controlar</summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub loadTickets()
        ' busco la ultima data que existeix a les carpetes. maxim ahir
        ' MessageBox.Show(LastDateFolder(), "Info", MessageBoxButtons.OK)
        Dim dateFolder As Date = LastDateFolder()

        ' busco la ultima data gestionada
        Dim dateTicket As Date = LastDate()
        'MessageBox.Show(dateTicket, "Info", MessageBoxButtons.OK)

        If dateFolder.DayOfYear > dateTicket.DayOfYear Then
            ' si no tinc totes les dades, les busco i les guardo al fitxer. avanço la data del ticket perque los d'este dia ja los tinc
            dateTicket = dateTicket.AddDays(1)
            Log.Add("Carrego tiquets automaticament de la data " & dateTicket.ToString() & " a la data " & dateFolder.ToString(), "DEBUG")
            carregaTicketsNous(dateTicket)
        End If

    End Sub



    ''' <summary>Funcio que retorna un array de nifs de venedors en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Optional Date = Nothing</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Optional Date = Nothing</c>: Data final</param>
    ''' <param name="Ruta"><c>Optional String = "Tots"</c>: Ruta del venedor</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatNifVenedors(Optional ByVal DataInici As Date = Nothing, Optional ByVal DataFinal As Date = Nothing, Optional ByVal Ruta As String = "Tots")
        Dim Sql As String
        Dim ds As DataSet

        If DataInici = Nothing Then

            Sql = "SELECT v.nom FROM venedors v ORDER BY v.nom"

        Else

            Sql = "SELECT DISTINCT v.nom FROM tickets t LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
                "t.data >= '" & DB.convertDate(DataInici) & "'" &
                " AND t.data <= '" & DB.convertDate(DataFinal) & "'"
            If Ruta <> "Tots" Then
                Sql = Sql & " AND t.ruta = '" & Ruta & "'"
            End If
            Sql = Sql & " ORDER BY v.nom"

        End If

        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna un array de cifs en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatCifs(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT DISTINCT c.cif FROM tickets t LEFT JOIN cifs c ON t.idcif = c.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND c.id > 1 "
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        Sql = Sql & " ORDER BY c.cif"
        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna un array de nums de factura en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatNumFactures(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT DISTINCT t.numFactura FROM tickets t LEFT JOIN cifs c ON t.idcif = c.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND c.id > 1 "
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        Sql = Sql & " ORDER BY t.numFactura"
        ds = DB.doSelect(Sql)

        Return ds

    End Function



    ''' <summary>Funcio que retorna un array de tickets en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatTickets(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String, Optional ByVal nif As String = "Tots")
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT t.id AS 'Id ticket'" &
            ", t.`data` AS 'Data'" &
            ", t.ruta AS 'Ruta'" &
            ", v.nom AS 'Venedor'" &
            ", c.cif AS 'Cif establiment'" &
            ", c.nom AS 'Nom establiment'" &
            ", t.import AS 'Import'" &
            ", t.numFactura AS 'Num factura'" &
            " FROM tickets t LEFT JOIN cifs c ON t.idcif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'"
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            If Venedor = "" Then
                Sql = Sql & " AND (t.idVenedor IS NULL OR t.idVenedor = 0)"
            Else
                Sql = Sql & " AND v.nom = '" & Venedor & "'"
            End If

        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If
        If nif <> "Tots" Then
            Sql = Sql & " AND v.nif = '" & nif & "'"
        End If
        Sql = Sql & " ORDER BY v.nom, t.`data`, t.ruta"
        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna un array de cifs en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatEstabliments(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String)
        Dim Sql As String
        Dim ds As DataSet

        'Sql = "SELECT CASE WHEN t.numFactura is null THEN concat('" & DB.convertDate(DataFinal) & "', '-', c.cif) ELSE t.numFactura END AS 'Num Factura'" &
        Sql = "SELECT t.numFactura AS 'Num Factura'" &
            ", c.cif AS 'Cif establiment'" &
            ", c.nom AS 'Nom establiment'" &
            ", COUNT(t.id) AS 'Num tickets'" &
            ", ROUND( ((SUM(t.import) * 100) / 110), 2) AS 'Base imponible'" &
            ", ROUND( ( SUM(t.import) - (SUM(t.import) * 100) / 110), 2) AS '10% Iva'" &
            ", ROUND(SUM(t.import), 2) AS 'Total'" &
            " FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND c.id > 1 "
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND v.nom = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If
        Sql = Sql & " GROUP BY c.cif"
        Sql = Sql & " ORDER BY c.cif"
        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna un array de nif de venedors en los filtros demanats aplicats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function llistatNifsVenedors(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT v.nom AS 'Venedor'" &
            ", v.nif AS 'Nif venedor'" &
            ", COUNT(t.id) AS 'Num tickets'" &
            ", ROUND(SUM(t.import), 2) AS 'Total'" &
            " FROM tickets t LEFT JOIN cifs c ON t.idcif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND  c.id > 1 "
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND v.nom = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If
        Sql = Sql & " GROUP BY v.nom"
        Sql = Sql & " ORDER BY v.nom"
        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna el total dels tickets filtrats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function TotalTickets(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT ROUND(SUM(t.import), 2) AS 'Total'" &
            " FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'"
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND v.nom = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If

        ds = DB.doSelect(Sql)

        Return ds

    End Function

    ''' <summary>Funcio que retorna el total dels tickets filtrats</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <returns><c>DataSet</c>: llistat en les dades</returns>
    Public Function TotalSenseTickets(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String)
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT COUNT(t.id) AS 'Total'" &
            " FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND  c.id = 1 "
        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND v.nom = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If

        ds = DB.doSelect(Sql)

        Return ds

    End Function


    ''' <summary>Funcio que construix la casella de l'excel que correspon a una fila i columna</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="row"><c>Integer</c>: Fila de l'excel</param>
    ''' <param name="col"><c>Integer</c>: Columna de l'excel</param>
    ''' <returns><c>String</c>: conversio en format "lletra""numero"</returns>
    Private Function Casella(ByVal row As Integer, ByVal col As Integer) As String
        Return Chr(col + Asc("A")) & (row + 1).ToString
    End Function

    ''' <summary>Funcio que guarda en excel un dataGrid</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Dades"><c>DataGridView</c>: dades a guardar</param>
    ''' <param name="fname"><c>String</c>: Nom del fitxer</param>
    Public Sub exportExcel(ByVal Dades As DataGridView, ByVal fname As String)
        Dim row As Integer
        Dim col As Integer
        'Dim totalTickets As Double = 0
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object

        'Dades.Rows.Item(0).Cells.Item(0).ToString()
        'Dades.Columns.Item(0).HeaderText
        'MessageBox.Show(Dades.Columns.Item(0).HeaderText, "Info", MessageBoxButtons.OK)

        If Dades.RowCount > 0 Then
            If fname <> "" Then
                'Start a new workbook in Excel
                oExcel = CreateObject("Excel.Application")
                oBook = oExcel.Workbooks.Add

                'Add data to cells of the first worksheet in the new workbook
                oSheet = oBook.Worksheets(1)

                'Capçalera
                For col = 0 To Dades.ColumnCount - 1
                    oSheet.Range(Casella(row, col)).Value = Dades.Columns.Item(col).HeaderText
                Next

                'dades
                For row = 0 To Dades.RowCount - 1
                    For col = 0 To Dades.ColumnCount - 1
                        oSheet.Range(Casella(row + 1, col)).Value = Dades.Item(col, row).Value.ToString
                    Next
                Next

                'oSheet.Range("A1:E1").Font.Bold = True

                'For Each dada As TicketClass In dades
                'totalTickets = totalTickets + Convert.ToDecimal(dada.Valor.Replace(".", ","))
                'row = row + 1
                'Next

                'Save the Workbook and Quit Excel
                oBook.SaveAs(fname)
                oExcel.Quit()
            End If
        Else
            MessageBox.Show("No hi ha res a guardar.", "Info", MessageBoxButtons.OK)
        End If
    End Sub


    ''' <summary>Funcio que crea els excel de les factures dels tickets a partir de les dades pasades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dades"><c>List(Of DataRow)</c>: Llistat de dades a gestionar</param>
    ''' <param name="imprimir"><c>Boolean</c>: Indica si s'han d'imprimir els excels o no</param>
    Public Sub crearFactura(ByVal dades As List(Of DataRow), ByVal imprimir As Boolean)
        Dim actPath As String = facturesPath & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString
        Dim fname As String = ""
        Dim row As Integer
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        Dim DRow As DataRow

        Dim totalIva As Double = 0
        Dim totalBase As Double = 0
        Dim totalImport As Double = 0
        Dim format As String = "#,##0.00_);[Red](#,##0.00)"

        'MessageBox.Show(dades.Count, "Info", MessageBoxButtons.OK)

        '1- miro que existeixque la carpeta de les factures i de la data d'avui
        If Not Directory.Exists(actPath) Then
            Directory.CreateDirectory(actPath)
        End If

        '2- inicio l'excel
        'Start a new workbook in Excel
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add

        'Add data to cells of the first worksheet in the new workbook
        oSheet = oBook.Worksheets(1)

        'Capçalera
        DRow = dades(0)
        fname = DRow(COL_NUMFACTURA)
        fname = fname.Replace("/", "-")
        fname = actPath & "/" & fname

        'MessageBox.Show(DRow(COL_NUMFACTURA), "Info", MessageBoxButtons.OK)
        oSheet.Range("A2").Value = DRow(COL_NUMFACTURA) 'num factura
        oSheet.Range("A3").Value = DRow(COL_NON_EMPRESA) 'nom empresa
        oSheet.Range("A4").Value = DRow(COL_CIF_EMPRESA) 'cif empresa

        oSheet.Range("F2").Value = Now.Day & "/" & Now.Month & "/" & Now.Year 'data factura
        oSheet.Range("F3").Value = "Frigorificos lluis SA" 'nom lluis
        oSheet.Range("F4").Value = "A43054378" 'nif lluis
        oSheet.Range("A2:F4").Font.Size = 12

        'capçalera linies
        oSheet.Range("A7").Value = "Concepte"
        oSheet.Range("D7").Value = "Import"
        oSheet.Range("E7").Value = "Iva"
        oSheet.Range("F7").Value = "Total Iva"
        oSheet.Range("G7").Value = "Total"
        oSheet.Range("B7:G7").Font.Bold = True
        oSheet.Range("B7:G7").Font.Size = 12

        'linies
        row = 8
        For Each V As DataRow In dades
            oSheet.Range("D" & row.ToString).NumberFormat = format
            oSheet.Range("F" & row.ToString).NumberFormat = format
            oSheet.Range("G" & row.ToString).NumberFormat = format

            oSheet.Range("A" & row.ToString).Value = "Tiquet " & V(COL_DATA_TIQUET) & " " & V(COL_RUTA) & " " & V(COL_NIF_VENEDOR)
            oSheet.Range("D" & row.ToString).Value = " " & V(COL_BASE_IMPONIBLE) & " €"
            oSheet.Range("E" & row.ToString).Value = "10 %"
            oSheet.Range("F" & row.ToString).Value = " " & V(COL_IVA) & " €"
            oSheet.Range("G" & row.ToString).Value = " " & V(COL_IMPORT).ToString & " €"

            totalIva = totalIva + V(COL_IVA) 'Convert.ToDecimal(V(COL_BASE_IMPONIBLE).Replace(".", ","))
            totalBase = totalBase + V(COL_BASE_IMPONIBLE)
            totalImport = totalImport + V(COL_IMPORT)

            row = row + 1
        Next

        'capçalera totals
        oSheet.Range("D38").Value = "Import"
        oSheet.Range("E38").Value = "% Iva"
        oSheet.Range("F38").Value = "Total Iva"
        oSheet.Range("G38").Value = "Total"
        oSheet.Range("D38:G38").Font.Bold = True
        oSheet.Range("D38:G38").Font.Size = 12

        'totals
        oSheet.Range("D39").NumberFormat = format
        oSheet.Range("F39:G39").NumberFormat = format

        oSheet.Range("D39").Value = " " & totalBase.ToString & " €"
        oSheet.Range("E39").Value = "10 %"
        oSheet.Range("F39").Value = " " & totalIva.ToString & " €"
        oSheet.Range("G39").Value = " " & totalImport.ToString & " €"
        oSheet.Range("G39").Font.Bold = True

        'guardo excel
        ' imrpimixo el excel si fa falta
        If imprimir Then
            oSheet.PrintOut(From:=1, To:=1, Copies:=1, Preview:=False)
        End If

        'Save the Workbook and Quit Excel
        oBook.SaveAs(fname)
        oExcel.Quit()

        'Private COL_DATA_TIQUET = 0 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_CIF_EMPRESA = 1 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_NON_EMPRESA = 2 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_IMPORT = 3 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_NUMFACTURA = 4 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_BASE_IMPONIBLE = 5 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_IVA = 6 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_RUTA = 7 'numero de columna del llistat que genera les factures dels tiquets
        'Private COL_NIF_VENEDOR = 8 'numero de columna del llistat que genera les factures dels tiquets
    End Sub

    ''' <summary>Funcio que realitza les factures de cada nif dins dels filtros aplicats i que no s'han facturat antes</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="DataInici"><c>Date</c>: Data inicial</param>
    ''' <param name="DataFinal"><c>Date</c>: Data final</param>
    ''' <param name="Ruta"><c>String</c>: Ruta del venedor</param>
    ''' <param name="Venedor"><c>String</c>: Nom del venedor</param>
    ''' <param name="Cif"><c>String</c>: Cif de l'establiment</param>
    ''' <param name="NumFactura"><c>String</c>: Num factura on esta el tiquet</param>
    ''' <param name="imprimir"><c>Boolean</c>: Indica si s'han d'imprimir o no</param>
    Public Sub ferFactures(ByVal DataInici As Date, ByVal DataFinal As Date, ByVal Ruta As String, ByVal Venedor As String, ByVal Cif As String, ByVal NumFactura As String, ByVal imprimir As Boolean)
        Dim Sql As String
        Dim id As Integer
        Dim cont As Integer
        Dim ds As DataSet
        Dim dades As New List(Of DataRow)
        Dim NumFacturaGestio As String = ""

        '1- busco els tickets dels filtros que no tinguen numFactura i la creo
        Sql = "UPDATE tickets SET numFactura = concat('" & DB.convertDate(DataFinal) & "', '-', (SELECT c.cif FROM cifs c WHERE c.id = idCif)) WHERE numFactura is null" &
            " AND data >= '" & DB.convertDate(DataInici) & "'" &
            " AND data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND idCif > 1 "

        If Ruta <> "Tots" Then
            Sql = Sql & " AND ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND nifVenedor = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            id = FindCif(Cif, "")
            Sql = Sql & " AND idCif = '" & id & "'"
        End If

        DB.doOperation(Sql)

        '2- busco els tickets que tenen lo num de factura que comença per la data i complixen els filtros
        Sql = "SELECT t.`data` AS 'Data'" &
            ", c.cif AS 'Cif establiment'" &
            ", c.nom AS 'Nom establiment'" &
            ", ROUND(t.import, 2) AS 'Import'" &
            ", t.numFactura AS 'Num factura'" &
            ", ROUND( ((t.import * 100) / 110), 2) AS 'Base imponible'" &
            ", ROUND( ( t.import - (t.import * 100) / 110), 2) AS '10% Iva'" &
            ", t.ruta AS 'Ruta'" &
            ", t.nifVenedor AS 'Nif venedor'" &
            " FROM tickets t LEFT JOIN cifs c ON t.idcif = c.id WHERE " &
            "t.data >= '" & DB.convertDate(DataInici) & "'" &
            " AND t.data <= '" & DB.convertDate(DataFinal) & "'" &
            " AND t.numFactura like '" & DB.convertDate(DataFinal) & "%'"

        If Ruta <> "Tots" Then
            Sql = Sql & " AND t.ruta = '" & Ruta & "'"
        End If
        If Venedor <> "Tots" Then
            Sql = Sql & " AND t.nifVenedor = '" & Venedor & "'"
        End If
        If Cif <> "Tots" Then
            Sql = Sql & " AND c.cif = '" & Cif & "'"
        End If
        If NumFactura <> "Tots" Then
            Sql = Sql & " AND t.numFactura = '" & NumFactura & "'"
        End If
        Sql = Sql & " ORDER BY t.numFactura, t.`data`"
        ds = DB.doSelect(Sql)

        'MessageBox.Show(ds.Tables(0).Rows.Count, "Info", MessageBoxButtons.OK)
        If ds.Tables(0).Rows.Count > 0 Then
            NumFacturaGestio = ""
            cont = 0
            dades.Clear()
            While cont < ds.Tables(0).Rows.Count
                If NumFacturaGestio <> ds.Tables(0).Rows(cont)(COL_NUMFACTURA) Then
                    If NumFacturaGestio <> "" Then
                        crearFactura(dades, imprimir)
                    End If
                    NumFacturaGestio = ds.Tables(0).Rows(cont)(COL_NUMFACTURA)
                    dades.Clear()
                End If
                dades.Add(ds.Tables(0).Rows(cont))
                cont = cont + 1
            End While
            If NumFacturaGestio <> "" Then
                crearFactura(dades, imprimir)
            End If

            'MessageBox.Show(ds.Tables(0).Rows(0)(0), "Info", MessageBoxButtons.OK)
        End If


    End Sub




    ''' <summary>Funcio que gestiona el venedor en el proces d'importacio de tiquets</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte a gestionar</param>
    ''' <returns><c>Boolean</c>: Si s'ha pogut gestionar o no</returns>
    Private Function importarTiquetsGestioVenedor(ByVal dada As TicketClass) As Boolean
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT t.id FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE"
        Sql = Sql & " t.data = '" & DB.convertDate(dada.Data) & "'"
        Sql = Sql & " AND c.cif = '" & dada.CIF_Cif & "'"
        Sql = Sql & " AND t.import = '" & dada.Import & "'"
        Sql = Sql & " AND t.idVenedor IS NULL"

        Log.Add("Busco si he de modificar venedor. sql: " & Sql, "IMPORT")
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            dada.Id = ds.Tables(0).Rows(0)(0)
            dada.IdVenedor = FindVenedor(dada.VEN_Nom, "")

            Log.Add("Modifico venedor del tiquet id[" & dada.Id & "] per " & dada.VEN_Nom, "IMPORT")

            If dada.IdVenedor = 0 Then
                Log.Add("Modifico venedor. No trobo el venedor " & dada.VEN_Nom & " a la base de dades!!!", "IMPORT ERROR")
                MessageBox.Show("No trobo el venedor " & dada.VEN_Nom & " a la base de dades!!!", "Error", MessageBoxButtons.OK)

            Else

                Sql = "UPDATE tickets SET "
                Sql = Sql & " IdVenedor = '" & dada.IdVenedor & "'"
                Sql = Sql & ", modificat = '1'"
                Sql = Sql & ", dataModificat = '" & DB.convertDateTime(Now) & "'"
                Sql = Sql & " WHERE id = " & dada.Id

                If DB.test Then
                    MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
                End If

                Log.Add("Modifico venedor. sql: " & Sql, "IMPORT")

                DB.doOperation(Sql)

                Return True
            End If
        End If

        Return False
    End Function

    ''' <summary>Funcio que gestiona el cif en el proces d'importacio de tiquets</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte a gestionar</param>
    ''' <returns><c>Boolean</c>: Si s'ha pogut gestionar o no</returns>
    Private Function importarTiquetGestioSenseTiquet(ByVal dada As TicketClass) As Boolean
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT t.id FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE"
        Sql = Sql & " t.data = '" & DB.convertDate(dada.Data) & "'"
        Sql = Sql & " AND v.nom = '" & dada.VEN_Nom & "'"
        Sql = Sql & " AND t.idCif = '1'"

        Log.Add("Busco si he de modificar cif i import. sql: " & Sql, "IMPORT")
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            dada.Id = ds.Tables(0).Rows(0)(0)
            dada.IdCif = FindCif(dada.CIF_Cif)

            Log.Add("Modifico cif i import del tiquet id[" & dada.Id & "] per " & dada.IdCif & " - " & dada.Import, "IMPORT")

            Sql = "UPDATE tickets SET "
            Sql = Sql & " IdCif = '" & dada.IdCif & "'"
            Sql = Sql & ", import = '" & dada.Import & "'"
            Sql = Sql & ", modificat = '1'"
            Sql = Sql & ", dataModificat = '" & DB.convertDateTime(Now) & "'"
            Sql = Sql & " WHERE id = " & dada.Id

            If DB.test Then
                MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
            End If

            Log.Add("Modifico cif i import. sql: " & Sql, "IMPORT")

            DB.doOperation(Sql)

            Return True
        End If

        Return False
    End Function

    ''' <summary>Funcio que gestiona la creacio d'un nou tiquet en el proces d'importacio de tiquets</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte a gestionar</param>
    ''' <returns><c>Boolean</c>: Si s'ha pogut gestionar o no</returns>
    Private Function importarTiquetGestioCreacio(ByVal dada As TicketClass) As Boolean
        Dim Sql As String
        Dim ds As DataSet

        Sql = "SELECT t.id FROM cifs c INNER JOIN tickets t ON t.idCif = c.id LEFT JOIN venedors v ON t.idVenedor = v.id WHERE"
        Sql = Sql & " t.data = '" & DB.convertDate(dada.Data) & "'"
        Sql = Sql & " AND v.nom = '" & dada.VEN_Nom & "'"
        Sql = Sql & " AND t.idCif > '1'"

        Log.Add("Busco si tinc el tiquet complet. sql: " & Sql, "IMPORT")
        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            ' error
            Log.Add("Busco si tinc el tiquet complet. El tiquet " & dada.ToStr() & " ja existeix!!!", "IMPORT ERROR")
            MessageBox.Show("El tiquet " & dada.ToStr() & " ja existeix!!!", "Error", MessageBoxButtons.OK)

        Else

            dada.IdCif = FindCif(dada.CIF_Cif)
            dada.IdVenedor = FindVenedor(dada.VEN_Nom, "")

            If dada.IdVenedor = 0 Then
                Log.Add("Creo el tiquet. No trobo el venedor " & dada.VEN_Nom & " a la base de dades!!!", "IMPORT ERROR")
                MessageBox.Show("No trobo el venedor " & dada.VEN_Nom & " a la base de dades!!!", "Error", MessageBoxButtons.OK)

            Else
                Log.Add("Creo el tiquet " & dada.ToStr(), "IMPORT")

                Sql = "INSERT INTO tickets ("
                Sql = Sql & "idCif"
                Sql = Sql & ", idVenedor"
                Sql = Sql & ", data"
                Sql = Sql & ", dataText"
                Sql = Sql & ", import"
                Sql = Sql & ", modificat"
                Sql = Sql & ", dataModificat"

                Sql = Sql & ") VALUES ("

                Sql = Sql & "'" & dada.IdCif & "'"
                Sql = Sql & ", '" & dada.IdVenedor & "'"
                Sql = Sql & ", '" & DB.convertDate(dada.Data) & "'"
                Sql = Sql & ", '" & dada.DataText & "'"
                Sql = Sql & ", '" & dada.Import & "'"
                Sql = Sql & ", '1'"
                Sql = Sql & ", '" & DB.convertDateTime(Now) & "'"

                Sql = Sql & ")"

                If DB.test Then
                    MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
                End If

                Log.Add("Creo el tiquet. sql: " & Sql, "IMPORT")

                DB.doOperation(Sql)

                Return True
            End If
        End If

        Return False
    End Function

    ''' <summary>Funcio que importa tiquets des de un llistat en excel</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="fname"><c>String</c>: ruta del fitxer a importar</param>
    Public Sub importarTiquetsExcel(ByVal fname As String)
        Dim row As Integer = 1
        Dim sheet As Integer = 1

        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object

        Dim dada As New TicketClass


        '1- obrir fitxer
        If fname <> "" Then
            'Start a new workbook in Excel
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.open(fname)

            'Add data to cells of the first worksheet in the new workbook
            While sheet <= oBook.Worksheets.count
                oSheet = oBook.Worksheets(sheet)
                row = 1

                '2- recorrer files
                While oSheet.Range(Casella(row, 1)).Value <> ""
                    dada.Clear()

                    dada.Data = oSheet.Range(Casella(row, 0)).Value
                    dada.VEN_Nom = oSheet.Range(Casella(row, 1)).Value
                    dada.CIF_Cif = oSheet.Range(Casella(row, 2)).Value
                    dada.Import = oSheet.Range(Casella(row, 3)).Value

                    Log.Add("Importo el tiquet: " & dada.ToStr(), "IMPORT")
                    If DB.test Then
                        dada.Show()
                    End If

                    '3- comprobar si existeix el tiquet
                    'a -> si data, cif i import existeix, es la dada d'un apoyo. modifico el venedor
                    If importarTiquetsGestioVenedor(dada) = False Then
                        'b -> si data i venedor i sense tiquet existeix, es un ticket inexistent que s'ha d'actualitzar. modifico cif i import
                        If importarTiquetGestioSenseTiquet(dada) = False Then
                            'c -> si data i venedor i no es sense tiquet -> ERROR -> SINO creo
                            importarTiquetGestioCreacio(dada)
                        End If
                    End If

                    row = row + 1
                End While

                sheet = sheet + 1
            End While

            'Quit Excel
            oExcel.Quit()
        End If

    End Sub


    ''' <summary>Funcio que marca el tiquet com a modificada</summary>
    ''' <remarks>Sempre que es modifica una dada, el tiquet es marca com a modificada manualment i es guarda la ultima vegada que s'ha modificat
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte en lo tiquet a modificar</param>
    ''' <returns><c>TicketClass</c>: Objecte en lo tiquet modificat</returns>
    Public Function MarcarModificat(ByVal dada As TicketClass) As TicketClass
        dada.Modificat = 1
        dada.DataModificat = Now

        Return dada
    End Function


    ''' <summary>Funcio que busca un tiquet per l'ID de la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="Id"><c>String</c>: ID del tiquet a buscar</param>
    ''' <returns><c>TicketClass</c>: tiquet trobat o un en blanc en cas contrari</returns>
    Public Function SearchById(ByVal Id As String) As TicketClass
        Dim dada As New TicketClass
        Dim Sql As String
        Dim ds As DataSet

        dada.Clear()

        Sql = "SELECT t.id"
        Sql = Sql & ", t.idCif"
        Sql = Sql & ", t.idVenedor"
        Sql = Sql & ", t.data"
        Sql = Sql & ", t.dataText"
        Sql = Sql & ", t.ruta" '5
        Sql = Sql & ", t.import"
        Sql = Sql & ", t.numFactura"

        Sql = Sql & ", c.cif"
        Sql = Sql & ", c.nom as 'establiment'"
        Sql = Sql & ", v.nom as 'venedor'" '10
        Sql = Sql & ", v.nif as 'nif'"

        Sql = Sql & ", t.modificat"
        Sql = Sql & ", t.dataModificat"

        Sql = Sql & " FROM tickets t"
        Sql = Sql & " LEFT JOIN cifs c ON t.idCif = c.id"
        Sql = Sql & " LEFT JOIN venedors v ON t.idVenedor = v.id"

        Sql = Sql & " WHERE"
        Sql = Sql & " t.id = '" & Id & "'"


        If DB.test Then
            MessageBox.Show("sql: " & Sql, "Error", MessageBoxButtons.OK)
        End If

        ds = DB.doSelect(Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            dada.Id = ds.Tables(0).Rows(0)(0)
            dada.IdCif = ds.Tables(0).Rows(0)(1)
            If IsDBNull(ds.Tables(0).Rows(0)(2)) = False Then
                dada.IdVenedor = ds.Tables(0).Rows(0)(2)
            End If
            dada.Data = ds.Tables(0).Rows(0)(3)
            dada.DataText = ds.Tables(0).Rows(0)(4)

            If IsDBNull(ds.Tables(0).Rows(0)(5)) = False Then
                dada.Ruta = ds.Tables(0).Rows(0)(5)
            End If

            dada.Import = ds.Tables(0).Rows(0)(6)
            If IsDBNull(ds.Tables(0).Rows(0)(7)) = False Then
                dada.NumFactura = ds.Tables(0).Rows(0)(7)
            End If
            If IsDBNull(ds.Tables(0).Rows(0)(8)) = False Then
                dada.CIF_Cif = ds.Tables(0).Rows(0)(8)
            End If
            If IsDBNull(ds.Tables(0).Rows(0)(9)) = False Then
                dada.CIF_Nom = ds.Tables(0).Rows(0)(9)
            End If
            If IsDBNull(ds.Tables(0).Rows(0)(10)) = False Then
                dada.VEN_Nom = ds.Tables(0).Rows(0)(10)
            End If
            If IsDBNull(ds.Tables(0).Rows(0)(11)) = False Then
                dada.VEN_Nif = ds.Tables(0).Rows(0)(11)
            End If

            dada.Modificat = ds.Tables(0).Rows(0)(12)
            If IsDBNull(ds.Tables(0).Rows(0)(13)) = False Then
                dada.DataModificat = ds.Tables(0).Rows(0)(13)
            End If

        End If

        Return dada

    End Function

    ''' <summary>Funcio que guarda les modificacions d'un tiquet a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte tiquet a guardar</param>
    Public Sub Update(ByVal dada As TicketClass)
        Dim sql As String

        sql = "UPDATE tickets SET "
        sql = sql & "idCif = '" & dada.IdCif & "'"
        sql = sql & ", IdVenedor = '" & dada.IdVenedor & "'"
        sql = sql & ", data = '" & DB.convertDate(dada.Data) & "'"
        sql = sql & ", dataText = '" & dada.DataText & "'"
        sql = sql & ", ruta = '" & dada.Ruta & "'"
        sql = sql & ", import = '" & dada.Import.ToString() & "'"
        sql = sql & ", numFactura = '" & dada.NumFactura & "'"
        sql = sql & ", modificat = '" & dada.Modificat & "'"
        sql = sql & ", dataModificat = '" & DB.convertDateTime(dada.DataModificat) & "'"

        sql = sql & " WHERE id = " & dada.Id

        If DB.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Modifico dades. sql: " & sql, "DEBUG")

        DB.doOperation(sql)
    End Sub

    ''' <summary>Funcio que crea un nou tiquet a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte a guardar</param>
    Public Sub Insert(ByVal dada As TicketClass)
        Dim sql As String

        sql = "INSERT INTO tickets ("
        sql = sql & "idCif"
        sql = sql & ", idVenedor"
        sql = sql & ", data"
        sql = sql & ", dataText"
        sql = sql & ", ruta" '5
        sql = sql & ", import"

        If dada.Modificat = 1 Then
            sql = sql & ", modificat"
            sql = sql & ", dataModificat"
        End If

        sql = sql & ") VALUES ("

        sql = sql & "'" & dada.IdCif & "'"
        sql = sql & ", '" & dada.IdVenedor & "'"
        sql = sql & ", '" & DB.convertDate(dada.Data) & "'"
        sql = sql & ", '" & dada.DataText & "'"
        sql = sql & ", '" & dada.Ruta & "'" '5
        sql = sql & ", '" & dada.Import & "'"

        If dada.Modificat = 1 Then
            sql = sql & ", '" & dada.Modificat & "'"
            sql = sql & ", '" & DB.convertDateTime(dada.DataModificat) & "'"
        End If

        sql = sql & ")"

        If DB.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Modifico dades. sql: " & sql, "DEBUG")

        DB.doOperation(sql)

    End Sub

    ''' <summary>Funcio que guarda les modificacions d'un establiment a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Objecte a guardar</param>
    Public Sub updateEstabliment(ByVal dada As TicketClass)
        Dim sql As String

        sql = "UPDATE cifs SET "
        sql = sql & "nom = '" & DB.cleanSql(dada.CIF_Nom) & "'"

        sql = sql & " WHERE id = " & dada.IdCif

        If DB.test Then
            MessageBox.Show("sql: " & sql, "Error", MessageBoxButtons.OK)
        End If

        Log.Add("Modifico dades. sql: " & sql, "DEBUG")

        DB.doOperation(sql)
    End Sub

    ''' <summary>Funcio que busca si un tiquet ja existeix a la base de dades</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data del tiquet</param>
    ''' <param name="treballador"><c>String</c>: Treballador</param>
    ''' <param name="id"><c>String</c>: id d'un tiquet que s'esta modificant</param>
    ''' <returns><c>Boolean</c>: retorna si ja existeix o no</returns>
    Public Function existeix(ByVal data As Date, ByVal treballador As String, ByVal id As String, Optional ByVal nif As String = "Tots") As Boolean
        Dim ds As DataSet

        ds = llistatTickets(data, data, "Tots", treballador, "Tots", "Tots", nif)

        If ds.Tables(0).Rows.Count > 0 Then
            If id = 0 Then
                Return True
            Else
                If id = ds.Tables(0).Rows(0)(0) Then ' si soc jo mateix, es que no existeix cap mes. control per a les modificacions
                    Return False
                Else
                    Return True
                End If
            End If

        End If

        Return False
    End Function

    ''' <summary>Funcio que busca les imatges dels tiquets al correu</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Dada a buscar</param>
    Public Sub getTiquetsFromEmail(ByVal dada As TicketClass)
        Dim Imap As New ImapClass
        Dim email As ImapEmailClass
        Dim pth As String
        Dim search As String
        Dim filename As String
        Dim num As Integer
        Dim att As Integer
        Dim cont As Integer = 1
        Dim img As New ImagesClass

        search = dada.getSearchSubject()
        pth = imatgesPath & dada.getImagePath()

        If Not Directory.Exists(pth) Then
            Directory.CreateDirectory(pth)
        End If

        ' busco si ja he descarregat estes imatges
        filename = pth & "tiquet-0" & dada.Ruta & "-1.jpg"
        If File.Exists(filename) = False Then

            Imap.connect()
            If Imap.Conected Then
                Imap.SelectFolder("INBOX")

                Imap.searchMessageBySubject(search)

                For num = 0 To Imap.Emails.Count - 1
                    email = Imap.Emails(num)
                    email = Imap.getMessage(email)

                    If email.Attachments.Count > 0 Then
                        For att = 0 To email.Attachments.Count - 1
                            cont = 1
                            filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"
                            While File.Exists(filename)
                                cont = cont + 1
                                filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"
                            End While

                            email.Attachments(att).SaveAs(filename)

                            'giro la imatge si fa falta
                            img.ImatgeVertical(filename)
                        Next
                    End If
                Next

                Imap.disconnect()
            End If
        End If
    End Sub


    'Public Sub getAllTiquetFromEmail()
    'Dim cont As Integer
    'Dim Tiquet As TicketClass

    '    For cont = 1 To 1117
    '        Tiquet = SearchById(cont)

    '        getTiquetsFromEmail(Tiquet)
    '    Next

    'End Sub

    ''' <summary>Funcio que busca el llistat d'imatges del tiquet</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="data"><c>Date</c>: Data del tiquet</param>
    ''' <param name="ruta"><c>String</c>: Ruta del tiquet</param>
    ''' <returns><c>String</c>: retorna la ruta de la imatge o cadena buida</returns>
    Public Function searchImages(ByVal data As Date, ByVal ruta As String) As String
        Dim dada As New TicketClass
        Dim pth As String
        Dim filename As String
        Dim cont As Integer = 1

        LlistatImatges.Clear()
        LlistatImatgesIndex = -1

        dada.Data = data
        dada.Ruta = ruta

        pth = imatgesPath & dada.getImagePath()

        If Directory.Exists(pth) Then
            cont = 1
            filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"

            If File.Exists(filename) = False Then
                ' intento tornar a baixar los tiquets
                getTiquetsFromEmail(dada)
            End If

            While File.Exists(filename)
                LlistatImatges.Add(filename)
                cont = cont + 1
                filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"
            End While
        End If

        If LlistatImatges.Count > 0 Then
            LlistatImatgesIndex = 0
            Return LlistatImatges(LlistatImatgesIndex)
        End If

        Return ""
    End Function

    ''' <summary>Funcio que busca la seguent imatge del llistat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: retorna la ruta de la imatge o cadena buida</returns>
    Public Function NextImage() As String
        If LlistatImatges.Count > 0 Then
            LlistatImatgesIndex = LlistatImatgesIndex + 1

            If LlistatImatgesIndex > LlistatImatges.Count - 1 Then
                LlistatImatgesIndex = 0
            End If

            Return LlistatImatges(LlistatImatgesIndex)
        End If

        Return ""
    End Function

    ''' <summary>Funcio que busca l'anterior imatge del llistat</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns><c>String</c>: retorna la ruta de la imatge o cadena buida</returns>
    Public Function PrevImage() As String
        If LlistatImatges.Count > 0 Then
            LlistatImatgesIndex = LlistatImatgesIndex - 1

            If LlistatImatgesIndex < 0 Then
                LlistatImatgesIndex = LlistatImatges.Count - 1
            End If

            Return LlistatImatges(LlistatImatgesIndex)
        End If

        Return ""
    End Function

    ''' <summary>Funcio que renombra la imatge d'un tiquet</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="oldName"><c>String</c>: Nom del tiquet</param>
    ''' <param name="newRuta"><c>String</c>: Ruta nova del tiquet</param>
    ''' <param name="Data"><c>Date</c>: Data nova del tiquet</param>
    Public Function RenameImatgeTiquet(ByVal oldName As String, ByVal newRuta As String, ByVal Data As Date) As String
        Dim filename As String
        Dim newfilename As String = ""
        Dim path As String = ""
        Dim lines As String()
        Dim num As Integer

        filename = oldName
        ' ruta
        lines = filename.Split("/")
        For num = 0 To lines.Count - 3
            path = path & lines(num) & "/"
        Next

        'data
        path = path & Data.Year.ToString() & "-"
        If Data.Month < 10 Then
            path = path & "0" & Data.Month.ToString() & "-"
        Else
            path = path & Data.Month.ToString() & "-"
        End If
        If Data.Day < 10 Then
            path = path & "0" & Data.Day.ToString() & "/"
        Else
            path = path & Data.Day.ToString() & "/"
        End If

        'tiquet-011-2
        path = path & "tiquet-0" & newRuta & "-"

        num = 1
        newfilename = path & num & ".jpg"
        While File.Exists(newfilename)
            num = num + 1
            newfilename = path & num & ".jpg"
        End While

        File.Move(oldName, newfilename)

        'reindexar la resta
        For num = LlistatImatgesIndex To LlistatImatges.Count - 2
            File.Move(LlistatImatges(num + 1), LlistatImatges(num))
        Next
        LlistatImatges.Remove(LlistatImatges(LlistatImatges.Count - 1))

        If LlistatImatges.Count > 0 Then
            If LlistatImatgesIndex > LlistatImatges.Count - 1 Then
                LlistatImatgesIndex = LlistatImatges.Count - 1
            End If
        Else
            Return ""
        End If

        Return LlistatImatges(LlistatImatgesIndex)
    End Function

    ''' <summary>Funcio que copia la imatge d'un tiquet del mobil al seu lloc</summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="dada"><c>TicketClass</c>: Dada a buscar</param>
    Public Sub buscarImatgeCamara(ByVal dada As TicketClass)
        Dim m As New PortableDevices.MTP
        Dim img As New ImagesClass

        Dim pth As String
        Dim filename As String
        Dim cont As Integer = 1

        pth = imatgesPath & dada.getImagePath()

        If Not Directory.Exists(pth) Then
            Directory.CreateDirectory(pth)
        End If

        cont = 1
        filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"
        While File.Exists(filename)
            cont = cont + 1
            filename = pth & "tiquet-0" & dada.Ruta & "-" & cont & ".jpg"
        End While

        m.copyImage(filename)
        img.redimensionarImatge(filename)
        img.ImatgeVertical(filename)

    End Sub

End Class
