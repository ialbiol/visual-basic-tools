﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Tickets
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.PBTiquet = New System.Windows.Forms.PictureBox()
        Me.BGirarImg = New System.Windows.Forms.Button()
        Me.DTCanviRuta = New System.Windows.Forms.DateTimePicker()
        Me.BCanviRuta = New System.Windows.Forms.Button()
        Me.CBCanviRuta = New System.Windows.Forms.ComboBox()
        Me.BTiquetSeg = New System.Windows.Forms.Button()
        Me.BTiquetAnt = New System.Windows.Forms.Button()
        Me.LTiquetNom = New System.Windows.Forms.Label()
        Me.DGDades = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DGCifs = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DGVenedors = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLNumTiquets = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLTotalTiquets = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLNumCifs = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel7 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLNumSenseTiquet = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel8 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel9 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TLNumPresentats = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BRecarregar = New System.Windows.Forms.Button()
        Me.BCarregar99 = New System.Windows.Forms.Button()
        Me.BTest = New System.Windows.Forms.Button()
        Me.BNouTiquet = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CBNumFactura = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CBCifs = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CBVenedors = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBRutes = New System.Windows.Forms.ComboBox()
        Me.DTDataFinal = New System.Windows.Forms.DateTimePicker()
        Me.DTDataInici = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMNouTiquet = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMImportarExcel = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMSortir = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMExcelTiquets = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMExcelEstabliments = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMExcelTreballadors = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMExcelFactures = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMExcelFacturesImprimir = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileTest = New System.Windows.Forms.OpenFileDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.PBTiquet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGDades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGCifs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.DGVenedors, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.DefaultExt = "*.xlsx"
        Me.SaveFileDialog1.Filter = "Excel file|*.xlsx"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(242, 27)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1120, 881)
        Me.TabControl1.TabIndex = 27
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.PBTiquet)
        Me.TabPage1.Controls.Add(Me.BGirarImg)
        Me.TabPage1.Controls.Add(Me.DTCanviRuta)
        Me.TabPage1.Controls.Add(Me.BCanviRuta)
        Me.TabPage1.Controls.Add(Me.CBCanviRuta)
        Me.TabPage1.Controls.Add(Me.BTiquetSeg)
        Me.TabPage1.Controls.Add(Me.BTiquetAnt)
        Me.TabPage1.Controls.Add(Me.LTiquetNom)
        Me.TabPage1.Controls.Add(Me.DGDades)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1112, 850)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Tickets"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'PBTiquet
        '
        Me.PBTiquet.Location = New System.Drawing.Point(6, 6)
        Me.PBTiquet.Name = "PBTiquet"
        Me.PBTiquet.Size = New System.Drawing.Size(344, 646)
        Me.PBTiquet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PBTiquet.TabIndex = 36
        Me.PBTiquet.TabStop = False
        '
        'BGirarImg
        '
        Me.BGirarImg.Location = New System.Drawing.Point(289, 821)
        Me.BGirarImg.Name = "BGirarImg"
        Me.BGirarImg.Size = New System.Drawing.Size(92, 26)
        Me.BGirarImg.TabIndex = 35
        Me.BGirarImg.Text = "Girar"
        Me.BGirarImg.UseVisualStyleBackColor = True
        '
        'DTCanviRuta
        '
        Me.DTCanviRuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTCanviRuta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTCanviRuta.Location = New System.Drawing.Point(41, 821)
        Me.DTCanviRuta.Margin = New System.Windows.Forms.Padding(2)
        Me.DTCanviRuta.Name = "DTCanviRuta"
        Me.DTCanviRuta.Size = New System.Drawing.Size(228, 24)
        Me.DTCanviRuta.TabIndex = 34
        '
        'BCanviRuta
        '
        Me.BCanviRuta.Location = New System.Drawing.Point(289, 786)
        Me.BCanviRuta.Name = "BCanviRuta"
        Me.BCanviRuta.Size = New System.Drawing.Size(92, 26)
        Me.BCanviRuta.TabIndex = 14
        Me.BCanviRuta.Text = "Canviar"
        Me.BCanviRuta.UseVisualStyleBackColor = True
        '
        'CBCanviRuta
        '
        Me.CBCanviRuta.FormattingEnabled = True
        Me.CBCanviRuta.Location = New System.Drawing.Point(41, 786)
        Me.CBCanviRuta.Name = "CBCanviRuta"
        Me.CBCanviRuta.Size = New System.Drawing.Size(228, 26)
        Me.CBCanviRuta.TabIndex = 13
        '
        'BTiquetSeg
        '
        Me.BTiquetSeg.Location = New System.Drawing.Point(186, 674)
        Me.BTiquetSeg.Name = "BTiquetSeg"
        Me.BTiquetSeg.Size = New System.Drawing.Size(83, 54)
        Me.BTiquetSeg.TabIndex = 12
        Me.BTiquetSeg.Text = "Tiquet seguent"
        Me.BTiquetSeg.UseVisualStyleBackColor = True
        '
        'BTiquetAnt
        '
        Me.BTiquetAnt.Location = New System.Drawing.Point(41, 674)
        Me.BTiquetAnt.Name = "BTiquetAnt"
        Me.BTiquetAnt.Size = New System.Drawing.Size(83, 54)
        Me.BTiquetAnt.TabIndex = 11
        Me.BTiquetAnt.Text = "Tiquet anterior"
        Me.BTiquetAnt.UseVisualStyleBackColor = True
        '
        'LTiquetNom
        '
        Me.LTiquetNom.AutoSize = True
        Me.LTiquetNom.Location = New System.Drawing.Point(38, 746)
        Me.LTiquetNom.Name = "LTiquetNom"
        Me.LTiquetNom.Size = New System.Drawing.Size(13, 18)
        Me.LTiquetNom.TabIndex = 10
        Me.LTiquetNom.Text = "-"
        '
        'DGDades
        '
        Me.DGDades.AllowUserToAddRows = False
        Me.DGDades.AllowUserToDeleteRows = False
        Me.DGDades.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGDades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGDades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGDades.Location = New System.Drawing.Point(355, 5)
        Me.DGDades.Margin = New System.Windows.Forms.Padding(2)
        Me.DGDades.Name = "DGDades"
        Me.DGDades.ReadOnly = True
        Me.DGDades.RowTemplate.Height = 28
        Me.DGDades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGDades.Size = New System.Drawing.Size(752, 647)
        Me.DGDades.TabIndex = 8
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DGCifs)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1112, 850)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Establiments"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DGCifs
        '
        Me.DGCifs.AllowUserToAddRows = False
        Me.DGCifs.AllowUserToDeleteRows = False
        Me.DGCifs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGCifs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGCifs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGCifs.Location = New System.Drawing.Point(5, 5)
        Me.DGCifs.Margin = New System.Windows.Forms.Padding(2)
        Me.DGCifs.Name = "DGCifs"
        Me.DGCifs.ReadOnly = True
        Me.DGCifs.RowTemplate.Height = 28
        Me.DGCifs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGCifs.Size = New System.Drawing.Size(1098, 838)
        Me.DGCifs.TabIndex = 13
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DGVenedors)
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1112, 850)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Venedors"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'DGVenedors
        '
        Me.DGVenedors.AllowUserToAddRows = False
        Me.DGVenedors.AllowUserToDeleteRows = False
        Me.DGVenedors.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVenedors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGVenedors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVenedors.Location = New System.Drawing.Point(5, 5)
        Me.DGVenedors.Margin = New System.Windows.Forms.Padding(2)
        Me.DGVenedors.Name = "DGVenedors"
        Me.DGVenedors.ReadOnly = True
        Me.DGVenedors.RowTemplate.Height = 28
        Me.DGVenedors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVenedors.Size = New System.Drawing.Size(1098, 838)
        Me.DGVenedors.TabIndex = 14
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.TLNumTiquets, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3, Me.TLTotalTiquets, Me.ToolStripStatusLabel4, Me.ToolStripStatusLabel5, Me.TLNumCifs, Me.ToolStripStatusLabel6, Me.ToolStripStatusLabel7, Me.TLNumSenseTiquet, Me.ToolStripStatusLabel8, Me.ToolStripStatusLabel9, Me.TLNumPresentats})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 926)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1365, 22)
        Me.StatusStrip1.TabIndex = 34
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(109, 17)
        Me.ToolStripStatusLabel1.Text = "Numero de tiquets:"
        '
        'TLNumTiquets
        '
        Me.TLNumTiquets.Name = "TLNumTiquets"
        Me.TLNumTiquets.Size = New System.Drawing.Size(13, 17)
        Me.TLNumTiquets.Text = "0"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel2.Text = "|"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(75, 17)
        Me.ToolStripStatusLabel3.Text = "Total tiquets:"
        '
        'TLTotalTiquets
        '
        Me.TLTotalTiquets.Name = "TLTotalTiquets"
        Me.TLTotalTiquets.Size = New System.Drawing.Size(13, 17)
        Me.TLTotalTiquets.Text = "0"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel4.Text = "|"
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(91, 17)
        Me.ToolStripStatusLabel5.Text = "Numero de cifs:"
        '
        'TLNumCifs
        '
        Me.TLNumCifs.Name = "TLNumCifs"
        Me.TLNumCifs.Size = New System.Drawing.Size(13, 17)
        Me.TLNumCifs.Text = "0"
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel6.Text = "|"
        '
        'ToolStripStatusLabel7
        '
        Me.ToolStripStatusLabel7.Name = "ToolStripStatusLabel7"
        Me.ToolStripStatusLabel7.Size = New System.Drawing.Size(136, 17)
        Me.ToolStripStatusLabel7.Text = "Numero de sense tiquet:"
        '
        'TLNumSenseTiquet
        '
        Me.TLNumSenseTiquet.Name = "TLNumSenseTiquet"
        Me.TLNumSenseTiquet.Size = New System.Drawing.Size(13, 17)
        Me.TLNumSenseTiquet.Text = "0"
        '
        'ToolStripStatusLabel8
        '
        Me.ToolStripStatusLabel8.Name = "ToolStripStatusLabel8"
        Me.ToolStripStatusLabel8.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel8.Text = "|"
        '
        'ToolStripStatusLabel9
        '
        Me.ToolStripStatusLabel9.Name = "ToolStripStatusLabel9"
        Me.ToolStripStatusLabel9.Size = New System.Drawing.Size(150, 17)
        Me.ToolStripStatusLabel9.Text = "Numero tiquets presentats:"
        '
        'TLNumPresentats
        '
        Me.TLNumPresentats.Name = "TLNumPresentats"
        Me.TLNumPresentats.Size = New System.Drawing.Size(13, 17)
        Me.TLNumPresentats.Text = "0"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.BRecarregar)
        Me.Panel1.Controls.Add(Me.BCarregar99)
        Me.Panel1.Controls.Add(Me.BTest)
        Me.Panel1.Controls.Add(Me.BNouTiquet)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.CBNumFactura)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.CBCifs)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.CBVenedors)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CBRutes)
        Me.Panel1.Controls.Add(Me.DTDataFinal)
        Me.Panel1.Controls.Add(Me.DTDataInici)
        Me.Panel1.Location = New System.Drawing.Point(4, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(232, 869)
        Me.Panel1.TabIndex = 35
        '
        'BRecarregar
        '
        Me.BRecarregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BRecarregar.Location = New System.Drawing.Point(61, 555)
        Me.BRecarregar.Name = "BRecarregar"
        Me.BRecarregar.Size = New System.Drawing.Size(107, 55)
        Me.BRecarregar.TabIndex = 47
        Me.BRecarregar.Text = "Recarregar dia"
        Me.BRecarregar.UseVisualStyleBackColor = True
        Me.BRecarregar.Visible = False
        '
        'BCarregar99
        '
        Me.BCarregar99.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BCarregar99.Location = New System.Drawing.Point(61, 649)
        Me.BCarregar99.Name = "BCarregar99"
        Me.BCarregar99.Size = New System.Drawing.Size(107, 57)
        Me.BCarregar99.TabIndex = 46
        Me.BCarregar99.Text = "Gestionar ruta 99"
        Me.BCarregar99.UseVisualStyleBackColor = True
        Me.BCarregar99.Visible = False
        '
        'BTest
        '
        Me.BTest.Location = New System.Drawing.Point(61, 730)
        Me.BTest.Name = "BTest"
        Me.BTest.Size = New System.Drawing.Size(75, 23)
        Me.BTest.TabIndex = 45
        Me.BTest.Text = "Test"
        Me.BTest.UseVisualStyleBackColor = True
        Me.BTest.Visible = False
        '
        'BNouTiquet
        '
        Me.BNouTiquet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BNouTiquet.Location = New System.Drawing.Point(61, 441)
        Me.BNouTiquet.Name = "BNouTiquet"
        Me.BNouTiquet.Size = New System.Drawing.Size(107, 55)
        Me.BNouTiquet.TabIndex = 44
        Me.BNouTiquet.Text = "Nou tiquet"
        Me.BNouTiquet.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(14, 335)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(94, 18)
        Me.Label14.TabIndex = 43
        Me.Label14.Text = "Num Factura"
        '
        'CBNumFactura
        '
        Me.CBNumFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBNumFactura.FormattingEnabled = True
        Me.CBNumFactura.Location = New System.Drawing.Point(11, 355)
        Me.CBNumFactura.Margin = New System.Windows.Forms.Padding(2)
        Me.CBNumFactura.Name = "CBNumFactura"
        Me.CBNumFactura.Size = New System.Drawing.Size(205, 26)
        Me.CBNumFactura.TabIndex = 42
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 265)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 18)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "Cif"
        '
        'CBCifs
        '
        Me.CBCifs.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBCifs.FormattingEnabled = True
        Me.CBCifs.Location = New System.Drawing.Point(11, 285)
        Me.CBCifs.Margin = New System.Windows.Forms.Padding(2)
        Me.CBCifs.Name = "CBCifs"
        Me.CBCifs.Size = New System.Drawing.Size(205, 26)
        Me.CBCifs.TabIndex = 40
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 198)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 18)
        Me.Label6.TabIndex = 39
        Me.Label6.Text = "Venedor"
        '
        'CBVenedors
        '
        Me.CBVenedors.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBVenedors.FormattingEnabled = True
        Me.CBVenedors.Location = New System.Drawing.Point(11, 218)
        Me.CBVenedors.Margin = New System.Windows.Forms.Padding(2)
        Me.CBVenedors.Name = "CBVenedors"
        Me.CBVenedors.Size = New System.Drawing.Size(205, 26)
        Me.CBVenedors.TabIndex = 38
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 137)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 18)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Ruta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 75)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 18)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Data final"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 15)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 18)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Data inici"
        '
        'CBRutes
        '
        Me.CBRutes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBRutes.FormattingEnabled = True
        Me.CBRutes.Location = New System.Drawing.Point(11, 157)
        Me.CBRutes.Margin = New System.Windows.Forms.Padding(2)
        Me.CBRutes.Name = "CBRutes"
        Me.CBRutes.Size = New System.Drawing.Size(205, 26)
        Me.CBRutes.TabIndex = 34
        '
        'DTDataFinal
        '
        Me.DTDataFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTDataFinal.Location = New System.Drawing.Point(11, 95)
        Me.DTDataFinal.Margin = New System.Windows.Forms.Padding(2)
        Me.DTDataFinal.Name = "DTDataFinal"
        Me.DTDataFinal.Size = New System.Drawing.Size(205, 24)
        Me.DTDataFinal.TabIndex = 33
        '
        'DTDataInici
        '
        Me.DTDataInici.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTDataInici.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTDataInici.Location = New System.Drawing.Point(11, 35)
        Me.DTDataInici.Margin = New System.Windows.Forms.Padding(2)
        Me.DTDataInici.Name = "DTDataInici"
        Me.DTDataInici.Size = New System.Drawing.Size(205, 24)
        Me.DTDataInici.TabIndex = 32
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem4})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1365, 24)
        Me.MenuStrip1.TabIndex = 36
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TMNouTiquet, Me.ToolStripMenuItem2, Me.TMImportarExcel, Me.ToolStripMenuItem3, Me.TMSortir})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(47, 20)
        Me.ToolStripMenuItem1.Text = "Fitxer"
        '
        'TMNouTiquet
        '
        Me.TMNouTiquet.Name = "TMNouTiquet"
        Me.TMNouTiquet.Size = New System.Drawing.Size(159, 22)
        Me.TMNouTiquet.Text = "Nou Tiquet"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(159, 22)
        Me.ToolStripMenuItem2.Text = "-"
        '
        'TMImportarExcel
        '
        Me.TMImportarExcel.Name = "TMImportarExcel"
        Me.TMImportarExcel.Size = New System.Drawing.Size(159, 22)
        Me.TMImportarExcel.Text = "Importar tiquets"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(159, 22)
        Me.ToolStripMenuItem3.Text = "-"
        '
        'TMSortir
        '
        Me.TMSortir.Name = "TMSortir"
        Me.TMSortir.Size = New System.Drawing.Size(159, 22)
        Me.TMSortir.Text = "Sortir"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TMExcelTiquets, Me.TMExcelEstabliments, Me.TMExcelTreballadors, Me.ToolStripMenuItem5, Me.TMExcelFactures, Me.TMExcelFacturesImprimir})
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(62, 20)
        Me.ToolStripMenuItem4.Text = "Exportar"
        '
        'TMExcelTiquets
        '
        Me.TMExcelTiquets.Name = "TMExcelTiquets"
        Me.TMExcelTiquets.Size = New System.Drawing.Size(224, 22)
        Me.TMExcelTiquets.Text = "Llistat tiquets"
        '
        'TMExcelEstabliments
        '
        Me.TMExcelEstabliments.Name = "TMExcelEstabliments"
        Me.TMExcelEstabliments.Size = New System.Drawing.Size(224, 22)
        Me.TMExcelEstabliments.Text = "Llistat establiments"
        '
        'TMExcelTreballadors
        '
        Me.TMExcelTreballadors.Name = "TMExcelTreballadors"
        Me.TMExcelTreballadors.Size = New System.Drawing.Size(224, 22)
        Me.TMExcelTreballadors.Text = "Llistat tiquets per treballador"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(224, 22)
        Me.ToolStripMenuItem5.Text = "-"
        '
        'TMExcelFactures
        '
        Me.TMExcelFactures.Name = "TMExcelFactures"
        Me.TMExcelFactures.Size = New System.Drawing.Size(224, 22)
        Me.TMExcelFactures.Text = "Crear factures"
        '
        'TMExcelFacturesImprimir
        '
        Me.TMExcelFacturesImprimir.Name = "TMExcelFacturesImprimir"
        Me.TMExcelFacturesImprimir.Size = New System.Drawing.Size(224, 22)
        Me.TMExcelFacturesImprimir.Text = "Crear i imprimir factures"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "*.xlsx"
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Excel file|*.xlsx"
        '
        'Tickets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1365, 948)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.TabControl1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Tickets"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tickets"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PBTiquet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGDades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DGCifs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DGVenedors, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DGDades As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DGCifs As System.Windows.Forms.DataGridView
    Friend WithEvents DGVenedors As System.Windows.Forms.DataGridView
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLNumTiquets As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLTotalTiquets As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLNumCifs As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel7 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLNumSenseTiquet As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel8 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel9 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TLNumPresentats As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents CBNumFactura As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CBCifs As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CBVenedors As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CBRutes As System.Windows.Forms.ComboBox
    Friend WithEvents DTDataFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTDataInici As System.Windows.Forms.DateTimePicker
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMNouTiquet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMImportarExcel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMSortir As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMExcelTiquets As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMExcelEstabliments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMExcelTreballadors As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMExcelFactures As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TMExcelFacturesImprimir As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BTiquetSeg As System.Windows.Forms.Button
    Friend WithEvents BTiquetAnt As System.Windows.Forms.Button
    Friend WithEvents LTiquetNom As System.Windows.Forms.Label
    Friend WithEvents BCanviRuta As System.Windows.Forms.Button
    Friend WithEvents CBCanviRuta As System.Windows.Forms.ComboBox
    Friend WithEvents BNouTiquet As System.Windows.Forms.Button
    Friend WithEvents BTest As System.Windows.Forms.Button
    Friend WithEvents OpenFileTest As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BCarregar99 As System.Windows.Forms.Button
    Friend WithEvents DTCanviRuta As System.Windows.Forms.DateTimePicker
    Friend WithEvents BGirarImg As System.Windows.Forms.Button
    Friend WithEvents PBTiquet As System.Windows.Forms.PictureBox
    Friend WithEvents BRecarregar As System.Windows.Forms.Button

End Class
