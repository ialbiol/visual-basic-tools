﻿Imports System.IO

''' <summary>Formulari principal del programa</summary>
''' <remarks>Variables globals:
''' <list type="table">
'''   <listheader>
'''      <term>Variable</term><description>Descripcio</description>
'''   </listheader>
'''   <item>
'''      <term>controller <c>TicketListClass</c></term><description>Objecte en tota la logica de funcionament del programa</description>
'''   </item>
'''   <item>
'''      <term>allok <c>Boolean = False</c></term><description>Variable que controla si tot esta ok per a poder carregar dades o esta pendent d'algun camp</description>
'''   </item>
'''   <item>
'''      <term>idTicket <c>String</c></term><description>Guarda el ID del tiquet que s'esta modificant</description>
'''   </item>
'''   <item>
'''      <term>editRow <c>Integer</c></term><description>Guarda la fila del dataGrid que s'esta modificant</description>
'''   </item>
'''</list>
''' </remarks>

Public Class Tickets

    Dim Controller As New TicketListClass
    Dim allok As Boolean = False
    Dim idTicket As String
    Dim editRow As Integer

#Region "events formulari"

    ''' <summary>Event formulari Close</summary>
    ''' <remarks>Gestiona les ultimes accions que ha de fer el controller
    ''' </remarks>
    Private Sub Tickets_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Controller.Dispose()
    End Sub

    ''' <summary>Event formulari Load</summary>
    ''' <remarks>Gestiona les primeres accions que ha de fer el controller
    ''' </remarks>
    Private Sub Tickets_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'miro si existeix la ruta per accedir als fitxers
        If Controller.checkBegin() = False Then
            Me.Close()
        End If

        'omplo el desplegable de les rutes
        CBRutes.Items.Add("Tots")
        For Each V As String In Controller.LlistatVenedors
            CBRutes.Items.Add(V)
        Next
        CBRutes.SelectedIndex = 0

        'obro el fitxer
        Controller.loadTickets()

        ' mostro interface
        DTDataInici.Value = Now.AddDays(-1)
        DTDataInici.MaxDate = Now.AddDays(-1)

        DTDataFinal.Value = Now.AddDays(-1)
        DTDataFinal.MaxDate = Now.AddDays(-1)

        DTCanviRuta.MaxDate = Now.AddDays(-1)
        DTCanviRuta.Value = Now.AddDays(-1)

        'carrego les rutes de les imatges dels tiquets
        CBCanviRuta.Items.Add("")
        For Each V As String In Controller.LlistatVenedors
            CBCanviRuta.Items.Add(V)
            CBCanviRuta.Items.Add(V & "-apo")
            CBCanviRuta.Items.Add(V & "-rep")
        Next

        allok = True
        mostrarDades()
    End Sub

#End Region

#Region "filtros de busqueda"

    ''' <summary>Event Data inici del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data d'inici
    ''' </remarks>
    Private Sub DTDataInici_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTDataInici.ValueChanged
        omplirCB()
        mostrarDades()
    End Sub

    ''' <summary>Event Data final del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova data final
    ''' </remarks>
    Private Sub DTDataFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTDataFinal.ValueChanged
        omplirCB()
        mostrarDades()
    End Sub

    ''' <summary>Event combo box del llistat de rutes del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova dada
    ''' </remarks>
    Private Sub CBRutes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBRutes.SelectedIndexChanged
        omplirCB()
        mostrarDades()
    End Sub

    ''' <summary>Event combo box del llistat de treballadors del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova dada
    ''' </remarks>
    Private Sub CBVenedors_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBVenedors.SelectedIndexChanged
        mostrarDades()
    End Sub

    ''' <summary>Event combo box del llistat de cifs del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova dada
    ''' </remarks>
    Private Sub CBCifs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBCifs.SelectedIndexChanged
        mostrarDades()
    End Sub

    ''' <summary>Event combo box del llistat de numeros de factures del filtro de busqueda ha canviat</summary>
    ''' <remarks>Torna a carregar les dades utilitzant la nova dada
    ''' </remarks>
    Private Sub CBNumFactura_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBNumFactura.SelectedIndexChanged
        mostrarDades()
    End Sub

#End Region

#Region "modificar dades"

    ''' <summary>Event doble click del llistat de dades</summary>
    ''' <remarks>Inicia el proces de modificacio del tiquet seleccionat
    ''' </remarks>
    Private Sub DGDades_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGDades.CellDoubleClick
        ' editar tiquet
        Dim eform As FEditarTicket

        editRow = e.RowIndex

        ' mostrar dades per a modificar
        If e.RowIndex >= 0 Then
            idTicket = DGDades.Item(0, e.RowIndex).Value.ToString
            Controller.Log.Add("Obro fitxa en id " & idTicket, "EDIT")

            eform = New FEditarTicket
            eform.SetIdTiquet(idTicket, Controller)
            eform.ShowDialog()

            idTicket = ""
            'omplirCB()
            mostrarDades()
        End If

    End Sub

#End Region

#Region "event mostrar tiquets"

    ''' <summary>Event click del llistat de dades</summary>
    ''' <remarks>Mostra la primera imatge del tiquet, si en te
    ''' </remarks>
    Private Sub DGDades_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGDades.CellContentClick
        Dim ruta As String

        If e.RowIndex >= 0 AndAlso IsDBNull(DGDades.Item(1, e.RowIndex).Value) = False AndAlso IsDBNull(DGDades.Item(2, e.RowIndex).Value) = False Then
            ruta = Controller.searchImages(DGDades.Item(1, e.RowIndex).Value, DGDades.Item(2, e.RowIndex).Value)

            LTiquetNom.Text = ruta

            If ruta <> "" Then
                PBTiquet.ImageLocation = ruta
            Else
                PBTiquet.ImageLocation = Controller.imatgesPathLimpia
            End If
        End If
    End Sub

    ''' <summary>Event click del boto seguent imatge</summary>
    ''' <remarks>Mostra la seguent imatge del tiquet, si en te
    ''' </remarks>
    Private Sub BTiquetAnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTiquetAnt.Click
        Dim ruta As String

        ruta = Controller.PrevImage()

        LTiquetNom.Text = ruta

        If ruta <> "" Then
            PBTiquet.ImageLocation = ruta
        Else
            PBTiquet.ImageLocation = Controller.imatgesPathLimpia
        End If
    End Sub

    ''' <summary>Event click del boto anterior imatge</summary>
    ''' <remarks>Mostra la anterior imatge del tiquet, si en te
    ''' </remarks>
    Private Sub BTiquetSeg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTiquetSeg.Click
        Dim ruta As String

        ruta = Controller.NextImage()

        LTiquetNom.Text = ruta

        If ruta <> "" Then
            PBTiquet.ImageLocation = ruta
        Else
            PBTiquet.ImageLocation = Controller.imatgesPathLimpia
        End If
    End Sub

#End Region

#Region "event del menu principal"

    ''' <summary>Event Nou tiquet del menu principal</summary>
    ''' <remarks>Inicia el proces de crear un nou tiquet
    ''' </remarks>
    Private Sub TMNouTiquet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMNouTiquet.Click
        Dim eform As FEditarTicket
        Dim Tiquet As New TicketClass
        ' nou tiquet

        Controller.Log.Add("Creo tiquet nou!!", "EDIT")
        editRow = 0

        Tiquet.Clear()
        Tiquet.Data = DTDataInici.Value

        eform = New FEditarTicket
        eform.SetTiquet(Tiquet, Controller)
        eform.ShowDialog()

        idTicket = ""
        'omplirCB()
        mostrarDades()

    End Sub

    ''' <summary>Event Boto Nou tiquet </summary>
    ''' <remarks>Inicia el proces de crear un nou tiquet
    ''' </remarks>
    Private Sub BNouTiquet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNouTiquet.Click
        TMNouTiquet_Click(sender, e)
    End Sub

    ''' <summary>Event importar tiquets d'un excel del menu principal</summary>
    ''' <remarks>Inicia el proces d'importar tiquets
    ''' </remarks>
    Private Sub TMImportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMImportarExcel.Click
        'importar tiquets excel
        Dim fname As String

        OpenFileDialog1.ShowDialog()
        fname = OpenFileDialog1.FileName

        If fname <> "" Then
            Controller.Log.Add("Importo tiquets del fitxer " & fname, "IMPORT")
            Controller.importarTiquetsExcel(fname)

            mostrarDades()
        End If
    End Sub

    ''' <summary>Event Sortir del menu principal</summary>
    ''' <remarks>Tanca el programa
    ''' </remarks>
    Private Sub TMSortir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMSortir.Click
        Me.Close()
    End Sub

    ''' <summary>Event exportar els tiquets en excel del menu principal</summary>
    ''' <remarks>Exporta el llistat de tiquets a un fitxer excel
    ''' </remarks>
    Private Sub TMExcelTiquets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMExcelTiquets.Click
        ' llistat tiquets en excel
        Dim fname As String

        SaveFileDialog1.ShowDialog()
        fname = SaveFileDialog1.FileName

        If fname <> "" Then
            Controller.Log.Add("Trec llistat tiquets en excel en nom " & fname, "EXPORT")
            Controller.exportExcel(DGDades, fname)
        End If
    End Sub

    ''' <summary>Event exportar els establiments en excel del menu principal</summary>
    ''' <remarks>Exporta el llistat dels establiments a un fitxer excel
    ''' </remarks>
    Private Sub TMExcelEstabliments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMExcelEstabliments.Click
        Dim fname As String

        SaveFileDialog1.ShowDialog()
        fname = SaveFileDialog1.FileName

        If fname <> "" Then
            Controller.Log.Add("Trec llistat cifs establiments en excel en nom " & fname, "EXPORT")
            Controller.exportExcel(DGCifs, fname)
        End If
    End Sub

    ''' <summary>Event exportar els treballadors en excel del menu principal</summary>
    ''' <remarks>Exporta el llistat de treballadors a un fitxer excel
    ''' </remarks>
    Private Sub TMExcelTreballadors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMExcelTreballadors.Click
        Dim fname As String

        SaveFileDialog1.ShowDialog()
        fname = SaveFileDialog1.FileName

        If fname <> "" Then
            Controller.Log.Add("Trec llistat tiquets de cada venedor i repartidor en excel en nom " & fname, "EXPORT")
            Controller.exportExcel(DGVenedors, fname)
        End If
    End Sub

    ''' <summary>Event exportar els tiquets en factures d'excel del menu principal</summary>
    ''' <remarks>Exporta les factures dels tiquets sense enviar-les a imprimir
    ''' </remarks>
    Private Sub TMExcelFactures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMExcelFactures.Click
        Controller.Log.Add("Trec factures en excel", "EXPORT")
        Controller.ferFactures(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString, False)
    End Sub

    ''' <summary>Event exportar els tiquets en factures d'excel del menu principal</summary>
    ''' <remarks>Exporta les factures dels tiquets enviant-les a imprimir
    ''' </remarks>
    Private Sub TMExcelFacturesImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMExcelFacturesImprimir.Click
        Controller.Log.Add("Trec factures en excel", "EXPORT")
        Controller.ferFactures(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString, True)
    End Sub

#End Region

#Region "event canvi nom imatge tiquet"

    ''' <summary>Event canviar ruta de la imatge del tiquet</summary>
    ''' <remarks>
    ''' </remarks>
    Private Sub BCanviRuta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCanviRuta.Click
        Dim ruta As String

        ruta = Controller.RenameImatgeTiquet(LTiquetNom.Text, CBCanviRuta.SelectedItem.ToString(), DTCanviRuta.Value)

        LTiquetNom.Text = ruta

        If ruta <> "" Then
            PBTiquet.ImageLocation = ruta
        Else
            PBTiquet.ImageLocation = Controller.imatgesPathLimpia
        End If
    End Sub

    ''' <summary>Event click del boto per a girar imatge</summary>
    ''' <remarks>Carrega els tiquets que entren manuals
    ''' </remarks>
    Private Sub BGirarImg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BGirarImg.Click
        Dim img As New ImagesClass

        PBTiquet.ImageLocation = Controller.imatgesPathLimpia

        img.girarImatge(LTiquetNom.Text)

        PBTiquet.ImageLocation = LTiquetNom.Text
    End Sub

#End Region

#Region "funcions propies"

    ''' <summary>Funcio que busca les dades dels desplegables i els ompli</summary>
    ''' <remarks>modifica les dades dels desplegables segons les dates dels filtros indicats
    ''' </remarks>
    Private Sub omplirCB()
        Dim dsVenedor As DataSet
        Dim dsCif As DataSet
        Dim dsNumFact As DataSet
        Dim row As Integer

        allok = False

        'omplo el desplegable dels venedors
        dsVenedor = Controller.llistatNifVenedors(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString)

        CBVenedors.Items.Clear()
        CBVenedors.Items.Add("Tots")
        If dsVenedor.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsVenedor.Tables(0).Rows.Count
                CBVenedors.Items.Add(dsVenedor.Tables(0).Rows(row)(0))
                row = row + 1
            End While
        End If
        CBVenedors.SelectedIndex = 0

        'omplo el desplegable dels cifs
        dsCif = Controller.llistatCifs(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString)

        CBCifs.Items.Clear()
        CBCifs.Items.Add("Tots")
        If dsCif.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsCif.Tables(0).Rows.Count
                CBCifs.Items.Add(dsCif.Tables(0).Rows(row)(0))
                row = row + 1
            End While
        End If
        CBCifs.SelectedIndex = 0

        'omplo el desplegable dels num de factura
        dsNumFact = Controller.llistatNumFactures(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString)

        CBNumFactura.Items.Clear()
        CBNumFactura.Items.Add("Tots")
        If dsNumFact.Tables(0).Rows.Count > 0 Then
            row = 0
            While row < dsNumFact.Tables(0).Rows.Count
                CBNumFactura.Items.Add(dsNumFact.Tables(0).Rows(row)(0))
                row = row + 1
            End While
        End If
        CBNumFactura.SelectedIndex = 0

        allok = True
    End Sub

    ''' <summary>Funcio que mostra les dades dels tiquets segons el filtros actuals</summary>
    ''' <remarks>omple els elements DataGrid en les dades procedents de la base de dades segons els filtros actuals
    ''' </remarks>
    Private Sub mostrarDades()
        Dim dsTickets As DataSet
        Dim dsCifs As DataSet
        Dim dsNifs As DataSet
        Dim dsTotal As DataSet
        Dim dsNumSenseTicket As DataSet

        If allok Then
            dsTickets = Controller.llistatTickets(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString)
            DGDades.DataSource = dsTickets
            DGDades.DataMember = "Tabla"

            dsCifs = Controller.llistatEstabliments(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString)
            DGCifs.DataSource = dsCifs
            DGCifs.DataMember = "Tabla"

            dsNifs = Controller.llistatNifsVenedors(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString)
            DGVenedors.DataSource = dsNifs
            DGVenedors.DataMember = "Tabla"

            idTicket = 0

            dsTotal = Controller.TotalTickets(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString)
            dsNumSenseTicket = Controller.TotalSenseTickets(DTDataInici.Value, DTDataFinal.Value, CBRutes.SelectedItem.ToString, CBVenedors.SelectedItem.ToString, CBCifs.SelectedItem.ToString, CBNumFactura.SelectedItem.ToString)

            TLNumTiquets.Text = DGDades.RowCount
            TLNumCifs.Text = DGCifs.RowCount
            TLTotalTiquets.Text = dsTotal.Tables(0).Rows(0)(0).ToString
            TLNumSenseTiquet.Text = dsNumSenseTicket.Tables(0).Rows(0)(0).ToString
            TLNumPresentats.Text = (DGDades.RowCount - dsNumSenseTicket.Tables(0).Rows(0)(0))
        End If
    End Sub

#End Region


#Region "funcions gestio manual"

    ''' <summary>Event click boto test</summary>
    ''' <remarks>Carrega un fitxer indicat. usat per a fer tests de funcionament
    ''' </remarks>
    Private Sub BTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTest.Click
        'importar tiquets test
        Dim fname As String
        Dim img As New ImagesClass

        OpenFileTest.ShowDialog()
        fname = OpenFileTest.FileName

        If fname <> "" Then
            ' Controller.Log.Add("TEST tiquets del fitxer " & fname, "TEST")
            'Controller.gestioFitxerHistoria(fname, "99", Now)

            'mostrarDades()

            'img.redimensionarImatge(fname)
            img.girarImatge(fname)
        End If

        'test imatge camara
        'Controller.buscarImatgeCamara()

    End Sub

    ''' <summary>Event click del boto per a carregar ruta 99</summary>
    ''' <remarks>Carrega els tiquets que entren manuals
    ''' </remarks>
    Private Sub BCarregar99_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCarregar99.Click
        Dim data As Date = Now
        Dim filePath As String

        'miro si existeix la carpeta
        filePath = Controller.CrearRutaHistoric(data, "99")

        If Directory.Exists(filePath) Then
            'miro si existeix el fitxer historia.txt
            If My.Computer.FileSystem.FileExists(filePath + "/historia.txt") Then
                Controller.gestioFitxerHistoria(filePath + "/historia.txt", "99", data)

                mostrarDades()
            Else
                MessageBox.Show("No puc accedir a la ruta " & filePath & "/historia.txt" & ". Has de liquidar les operacions al mobil", "Error", MessageBoxButtons.OK)
            End If

        Else
            MessageBox.Show("No puc accedir a la ruta " & filePath & ". Has de liquidar les operacions al mobil", "Error", MessageBoxButtons.OK)

        End If
    End Sub

    ''' <summary>Event click del boto per a recarregar el dia</summary>
    ''' <remarks>Torna a carregar els tiquets del dia d'inici
    ''' </remarks>
    Private Sub BRecarregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BRecarregar.Click
        Controller.carregarTicketsNousData(DTDataInici.Value.Date)

        mostrarDades()
    End Sub

#End Region


End Class
